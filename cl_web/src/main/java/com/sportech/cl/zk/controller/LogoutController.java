package com.sportech.cl.zk.controller;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Listen;

import com.sportech.cl.zk.service.AuthenticationService;

public class LogoutController extends SelectorComposer<Component> {

	private static final long serialVersionUID = 1L;
    
    AuthenticationService authService = new AuthenticationServiceImpl();
    
    @Listen("onClick=#logout")
    public void doLogout(){
            authService.logout();                
            Executions.sendRedirect("/login.zul");
    }
}