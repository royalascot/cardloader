package com.sportech.cl.zk.validator;

import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.zkoss.bind.Property;
import org.zkoss.bind.ValidationContext;
import org.zkoss.bind.validator.AbstractValidator;

public class PatternValidator extends AbstractValidator {

	public void validate(ValidationContext ctx) {

		Map<String, Property> beanProps = ctx.getProperties(ctx.getProperty().getBase());

		validateDescription(ctx, (String) beanProps.get("description").getValue());

	}

	private void validateDescription(ValidationContext ctx, String text) {
		if (StringUtils.isBlank(text)) {
			this.addInvalidMessage(ctx, "description", "Description can not be blank.");
		}
	}

}
