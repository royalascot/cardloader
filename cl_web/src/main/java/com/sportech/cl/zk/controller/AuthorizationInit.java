package com.sportech.cl.zk.controller;

import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Page;
import org.zkoss.zk.ui.util.Initiator;

import com.sportech.cl.model.security.SecurityToken;
import com.sportech.cl.zk.service.AuthenticationService;

public class AuthorizationInit implements Initiator {
	
	private AuthenticationService authService = new AuthenticationServiceImpl();

	public void doInit(Page page, Map<String, Object> args) throws Exception {
		if (args != null) {
			String roles = (String) args.get("role");
			if (!StringUtils.isEmpty(roles)) {			
				SecurityToken cre = authService.getUserToken();
				if (cre == null) {
					Executions.sendRedirect("/login.zul");
					return;
				}
				if (authService.isUserInRole(roles, cre)) {
					return;
				}
				Executions.sendRedirect("/home.zul");
				return;
			}
		}
	}
	
}
