package com.sportech.cl.zk.service;

import com.sportech.cl.model.security.SecurityToken;

public interface AuthenticationService {
	public void logout();
	public boolean login(String nm, String pd);
	public SecurityToken getUserToken();
	public boolean isUserInRole(String role, SecurityToken token);
}
