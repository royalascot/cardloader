package com.sportech.cl.zk.validator;

import java.util.List;
import java.util.Map;

import org.zkoss.bind.Property;
import org.zkoss.bind.ValidationContext;
import org.zkoss.bind.validator.AbstractValidator;

import com.sportech.cl.model.database.Tote;
import com.sportech.cl.model.database.ToteOutline;
import com.sportech.cl.viewmodel.ToteFilter;
import com.sportech.cl.viewmodel.ToteViewModel;

public class ToteValidator extends AbstractValidator {

	private ToteViewModel vm = new ToteViewModel();

	public void validate(ValidationContext ctx) {
		validateUnique(ctx, "toteCode", "Tote code already exists.");
		validateUnique(ctx, "loginName", "Tote login name shoud be unique.");
		validateUnique(ctx, "description", "Tote name shoud be unique.");
	}

	private void validateUnique(ValidationContext ctx, String field, String message) {
		Map<String, Property> beanProps = ctx.getProperties(ctx.getProperty().getBase());
		String text = (String) beanProps.get(field).getValue();
		Tote tote = (Tote) ctx.getBindContext().getValidatorArg("tote");
		ToteFilter filter = new ToteFilter();
		if ("toteCode".equals(field)) {
			filter.setToteCode(text);
		} else if ("description".equals(field)) {
			filter.setToteName(text);
		} else if ("loginName".equals(field)) {
			filter.setToteLogin(text);
		}
		List<ToteOutline> totes = getToteList(filter);
		if (totes.size() > 0) {
			if (tote != null && tote.getId() != null) {
				for (ToteOutline t : totes) {
					if (t.getId().longValue() == tote.getId().longValue()) {
						return;
					}
				}
			}
			this.addInvalidMessage(ctx, field, message);
		}
	}

	private List<ToteOutline> getToteList(ToteFilter filter) {
		return vm.getToteList(filter);
	}

}
