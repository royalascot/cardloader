package com.sportech.cl.zk.controller;

import java.util.Map;

import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Page;
import org.zkoss.zk.ui.util.Initiator;

import com.sportech.cl.model.security.SecurityToken;
import com.sportech.cl.zk.service.AuthenticationService;

public class AuthenticationInit implements Initiator {

	private AuthenticationService authService = new AuthenticationServiceImpl();

	public void doInit(Page page, Map<String, Object> args) throws Exception {

		SecurityToken cre = authService.getUserToken();
		if (cre == null) {
			Executions.sendRedirect("/login.zul");
			return;
		}
	}
}