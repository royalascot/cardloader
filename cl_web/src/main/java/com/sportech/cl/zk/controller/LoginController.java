package com.sportech.cl.zk.controller;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Label;
import org.zkoss.zul.Textbox;

import com.sportech.cl.model.security.SecurityToken;
import com.sportech.cl.zk.service.AuthenticationService;

public class LoginController extends SelectorComposer<Component> {
	private static final long serialVersionUID = 1L;

	// wire components
	@Wire
	Textbox account;
	@Wire
	Textbox password;
	@Wire
	Label message;

	// services
	AuthenticationService authService = new AuthenticationServiceImpl();

	@Listen("onClick=#login; onOK=#loginWin")
	public void doLogin() {
		
		String nm = account.getValue();
		String pd = password.getValue();

		if (!authService.login(nm, pd)) {
			message.setValue("Login failed.");
			return;
		}

		SecurityToken cre = authService.getUserToken();
		message.setValue("Welcome, " + cre.getLoginName());
		message.setSclass("");
		Executions.sendRedirect("/dashboard.zul");
	}
}
