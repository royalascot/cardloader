package com.sportech.cl.zk.components;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.IdSpace;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Div;
import org.zkoss.zul.Label;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Listbox;

public class DualListbox<E extends Comparable<E>> extends Div implements IdSpace {
	
	private static final long serialVersionUID = 5183321186606483396L;
	
	@Wire
	private Listbox candidateLb;
	@Wire
	private Listbox chosenLb;

	@Wire
	private Label leftTitle;

	@Wire
	private Label rightTitle;

	private ListModelList<E> candidateModel;
	private ListModelList<E> chosenDataModel;
	private String candidateTitle;
	private String chosenTitle;
	
	public DualListbox() {
		Executions.createComponents("/duallistbox.zul", this, null);
		Selectors.wireComponents(this, this, false);
		Selectors.wireEventListeners(this, this);
		chosenLb.setModel(chosenDataModel = new ListModelList<E>());
		chosenDataModel.setMultiple(true);
	}
	
	public String getCandidateTitle() {
		return candidateTitle;
	}

	public void setCandidateTitle(String candidateTitle) {
		this.candidateTitle = candidateTitle;
		if (leftTitle != null) {
			leftTitle.setValue(candidateTitle);
		}
	}

	public String getChosenTitle() {
		return chosenTitle;
	}

	public void setChosenTitle(String chosenTitle) {
		this.chosenTitle = chosenTitle;
		if (rightTitle != null) {
			rightTitle.setValue(chosenTitle);
		}
	}

	@Listen("onClick = #chooseBtn")
	public void chooseItem() {
		Events.postEvent(new ChooseEvent<E>(this, chooseOne()));
	}

	@Listen("onClick = #removeBtn")
	public void unchooseItem() {
		Events.postEvent(new ChooseEvent<E>(this, unchooseOne()));
	}

	@Listen("onClick = #chooseAllBtn")
	public void chooseAllItem() {
		for (int i = 0, j = candidateModel.getSize(); i < j; i++) {
			chosenDataModel.add(candidateModel.getElementAt(i));
		}
		candidateModel.clear();
		Set<E> set = new HashSet<E>();
		set.addAll(chosenDataModel);
		Events.postEvent(new ChooseEvent<E>(this, set));
	}

	@Listen("onClick = #removeAllBtn")
	public void unchooseAll() {
		for (int i = 0, j = chosenDataModel.getSize(); i < j; i++) {
			candidateModel.add(chosenDataModel.getElementAt(i));
		}
		chosenDataModel.clear();
		Set<E> set = new HashSet<E>();
		Events.postEvent(new ChooseEvent<E>(this, set));
	}

	/**
	 * Set new candidate ListModelList.
	 * 
	 * @param candidate is the data of candidate list model
	 */
	public void setModel(ListModelList<E> candidate) {
		candidateModel = candidate;
		Collections.sort(candidateModel);
		candidateLb.setModel(candidate);
		candidateModel.setMultiple(true);
		chosenDataModel.clear();
	}

	public void setCandidateModel(ListModelList<E> candidate) {
		candidateModel = candidate;
		Collections.sort(candidateModel);
		candidateModel.setMultiple(true);
		candidateLb.setModel(candidate);
	}
	
	public void setChosenModel(ListModelList<E> candidate) {
		chosenDataModel = candidate;
		Collections.sort(chosenDataModel);
		chosenDataModel.setMultiple(true);
		chosenLb.setModel(candidate);
	}
	
	/**
	 * @return current chosen data list
	 */
	public Collection<E> getChosenData() {
		return chosenDataModel;
	}

	private Set<E> chooseOne() {
		Set<E> set = candidateModel.getSelection();
		List<E> items = new ArrayList<E>();
		items.addAll(set);
		chosenDataModel.addAll(items);
		candidateModel.removeAll(items);
		return set;
	}

	private Set<E> unchooseOne() {
		Set<E> set = chosenDataModel.getSelection();
		List<E> items = new ArrayList<E>();
		items.addAll(set);
		chosenDataModel.removeAll(items);
		candidateModel.addAll(items);
		return set;
	}

	public class ChooseEvent<T> extends Event {
		
		private static final long serialVersionUID = -7334906383953342976L;

		public ChooseEvent(Component target, Set<T> data) {
			super("onChoose", target, data);
		}
	}
	
}