package com.sportech.cl.zk.controller;

import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.Sessions;

import com.sportech.cl.global.utils.config.ConfigLoader;
import com.sportech.cl.model.database.Role;
import com.sportech.cl.model.response.LoginResponce;
import com.sportech.cl.model.security.SecurityToken;
import com.sportech.cl.services.security.AuthenticateRemote;
import com.sportech.cl.services.security.AuthenticateRemoteLocator;
import com.sportech.cl.zk.service.AuthenticationService;

public class AuthenticationServiceImpl implements AuthenticationService {

	protected AuthenticateRemote loginService = AuthenticateRemoteLocator.getRemoteIface(ConfigLoader.getCfg());

	public boolean login(String nm, String pd) {

		Session sess = Sessions.getCurrent();
		LoginResponce response = loginService.Logon(nm, pd);
		if (response != null && response.getStatus() == 0) {
			SecurityToken cre = response.getToken();
			sess.setAttribute("userCredential", cre);
			return true;
		}
		return false;
	}

	public void logout() {
		Session sess = Sessions.getCurrent();
		sess.removeAttribute("userCredential");
		sess.invalidate();
	}

	public SecurityToken getUserToken() {
		Session sess = Sessions.getCurrent();
		SecurityToken t = (SecurityToken) sess.getAttribute("userCredential");
		return t;
	}
	
	public boolean isUserInRole(String role, SecurityToken token) {	
		if (token != null && token.getUser() != null && token.getUser().getRoles() != null) {
			Set<Role> roles = token.getUser().getRoles();
			role = role + "|Administrator|";
			for (Role r : roles) {
				if (StringUtils.containsIgnoreCase(role, r.getName())) {
					return true;
				}
			}			
		}
		return false;
	}

}