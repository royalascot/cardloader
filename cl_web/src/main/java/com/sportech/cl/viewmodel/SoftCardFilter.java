package com.sportech.cl.viewmodel;

import java.util.Date;

import org.apache.commons.lang3.StringUtils;

public class SoftCardFilter {

	private Date cardDate = null;
	private String description = null;
	private String approvalStatus = null;
	private String loadCode = null;
	private String toteName = null;

	public Date getCardDate() {
		return cardDate;
	}

	public void setCardDate(Date cardDate) {
		this.cardDate = cardDate;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String value) {
		if (StringUtils.isBlank(value)) {
			value = null;
		}
		this.description = value;
	}
	
	public String getApprovalStatus() {
		return approvalStatus;
	}

	public void setApprovalStatus(String approvalStatus) {
		this.approvalStatus = approvalStatus;
	}

	public String getLoadCode() {
		return loadCode;
	}

	public void setLoadCode(String loadCode) {
		this.loadCode = loadCode;
	}

	public String getToteName() {
		return toteName;
	}

	public void setToteName(String toteName) {
		this.toteName = toteName;
	}

}
