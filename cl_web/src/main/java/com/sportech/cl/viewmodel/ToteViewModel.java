package com.sportech.cl.viewmodel;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang.StringUtils;
import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Window;

import com.sportech.cl.global.utils.config.ConfigLoader;
import com.sportech.cl.model.common.PageParams;
import com.sportech.cl.model.database.Pattern;
import com.sportech.cl.model.database.PatternOutline;
import com.sportech.cl.model.database.PatternTote;
import com.sportech.cl.model.database.SoftCard;
import com.sportech.cl.model.database.SoftCardOutline;
import com.sportech.cl.model.database.SoftTote;
import com.sportech.cl.model.database.Tote;
import com.sportech.cl.model.database.ToteOutline;
import com.sportech.cl.model.database.Track;
import com.sportech.cl.model.response.GenericResponse;
import com.sportech.cl.model.response.PatternResponse;
import com.sportech.cl.model.response.SoftCardResponse;
import com.sportech.cl.model.response.ToteListResponse;
import com.sportech.cl.model.response.ToteResponse;
import com.sportech.cl.model.security.SecurityToken;
import com.sportech.cl.services.cards.SoftCardsRemote;
import com.sportech.cl.services.cards.SoftCardsRemoteLocator;
import com.sportech.cl.services.dictionaries.TotesRemote;
import com.sportech.cl.services.dictionaries.TotesRemoteLocator;
import com.sportech.cl.services.patterns.PatternsRemote;
import com.sportech.cl.services.patterns.PatternsRemoteLocator;
import com.sportech.cl.zk.components.DualListbox;

public class ToteViewModel extends BaseViewModel {

	private TotesRemote toteService = TotesRemoteLocator.getRemoteIface(ConfigLoader.getCfg());

	private SoftCardsRemote softCardService = SoftCardsRemoteLocator.getRemoteIface(ConfigLoader.getCfg());

	private PatternsRemote patternService = PatternsRemoteLocator.getRemoteIface(ConfigLoader.getCfg());

	private ToteOutline selectedTote;

	private List<ToteOutline> toteList;

	private Set<Integer> chosenToteIds = new HashSet<Integer>();

	private ToteFilter toteFilter = new ToteFilter();

	private Pattern patternDetail;

	private SoftCard cardDetail;

	private Tote detail;

	private String deleteMessage = null;

	private String retypedPassword = null;

	private String firstPassword = null;

	private ListModelList<Track> candidateTracks = new ListModelList<Track>();

	private ListModelList<Track> chosenTracks = new ListModelList<Track>();

	@Init
	public void init(@BindingParam("card") SoftCardOutline card, @BindingParam("pattern") PatternOutline pattern,
	        @BindingParam("patternDetail") Pattern patternDetail, @BindingParam("patternId") Long patternId,
	        @BindingParam("cardDetail") SoftCard cardDetail, @BindingParam("tote") Tote toteDetail) {
		if (toteDetail != null) {
			detail = toteDetail;
			setupDetail();
			return;
		}
		if (patternDetail != null) {
			initFromPatternDetail(patternDetail);
		}
		if (cardDetail != null) {
			initFromCardDetail(cardDetail);
		} else if (card != null) {
			initFromSoftCard(card);
		} else if (pattern != null) {
			initFromPattern(pattern);
		} else if (patternId != null) {
			PatternOutline dummy = new PatternOutline();
			dummy.setId(patternId);
			initFromPattern(dummy);
		}
	}

	public ListModelList<ToteOutline> getChosenTotes() {
		getToteList(toteFilter);
		List<ToteOutline> totes = new ArrayList<ToteOutline>();
		for (ToteOutline t : toteList) {
			if (chosenToteIds.contains(t.getId().intValue())) {
				totes.add(t);
			}
		}
		return new ListModelList<ToteOutline>(totes);
	}

	public ListModelList<ToteOutline> getCandidateTotes() {
		getToteList(toteFilter);
		List<ToteOutline> totes = new ArrayList<ToteOutline>();
		for (ToteOutline t : toteList) {
			if (!chosenToteIds.contains(t.getId().intValue())) {
				totes.add(t);
			}
		}
		return new ListModelList<ToteOutline>(totes);
	}

	public ListModelList<ToteOutline> getTotes() {
		getToteList(toteFilter);
		return new ListModelList<ToteOutline>(toteList);
	}

	public ListModelList<ToteOutline> getAllTotes() {
		getToteList(toteFilter);
		return new ListModelList<ToteOutline>(toteList);
	}

	public ListModelList<Track> getCandidateTracks() {
		return candidateTracks;
	}

	public void setCandidateTracks(ListModelList<Track> candidateTracks) {
		this.candidateTracks = candidateTracks;
	}

	public ListModelList<Track> getChosenTracks() {
		return chosenTracks;
	}

	public void setChosenTracks(ListModelList<Track> chosenTracks) {
		this.chosenTracks = chosenTracks;
	}

	public List<ToteOutline> getToteList(ToteFilter filter) {
		SecurityToken token = getToken();
		ToteOutline[] query = new ToteOutline[1];
		query[0] = new ToteOutline();
		PageParams pageParams = new PageParams();
		pageParams.setFirstResult(0);
		pageParams.setMaxResult(Integer.MAX_VALUE);
		if (!StringUtils.isEmpty(filter.getToteCode())) {
			query[0].setToteCode(filter.getToteCode());
		}
		if (!StringUtils.isEmpty(filter.getToteLogin())) {
			query[0].setLoginName(filter.getToteLogin());
		}
		if (!StringUtils.isEmpty(filter.getToteName())) {
			query[0].setDescription(filter.getToteName());
		}

		ToteListResponse result = toteService.find(token, query, null, pageParams);
		if (result.getStatus() == 0) {
			toteList = result.getTotes();
		} else {
			toteList = new ArrayList<ToteOutline>();
		}
		return toteList;
	}

	public ToteOutline getSelectedTote() {
		return selectedTote;
	}

	public void setSelectedTote(ToteOutline selectedTote) {
		this.selectedTote = selectedTote;
	}

	public void setToteList(List<ToteOutline> toteList) {
		this.toteList = toteList;
	}

	public ToteFilter getToteFilter() {
		return toteFilter;
	}

	public void setToteFilter(ToteFilter toteFilter) {
		this.toteFilter = toteFilter;
	}

	public Tote getDetail() {
		return detail;
	}

	public void setDetail(Tote detail) {
		this.detail = detail;
	}

	public String getDeleteMessage() {
		return deleteMessage;
	}

	public void setDeleteMessage(String deleteMessage) {
		this.deleteMessage = deleteMessage;
	}

	public String getRetypedPassword() {
		return retypedPassword;
	}

	public void setRetypedPassword(String retypedPassword) {
		this.retypedPassword = retypedPassword;
	}

	public String getFirstPassword() {
		return firstPassword;
	}

	public void setFirstPassword(String firstPassword) {
		this.firstPassword = firstPassword;
	}

	public boolean isShowCards() {
		return isUserInRole("Tote Card Approver");
	}
	
	private void initFromSoftCard(SoftCardOutline hco) {
		if (hco != null) {
			long id = hco.getId();
			SecurityToken token = getToken();
			SoftCardResponse hcr = softCardService.get(token, id);
			if (hcr.getStatus() == 0) {
				cardDetail = hcr.getCard();
				initFromCardDetail(cardDetail);
			}
		}
	}

	private void initFromCardDetail(SoftCard c) {
		if (c != null) {
			cardDetail = c;
			chosenToteIds = new HashSet<Integer>();
			for (SoftTote t : cardDetail.getTotes()) {
				chosenToteIds.add(t.getToteId());
			}
		}
	}

	private void setupDetail() {
		if (detail != null) {
			chosenTracks = new ListModelList<Track>(detail.getTracks());
			Collections.sort(chosenTracks);
			TreeSet<Track> t = new TreeSet<Track>(getTracks());
			t.removeAll(detail.getTracks());
			candidateTracks = new ListModelList<Track>(t);
			firstPassword = null;
			retypedPassword = null;
		}
	}

	private void initFromPattern(PatternOutline hco) {
		if (hco != null) {
			long id = hco.getId();
			SecurityToken token = getToken();
			PatternResponse p = patternService.getPattern(token, id);
			if (p.getStatus() == 0) {
				Pattern pattern = p.getPattern();
				initFromPatternDetail(pattern);
			}
		}
	}

	private void initFromPatternDetail(Pattern pattern) {
		this.patternDetail = pattern;
		Collection<PatternTote> pts = pattern.getTotes();
		chosenToteIds = new HashSet<Integer>();
		for (PatternTote tote : pts) {
			chosenToteIds.add(tote.getToteId());
		}
	}

	@Command
	@NotifyChange({ "totes" })
	public void changeFilter() {
		toteList = getToteList(toteFilter);
	}

	@Command
	@NotifyChange({ "detail", "totes", "chosenTracks", "candidateTracks" })
	public void addTote() {
		detail = new Tote();
		detail.setTracks(new ArrayList<Track>());
		detail.setActive(true);
		setupDetail();
		Map<String, Object> arg = new HashMap<String, Object>();
		arg.put("tote", detail);
		Window window = (Window) Executions.createComponents("toteeditor.zul", null, arg);
		window.doModal();
	}

	@Command
	@NotifyChange({ "detail", "chosenTracks", "candidateTracks" })
	public void editTote(@BindingParam("tote") ToteOutline c) {
		SecurityToken token = getToken();
		ToteResponse r = toteService.get(token, c.getId());
		if (r.getStatus() == 0) {
			detail = r.getTote();
			setupDetail();
		}
		Map<String, Object> arg = new HashMap<String, Object>();
		arg.put("tote", detail);
		Window window = (Window) Executions.createComponents("toteeditor.zul", null, arg);
		window.doModal();
	}

	@Command
	public void viewPatterns(@BindingParam("tote") ToteOutline c) {
		String url = "patterns.zul?toteId=" + c.getId();
		Executions.getCurrent().sendRedirect(url);
	}

	@Command
	public void viewCards(@BindingParam("tote") ToteOutline c) {
		Map<String, Object> arg = new HashMap<String, Object>();
		arg.put("tote", c);
		Window window = (Window) Executions.createComponents("totecards.zul", null, arg);
		window.doModal();
	}

	@SuppressWarnings("unchecked")
	@Command
	public void refreshTotes(@ContextParam(ContextType.COMPONENT) Component comp) {
		if (comp != null) {
			DualListbox<ToteOutline> db = (DualListbox<ToteOutline>) comp;
			Collection<ToteOutline> totes = db.getChosenData();
			if (totes != null) {
				if (patternDetail != null) {
					Set<PatternTote> ptSet = new HashSet<PatternTote>();
					for (ToteOutline t : totes) {
						PatternTote pt = new PatternTote();
						pt.setToteId(t.getId().intValue());
						ptSet.add(pt);
					}
					patternDetail.setTotes(ptSet);
				}
			}
		}
	}

	@SuppressWarnings("unchecked")
	@Command
	public void refreshCardTotes(@ContextParam(ContextType.COMPONENT) Component comp) {
		if (comp != null) {
			Collection<ToteOutline> totes = ((DualListbox<ToteOutline>) comp).getChosenData();
			if (totes != null) {
				if (cardDetail != null) {
				    cardDetail.getTotes().clear();
					for (ToteOutline t : totes) {				    
						SoftTote pt = new SoftTote();
						pt.setParent(cardDetail);
						pt.setToteId(t.getId().intValue());
						if (!cardDetail.getTotes().contains(pt)) {
						    cardDetail.getTotes().add(pt);
						}
					}
				}
			}
		}
	}

	@Command
	@NotifyChange({ "detail", "totes", "deleteMessage", "chosenTracks", "candidateTracks" })
	public void submit(@ContextParam(ContextType.VIEW) Window comp) {
		if (!StringUtils.isEmpty(firstPassword)) {
			if (!StringUtils.equals(firstPassword, retypedPassword)) {
				Messagebox.show("Your passwords do not match.", "Validation Error", Messagebox.OK, Messagebox.ERROR);
				return;
			}
			String password = DigestUtils.sha256Hex(firstPassword);
			detail.setPassword(password);
		}

		if (detail != null) {
			GenericResponse response;
			if (chosenTracks != null) {
				List<Track> newTracks = new ArrayList<Track>();
				newTracks.addAll(chosenTracks);
				detail.setTracks(newTracks);
			}
			response = toteService.saveTote(detail);
			if (response.getStatus() == 0) {
				detail = null;
				comp.detach();
			} else {
				Messagebox.show("Failed to save tote.");
			}
		}
	}
	
	public class ConfirmListener implements EventListener<Event> {
		private ToteViewModel parent;
		private Window parentWindow;
		public ConfirmListener(ToteViewModel vm, Window window) {
			parent = vm;
			parentWindow = window;
		}

		public void onEvent(Event evt) {
			switch (((Integer) evt.getData()).intValue()) {
			case Messagebox.YES:
				parent.deleteTote(parentWindow);
				break; // the Yes button is pressed
			case Messagebox.NO:
				break; // the No button is pressed
			}
		}

	};
	
	@Command
	@NotifyChange({ "totes", "detail" })
	public void confirmDeleteTote(@ContextParam(ContextType.VIEW) Window comp) {
		if (detail != null && detail.getId() != null) {
			String message = "Are you sure to delete Tote '" + detail.getDescription() + "'?";
			Messagebox.show(message, "Confirm", Messagebox.YES | Messagebox.NO, Messagebox.QUESTION, new ConfirmListener(this, comp));			
		}
	}

	@Command
	@NotifyChange({ "totes", "detail", "deleteMessage" })
	public void confirmDeleteToteWithModal() {
		if (detail != null) {
			deleteMessage = "Are you sure to delete Tote '" + detail.getDescription() + "'?";
		}
	}

	@Command
	@NotifyChange({ "totes", "detail" })
	public void deleteTote(@ContextParam(ContextType.VIEW) Window comp) {
		SecurityToken token = getToken();
		if (detail != null) {
			GenericResponse response;
			response = toteService.delete(token, detail.getId());
			if (response.getStatus() == 0) {
				detail = null;
				if (comp != null) {
					comp.detach();
				}
				BindUtils.postGlobalCommand(null, null, "refreshTotes", null);
			} else {
				Messagebox.show("Can not delete this tote:  Load cards for this tote exist.");
			}
		}
	}

	@Command
	@NotifyChange({ "detail" })
	public void closeDialog(@ContextParam(ContextType.VIEW) Window comp) {
		detail = null;
		comp.detach();
	}

	@Command
	@NotifyChange({ "detail", "totes", "deleteMessage" })
	public void deleteConfirmed(@ContextParam(ContextType.VIEW) Window comp) {
		deleteMessage = null;
		deleteTote(comp);
	}

	@Command
	@NotifyChange("deleteMessage")
	public void cancelDelete() {
		deleteMessage = null;
	}

	@GlobalCommand
	@NotifyChange({ "totes", "detail" })
	public void refreshTotes() {
	}

}
