package com.sportech.cl.viewmodel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zul.ListModel;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Popup;
import org.zkoss.zul.Window;

import com.sportech.cl.global.utils.config.ConfigLoader;
import com.sportech.cl.model.common.PageParams;
import com.sportech.cl.model.database.HardCardOutline;
import com.sportech.cl.model.database.PatternOutline;
import com.sportech.cl.model.database.SoftCardOutline;
import com.sportech.cl.model.response.PatternListResponse;
import com.sportech.cl.model.response.SoftCardListResponse;
import com.sportech.cl.model.security.SecurityToken;
import com.sportech.cl.services.cards.SoftCardsRemote;
import com.sportech.cl.services.cards.SoftCardsRemoteLocator;
import com.sportech.cl.services.patterns.PatternsRemote;
import com.sportech.cl.services.patterns.PatternsRemoteLocator;

public class TrackPatternViewModel extends BaseViewModel {

	private PatternsRemote patternService = PatternsRemoteLocator.getRemoteIface(ConfigLoader.getCfg());

	private SoftCardsRemote softCardService = SoftCardsRemoteLocator.getRemoteIface(ConfigLoader.getCfg());

	private List<PatternOutline> patternList;

	private PatternFilter patternFilter = new PatternFilter();

	private Set<Long> usedPatternIds = new HashSet<Long>();

	private String trackName = null;
	
	private Long trackId = null;

	private HardCardViewModel parentViewModel;

	@Init
	public void init(@BindingParam("card") HardCardOutline card, @BindingParam("parent") HardCardViewModel parent,
			@BindingParam("trackId") Long trackId) {
		if (parent != null) {
			parentViewModel = parent;
			if (parentViewModel.getDetail() != null) {
				this.trackName = parentViewModel.getDetail().getTrack().getName();
				this.trackId =parentViewModel.getDetail().getTrack().getId();
				setUsedPatterns(parentViewModel.getDetail().getId());
			}
		}
		if (card != null) {
			this.trackName = card.getTrackName();
			this.trackId = card.getTrackId();
			setUsedPatterns(card.getId());
		} else {
			this.trackId = trackId;
		}
	}

	public ListModel<PatternOutline> getPatterns() {
		getPatternList(patternFilter);
		ListModelList<PatternOutline> patterns = new ListModelList<PatternOutline>(patternList);
		patterns.setMultiple(true);
		return patterns;
	}

	public List<PatternOutline> getPatternList(PatternFilter filter) {
		SecurityToken token = getToken();
		PatternOutline[] query = new PatternOutline[1];
		query[0] = new PatternOutline();
		query[0].setDescription(filter.getDescription());
		query[0].setRaceCount(filter.getRaceCount());
		query[0].setTrackEquibaseId(filter.getTrackCode());
		query[0].setTrackName(trackName);
		query[0].setTrackId(trackId);
		query[0].setLoadCode(filter.getLoadCode());
		PageParams pageParams = new PageParams();
		pageParams.setFirstResult(0);
		pageParams.setMaxResult(Integer.MAX_VALUE);
		PatternListResponse result = patternService.find(token, query, false, pageParams);
		if (result.getStatus() == 0) {
			patternList = result.getPatterns();
			if (parentViewModel != null) {
				List<PatternOutline> selectedPatterns = new ArrayList<PatternOutline>();
				for (PatternOutline p : patternList) {
					if (!usedPatternIds.contains(p.getId())) {
						selectedPatterns.add(p);
					}
				}
				parentViewModel.setSelectedPatterns(selectedPatterns);
			}
		} else {
			patternList = new ArrayList<PatternOutline>();
		}
		return patternList;
	}

	public void setPatternList(List<PatternOutline> patternList) {
		this.patternList = patternList;
	}

	public PatternFilter getPatternFilter() {
		return patternFilter;
	}

	public void setPatternFilter(PatternFilter patternFilter) {
		this.patternFilter = patternFilter;
	}

	public Set<Long> getUsedPatternIds() {
		return usedPatternIds;
	}

	public void setUsedPatternIds(Set<Long> usedPatternIds) {
		this.usedPatternIds = usedPatternIds;
	}

	private List<SoftCardOutline> findSoftCards(Long id) {
		SoftCardOutline s = new SoftCardOutline();
		s.setHardCardId(id);
		SecurityToken token = getToken();
		SoftCardListResponse response = softCardService.find(token, new SoftCardOutline[] { s }, null, null, null, null);
		if (response != null && response.getStatus() == 0) {
			return response.getCards();
		} else {
			return new ArrayList<SoftCardOutline>();
		}
	}

	private void setUsedPatterns(Long id) {
		usedPatternIds = new HashSet<Long>();
		List<SoftCardOutline> createdCards = findSoftCards(id);
		for (SoftCardOutline s : createdCards) {
			usedPatternIds.add(s.getPatternId());
		}
	}

	@Command
	@NotifyChange({ "patterns" })
	public void changeFilter() {
		patternList = getPatternList(patternFilter);
	}

	@Command
	public void editPattern(@BindingParam("pattern") PatternOutline c) {
		Map<String, Object> arg = new HashMap<String, Object>();
		arg.put("pattern", c);
		Window window = (Window) Executions.createComponents("patterndetail.zul", null, arg);
		window.doModal();
	}

	@Command
	public void viewTotes(@BindingParam("pattern") PatternOutline c) {
		Map<String, Object> arg = new HashMap<String, Object>();
		arg.put("pattern", c);
		arg.put("selectedTab", "Totes");
		Window window = (Window) Executions.createComponents("patterndetail.zul", null, arg);
		window.doModal();
	}

	@Command
	public void viewToteNames(@ContextParam(ContextType.VIEW) Window comp, @BindingParam("pattern") PatternOutline c) {
		Map<String, Object> arg = new HashMap<String, Object>();
		arg.put("pattern", c);
		Popup p = (Popup) Executions.createComponents("totepopup.zul", comp, arg);
		p.open(comp, "at_pointer");
	}

}
