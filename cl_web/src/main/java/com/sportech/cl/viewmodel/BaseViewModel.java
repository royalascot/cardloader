package com.sportech.cl.viewmodel;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import org.apache.commons.lang3.StringUtils;
import org.zkoss.bind.annotation.Command;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zul.ListModelList;

import com.sportech.cl.global.utils.config.ConfigLoader;
import com.sportech.cl.model.common.PageParams;
import com.sportech.cl.model.common.PoolMergeType;
import com.sportech.cl.model.database.CardConfig;
import com.sportech.cl.model.database.Track;
import com.sportech.cl.model.response.TrackListResponse;
import com.sportech.cl.model.security.SecurityToken;
import com.sportech.cl.services.dictionaries.PoolTypesInterface;
import com.sportech.cl.services.dictionaries.PoolTypesRemoteLocator;
import com.sportech.cl.services.dictionaries.TracksInterface;
import com.sportech.cl.services.dictionaries.TracksRemoteLocator;
import com.sportech.cl.zk.controller.AuthenticationServiceImpl;
import com.sportech.cl.zk.service.AuthenticationService;
import com.sportech.common.model.CardType;
import com.sportech.common.model.PerformanceType;
import com.sportech.common.model.PoolType;

public class BaseViewModel {
    
    protected PoolTypesInterface poolTypeService = PoolTypesRemoteLocator.getRemoteIface(ConfigLoader.getCfg());
	protected TracksInterface trackService = TracksRemoteLocator.getRemoteIface(ConfigLoader.getCfg());
	protected AuthenticationService authService = new AuthenticationServiceImpl();

	public Comparator<TimeZone> ZoneComparator = new Comparator<TimeZone>() {
		public int compare(TimeZone p1, TimeZone p2) {
			String r1 = p1.getDisplayName();
			String r2 = p2.getDisplayName();
			if (r1 != null) {
				return -r1.compareTo(r2);
			}
			return -1;
		}
	};

	public ListModelList<TimeZone> getTimeZones() {
		List<TimeZone> tzs = new ArrayList<TimeZone>();
		String[] allTimeZones = TimeZone.getAvailableIDs();
		Arrays.sort(allTimeZones);
		for (String s : allTimeZones) {
			TimeZone t = TimeZone.getTimeZone(s);
			tzs.add(t);
		}
		return new ListModelList<TimeZone>(tzs);
	}

	public ListModelList<PoolType> getPoolTypes() {
		return new ListModelList<PoolType>(PoolType.values());
	}

	public ListModelList<CardType> getCardTypes() {
		return new ListModelList<CardType>(CardType.values());
	}

	public ListModelList<PerformanceType> getPerfTypes() {
		return new ListModelList<PerformanceType>(PerformanceType.values());
	}

	public List<Track> getTracks() {
		return getTracks(null);
	}

	protected List<Track> getTracks(TrackFilter filter) {
		SecurityToken token = getToken();
		Track[] query = new Track[1];
		query[0] = new Track();
		query[0].setActive(true);
		if (filter != null) {
			if (!StringUtils.isBlank(filter.getCode())) {
				query[0].setEquibaseId(filter.getCode());
			}
			if (!StringUtils.isBlank(filter.getCountry())) {
				query[0].setCountryCode(filter.getCountry());
			}
			if (!StringUtils.isBlank(filter.getName())) {
				query[0].setName(filter.getName() + "%");
			}
			if (filter.getCardType() != null) {
				query[0].setTrackType(filter.getCardType());
			}
			if (filter.getTrackId() != null) {
				query[0].setId(filter.getTrackId());
			}
			if (!StringUtils.isBlank(filter.getToteCode())) {
			    query[0].setToteTrackId("%" + filter.getToteCode() + "%");
			}
		}
		PageParams pageParams = new PageParams();
		pageParams.setFirstResult(0);
		pageParams.setMaxResult(Integer.MAX_VALUE);
		TrackListResponse response = trackService.find(token, query, true, pageParams);
		if (response.getStatus() == 0) {
			return response.getTracks();
		} else {
			return new ArrayList<Track>();
		}
	}

	public List<String> getTrackNames() {
		List<String> names = new ArrayList<String>();
		List<Track> tracks = getTracks();
		for (Track t : tracks) {
			names.add(t.getName());
		}
		return names;
	}

	public List<PoolMergeType> getMergeTypes() {
		List<PoolMergeType> mergeTypes = new ArrayList<PoolMergeType>();
		for (PoolMergeType pmt : PoolMergeType.values()) {
			mergeTypes.add(pmt);
		}
		return mergeTypes;
	}

	public Date getWorkingDate() {
		Session sess = Sessions.getCurrent();
		Date date = (Date) sess.getAttribute("userWorkingDate");
		if (date == null) {
			date = new Date();
			sess.setAttribute("userWorkingDate", date);
		}
		return date;
	}

	public void setWorkingDate(Date workingDate) {
		Session sess = Sessions.getCurrent();
		sess.setAttribute("userWorkingDate", workingDate);
	}

	public static Date addDate(Date startDate, int days) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(startDate);
		cal.add(Calendar.DATE, days);
		Date endDate = cal.getTime();
		return endDate;
	}

	public SecurityToken getToken() {
		return authService.getUserToken();
	}

	public boolean isUserInRole(String role) {
		SecurityToken token = getToken();
		return authService.isUserInRole(role, token);
	}
	
	public List<CardConfig> getCardConfigs() {
	    return poolTypeService.find(null);
	}
	
	@Command
	public void logout() {
		authService.logout();
		Executions.sendRedirect("/login.zul");
	}
	
}
