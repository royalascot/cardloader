package com.sportech.cl.viewmodel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.DependsOn;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Window;

import com.sportech.cl.model.database.CardConfig;
import com.sportech.cl.model.database.Pattern;
import com.sportech.cl.model.database.Track;
import com.sportech.cl.model.response.GenericResponse;
import com.sportech.cl.model.security.SecurityToken;

public class TrackViewModel extends BaseViewModel {

    private Track detail;

    private TrackFilter filter = new TrackFilter();

    @Init
    public void init(@BindingParam("trackCode") String trackCode, @BindingParam("trackId") Long trackId,
            @BindingParam("item") Track track) {
        if (track != null) {
            detail = track;
            return;
        }
        if (!StringUtils.isBlank(trackCode)) {
            filter.setCode(trackCode);
        }
        if (trackId != null) {
            filter.setTrackId(trackId);
        }
    }

    public List<CardConfig> getCardTemplates() {
        List<CardConfig> items = poolTypeService.find(null);
        return items;
    }

    @DependsOn("detail")
    public List<Pattern> getTrackPatterns() {
        if (detail != null) {
            return trackService.getTrackPatterns(detail);
        }
        return new ArrayList<Pattern>();
    }

    @Override
    public List<Track> getTracks() {
        return getTracks(filter);
    }

    public TrackFilter getFilter() {
        return filter;
    }

    public void setFilter(TrackFilter filter) {
        this.filter = filter;
    }

    public Track getDetail() {
        return detail;
    }

    public void setDetail(Track detail) {
        this.detail = detail;
    }

    @Command
    @NotifyChange({ "tracks", "trackPatterns" })
    public void addTrack() {
        detail = new Track();
        Map<String, Object> arg = new HashMap<String, Object>();
        arg.put("track", detail);
        Window window = (Window) Executions.createComponents("trackeditor.zul", null, arg);
        window.doModal();
    }

    @Command
    @NotifyChange({ "trackPatterns" })
    public void editTrack(@BindingParam("track") Track track) {
        Map<String, Object> arg = new HashMap<String, Object>();
        arg.put("track", track);
        Window window = (Window) Executions.createComponents("trackeditor.zul", null, arg);
        window.doModal();
    }

    public class DeleteListener implements EventListener<Event> {
        private Window comp;
        public DeleteListener(Window win) {
            this.comp = win;
        }
            public void onEvent(Event event) {
                if (Messagebox.ON_YES.equals(event.getName())) {
                    SecurityToken token = getToken();
                    if (detail != null) {
                        GenericResponse response;
                        detail.setActive(false);
                        response = trackService.delete(token, detail.getId());
                        if (response.getStatus() == 0) {
                            detail = null;
                            BindUtils.postGlobalCommand(null, null, "refreshTracks", null);
                            comp.detach();
                        } else {
                            Messagebox.show("Can not delete track in use.", "Error Deleting Track", Messagebox.OK,
                                    Messagebox.ERROR);
                        }
                    }
                }
            }            
    }
    
    @Command
    @NotifyChange({ "tracks", "trackPatterns" })
    public void deleteTrack(@ContextParam(ContextType.VIEW) Window comp) {
        Messagebox.show("Are you sure you want to delete the track " + detail.getName() + "?", "Track Deletion Confirmation",
                Messagebox.YES + Messagebox.NO, Messagebox.QUESTION, new DeleteListener(comp)); 
    }

    @Command
    @NotifyChange({ "tracks", "trackPatterns" })
    public void submit(@ContextParam(ContextType.VIEW) Window comp) {
        SecurityToken token = getToken();
        if (detail != null) {
            GenericResponse response;
            if (detail.getId() == null) {
                if (StringUtils.isEmpty(detail.getToteTrackId())) {
                    detail.setToteTrackId("GUI_" + detail.getEquibaseId());
                }
                response = trackService.add(token, detail);
            } else {
                response = trackService.update(token, detail.getId(), detail);
            }
            if (response.getStatus() == 0) {
                detail = null;
                comp.detach();
            } else {
                String errorMessage = StringUtils.join(response.getErrors(), " ");
                Messagebox.show(errorMessage, "Error Saving Track", Messagebox.OK, Messagebox.ERROR);
            }
        }
    }

    @Command
    @NotifyChange({ "tracks" })
    public void changeFilter() {
    }

    @Command
    public void createPattern(@ContextParam(ContextType.VIEW) Window comp, @BindingParam("track") Track track) {
        comp.detach();
        Map<String, Object> arg = new HashMap<String, Object>();
        arg.put("track", track);
        Window window = (Window) Executions.createComponents("patterndetail.zul", null, arg);
        window.doModal();
    }

    @GlobalCommand
    @NotifyChange({ "tracks", "trackPatterns" })
    public void refreshTracks() {
    }

}
