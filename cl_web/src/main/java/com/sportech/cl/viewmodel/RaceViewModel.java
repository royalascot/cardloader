package com.sportech.cl.viewmodel;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.DependsOn;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Messagebox;

import com.sportech.cl.global.utils.config.ConfigLoader;
import com.sportech.cl.model.comparator.RunnerComparator;
import com.sportech.cl.model.database.HardCard;
import com.sportech.cl.model.database.HardRace;
import com.sportech.cl.model.database.Runner;
import com.sportech.cl.model.database.SoftCard;
import com.sportech.cl.model.response.HardCardResponse;
import com.sportech.cl.model.security.SecurityToken;
import com.sportech.cl.services.cards.HardCardsRemote;
import com.sportech.cl.services.cards.HardCardsRemoteLocator;

public class RaceViewModel extends BaseViewModel {

	private HardCardsRemote hardCardService = HardCardsRemoteLocator.getRemoteIface(ConfigLoader.getCfg());

	private List<HardRace> races = new ArrayList<HardRace>();

	private HardRace race;

	private HardCard parent;

	private String deleteMessage = null;

	private Runner runnertoDelete = null;

	private HardRace racetoDelete = null;

	private boolean readonly = false;

	private Integer currentRaceNumber = null;

	@Init
	public void init(@BindingParam("card") HardCard card, @BindingParam("loadCard") SoftCard loadCard) {
		if (card != null) {
			initFromHostCard(card);
		} else if (loadCard != null) {
			SecurityToken token = getToken();
			HardCardResponse response = hardCardService.get(token, loadCard.getHardCardId());
			if (response.getStatus() == 0) {
				initFromHostCard(response.getCard());
			}
		}
	}

	private void initFromHostCard(HardCard card) {
		if (card != null) {
			parent = card;
			getRaces();
			if (races != null && races.size() > 0) {
				race = races.get(0);
			}
			if (card.getApproved() != null && card.getApproved()) {
				readonly = true;
			}
		}
	}

	public Integer getCurrentRaceNumber() {
		if (currentRaceNumber == null) {
			if (race != null) {
				currentRaceNumber = race.getNumber().intValue() - 1;
			}
		}
		return currentRaceNumber;
	}

	public void setCurrentRaceNumber(Integer currentRaceNumber) {
		this.currentRaceNumber = currentRaceNumber;
	}

	public ListModelList<String> getRaceNumbers() {
		List<HardRace> races = getRaces();
		List<String> numbers = new ArrayList<String>();
		for (HardRace hr : races) {
			numbers.add("" + hr.getNumber());
		}
		return new ListModelList<String>(numbers);
	}

	public List<HardRace> getRaces() {
		races = new ArrayList<HardRace>();
		if (parent != null) {
			races.addAll(parent.getRaces());
		}
		return races;
	}

	public void setRaces(List<HardRace> races) {
		this.races = races;
	}

	public Long getRaceCount() {
		if (races != null) {
			return new Long(races.size());
		}
		return 0L;
	}

	public boolean isReadonly() {
		return readonly;
	}

	public void setReadonly(boolean readonly) {
		this.readonly = readonly;
	}

	@DependsOn({ "race", "raceCount" })
	public boolean getCanAddRace() {
		if (readonly) {
			return true;
		}
		if (getRaceCount() == 0 && race == null) {
			return false;
		}
		if (race != null && race.getNumber() != null && race.getNumber().longValue() == getRaceCount().longValue()) {
			return false;
		}
		return true;
	}

	public HardRace getRace() {
		if (race != null) {
			Collections.sort(race.getRunners(), new RunnerComparator());
		}
		return race;
	}

	public void setRace(HardRace race) {
		this.race = race;
	}

	public String getDeleteMessage() {
		return deleteMessage;
	}

	public void setDeleteMessage(String deleteMessage) {
		this.deleteMessage = deleteMessage;
	}

	@Command
	@NotifyChange("race")
	public void previousRace() {
		if (race != null) {
			int r = race.getNumber().intValue();
			r = r - 2;
			if (r <= 0) {
				r = 0;
			}
			race = races.get(r);
		}
	}

	@Command
	@NotifyChange("race")
	public void nextRace() {
		if (race != null) {
			int r = race.getNumber().intValue();
			if (r >= races.size()) {
				r = races.size() - 1;
			}
			race = races.get(r);
		}
	}

	@Command
	@NotifyChange("race")
	public void changeRace() {
		if (currentRaceNumber != null) {
			int r = currentRaceNumber + 1;
			if (r > 0 && r <= getRaceCount()) {
				race = races.get(r - 1);
			}
		}
	}

	@Command
	@NotifyChange({ "parent", "races", "race", "raceCount", "raceNumbers", "currentRaceNumber" })
	public void addRace() {
		if (parent != null) {
			HardRace hr = new HardRace();
			hr.setParent(parent);
			hr.setNumber(new Long(parent.getRaceCount() + 1));
			parent.getRaces().add(hr);
			race = hr;
			currentRaceNumber = hr.getNumber().intValue() - 1;
		}
	}

	@Command
	@NotifyChange({ "parent", "races", "race", "raceCount", "raceNumbers", "currentRaceNumber" })
	public void removeRace() {
		if (parent != null && race != null) {
			parent.getRaces().remove(race);
			getRaces();
			if (races != null && races.size() > 0) {
				race = races.get(races.size() - 1);
				currentRaceNumber = race.getNumber().intValue() - 1;
			}
		}
	}

	@Command
	@NotifyChange({ "race" })
	public void refreshRunnerNumber(@BindingParam("runner") Runner runner) {
		if (race != null) {
			if (!StringUtils.isBlank(runner.getProgramNumber())) {
				String rn = StringUtils.upperCase(runner.getProgramNumber());
				String lastrn = rn.substring(rn.length() - 1);
				if (StringUtils.isAlpha(lastrn)) {
					String firstrn = rn.substring(0, rn.length() - 1);
					Runner coupledRunner = null;
					for (Runner r : race.getRunners()) {
						String n = r.getProgramNumber();
						if (StringUtils.endsWithIgnoreCase(rn, n)) {
							continue;
						}
						if (!StringUtils.isBlank(n)) {
							String last = n.substring(n.length() - 1);
							if (StringUtils.endsWithIgnoreCase(last, lastrn)) {
								Messagebox.show("Invalid program number", "Runner Validation Error", Messagebox.OK, Messagebox.ERROR);
								runner.setProgramNumber(firstrn);
								return;
							}
							String first = n.substring(0, n.length() - 1);
							if (firstrn.equals(first)) {
								coupledRunner = r;
							}
						}
					}
					if (coupledRunner != null) {
						coupledRunner.setCoupleIndicator(lastrn);
						runner.setCoupleIndicator(lastrn);
					}
					runner.setProgramNumber(rn);
				}
			}
		}
	}

	@Command
	@NotifyChange({ "race" })
	public void addRunner() {
		if (race != null) {
			int number = race.getRunners().size() + 1;
			Runner r = new Runner();
			r.setParent(race);
			r.setScratched(false);
			r.setProgramNumber("" + number);
			r.setPosition(new Long(number));
			race.getRunners().add(r);
		}
	}

	@Command
	@NotifyChange({ "race" })
	public void removeRunner(@BindingParam("runner") Runner runner) {
		if (race != null) {
			race.getRunners().remove(runner);
		}
	}

	@Command
	@NotifyChange({ "race" })
	public void removeRunnerConfirmed(@BindingParam("runner") Runner runner) {
		if (race != null) {
			race.getRunners().remove(runner);
		}
	}

	@Command
	@NotifyChange({ "race", "races", "deleteMessage", "raceCount", "parent" })
	public void deleteConfirmed() {
		deleteMessage = null;
		if (runnertoDelete != null) {
			removeRunner(runnertoDelete);
			runnertoDelete = null;
		} else if (racetoDelete != null) {
			removeRace();
			racetoDelete = null;
		}
	}

	@Command
	@NotifyChange({ "race", "races", "deleteMessage", "raceCount", "parent" })
	public void confirmDeleteRunner(@BindingParam("runner") Runner runner) {
		if (runner != null) {
			runnertoDelete = runner;
			racetoDelete = null;
			deleteMessage = "Do you want to delete runner '" + StringUtils.defaultString(runner.getName()) + "'?";
		}
	}

	@Command
	@NotifyChange("deleteMessage")
	public void confirmDeleteRace() {
		if (race != null) {
			runnertoDelete = null;
			racetoDelete = race;
			deleteMessage = "Do you want to delete race " + race.getNumber() + "?";
		}
	}

	@Command
	@NotifyChange("deleteMessage")
	public void cancelDelete() {
		deleteMessage = null;
		runnertoDelete = null;
		racetoDelete = null;
	}

}
