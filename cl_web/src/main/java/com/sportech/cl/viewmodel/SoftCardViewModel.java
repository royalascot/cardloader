package com.sportech.cl.viewmodel;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.DependsOn;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zhtml.Filedownload;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zul.ListModel;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Window;

import com.sportech.cl.global.utils.config.ConfigLoader;
import com.sportech.cl.model.common.PageParams;
import com.sportech.cl.model.comparator.SoftRaceComparator;
import com.sportech.cl.model.database.SoftCard;
import com.sportech.cl.model.database.SoftCardOutline;
import com.sportech.cl.model.database.SoftRace;
import com.sportech.cl.model.response.GenericResponse;
import com.sportech.cl.model.response.SoftCardListResponse;
import com.sportech.cl.model.response.SoftCardResponse;
import com.sportech.cl.model.security.SecurityToken;
import com.sportech.cl.services.cards.SoftCardsRemote;
import com.sportech.cl.services.cards.SoftCardsRemoteLocator;
import com.sportech.cl.utils.exporter.BetfredExporter;
import com.sportech.common.util.DateHelper;

public class SoftCardViewModel extends BaseViewModel {

    private SoftCardsRemote softCardService = SoftCardsRemoteLocator.getRemoteIface(ConfigLoader.getCfg());

    private SoftCardFilter cardFilter = new SoftCardFilter();

    private SoftCardOutline selected;

    private SoftRace selectedRace = null;

    private List<SoftCardOutline> selectedCards;

    private List<SoftCardOutline> softCards = new ArrayList<SoftCardOutline>();

    private Boolean approved = false;

    private SoftCard detail;

    @Init
    public void init(@BindingParam("selectedCard") SoftCardOutline selectedCard, @BindingParam("approved") Boolean approvedFlag) {
        selected = null;
        if (selectedCard != null) {
            selected = selectedCard;
            detail = getSingleCard(selectedCard);
            if (detail != null && detail.getRaces() != null && detail.getRaces().size() > 0) {
                selectedRace = Collections.min(detail.getRaces(), new SoftRaceComparator());
            }
        }
        approved = approvedFlag;
    }

    public ListModel<SoftCardOutline> getCards() {
        softCards = getFilterCards(cardFilter);
        ListModelList<SoftCardOutline> cards = new ListModelList<SoftCardOutline>(softCards);
        cards.setMultiple(true);
        return cards;
    }

    public List<SoftCardOutline> getFilterCards(SoftCardFilter filter) {
        SecurityToken token = getToken();
        SoftCardOutline[] query = new SoftCardOutline[1];
        query[0] = new SoftCardOutline();
        PageParams pageParams = new PageParams();
        pageParams.setFirstResult(0);
        pageParams.setMaxResult(Integer.MAX_VALUE);

        Date startDate = getWorkingDate();
        Date endDate = addDate(startDate, 0);
        if (filter.getCardDate() != null) {
            startDate = filter.getCardDate();
            endDate = filter.getCardDate();
        }

        if (!StringUtils.isBlank(filter.getDescription())) {
            query[0].setDescription(filter.getDescription() + "%");
        }

        if (approved != null) {
            query[0].setApproved(approved);
        }

        if (!StringUtils.isBlank(cardFilter.getLoadCode())) {
            query[0].setLoadCode(cardFilter.getLoadCode());
        }

        if (!StringUtils.isBlank(filter.getToteName())) {
            query[0].setToteName("%" + filter.getToteName() + "%");
        }

        SoftCardListResponse result = softCardService.find(token, query, startDate, endDate, true, pageParams);
        if (result.getStatus() == 0) {
            return result.getCards();
        } else {
            return new ArrayList<SoftCardOutline>();
        }
    }

    @NotifyChange({ "detail" })
    public void setSelectedCard(SoftCardOutline selected) {
        this.selected = selected;
    }

    public SoftCardOutline getSelectedCard() {
        return selected;
    }

    private SoftCard getSingleCard(SoftCardOutline hco) {
        if (hco != null) {
            long id = hco.getId();
            SecurityToken token = getToken();
            SoftCardResponse hcr = softCardService.get(token, id);
            if (hcr.getStatus() == 0) {
                return hcr.getCard();
            }
        }
        return null;
    }

    public SoftCardFilter getCardFilter() {
        return cardFilter;
    }

    public void setCardFilter(SoftCardFilter cardFilter) {
        this.cardFilter = cardFilter;
    }

    public List<SoftCardOutline> getSelectedCards() {
        return selectedCards;
    }

    public void setSelectedCards(List<SoftCardOutline> selectedCards) {
        this.selectedCards = selectedCards;
    }

    public SoftRace getSelectedRace() {
        return selectedRace;
    }

    public void setSelectedRace(SoftRace selectedRace) {
        this.selectedRace = selectedRace;
    }

    public SoftCard getDetail() {
        return detail;
    }

    public void setDetail(SoftCard detail) {
        this.detail = detail;
    }

    public String getDeleteMessage() {
        return deleteMessage;
    }

    public void setDeleteMessage(String deleteMessage) {
        this.deleteMessage = deleteMessage;
    }

    @Command
    @NotifyChange({ "cards" })
    public void changeFilter() {
    }

    @Command
    public void editCard(@BindingParam("card") SoftCardOutline c) {
        selected = c;
        Map<String, Object> arg = new HashMap<String, Object>();
        arg.put("selectedCard", c);
        Window window = (Window) Executions.createComponents("loadcarddetail.zul", null, arg);
        window.doModal();
    }

    @DependsOn("selectedCards")
    public boolean isPendingLoadCardButtonDisabled() {
        return (selectedCards == null || selectedCards.size() == 0);
    }

    @DependsOn("selectedCards")
    public boolean isMirrorCardButtonDisabled() {
        return (selectedCards == null || selectedCards.size() == 0);
    }

    @DependsOn("selectedCards")
    public String getPendingLoadCardButtonTitle() {
        if (isPendingLoadCardButtonDisabled()) {
            return "Activate Selected";
        } else {
            int size = selectedCards.size();
            return "Activate " + size + " Selected";
        }
    }

    @Command
    @NotifyChange({ "cards", "selectedCards" })
    public void approveCards() {
        if (selectedCards != null) {
            SecurityToken token = getToken();
            for (SoftCardOutline h : selectedCards) {
                Long id = h.getId();
                softCardService.approve(token, id, true);
            }
            Messagebox.show("Successfully activated " + selectedCards.size() + " load card(s).", "Activating Load Cards",
                    Messagebox.OK, Messagebox.INFORMATION);
            selectedCards = null;
        }
    }

    @Command
    @NotifyChange({ "cards", "selectedCards" })
    public void disableCards() {
        if (selectedCards != null) {
            SecurityToken token = getToken();
            for (SoftCardOutline h : selectedCards) {
                Long id = h.getId();
                softCardService.approve(token, id, false);
            }
            Messagebox.show("Successfully deactivated " + selectedCards.size() + " load card(s).", "Deactivating Load Cards",
                    Messagebox.OK, Messagebox.INFORMATION);
            selectedCards = null;
        }
    }
    @Command
	@NotifyChange({ "selectedHardCards" })
	public void exportCards() {
        if (selectedCards != null) {
            List<SoftCard> cards = new ArrayList<SoftCard>();
            for (SoftCardOutline h : selectedCards) {
            	SoftCard sc = getSingleCard(h);
            	if (sc != null) {
            		cards.add(sc);
            	}
            }
            selectedCards = null;
			if (cards.size() > 0) {
				Date d = getWorkingDate();
				String fileName = DateHelper.dateToStr(d) + "card.xml";
				ByteArrayOutputStream bos = new ByteArrayOutputStream();
				BetfredExporter exporter = new BetfredExporter();
				exporter.export(cards, bos);
				Filedownload.save(bos.toByteArray(), "text/xml", fileName);
			}
		}
	}
    
    @Command
    @NotifyChange({ "cards", "selectedCards", "deleteMessage" })
    public void deleteCards() {
        if (selectedCards != null) {
            SecurityToken token = getToken();
            for (SoftCardOutline h : selectedCards) {
                Long id = h.getId();
                softCardService.delete(token, id);
            }
            selectedCards = null;
            deleteMessage = null;
        }
    }

    @Command
    @NotifyChange({ "cards", "selectedCards" })
    public void createMirror() {
        if (selectedCards != null) {
            List<SoftCardOutline> parents = new ArrayList<SoftCardOutline>();
            for (SoftCardOutline h : selectedCards) {
                parents.add(h);
            }
            selectedCards = null;
            Map<String, Object> arg = new HashMap<String, Object>();
            arg.put("parents", parents);
            Window window = (Window) Executions.createComponents("createmirror.zul", null, arg);
            window.doModal();
        }
    }

    private String deleteMessage = null;

    @Command
    @NotifyChange("deleteMessage")
    public void confirmDelete() {
        if (selectedCards != null) {
            deleteMessage = "Do you want to delete " + selectedCards.size() + " selected cards?";
        }
    }

    @Command
    @NotifyChange("deleteMessage")
    public void cancelDelete() {
        deleteMessage = null;
    }

    @Command
    public void closeCardEditWindow(@ContextParam(ContextType.VIEW) Window comp) {
        comp.detach();
    }

    @Command
    @NotifyChange({ "cards", "selectedCards" })
    public void saveLoadCard(@ContextParam(ContextType.VIEW) Window comp) {
        SecurityToken token = getToken();
        if (detail != null) {
            GenericResponse response;
            if (detail.getId() == null) {
                response = softCardService.add(token, detail);
            } else {
                response = softCardService.update(token, detail.getId(), detail);
            }
            if (response.getStatus() == 0) {
                comp.detach();
            } else {
                String errorMessage = StringUtils.join(response.getErrors(), " ");
                Messagebox.show(errorMessage, "Error Saving Load Card", Messagebox.OK, Messagebox.ERROR);
            }
        }
    }

    @Command
    @NotifyChange({ "cards", "selectedCards" })
    public void approveLoadCard(@ContextParam(ContextType.VIEW) Window comp) {
        if (detail != null) {
            SecurityToken token = getToken();
            GenericResponse response = softCardService.approve(token, detail.getId(), true);
            if (response.getStatus() == 0) {
                comp.detach();
            } else {
                Messagebox.show("Failed to approve load card.");
            }
        }
    }

    @GlobalCommand
    @NotifyChange({ "cards", "selectedCards" })
    public void refreshLoadCards() {

    }

    @Command
    public void viewPattern(@BindingParam("patternId") Long patternId) {
        Map<String, Object> arg = new HashMap<String, Object>();
        arg.put("patternId", patternId);
        Window window = (Window) Executions.createComponents("patterndetail.zul", null, arg);
        window.doModal();
    }

}
