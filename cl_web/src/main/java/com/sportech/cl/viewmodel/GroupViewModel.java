package com.sportech.cl.viewmodel;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.TreeSet;

import org.apache.commons.lang3.StringUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Messagebox;

import com.sportech.cl.global.utils.config.ConfigLoader;
import com.sportech.cl.model.common.PageParams;
import com.sportech.cl.model.database.Group;
import com.sportech.cl.model.database.Role;
import com.sportech.cl.model.database.Tote;
import com.sportech.cl.model.database.Track;
import com.sportech.cl.model.database.User;
import com.sportech.cl.model.response.GenericResponse;
import com.sportech.cl.services.security.UsersRemote;
import com.sportech.cl.services.security.UsersRemoteLocator;

public class GroupViewModel extends BaseViewModel {

	private final String EDITABLE_GROUP = "User";

	private UsersRemote userService = UsersRemoteLocator.getRemoteIface(ConfigLoader.getCfg());

	private Group detail = null;

	private ListModelList<User> candidateUsers = new ListModelList<User>();

	private ListModelList<User> chosenUsers = new ListModelList<User>();

	private ListModelList<Role> candidateRoles = new ListModelList<Role>();

	private ListModelList<Role> chosenRoles = new ListModelList<Role>();

	private ListModelList<Track> candidateTracks = new ListModelList<Track>();

	private ListModelList<Track> chosenTracks = new ListModelList<Track>();

	private ListModelList<Tote> candidateTotes = new ListModelList<Tote>();

	private ListModelList<Tote> chosenTotes = new ListModelList<Tote>();

	public Group getDetail() {
		return detail;
	}

	public void setDetail(Group detail) {
		this.detail = detail;
	}

	public List<Group> getGroups() {
		return getFilteredGroups(null);
	}

	private List<Group> getFilteredGroups(String name) {
		PageParams pageParams = new PageParams();
		pageParams.setFirstResult(0);
		pageParams.setMaxResult(Integer.MAX_VALUE);
		Group[] query = new Group[1];
		query[0] = new Group();
		if (name != null) {
			query[0].setName(name);
		} else {
			query[0].setCategory(EDITABLE_GROUP);
		}
		List<Group> groups = userService.find(getToken(), query, true, null);
		return groups;
	}

	public List<User> getAllUsers() {
		PageParams pageParams = new PageParams();
		pageParams.setFirstResult(0);
		pageParams.setMaxResult(Integer.MAX_VALUE);
		User[] query = new User[1];
		query[0] = new User();
		query[0].setSalt("OPERATORS");
		List<User> users = userService.find(getToken(), query, true, null);
		if (users == null) {
			users = new ArrayList<User>();
		}
		return users;
	}

	public List<Role> getAllRoles() {
		PageParams pageParams = new PageParams();
		pageParams.setFirstResult(0);
		pageParams.setMaxResult(Integer.MAX_VALUE);
		Role[] query = new Role[1];
		query[0] = new Role();
		List<Role> roles = userService.find(getToken(), query, true, null);
		if (isUserInRole("Administrator")) {
			return roles;
		} else {
			List<Role> nonAdmin = new ArrayList<Role>();
			for (Role r : roles) {
				if (r.getId() != 1) {
					nonAdmin.add(r);
				}
			}
			return nonAdmin;
		}
	}

	public List<Tote> getAllTotes() {
		return userService.getAllTotes();
	}

	public ListModelList<User> getCandidateUsers() {
		return candidateUsers;
	}

	public void setCandidateUsers(ListModelList<User> candidateUsers) {
		this.candidateUsers = candidateUsers;
	}

	public ListModelList<User> getChosenUsers() {
		return chosenUsers;
	}

	public void setChosenUsers(ListModelList<User> chosenUsers) {
		this.chosenUsers = chosenUsers;
	}

	public ListModelList<Role> getCandidateRoles() {
		return candidateRoles;
	}

	public void setCandidateRoles(ListModelList<Role> candidateRoles) {
		this.candidateRoles = candidateRoles;
	}

	public ListModelList<Role> getChosenRoles() {
		return chosenRoles;
	}

	public void setChosenRoles(ListModelList<Role> chosenRoles) {
		this.chosenRoles = chosenRoles;
	}

	public ListModelList<Track> getCandidateTracks() {
		return candidateTracks;
	}

	public void setCandidateTracks(ListModelList<Track> candidateTracks) {
		this.candidateTracks = candidateTracks;
	}

	public ListModelList<Track> getChosenTracks() {
		return chosenTracks;
	}

	public void setChosenTracks(ListModelList<Track> chosenTracks) {
		this.chosenTracks = chosenTracks;
	}

	public ListModelList<Tote> getCandidateTotes() {
		return candidateTotes;
	}

	public void setCandidateTotes(ListModelList<Tote> candidateTotes) {
		this.candidateTotes = candidateTotes;
	}

	public ListModelList<Tote> getChosenTotes() {
		return chosenTotes;
	}

	public void setChosenTotes(ListModelList<Tote> chosenTotes) {
		this.chosenTotes = chosenTotes;
	}

	private void setupDetail() {
		if (detail != null) {
			chosenUsers = new ListModelList<User>(detail.getUsers());
			chosenRoles = new ListModelList<Role>(detail.getRoles());
			chosenTracks = new ListModelList<Track>(detail.getTracks());
			chosenTotes = new ListModelList<Tote>(detail.getTotes());
			Collections.sort(chosenUsers);
			Collections.sort(chosenRoles);
			Collections.sort(chosenTotes);
			Collections.sort(chosenTracks);
			TreeSet<Role> roles = new TreeSet<Role>(getAllRoles());
			TreeSet<User> users = new TreeSet<User>(getAllUsers());
			TreeSet<Track> tracks = new TreeSet<Track>(getTracks());
			TreeSet<Tote> totes = new TreeSet<Tote>(getAllTotes());
			roles.removeAll(detail.getRoles());
			users.removeAll(detail.getUsers());
			tracks.removeAll(detail.getTracks());
			totes.removeAll(detail.getTotes());
			candidateRoles = new ListModelList<Role>(roles);
			candidateUsers = new ListModelList<User>(users);
			candidateTracks = new ListModelList<Track>(tracks);
			candidateTotes = new ListModelList<Tote>(totes);
		}
	}

	@Command
	@NotifyChange({ "*" })
	public void addGroup() {
		Group u = new Group();
		u.setRoles(new ArrayList<Role>());
		u.setTracks(new ArrayList<Track>());
		u.setUsers(new ArrayList<User>());
		u.setTotes(new ArrayList<Tote>());
		u.setName("");
		u.setCategory(EDITABLE_GROUP);
		detail = u;
		setupDetail();
	}

	@Command
	@NotifyChange({ "*" })
	public void viewGroup(@BindingParam("group") Group p) {
		if (p != null) {
			detail = userService.getGroup(p.getId());
			setupDetail();
		}
	}

	@Command
	@NotifyChange({ "*" })
	public void deleteGroup() {
		if (detail != null) {
			GenericResponse response;
			response = userService.deleteGroup(detail.getId());
			if (response.getStatus() == 0) {
				detail = null;
			} else {
				Messagebox.show("Can not delete group in use.", "Error Deleting Group", Messagebox.OK, Messagebox.ERROR);
			}
		}
	}

	@Command
	@NotifyChange({ "*" })
	public void saveGroup() {
		if (StringUtils.isEmpty(detail.getName())) {
			Messagebox.show("Group name is required.", "Error Saving Group", Messagebox.OK, Messagebox.ERROR);
			return;
		}
		List<Group> existingGroups = getFilteredGroups(detail.getName());
		if (existingGroups != null && existingGroups.size() > 0) {
			if ((detail.getId() == null) || (detail.getId().longValue() != existingGroups.get(0).getId().longValue())) {
				Messagebox.show("Group name exists. Please use another group name.", "Error Saving Group", Messagebox.OK,
				        Messagebox.ERROR);
				return;
			}
		}

		if (chosenRoles != null) {
			List<Role> newRoles = new ArrayList<Role>();
			newRoles.addAll(chosenRoles);
			detail.setRoles(newRoles);
		}
		if (chosenUsers != null) {
			List<User> newUsers = new ArrayList<User>();
			newUsers.addAll(chosenUsers);
			detail.setUsers(newUsers);
		}
		if (chosenTracks != null) {
			List<Track> newTracks = new ArrayList<Track>();
			newTracks.addAll(chosenTracks);
			detail.setTracks(newTracks);
		}
		if (chosenTotes != null) {
			List<Tote> newTotes = new ArrayList<Tote>();
			newTotes.addAll(chosenTotes);
			detail.setTotes(newTotes);
		}

		userService.saveGroup(detail);
		detail = null;
	}

	@Command
	@NotifyChange({ "detail" })
	public void closeDialog() {
		detail = null;
	}

}
