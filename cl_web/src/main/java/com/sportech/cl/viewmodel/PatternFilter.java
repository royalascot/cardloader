package com.sportech.cl.viewmodel;

import org.apache.commons.lang.StringUtils;

import com.sportech.common.model.CardType;
import com.sportech.cl.model.database.ToteOutline;

public class PatternFilter {
	
	private String trackCode;
	private String loadCode;
	private String description;
	private Integer raceCount;
	private String toteId;
	private ToteOutline tote;
	private CardType trackType;
	private Long trackId;
	
	public Long getTrackId() {
		return trackId;
	}

	public void setTrackId(Long trackId) {
		this.trackId = trackId;
	}

	public CardType getTrackType() {
		return trackType;
	}

	public void setTrackType(CardType trackType) {
		this.trackType = trackType;
	}

	public String getTrackCode() {
		return trackCode;
	}

	public void setTrackCode(String trackCode) {
		if (StringUtils.isBlank(trackCode)) {
			trackCode = null;
		}
		this.trackCode = trackCode;
	}

	public String getLoadCode() {
		return loadCode;
	}

	public void setLoadCode(String loadCode) {
		if (StringUtils.isBlank(loadCode)) {
			loadCode = null;
		}
		this.loadCode = loadCode;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		if (StringUtils.isBlank(description)) {
			description = null;
		}
		this.description = description;
	}

	public Integer getRaceCount() {
		return raceCount;
	}

	public void setRaceCount(Integer raceCount) {
		this.raceCount = raceCount;
	}

	public String getToteId() {
		return toteId;
	}

	public void setToteId(String toteId) {
		this.toteId = toteId;
	}

	public ToteOutline getTote() {
		return tote;
	}

	public void setTote(ToteOutline tote) {
		this.tote = tote;
		this.toteId = "" + tote.getId();
	}
	
}
