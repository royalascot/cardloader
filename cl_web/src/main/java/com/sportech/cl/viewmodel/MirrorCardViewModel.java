package com.sportech.cl.viewmodel;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Window;

import com.sportech.cl.global.utils.config.ConfigLoader;
import com.sportech.cl.model.comparator.SoftRaceComparator;
import com.sportech.cl.model.database.SoftCard;
import com.sportech.cl.model.database.SoftCardOutline;
import com.sportech.cl.model.database.SoftPool;
import com.sportech.cl.model.database.SoftRace;
import com.sportech.cl.model.database.SoftTote;
import com.sportech.cl.model.response.GenericResponse;
import com.sportech.cl.model.response.SoftCardResponse;
import com.sportech.cl.model.security.SecurityToken;
import com.sportech.cl.services.cards.SoftCardsRemote;
import com.sportech.cl.services.cards.SoftCardsRemoteLocator;

public class MirrorCardViewModel extends BaseViewModel {

	static private final Logger log = Logger.getLogger(MirrorCardViewModel.class);

	private SoftCardsRemote softCardService = SoftCardsRemoteLocator.getRemoteIface(ConfigLoader.getCfg());

	private List<SoftCard> sourceCards = null;

	private SoftRace selectedRace = null;

	
	private SoftCard detail;

	public class DummyCard {
		
		private String name;
		private String code;
		private int number = 0;
		private Set<SoftRace> races = new TreeSet<SoftRace>(new SoftRaceComparator());
		
		public String getCode() {
			return code;
		}

		public void setCode(String code) {
			this.code = code;
		}

		public String getName() {
			return name;
		}
		
		public void setName(String name) {
			this.name = name;
		}
		
		public Set<SoftRace> getRaces() {
			return races;
		}
		
		public void setRaces(Set<SoftRace> races) {
			this.races = races;
		}

		public int getNumber() {
			return number;
		}

		public void setNumber(int number) {
			this.number = number;
		}		
		
	}
	
	private List<DummyCard> sourceRaces = null;

	@Init
	public void init(@BindingParam("parents") List<SoftCardOutline> selectedCards) {
		try {
			detail = new SoftCard();
			sourceCards = new ArrayList<SoftCard>();
			sourceRaces = new ArrayList<DummyCard>();
			List<Long> cardIds = new ArrayList<Long>();
			List<String> cardCodes = new ArrayList<String>();
			SoftCard firstCard = null;
			int count = 0;
			for (SoftCardOutline sco : selectedCards) {
				SoftCard sc = getSingleCard(sco);
				sourceCards.add(sc);
				if (firstCard == null) {
					firstCard = sc;
				}
				DummyCard dc = new DummyCard();
				dc.setNumber(count);
				count++;
				dc.setName("Races from Card " + count + " - " +  sc.getLoadCode());
				dc.setCode(sc.getLoadCode());
				cardIds.add(sc.getId());
				cardCodes.add(sc.getLoadCode());
				dc.getRaces().addAll(sc.getRaces());
				sourceRaces.add(dc);
				
			}
			detail.setApproved(false);
			detail.setSource(StringUtils.join(cardIds, ","));
			detail.setDescription("Mirror Card from " + StringUtils.join(cardCodes, " and "));
			detail.setHardCardId(firstCard.getHardCardId());
			detail.setPatternId(firstCard.getPatternId());
			detail.setPatternDescription(firstCard.getPatternDescription());
			detail.setCardDate(firstCard.getCardDate());
			detail.setToteApprovalList(firstCard.getToteApprovalList());
			detail.setToteName(firstCard.getToteName());
			detail.setTrack(firstCard.getTrack());
			detail.setCardType(firstCard.getCardType());
			detail.setPerfType(firstCard.getPerfType());
			String loadCode = StringUtils.join(cardCodes, "");
			detail.setLoadCode(StringUtils.left(loadCode, 10));
			detail.setRaces(new TreeSet<SoftRace>(new SoftRaceComparator()));
			detail.setPools(new HashSet<SoftPool>());
		} catch (Exception e) {
			log.error("Error initializing mirror card", e);
		}
	}

	public SoftRace getSelectedRace() {
		return selectedRace;
	}

	public void setSelectedRace(SoftRace selectedRace) {
		this.selectedRace = selectedRace;
	}

	public SoftCard getDetail() {
		return detail;
	}

	public void setDetail(SoftCard detail) {
		this.detail = detail;
	}

	public List<SoftCard> getSourceCards() {
		return sourceCards;
	}

	public void setSourceCards(List<SoftCard> sourceCards) {
		this.sourceCards = sourceCards;
	}

	public List<DummyCard> getSourceRaces() {
		return sourceRaces;
	}

	public void setSourceRaces(List<DummyCard> sourceRaces) {
		this.sourceRaces = sourceRaces;
	}

	@Command
	public void closeCardEditWindow(@ContextParam(ContextType.VIEW) Window comp) {
		comp.detach();
	}

	@Command
	public void publishCard(@ContextParam(ContextType.VIEW) Window comp) {
		detail.setApproved(true);
		saveLoadCard(comp);
	}

	@Command
	public void saveLoadCard(@ContextParam(ContextType.VIEW) Window comp) {
		GenericResponse response = saveCard();
		if (0 == response.getStatus()) {
			comp.detach();
		} else {
			String errorMessage = StringUtils.join(response.getErrors(), " ");
			Messagebox.show(errorMessage, "Error Saving Load Card", Messagebox.OK, Messagebox.ERROR);
		}
	}

	@Command
	@NotifyChange({ "detail", "sourceRaces" })
	public void addRace(@BindingParam("num") DummyCard card, @BindingParam("parameter") SoftRace race) {
		if (detail != null && race != null) {
			for (SoftRace r : detail.getRaces()) {
				if (r.getParentRace().getId() == race.getId()) {
					return;
				}
			}
			int t = card.getNumber();
			SoftRace r = new SoftRace();
			r.setNumber(1L + detail.getRaces().size());
			r.setParentRace(race);
			r.setSource("" + (t + 1) + "-(" + card.getCode() + ")-" + race.getNumber());
			r.setParent(detail);
			detail.getRaces().add(r);
			detail.setRaceCount(detail.getRaces().size());
			sourceRaces.get(t).getRaces().remove(race);
		}
	}

	@Command
	@NotifyChange({ "detail", "sourceRaces" })
	public void deleteRace(@BindingParam("parameter") SoftRace race) {
		long raceNumber = 100;
		if (detail != null && race != null) {
			raceNumber = race.getNumber();
			detail.getRaces().remove(race);
			race.setParent(null);
		}
		ArrayList<SoftRace> temp = new ArrayList<SoftRace>();
		temp.addAll(detail.getRaces());
		detail.getRaces().clear();
		for (SoftRace t : temp) {
			if (t.getNumber() > raceNumber) {
				t.setNumber(t.getNumber() - 1);
			}
			detail.getRaces().add(t);
		}
		detail.setRaceCount(detail.getRaces().size());
		String[] sourceParts = StringUtils.split(race.getSource(), "-");
		if (sourceParts != null && sourceParts.length > 0) {
			int index = Integer.parseInt(sourceParts[0]);
			if (index > 0) {
				sourceRaces.get(index - 1).getRaces().add(race.getParentRace());
			}
		}
	}

	@Command
	@NotifyChange({ "detail" })
	public void changeRaceNumber(@BindingParam("parameter") SoftRace race, @BindingParam("delta") Integer delta) {
		long raceNumber = 100;
		if (detail != null && race != null) {
			raceNumber = race.getNumber();
			if (raceNumber + delta < 1 || raceNumber + delta > detail.getRaces().size()) {
				return;
			}
		}
		ArrayList<SoftRace> temp = new ArrayList<SoftRace>();
		temp.addAll(detail.getRaces());
		detail.getRaces().clear();
		for (SoftRace t : temp) {
			if (t.getNumber() == raceNumber) {
				t.setNumber(t.getNumber() + delta);
			} else if (t.getNumber() == raceNumber + delta) {
				t.setNumber(t.getNumber() - delta);
			}
			detail.getRaces().add(t);
		}
		detail.setRaceCount(detail.getRaces().size());
	}

	private GenericResponse saveCard() {
		SecurityToken token = getToken();
		GenericResponse response = new GenericResponse();
		if (detail != null) {
			Set<SoftRace> races = new HashSet<SoftRace>();
			races.addAll(detail.getRaces());
			detail.setRaces(races);

			Set<SoftTote> sc_totes = new HashSet<SoftTote>();
			if (sourceCards.size() > 0) {
				for (SoftTote t : sourceCards.get(0).getTotes()) {
					SoftTote sc_t = new SoftTote();
					sc_t.setToteId(t.getToteId());
					sc_totes.add(sc_t);
				}
			}
			detail.setTotes(sc_totes);

			detail.setChildParents();
			if (detail.getId() == null) {
				response = softCardService.add(token, detail);
			} else {
				response = softCardService.update(token, detail.getId(), detail);
			}
		}
		return response;
	}

	private SoftCard getSingleCard(SoftCardOutline hco) {
		if (hco != null) {
			long id = hco.getId();
			SecurityToken token = getToken();
			SoftCardResponse hcr = softCardService.get(token, id);
			if (hcr.getStatus() == 0) {
				return hcr.getCard();
			}
		}
		return null;
	}

}
