package com.sportech.cl.viewmodel;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.zkoss.bind.annotation.AfterCompose;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Bandbox;
import org.zkoss.zul.Column;
import org.zkoss.zul.Grid;
import org.zkoss.zul.ListModel;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Window;

import com.sportech.cl.model.database.PoolTableCell;
import com.sportech.cl.model.database.PoolTableRow;
import com.sportech.cl.model.database.utils.Pool;
import com.sportech.cl.model.database.utils.PoolContainer;
import com.sportech.cl.model.database.utils.PoolHelper;
import com.sportech.common.model.PoolType;

public class PoolViewModel extends BaseViewModel {

    static private final Logger log = Logger.getLogger(PoolViewModel.class);

    public class PoolNameType {

        private String name;
        private String nameType;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getNameType() {
            return nameType;
        }

        public void setNameType(String nameType) {
            this.nameType = nameType;
        }

        public String getColumnWidth() {
            if (!StringUtils.isEmpty(name) && name.length() > 10) {
                return "85px";
            } else if (!StringUtils.isEmpty(name) && name.length() > 8) {
                return "75px";
            }
            return "50px";
        }
    };

    private List<Pool> sortedPools = new ArrayList<Pool>();

    private PoolContainer container;

    private List<Long> selectedLegs = new ArrayList<Long>();

    private boolean readonly = false;

    private String poolValidationMessage = null;

    private PoolHelper poolHelper = null;

    private boolean canDisableRace = false;

    @Wire("#poolGrid")
    Grid poolGrid;

    @AfterCompose
    public void afterCompose(@ContextParam(ContextType.VIEW) Component view) {
        Selectors.wireComponents(view, this, false);
    }

    @Init
    public void init(@BindingParam("container") PoolContainer container) {
        poolHelper = new PoolHelper(container);
        if (container != null) {
            this.container = container;
            this.canDisableRace = container.canDisableRace();
            this.readonly = container.isReadonly();
            poolHelper.setupSortedPools(container.getPoolCollection());
            sortedPools = poolHelper.getSortedPools();
        }
    }

    public ListModelList<PoolType> getPoolTypes() {
        return new ListModelList<PoolType>(PoolType.values());
    }

    public Collection<String> getPoolNames() {
        return poolHelper.getPoolNames();
    }

    public List<Long> getSelectedLegs() {
        return selectedLegs;
    }

    public void setSelectedLegs(List<Long> selectedLegs) {
        this.selectedLegs = selectedLegs;
    }

    public boolean isReadonly() {
        return readonly;
    }

    public void setReadonly(boolean readonly) {
        this.readonly = readonly;
    }

    public boolean isCanDisableRace() {
        return canDisableRace;
    }

    public void setCanDisableRace(boolean canDisableRace) {
        this.canDisableRace = canDisableRace;
    }

    public String getPoolValidationMessage() {
        return poolValidationMessage;
    }

    public void setPoolValidationMessage(String poolValidationMessage) {
        this.poolValidationMessage = poolValidationMessage;
    }

    public List<PoolNameType> getPoolNameTemplates() {
        List<PoolNameType> pools = new ArrayList<PoolNameType>();
        PoolNameType prace = new PoolNameType();
        prace.nameType = "race";
        pools.add(prace);
        Collection<String> names = poolHelper.getPoolNames();
        for (String name : names) {
            PoolNameType p = new PoolNameType();
            p.name = name;
            p.nameType = "pool";
            pools.add(p);
        }
        return pools;
    }

    public boolean isShowRunner() {
        if (container != null) {
            if (container.hasRunners()) {
                return true;
            }        
        }
        return false;
    }
    
    public ListModel<PoolTableRow> getPoolTableRows() {
        return new ListModelList<PoolTableRow>(poolHelper.getPoolTableRows(container.getRaceCount()));
    }

    @GlobalCommand
    @NotifyChange({ "poolTableRows", "poolNameTemplates" })
    public void notifyAddingPoolType(@BindingParam("poolTypes") List<PoolType> poolTypes, @BindingParam("addRaces") Boolean addRaces) {
        if (container != null && poolTypes != null) {
            for (PoolType poolType : poolTypes) {
                long newMask = 0L;
                if (addRaces != null && addRaces) {
                    log.debug("Adding races to new pool");
                    if (!poolType.isMultiRace()) {
                        int s = container.getRaceCount();
                        for (int l = 0; l < s; l++) {
                            long mask = 1 << l;
                            newMask = newMask | mask;
                        }
                    }
                }
                Pool p = container.addPool(poolType.getId(), newMask);
                p.setTotePoolType(poolType.getToteId());
            }
            init(container);
        }
        if (poolGrid != null) {
            poolGrid.invalidate();
        }
    }

    @Command
    @NotifyChange({ "poolTableRows" })
    public void refreshPool(@BindingParam("cell") PoolTableCell cell) {
        if (container != null) {
            if (cell.getCellType().equals(PoolTableCell.SINGLE)) {
                PoolHelper.refreshSinglePool(container, cell.getPoolTypeId(), cell.getRaceNumber(), cell.getChecked());
            } else {
                PoolHelper.refreshMultiPool(container, cell.getPoolTypeId(), cell.getRaceNumber(), cell.getRaceList());
            }
            refreshDisplay();
        }
    }

    private void refreshDisplay() {
        if (poolHelper.setupSortedPools(container.getPoolCollection())) {
            sortedPools = poolHelper.getSortedPools();
            if (poolGrid != null) {
                for (Component c : poolGrid.getColumns().getChildren()) {
                    if (c != null) {
                        Column cc = (Column) c;
                        if (cc != null) {
                            String oldPoolNames = poolHelper.getOldPoolNames();
                            if (!StringUtils.equalsIgnoreCase(cc.getLabel(), "race") && !oldPoolNames.contains(cc.getLabel())) {
                                poolGrid.getColumns().getChildren().remove(cc);
                                poolGrid.invalidate();
                                break;
                            }
                        }

                    }
                }
            }
        }
    }

    @Command
    public void addPoolType() {
        Map<String, Object> arg = new HashMap<String, Object>();
        arg.put("pool", this);
        Window window = (Window) Executions.createComponents("pooltypepopup.zul", null, arg);
        window.doModal();
    }

    @GlobalCommand
    @NotifyChange("poolTableRows")
    public void refreshPoolRaces(@BindingParam("raceCount") Integer raceCount) {
        if (raceCount != null && raceCount >= 0) {
            container.setRaceCount(raceCount);
            init(container);
        }
    }

    @Command
    public void updatePoolName(@BindingParam("cell") PoolTableCell cell) {
        if (container != null) {
            PoolHelper.updatePoolProperties(container, cell);
        }
    }
    
    @Command
    public void updatePoolGuarantee(@BindingParam("cell") PoolTableCell cell) {
        if (container != null) {
            PoolHelper.updatePoolProperties(container, cell);
        }
    }
    
    @Command
    public void openPoolPopup(@ContextParam(ContextType.COMPONENT) Component comp, @BindingParam("cell") PoolTableCell cell) {
        if (comp != null) {
            Bandbox b = (Bandbox) comp;
            b.setOpen(true);
        }
    }

    @Command
    @NotifyChange({ "poolTableRows" })
    public void refreshPoolEditor(@ContextParam(ContextType.COMPONENT) Component comp, @BindingParam("cell") PoolTableCell cell) {
        if (comp != null) {
            Bandbox b = (Bandbox) comp;
            if (!b.isOpen()) {
                Set<Long> legs = new HashSet<Long>();
                if (cell.getSelectedLegs() != null) {
                    legs.addAll(cell.getSelectedLegs());
                    String l = PoolHelper.buildRaceList(cell.getRaceNumber(), legs);
                    b.setValue(l);
                    cell.setRaceList(l);
                    log.info("Selected new pool legs for race " + cell.getRaceNumber() + " legs:" + l);
                    refreshPool(cell);
                }
                b.setOpen(false);
            }
        }
    }

    private boolean validatePoolLegs(PoolTableCell cell) {
        poolValidationMessage = null;
        Set<Long> legs = new HashSet<Long>();
        if (cell.getSelectedLegs() != null && cell.getSelectedLegs().size() > 0) {
            legs.addAll(cell.getSelectedLegs());
            PoolType pt = PoolType.fromId(cell.getPoolTypeId());
            if (pt != null) {
                if ((legs.size() < pt.getRaceCount() - 1) || legs.size() >= pt.getMaxRaceCount()) {
                    poolValidationMessage = "Invalid pool";
                    return false;
                }
            }
            Long endLeg = Collections.max(legs);
            for (Pool p : sortedPools) {
                if (p.getPoolTypeId().intValue() == cell.getPoolTypeId().intValue()) {
                    if (p.getRaceBitmap() > 0L) {
                        Set<Long> raceNumbers = new HashSet<Long>();
                        Long startLeg = PoolHelper.getRacesFromBitmap(p.getRaceBitmap(), raceNumbers);
                        if ((!cell.getRaceNumber().equals(startLeg)) && Collections.max(raceNumbers).equals(endLeg)) {
                            poolValidationMessage = "End race confliction";
                            return false;
                        }
                        if (pt != null && pt.isExchange()) {
                            HashSet<Long> intersection = new HashSet<Long>(legs);
                            intersection.add(cell.getRaceNumber());
                            intersection.retainAll(raceNumbers);
                            if ((!cell.getRaceNumber().equals(startLeg)) && !intersection.isEmpty()) {
                                poolValidationMessage = "Race confliction";
                                return false;
                            }
                        }
                    }
                }
            }
        }
        return true;
    }

    @Command
    @NotifyChange({ "poolValidationMessage" })
    public void refreshPoolLegSelection(@BindingParam("cell") PoolTableCell cell) {
        validatePoolLegs(cell);
    }

    @Command
    public void refreshPoolNameEditor(@ContextParam(ContextType.COMPONENT) Component comp, @BindingParam("cell") PoolTableCell cell) {
        if (comp != null) {
            Bandbox b = findBandbox(comp);
            if (b != null) {
                b.setOpen(false);
                updatePoolName(cell);
            }
        }
    }

    @Command
    @NotifyChange({ "poolTableRows" })
    public void refreshPoolLegs(@ContextParam(ContextType.COMPONENT) Component comp, @BindingParam("cell") PoolTableCell cell) {
        if (cell != null) {
            Bandbox b = findBandbox(comp);
            if (b != null) {
                b.setOpen(false);
                refreshPoolEditor(b, cell);
            }
        }
    }

    @Command
    @NotifyChange({ "poolTableRows", "poolNameTemplates" })
    public void disableRace(@BindingParam("race") String raceNumber) {
        long r = Long.parseLong(raceNumber);
        PoolHelper poolHelper = new PoolHelper(container);
        poolHelper.disablePoolsforRace(r);
        init(container);
        if (poolGrid != null) {
            poolGrid.invalidate();
        }
    }

    private static Bandbox findBandbox(Component comp) {
        while (comp != null) {
            if (comp instanceof Bandbox) {
                Bandbox b = (Bandbox) comp;
                return b;
            } else {
                comp = comp.getParent();
            }
        }
        return null;
    }

}
