package com.sportech.cl.viewmodel;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Executions;

public class SideBarViewModel extends BaseViewModel {

	private static SimpleDateFormat comparibleFormat = new SimpleDateFormat("yyyy/MM/dd");

	private boolean showHostCards;
	private boolean showLoadCards;
	private boolean showPatterns;
	private boolean showImports;
	private boolean showTotes;
	private boolean showTracks;
	private boolean showUsers;
	private boolean showGroups;
	private boolean showDashboard;
	private boolean showSettings;
	private boolean showToteCards;
	
	@Init
	public void init() {
		if (isUserInRole("Host Card Viewer,Host Card Editor")) {
			showHostCards = true;
		}
		if (isUserInRole("Load Card Viewer,Load Card Editor")) {
			showLoadCards = true;
		}
		if (isUserInRole("Pattern Viewer,Pattern Editor")) {
			showPatterns = true;
		}		
		if (isUserInRole("Import Viewer,Import Editor")) {
			showImports = true;
		}
		if (isUserInRole("Tote Admin")) {
			showTotes = true;
		}
		if (isUserInRole("Track Admin")) {
			showTracks = true;
		}
		if (isUserInRole("User Admin")) {
			showUsers = true;
		}
		if (isUserInRole("Group Admin")) {
			showGroups = true;
		}
		if (isUserInRole("Host Card Viewer,Host Card Editor,Load Card Viewer,Load Card Editor")) {
			showDashboard = true;			
		}
		if (isUserInRole("Tote Card Approver")) {
			showToteCards = true;
		}
	}
	
	public boolean isShowToteCards() {
		return showToteCards;
	}

	public void setShowToteCards(boolean showToteCards) {
		this.showToteCards = showToteCards;
	}

	public boolean isShowGroups() {
		return showGroups;
	}

	public void setShowGroups(boolean showGroups) {
		this.showGroups = showGroups;
	}

	public boolean isShowUsers() {
		return showUsers;
	}

	public void setShowUsers(boolean showUsers) {
		this.showUsers = showUsers;
	}

	public boolean isShowTotes() {
		return showTotes;
	}

	public void setShowTotes(boolean showTotes) {
		this.showTotes = showTotes;
	}

	public boolean isShowTracks() {
		return showTracks;
	}

	public void setShowTracks(boolean showTracks) {
		this.showTracks = showTracks;
	}

	public boolean isShowImports() {
		return showImports;
	}

	public void setShowImports(boolean showImports) {
		this.showImports = showImports;
	}

	public boolean isShowHostCards() {
		return showHostCards;
	}

	public void setShowHostCards(boolean showHostCards) {
		this.showHostCards = showHostCards;
	}

	public boolean isShowLoadCards() {
		return showLoadCards;
	}

	public void setShowLoadCards(boolean showLoadCards) {
		this.showLoadCards = showLoadCards;
	}

	public boolean isShowPatterns() {
		return showPatterns;
	}

	public void setShowPatterns(boolean showPatterns) {
		this.showPatterns = showPatterns;
	}

	public boolean isShowDashboard() {
		return showDashboard;
	}

	public void setShowDashboard(boolean showDashboard) {
		this.showDashboard = showDashboard;
	}

	public boolean isShowSettings() {
		return showSettings;
	}

	public void setShowSettings(boolean showSettings) {
		this.showSettings = showSettings;
	}

	public boolean isShowWarning() {
		String current = comparibleFormat.format(new Date());
		String working = comparibleFormat.format(getWorkingDate());
		return !current.equals(working);
	}

	@Command
	@NotifyChange("showWarning")
	public void refreshWorkingDate() {
		Executions.getCurrent().sendRedirect("dashboard.zul");
	}
	
}
