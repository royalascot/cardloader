package com.sportech.cl.viewmodel;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.zkoss.bind.annotation.AfterCompose;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Button;
import org.zkoss.zul.Include;
import org.zkoss.zul.ListModel;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Window;

import com.sportech.cl.global.utils.config.ConfigLoader;
import com.sportech.cl.model.common.PageParams;
import com.sportech.cl.model.comparator.HardRaceComparator;
import com.sportech.cl.model.database.HardCard;
import com.sportech.cl.model.database.HardCardOutline;
import com.sportech.cl.model.database.HardPool;
import com.sportech.cl.model.database.HardRace;
import com.sportech.cl.model.database.PatternOutline;
import com.sportech.cl.model.response.GenericResponse;
import com.sportech.cl.model.response.HardCardListResponse;
import com.sportech.cl.model.response.HardCardResponse;
import com.sportech.cl.model.security.SecurityToken;
import com.sportech.cl.services.cards.HardCardsRemote;
import com.sportech.cl.services.cards.HardCardsRemoteLocator;
import com.sportech.cl.services.cards.SoftCardsRemote;
import com.sportech.cl.services.cards.SoftCardsRemoteLocator;
import com.sportech.common.model.PerformanceType;

public class HardCardViewModel extends BaseViewModel {

    private HardCardsRemote hardCardService = HardCardsRemoteLocator.getRemoteIface(ConfigLoader.getCfg());

    private HardCardFilter cardFilter = new HardCardFilter();

    private HardCardOutline selected;

    private HardCard detail;

    private HardRace selectedRace = new HardRace();

    private List<HardCardOutline> cards = new ArrayList<HardCardOutline>();

    private List<HardCardOutline> selectedCards = new ArrayList<HardCardOutline>();

    private List<PatternOutline> selectedPatterns = new ArrayList<PatternOutline>();

    private PatternOutline pendingSelectedPattern = null;

    private TrackPatternViewModel patternViewModel = null;

    private String confirmMessage = null;

    @Wire("#createButton")
    Button createButton;

    @Wire("#publishButton")
    Button publishButton;   

    @AfterCompose
    public void afterCompose(@ContextParam(ContextType.VIEW) Component view) {
        Selectors.wireComponents(view, this, false);
    }

    @Init
    public void init(@BindingParam("hostCard") HardCard hostCard, @BindingParam("selectedCard") HardCardOutline selectedCard,
            @BindingParam("trackCode") String trackCode) {
        if (!StringUtils.isEmpty(trackCode)) {
            cardFilter.setTrackCode(trackCode);
        }
        if (hostCard != null) {
            detail = hostCard;
            return;
        }
        selectedRace = null;
        selected = selectedCard;
        if (selected != null) {
            detail = getSingleCard(selected);
            if (detail != null && detail.getRaces() != null && detail.getRaces().size() > 0) {
                selectedRace = Collections.min(detail.getRaces(), new HardRaceComparator());
            }
        }
    }

    public ListModel<HardCardOutline> getCards() {
        getFilterCards(cardFilter);
        ListModelList<HardCardOutline> model = new ListModelList<HardCardOutline>(cards);
        model.setMultiple(true);
        return model;
    }

    public List<HardCardOutline> getFilterCards(HardCardFilter filter) {
        SecurityToken token = getToken();
        HardCardOutline[] query = new HardCardOutline[1];
        query[0] = new HardCardOutline();
        PageParams pageParams = new PageParams();
        pageParams.setFirstResult(0);
        pageParams.setMaxResult(Integer.MAX_VALUE);

        Date startDate = getWorkingDate();
        Date endDate = addDate(startDate, 0);
        if (filter.getCardDate() != null) {
            startDate = filter.getCardDate();
            endDate = filter.getCardDate();
        }
        if (!StringUtils.isBlank(filter.getApprovalStatus())) {
            if (StringUtils.startsWithIgnoreCase(filter.getApprovalStatus(), "Y")) {
                query[0].setApproved(true);
            } else if (StringUtils.startsWithIgnoreCase(filter.getApprovalStatus(), "N")) {
                query[0].setApproved(false);
            }
        }
        if (!StringUtils.isBlank(filter.getTrackCountryCode())) {
            query[0].setTrackCountryCode(filter.getTrackCountryCode());
        }
        if (!StringUtils.isBlank(filter.getTrackCode())) {
            query[0].setTrackEquibaseId(filter.getTrackCode());
        }
        if (!StringUtils.isBlank(filter.getDescription())) {
            query[0].setDescription(filter.getDescription() + "%");
        }
        if (filter.getCardType() != null) {
            query[0].setCardType(filter.getCardType());
        }

        HardCardListResponse result = hardCardService.find(token, query, startDate, endDate, true, pageParams);
        if (result.getStatus() == 0) {
            cards = result.getCards();
        } else {
            cards = new ArrayList<HardCardOutline>();
        }
        return cards;
    }

    @NotifyChange({ "detail" })
    public void setSelectedCard(HardCardOutline selected) {
        this.selected = selected;
    }

    public HardCardOutline getSelectedCard() {
        return selected;
    }

    private HardCard getSingleCard(HardCardOutline hco) {
        if (hco != null) {
            long id = hco.getId();
            SecurityToken token = getToken();
            HardCardResponse hcr = hardCardService.get(token, id);
            if (hcr.getStatus() == 0) {
                return hcr.getCard();
            }
        }
        return null;
    }

    public HardCard getDetail() {
        return detail;
    }

    public void setDetail(HardCard detail) {
        this.detail = detail;
    }

    public HardRace getSelectedRace() {
        return selectedRace;
    }

    public void setSelectedRace(HardRace selectedRace) {
        this.selectedRace = selectedRace;
    }

    public HardCardFilter getCardFilter() {
        return cardFilter;
    }

    public void setCardFilter(HardCardFilter cardFilter) {
        this.cardFilter = cardFilter;
    }

    public List<PatternOutline> getSelectedPatterns() {
        return selectedPatterns;
    }

    public TrackPatternViewModel getPatternViewModel() {
        if (patternViewModel == null) {
            patternViewModel = new TrackPatternViewModel();
            patternViewModel.init(null, this, detail.getTrack().getId());
        }
        return patternViewModel;
    }

    @NotifyChange("creationDisabled")
    public void setSelectedPatterns(List<PatternOutline> selectedPatterns) {
        this.selectedPatterns = selectedPatterns;
    }

    public List<HardCardOutline> getSelectedCards() {
        return selectedCards;
    }

    public void setSelectedCards(List<HardCardOutline> selectedCards) {
        this.selectedCards = selectedCards;
    }

    public Boolean getCreationDisabled() {
        return selectedPatterns == null || selectedPatterns.size() == 0;
    }

    public String getConfirmMessage() {
        return confirmMessage;
    }

    public void setConfirmMessage(String confirmMessage) {
        this.confirmMessage = confirmMessage;
    }

    @Command
    @NotifyChange({ "patternViewModel", "selectedPatterns" })
    public void changePatternFilter() {
        if (patternViewModel != null) {
            patternViewModel.changeFilter();
        }
    }

    @Command
    @NotifyChange({ "cards" })
    public void changeFilter() {
        cards = getFilterCards(cardFilter);
    }

    @Command
    public void editCard(@BindingParam("card") HardCardOutline c) {
        selected = c;
        Map<String, Object> arg = new HashMap<String, Object>();
        arg.put("selectedCard", c);
        Window window = (Window) Executions.createComponents("hostcarddetail.zul", null, arg);
        window.doModal();
    }

    @Command
    public void addHostCard() {
        HardCard hc = new HardCard();
        hc.setApproved(false);
        hc.setRaces(new HashSet<HardRace>());
        hc.setRaceCount(0);
        hc.setProvider("User Created");
        hc.setCardDate(DateUtils.truncate(new Date(), Calendar.DATE));
        hc.setPerfType(PerformanceType.Matinee);
        hc.setPools(new HashSet<HardPool>());
        Map<String, Object> arg = new HashMap<String, Object>();
        arg.put("hostCard", hc);
        Window window = (Window) Executions.createComponents("hostcarddetail.zul", null, arg);
        window.doModal();
    }

    @Command
    public void createPattern(@BindingParam("card") HardCardOutline c) {
        selected = c;
        HardCard hc = getSingleCard(c);
        Map<String, Object> arg = new HashMap<String, Object>();
        arg.put("card", hc);
        Window window = (Window) Executions.createComponents("patterndetail.zul", null, arg);
        window.doModal();
    }

    @Command
    public void viewPatterns(@BindingParam("card") HardCardOutline c) {
        String url = "patterns.zul?trackcode=" + c.getTrackId();
        Executions.getCurrent().sendRedirect(url);
    }

    @Command
    public void createLoadCards(@BindingParam("card") HardCardOutline c) {
        selected = c;
        Map<String, Object> arg = new HashMap<String, Object>();
        arg.put("selectedCard", c);
        Window window = (Window) Executions.createComponents("createloadcards.zul", null, arg);
        window.doModal();
    }

    @Command
    public void generateLoadCards(@ContextParam(ContextType.VIEW) Window comp, @BindingParam("card") HardCardOutline c,
            @BindingParam("withApproval") Boolean withApproval) {
        SoftCardsRemote softCardService = SoftCardsRemoteLocator.getRemoteIface(ConfigLoader.getCfg());
        SecurityToken token = getToken();
        if (selectedPatterns != null) {
            for (PatternOutline p : selectedPatterns) {
                softCardService.createFromHardCard(token, c.getId(), p.getId(), p.getMergeType(), p.getForceRaceCount(),
                        withApproval);
            }
        }
        comp.detach();
    }

    @Command
    @NotifyChange({ "selectedPatterns", "confirmMessage" })
    public void selectionConfirmed() {
        confirmMessage = null;
    }

    @Command
    @NotifyChange({ "selectedPatterns", "confirmMessage" })
    public void cancelSelection() {
        if (pendingSelectedPattern != null) {
            selectedPatterns.remove(pendingSelectedPattern);
            pendingSelectedPattern = null;
        }
        confirmMessage = null;
    }

    @Command
    @NotifyChange({ "selectedPatterns", "confirmMessage" })
    public void confirmSelection() {
        if (selectedPatterns != null) {
            for (PatternOutline p : selectedPatterns) {
                if (this.patternViewModel.getUsedPatternIds().contains(p.getId())) {
                    confirmMessage = "This pattern has been used. Are you sure to use it again?";
                    pendingSelectedPattern = p;
                    return;
                }
            }
        }
        confirmMessage = null;
    }

    @Command
    public void closeCardEditWindow(@ContextParam(ContextType.VIEW) Window comp) {
        comp.detach();
    }

    @GlobalCommand
    @NotifyChange({ "cards", "selectCard", "selectedCards" })
    public void refreshHostCard() {
    }

    @Command
    @NotifyChange({ "cards", "selectedCards" })
    public void saveHostCard(@ContextParam(ContextType.VIEW) Window comp) {
        SecurityToken token = getToken();
        if (detail != null) {
            if (detail.getConfig() == null) {
                Messagebox.show("Missing card configuration", "Error Saving Card", Messagebox.OK, Messagebox.ERROR);
                return;
            }

            if (detail.getFeatureRace() != null) {
                int fr = detail.getFeatureRace();
                if (fr <= 0 || fr > detail.getRaceCount()) {
                    Messagebox.show("Invalid feature race number.", "Error Saving Card", Messagebox.OK, Messagebox.ERROR);
                    return;
                }
            }

            GenericResponse response;
            if (detail.getId() == null) {
                response = hardCardService.add(token, detail);
            } else {
                response = hardCardService.update(token, detail.getId(), detail);
            }
            if (response.getStatus() == 0) {
                comp.detach();
            } else {
                String errorMessage = StringUtils.join(response.getErrors(), " ");
                Messagebox.show(errorMessage, "Error Saving Host Card", Messagebox.OK, Messagebox.ERROR);
            }
        }
    }

    @Command
    @NotifyChange({ "cards", "selectedCards" })
    public void approveHostCard(@ContextParam(ContextType.VIEW) Window comp) {
        if (detail != null) {
            SecurityToken token = getToken();
            GenericResponse response = hardCardService.approve(token, detail.getId(), true);
            if (response.getStatus() == 0) {
                comp.detach();
            } else {
                String msg = StringUtils.join(response.getErrors(), " ");
                msg = StringUtils.remove(msg, "Generic failure. ");
                Messagebox.show(msg, "Error Approving Card", Messagebox.OK, Messagebox.ERROR);
            }
        }
    }

    @Command
    @NotifyChange({ "detail", "selectedCard", "selectedRace" })
    public void addRace() {
        if (detail != null) {
            HardRace hr = new HardRace();
            hr.setParent(detail);
            hr.setNumber(new Long(detail.getRaceCount() + 1));
            detail.getRaces().add(hr);
        }
    }

    @Command
    public void addPattern(@ContextParam(ContextType.VIEW) Window comp) {
        if (selected != null) {
            comp.detach();
            createPattern(selected);
        }
    }

    @Command
    public void editPattern(@BindingParam("pattern") PatternOutline p) {
        if (patternViewModel != null) {
            patternViewModel.editPattern(p);
        }
    }

    @Command
    public void viewPatternTotes(@BindingParam("pattern") PatternOutline p) {
        if (patternViewModel != null) {
            patternViewModel.viewTotes(p);
        }
    }

    public boolean isShowClear() {
        if (detail != null && (detail.getApproved() != null) && (detail.getApproved() == true) && SecurityToken.isAdmin(getToken())) {
            return true;
        }
        return false;
    }

    @Command
    @NotifyChange({ "detail", "showClear" })
    public void clearHostCard(@ContextParam(ContextType.VIEW) Window comp) {
        if (detail != null) {
            SecurityToken token = getToken();
            GenericResponse response = hardCardService.approve(token, detail.getId(), false);
            if (response.getStatus() == 0) {
                HardCardResponse hcr = hardCardService.get(token, detail.getId());
                if (hcr.getStatus() == 0) {
                    detail = hcr.getCard();
                }
                Iterator<Component> it = org.zkoss.zk.ui.select.Selectors.iterable(comp.getPage(), ".myclass").iterator();
                while (it.hasNext()) {
                    Include include = (Include) it.next();
                    if (include != null) {
                        String src = include.getSrc();
                        include.setSrc(null);
                        if (StringUtils.contains(src, "races")) {
                            include.setDynamicProperty("card", detail);
                        } else {
                            include.setDynamicProperty("container", detail);
                        }
                        include.setDynamicProperty("readonly", detail.getApproved());
                        include.setSrc(src);
                    }
                }
                comp.getPage().invalidate();
            } else {
                Messagebox.show("Failed to clear host card.");
            }
        }
    }

    @Command
    @NotifyChange({ "cards", "detail", "deleteMessage" })
    public void deleteHostCard() {
        deleteMessage = null;
        if (detail != null) {
            SecurityToken token = getToken();
            GenericResponse response = hardCardService.delete(token, detail.getId());
            if (response.getStatus() == 0) {
                detail = null;
            } else {
                Messagebox.show("Failed to delete host card.");
            }
        }
    }

    public String getDeleteMessage() {
        return deleteMessage;
    }

    public void setDeleteMessage(String deleteMessage) {
        this.deleteMessage = deleteMessage;
    }

    private String deleteMessage = null;

    @Command
    @NotifyChange("deleteMessage")
    public void confirmDelete(@BindingParam("card") HardCardOutline p) {
        if (p != null) {
            deleteMessage = "This operation will also delete all associated load cards. Are you sure to delete this card: '" + p.getDescription() + "'?";
            detail = getSingleCard(p);
        }
    }

    @Command
    @NotifyChange("deleteMessage")
    public void cancelDelete() {
        deleteMessage = null;
    }

}
