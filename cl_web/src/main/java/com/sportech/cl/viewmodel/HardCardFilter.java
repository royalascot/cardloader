package com.sportech.cl.viewmodel;

import java.util.Date;

import org.apache.commons.lang3.StringUtils;

import com.sportech.common.model.CardType;

public class HardCardFilter {
	
	private Date cardDate = null;
	private String description = null;
	private String trackCode = null;
	private String trackCountryCode = null;
	private String approvalStatus = " ";
	private CardType cardType = null;

	public CardType getCardType() {
		return cardType;
	}

	public void setCardType(CardType cardType) {
		this.cardType = cardType;
	}

	public Date getCardDate() {
		return cardDate;
	}

	public void setCardDate(Date cardDate) {
		this.cardDate = cardDate;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String value) {
		if (StringUtils.isBlank(value)) {
			value = null;
		}
		this.description = value;
	}

	public String getApprovalStatus() {
		return approvalStatus;
	}

	public void setApprovalStatus(String approvalStatus) {
		this.approvalStatus = approvalStatus;
	}

	public String getTrackCountryCode() {
		return trackCountryCode;
	}

	public void setTrackCountryCode(String trackCountryCode) {
		this.trackCountryCode = trackCountryCode;
	}

	public String getTrackCode() {
		return trackCode;
	}

	public void setTrackCode(String trackCode) {
		this.trackCode = trackCode;
	}

}
