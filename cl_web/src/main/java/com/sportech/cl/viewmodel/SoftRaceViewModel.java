package com.sportech.cl.viewmodel;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.DependsOn;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Messagebox;

import com.sportech.cl.model.comparator.RunnerComparator;
import com.sportech.cl.model.database.Runner;
import com.sportech.cl.model.database.SoftCard;
import com.sportech.cl.model.database.SoftRace;

public class SoftRaceViewModel extends BaseViewModel {

	private List<SoftRace> races = new ArrayList<SoftRace>();

	private SoftRace race;

	private SoftCard loadCard;

	private String deleteMessage = null;

	private SoftRace racetoDelete = null;

	private boolean readonly = false;

	private Integer currentRaceNumber = null;	

	@Init
	public void init(@BindingParam("loadCard") SoftCard loadCard) {
		if (loadCard != null) {
			this.loadCard = loadCard;
			getRaces();
			if (races != null && races.size() > 0) {
				race = races.get(0);
			}
			if (loadCard.getApproved() != null && loadCard.getApproved()) {
				readonly = true;
			}		
		}
	}

	public Integer getCurrentRaceNumber() {
		if (currentRaceNumber == null) {
			if (race != null) {
				currentRaceNumber = race.getNumber().intValue() - 1;
			}
		}
		return currentRaceNumber;
	}

	public void setCurrentRaceNumber(Integer currentRaceNumber) {
		this.currentRaceNumber = currentRaceNumber;
	}

	public ListModelList<String> getRaceNumbers() {
		List<SoftRace> races = getRaces();
		List<String> numbers = new ArrayList<String>();
		for (SoftRace hr : races) {
			numbers.add("" + hr.getNumber());
		}
		return new ListModelList<String>(numbers);
	}

	public List<SoftRace> getRaces() {
		races = new ArrayList<SoftRace>();
		if (loadCard != null) {
			races.addAll(loadCard.getRaces());
		}
		return races;
	}

	public void setRaces(List<SoftRace> races) {
		this.races = races;
	}

	public Long getRaceCount() {
		if (races != null) {
			return new Long(races.size());
		}
		return 0L;
	}

	public boolean isReadonly() {
		return readonly;
	}

	public void setReadonly(boolean readonly) {
		this.readonly = readonly;
	}

	@DependsOn({ "race", "raceCount" })
	public boolean getCanAddRace() {
		if (readonly) {
			return true;
		}
		if (getRaceCount() == 0 && race == null) {
			return false;
		}
		if (race != null && race.getNumber() != null && race.getNumber().longValue() == getRaceCount().longValue()) {
			return false;
		}
		return true;
	}

	public SoftRace getRace() {
		if (race != null) {
			Collections.sort(race.getRace().getRunners(), new RunnerComparator());
		}
		return race;
	}

	public void setRace(SoftRace race) {
		this.race = race;
	}

	public String getDeleteMessage() {
		return deleteMessage;
	}

	public void setDeleteMessage(String deleteMessage) {
		this.deleteMessage = deleteMessage;
	}

	@Command
	@NotifyChange("race")
	public void previousRace() {
		if (race != null) {
			int r = race.getNumber().intValue();
			r = r - 2;
			if (r <= 0) {
				r = 0;
			}
			race = races.get(r);
		}
	}

	@Command
	@NotifyChange("race")
	public void nextRace() {
		if (race != null) {
			int r = race.getNumber().intValue();
			if (r >= races.size()) {
				r = races.size() - 1;
			}
			race = races.get(r);
		}
	}

	@Command
	@NotifyChange("race")
	public void changeRace() {
		if (currentRaceNumber != null) {
			int r = currentRaceNumber + 1;
			if (r > 0 && r <= getRaceCount()) {
				race = races.get(r - 1);
			}
		}
	}

	@Command
	@NotifyChange({ "parent", "races", "race", "raceCount", "raceNumbers", "currentRaceNumber" })
	public void addRace() {
		if (loadCard != null) {
			SoftRace hr = new SoftRace();
			hr.setParent(loadCard);
			hr.setNumber(new Long(loadCard.getRaceCount() + 1));
			loadCard.getRaces().add(hr);
			race = hr;
			currentRaceNumber = hr.getNumber().intValue() - 1;
		}
	}

	@Command
	@NotifyChange({ "parent", "races", "race", "raceCount",  "raceNumbers", "currentRaceNumber" })
	public void removeRace() {
		if (loadCard != null && race != null) {
			loadCard.getRaces().remove(race);
			getRaces();
			if (races != null && races.size() > 0) {
				race = races.get(races.size() - 1);
				currentRaceNumber = race.getNumber().intValue() - 1;
			}
		}
	}

	@Command
	@NotifyChange({ "race" })
	public void refreshRunnerNumber(@BindingParam("runner") Runner runner) {
		if (race != null) {
			if (!StringUtils.isBlank(runner.getProgramNumber())) {
				String rn = StringUtils.upperCase(runner.getProgramNumber());
				String lastrn = rn.substring(rn.length() - 1);
				if (StringUtils.isAlpha(lastrn)) {
					String firstrn = rn.substring(0, rn.length() - 1);
					Runner coupledRunner = null;
					for (Runner r : race.getRace().getRunners()) {
						String n = r.getProgramNumber();
						if (StringUtils.endsWithIgnoreCase(rn, n)) {
							continue;
						}
						if (!StringUtils.isBlank(n)) {
							String last = n.substring(n.length() - 1);
							if (StringUtils.endsWithIgnoreCase(last, lastrn)) {
								Messagebox.show("Invalid program number", "Runner Validation Error", Messagebox.OK,
								        Messagebox.ERROR);
								runner.setProgramNumber(firstrn);
								return;
							}
							String first = n.substring(0, n.length() - 1);
							if (firstrn.equals(first)) {
								coupledRunner = r;
							}
						}
					}
					if (coupledRunner != null) {
						coupledRunner.setCoupleIndicator(lastrn);
						runner.setCoupleIndicator(lastrn);
					}
					runner.setProgramNumber(rn);
				}
			}
		}
	}

	@Command
	@NotifyChange({ "race", "races", "deleteMessage", "raceCount", "parent" })
	public void deleteConfirmed() {
		deleteMessage = null;
		if (racetoDelete != null) {
			removeRace();
			racetoDelete = null;
		}
	}

	@Command
	@NotifyChange("deleteMessage")
	public void confirmDeleteRace() {
		if (race != null) {
			racetoDelete = race;
			deleteMessage = "Do you want to delete race " + race.getNumber() + "?";
		}
	}

	@Command
	@NotifyChange("deleteMessage")
	public void cancelDelete() {
		deleteMessage = null;
		racetoDelete = null;
	}

}
