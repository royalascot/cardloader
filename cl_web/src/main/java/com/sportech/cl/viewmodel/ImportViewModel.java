package com.sportech.cl.viewmodel;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zul.ListModel;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Window;

import com.sportech.cl.global.utils.config.ConfigLoader;
import com.sportech.cl.model.common.PageParams;
import com.sportech.cl.model.database.HardCardOutline;
import com.sportech.cl.model.database.ImportOutline;
import com.sportech.cl.model.response.HardCardListResponse;
import com.sportech.cl.model.response.ImportListResponse;
import com.sportech.cl.model.response.StringResponse;
import com.sportech.cl.model.security.SecurityToken;
import com.sportech.cl.services.cards.HardCardsRemote;
import com.sportech.cl.services.cards.HardCardsRemoteLocator;
import com.sportech.cl.services.imports.ImportsRemote;
import com.sportech.cl.services.imports.ImportsRemoteLocator;

public class ImportViewModel extends BaseViewModel {

	private ImportsRemote importService = ImportsRemoteLocator.getRemoteIface(ConfigLoader.getCfg());
	private HardCardsRemote hardCardService = HardCardsRemoteLocator.getRemoteIface(ConfigLoader.getCfg());

	private ImportOutline selectedImport;

	private ImportFilter importFilter = new ImportFilter();

	@Init
	public void init() {
		importFilter.setStartDate(addDate(getWorkingDate(), -7));
		importFilter.setEndDate(getWorkingDate());
	}

	public ListModel<ImportOutline> getImports() {
		return new ListModelList<ImportOutline>(getImportList(importFilter));
	}

	public List<ImportOutline> getImportList(ImportFilter filter) {
		SecurityToken token = getToken();
		ImportOutline[] query = new ImportOutline[1];
		query[0] = new ImportOutline();
		PageParams pageParams = new PageParams();
		pageParams.setFirstResult(0);
		pageParams.setMaxResult(Integer.MAX_VALUE);
		Date startDate = filter.getStartDate();
		Date endDate = filter.getEndDate();
		if (!StringUtils.isBlank(filter.getFileName())) {
			query[0].setFilename("%" + filter.getFileName() + "%");
		}
		if (!StringUtils.isBlank(filter.getStatus())) {
			query[0].setStatus("%" + filter.getStatus() + "%");
		}
		ImportListResponse result = importService.find(token, query, startDate, endDate, true, pageParams);
		if (result.getStatus() == 0) {
			return result.getImports();
		} else {
			return new ArrayList<ImportOutline>();
		}
	}

	public List<HardCardOutline> getCards() {
		if (selectedImport != null) {
			SecurityToken token = getToken();
			long id = selectedImport.getId();
			HardCardOutline h = new HardCardOutline();
			h.setImportId(id);
			HardCardOutline[] filter = new HardCardOutline[1];
			filter[0] = h;
			PageParams pageParams = new PageParams();
			pageParams.setFirstResult(0);
			pageParams.setMaxResult(Integer.MAX_VALUE);
			HardCardListResponse response = hardCardService.find(token, filter, null, null, false, pageParams);
			return response.getCards();
		}
		return new ArrayList<HardCardOutline>();
	}

	public String getSelectedImportLog() {
		if (selectedImport != null) {
			SecurityToken token = getToken();
			long id = selectedImport.getId();
			StringResponse response = importService.getImportLog(token, id);
			return response.getString();
		}
		return "";
	}

	public ImportOutline getSelectedImport() {
		return selectedImport;
	}

	@NotifyChange({ "cards", "selectedImportLog" })
	public void setSelectedImport(ImportOutline selectedImport) {
		this.selectedImport = selectedImport;
	}

	public ImportFilter getImportFilter() {
		return importFilter;
	}

	public void setImportFilter(ImportFilter importFilter) {
		this.importFilter = importFilter;
	}

	@Command
	@NotifyChange({ "imports" })
	public void changeFilter() {
	}

	@Command
	public void editCard(@BindingParam("card") HardCardOutline c) {
		Map<String, Object> arg = new HashMap<String, Object>();
		arg.put("selectedCard", c);
		Window window = (Window) Executions.createComponents("hostcarddetail.zul", null, arg);
		window.doModal();
	}

}
