package com.sportech.cl.viewmodel;

import com.sportech.common.model.CardType;

public class TrackFilter {

	private Long trackId = null;
	private String country;
	private String name;
	private String code;
	private CardType cardType = null;
	private String toteCode;
	
	public Long getTrackId() {
		return trackId;
	}

	public void setTrackId(Long trackId) {
		this.trackId = trackId;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public CardType getCardType() {
		return cardType;
	}

	public void setCardType(CardType cardType) {
		this.cardType = cardType;
	}

    public String getToteCode() {
        return toteCode;
    }

    public void setToteCode(String toteCode) {
        this.toteCode = toteCode;
    }

}
