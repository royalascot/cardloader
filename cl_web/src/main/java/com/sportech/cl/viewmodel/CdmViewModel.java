package com.sportech.cl.viewmodel;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.zkoss.bind.Property;
import org.zkoss.bind.ValidationContext;
import org.zkoss.bind.Validator;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.bind.validator.AbstractValidator;

import com.sportech.cl.global.utils.config.ConfigLoader;
import com.sportech.cl.model.database.CardConfig;
import com.sportech.cl.model.database.PoolMinimum;
import com.sportech.cl.model.database.PoolParameter;
import com.sportech.cl.model.database.SoftCard;
import com.sportech.cl.model.response.HardCardResponse;
import com.sportech.cl.services.cards.HardCardsRemote;
import com.sportech.cl.services.cards.HardCardsRemoteLocator;
import com.sportech.common.model.PoolType;
import com.sportech.common.util.ListHelper;

public class CdmViewModel extends BaseViewModel {

    private static final BigDecimal TOTE_LIMIT = new BigDecimal(320);
    
    private static final long RACE_NUMBER_LIMIT = 32;

    private PoolMinimum detail;

    private CardConfig filter = new CardConfig();
   
    private HardCardsRemote hardCardService = HardCardsRemoteLocator.getRemoteIface(ConfigLoader.getCfg());

    private boolean readonly = false;
    
    private boolean defaultPanel = false;
    
    @Init
    public void init(@BindingParam("container") CardConfig item, @BindingParam("card") SoftCard card,
            @BindingParam("ro")Boolean ro, @BindingParam("showDefault") Integer showDefault) {
        if (ro != null) {
            readonly = ro;
        }
        
        if (showDefault != null && showDefault > 0) {
            defaultPanel = true;
        }
        
        if (card != null) {
            readonly = true;
            Long id = card.getHardCardId();
            HardCardResponse hcp = hardCardService.get(getToken(), id);
            if (hcp != null && hcp.isOK()) {
                item = hcp.getCard().getConfig();                
            }
        }
        if (item != null) {            
            if (item.getPoolMinimum() != null) {
                detail = item.getPoolMinimum();
            } else {
                detail = new PoolMinimum();
            }
            item.setPoolMinimum(detail);
            populatePoolType(detail.getDefaultMinimums());
            populatePoolType(detail.getOverrideMinimums());
        }
    }

    public boolean isReadonly() {
        return readonly;
    }

    public boolean isDefaultPanel() {
        return defaultPanel;
    }
    
    public void setReadonly(boolean readonly) {
        this.readonly = readonly;
    }

    public List<PoolType> getDefaultPoolTypes() {
        List<PoolType> poolTypes = getPoolTypes();
        if (detail != null && detail.getDefaultMinimums() != null) {
            for (PoolParameter p : detail.getDefaultMinimums()) {
                poolTypes.remove(p.getPoolType());
            }
        }
        return poolTypes;
    }

    public List<PoolType> getOverPoolTypes() {
        List<PoolType> poolTypes = getPoolTypes();
        return poolTypes;
    }

    public List<CardConfig> getEntries() {
        List<CardConfig> items = poolTypeService.find(filter);
        if (items != null) {
            for (CardConfig c : items) {
                if (c.getPoolMinimum() != null) {
                    populatePoolType(c.getPoolMinimum().getDefaultMinimums());
                    populatePoolType(c.getPoolMinimum().getOverrideMinimums());
                }
            }
        }
        return items;
    }

    public static String validateAmount(ValidationContext ctx) {
        Map<String, Property> beanProps = ctx.getProperties(ctx.getProperty().getBase());
        PoolParameter pp = (PoolParameter) ctx.getProperty().getBase();
        if (pp != null) {
            BigDecimal incAmount = pp.getIncrementAmount();
            BigDecimal minAmount = (BigDecimal) beanProps.get("minAmount").getValue();
            if (minAmount == null) {
                return "Amounts cannot be empty.";
            }
            if (minAmount.compareTo(incAmount) < 0) {
                return "Increment Amount cannot be greater than minimum amount.";
            }
        }
        return null;
    }

    public static String validateAmounts(ValidationContext ctx, String name) {
        Map<String, Property> beanProps = ctx.getProperties(ctx.getProperty().getBase());
        PoolParameter pp = (PoolParameter) ctx.getProperty().getBase();
        if (pp != null) {
            BigDecimal minAmount = (BigDecimal) beanProps.get(name).getValue();
            if (minAmount == null) {
                return "Amounts cannot be empty.";
            }
            if (TOTE_LIMIT.compareTo(minAmount) < 0) {
                return "Amounts cannot be more than 320.";
            }
        }
        return null;
    }

    public static String validateRaceNumber(ValidationContext ctx, String name) {
        Map<String, Property> beanProps = ctx.getProperties(ctx.getProperty().getBase());
        PoolParameter pp = (PoolParameter) ctx.getProperty().getBase();
        if (pp != null) {
            String rn = (String) beanProps.get(name).getValue();
            if (rn == null) {
                return "Races cannot be empty.";
            }
            Set<Long> sets = ListHelper.parseList(rn);
            if (sets == null) {
                return "Invalid race numbers.";
            }
            for (Long l : sets) {
                if (l <= 0 || l > RACE_NUMBER_LIMIT) {
                    return "Race numbers cannot exceed " + RACE_NUMBER_LIMIT + ".";
                }
            }
        }
        return null;
    }

    public Validator getDefaultMinAmountValidator() {
        return new AbstractValidator() {
            public void validate(ValidationContext ctx) {
                String msg = validateAmounts(ctx, "minAmount");
                if (msg != null) {
                    this.addInvalidMessage(ctx, "defaultCdm", msg);
                }
            }
        };
    }

    public Validator getOverrideMinAmountValidator() {
        return new AbstractValidator() {
            public void validate(ValidationContext ctx) {
                String msg = validateAmounts(ctx, "minAmount");
                if (msg != null) {
                    this.addInvalidMessage(ctx, "overrideCdm", msg);
                }
            }
        };
    }

    public Validator getRaceNumberValidator() {
        return new AbstractValidator() {
            public void validate(ValidationContext ctx) {
                String msg = validateRaceNumber(ctx, "raceNumber");
                if (msg != null) {
                    this.addInvalidMessage(ctx, "overrideCdm", msg);
                }
            }
        };
    }

    public PoolMinimum getDetail() {
        return detail;
    }

    public void setDetail(PoolMinimum detail) {
        this.detail = detail;
    }

    public Validator getDefaultValidator() {
        return new AbstractValidator() {
            public void validate(ValidationContext ctx) {
                String msg = validateAmounts(ctx, "incrementAmount");
                if (msg != null) {
                    this.addInvalidMessage(ctx, "overrideCdm", msg);
                }
            }
        };
    }

    public Validator getOverrideValidator() {
        return new AbstractValidator() {
            public void validate(ValidationContext ctx) {
                String msg = validateAmounts(ctx, "incrementAmount");
                if (msg != null) {
                    this.addInvalidMessage(ctx, "overrideCdm", msg);
                }
            }
        };
    }

    @Command
    @NotifyChange({ "detail" })
    public void insertParameter(@BindingParam("type") String t) {
        if (detail != null) {
            PoolParameter pending = new PoolParameter();
            if (StringUtils.endsWithIgnoreCase(t, "D")) {
                detail.getDefaultMinimums().add(pending);
            } else {
                detail.getOverrideMinimums().add(pending);
            }
        }
    }

    @Command
    @NotifyChange({ "detail" })
    public void deleteParameter(@BindingParam("type") String t, @BindingParam("parameter") PoolParameter item) {
        if (detail != null) {
            if (StringUtils.equalsIgnoreCase(t, "D")) {
                detail.getDefaultMinimums().remove(item);
            } else {
                detail.getOverrideMinimums().remove(item);
            }
        }
    }

    private void populatePoolType(Set<PoolParameter> parameters) {
        if (parameters != null) {
            for (PoolParameter p : parameters) {
                if (p.getId() != null) {
                    p.setPoolType(PoolType.fromId(p.getId().intValue()));
                }
            }
        }
    }

}
