package com.sportech.cl.viewmodel;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Window;

import com.sportech.cl.model.database.CardConfig;
import com.sportech.cl.model.database.utils.CdmHelper;

public class CardTemplateViewModel extends BaseViewModel {

    private CardConfig detail;

    private CardConfig filter = new CardConfig();

    @Init
    public void init(@BindingParam("item") CardConfig item) {
        if (item != null) {
            detail = item;
        }
    }

    public List<CardConfig> getEntries() {
        List<CardConfig> items = poolTypeService.find(filter);
        return items;
    }

    public CardConfig getDetail() {
        return detail;
    }

    public void setDetail(CardConfig detail) {
        this.detail = detail;
    }

    @GlobalCommand
    @NotifyChange({ "entries" })
    public void refreshCardTemplates() {
    }

    @Command
    public void addItem() {
        detail = new CardConfig();
        detail.setName("Default");
        List<CardConfig> configs = poolTypeService.find(detail);
        if (configs != null && configs.size() > 0) {
            detail.setSettings(configs.get(0).getSettings());
        }
        detail.setId(null);
        detail.setName(null);
        showDialog(detail);
    }

    @Command
    public void editItem(@BindingParam("item") CardConfig item) {
        showDialog(item);
    }

    @Command
    public void deleteItem(@ContextParam(ContextType.VIEW) Window comp) {
        if (detail != null) {
            try {
                Boolean result = poolTypeService.delete(detail.getId());
                if (result != null && result) {
                    detail = null;
                } else {
                    Messagebox.show("Can not delete card configuration in use.");
                }
            } catch (Exception e) {
                Messagebox.show("Can not delete card configuration in use.", "Error Deleting Card Configuration", Messagebox.OK, Messagebox.ERROR);
            }
        }
        comp.detach();
    }

    @Command
    public void submit(@ContextParam(ContextType.VIEW) Window comp) {
        if (detail != null) {
            if (detail.getPoolMinimum() != null) {
                String message = CdmHelper.validate(detail.getPoolMinimum().getDefaultMinimums());
                if (message != null) {
                    Messagebox.show(message, "Error Saving Card Template", Messagebox.OK, Messagebox.ERROR);
                    return;
                }
                message = CdmHelper.validate(detail.getPoolMinimum().getOverrideMinimums());
                if (message != null) {
                    Messagebox.show(message, "Error Saving Card Template", Messagebox.OK, Messagebox.ERROR);
                    return;
                }
                message = CdmHelper.validateRaces(detail.getPoolMinimum().getDefaultMinimums(), detail.getPoolMinimum()
                        .getOverrideMinimums());
                if (message != null) {
                    Messagebox.show(message, "Saving Card Template Warning", Messagebox.OK, Messagebox.EXCLAMATION);
                }
                detail.refreshSettings();
                poolTypeService.save(detail);
            }
        }
        detail = null;
        comp.detach();
    }

    @Command
    public void closeDialog(@ContextParam(ContextType.VIEW) Window comp) {
        comp.detach();
    }

    private void showDialog(CardConfig c) {
        Map<String, Object> arg = new HashMap<String, Object>();
        arg.put("item", c);
        Window window = (Window) Executions.createComponents("cardtemplatedetail.zul", null, arg);
        window.doModal();
    }

}
