package com.sportech.cl.viewmodel;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.zkoss.bind.BindContext;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.SelectEvent;
import org.zkoss.zul.ListModel;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Window;

import com.sportech.cl.global.utils.config.ConfigLoader;
import com.sportech.cl.model.common.PageParams;
import com.sportech.cl.model.common.PoolMergeType;
import com.sportech.cl.model.database.HardCard;
import com.sportech.cl.model.database.HardPool;
import com.sportech.cl.model.database.Pattern;
import com.sportech.cl.model.database.PatternOutline;
import com.sportech.cl.model.database.PatternPool;
import com.sportech.cl.model.database.PatternTote;
import com.sportech.cl.model.database.Tote;
import com.sportech.cl.model.database.ToteOutline;
import com.sportech.cl.model.database.Track;
import com.sportech.cl.model.response.GenericResponse;
import com.sportech.cl.model.response.PatternListResponse;
import com.sportech.cl.model.response.PatternResponse;
import com.sportech.cl.model.response.ToteListResponse;
import com.sportech.cl.model.response.ToteResponse;
import com.sportech.cl.model.rule.RaceRuleHelper;
import com.sportech.cl.model.rule.RelationOperatorType;
import com.sportech.cl.model.rule.RuleCondition;
import com.sportech.cl.model.rule.RuleDefinitions;
import com.sportech.cl.model.rule.RuleField;
import com.sportech.cl.model.rule.RuleType;
import com.sportech.cl.model.security.SecurityToken;
import com.sportech.cl.services.dictionaries.TotesRemote;
import com.sportech.cl.services.dictionaries.TotesRemoteLocator;
import com.sportech.cl.services.patterns.PatternsRemote;
import com.sportech.cl.services.patterns.PatternsRemoteLocator;
import com.sportech.common.model.CardType;

public class PatternViewModel extends BaseViewModel {

    static private final Logger log = Logger.getLogger(PatternViewModel.class);

    private PatternsRemote patternService = PatternsRemoteLocator.getRemoteIface(ConfigLoader.getCfg());

    private TotesRemote toteService = TotesRemoteLocator.getRemoteIface(ConfigLoader.getCfg());

    private PatternOutline selectedPattern;

    private Pattern detail;

    private List<PatternOutline> patternList;

    private PatternFilter patternFilter = new PatternFilter();

    private static final String[] tabNames = new String[] { "Pattern", "Rules", "Totes" };

    private RelationOperatorType[] raceOperators = new RelationOperatorType[0];

    private String pickedTab = tabNames[0];

    private CardType trackType = null;

    private String trackCode = null;

    private Long trackId = null;

    private Integer mergeType = null;

    private Integer currentRaceFieldIndex;

    private Integer currentRaceOperatorIndex;

    private Integer currentActionIndex;

    private List<RuleCondition> currentRules = new ArrayList<RuleCondition>();

    private String ruleStringValue;

    private Date ruleTimeValue;

    private String ruleParams;

    @Init
    public void init(@BindingParam("pattern") PatternOutline p, @BindingParam("templateCard") HardCard templateCard,
            @BindingParam("trackCode") String trackCode, @BindingParam("tote") String toteId,
            @BindingParam("selectedTab") String tab, @BindingParam("patternId") Long patternId, @BindingParam("track") Track track) {
        if (!StringUtils.isEmpty(tab)) {
            this.pickedTab = tab;
        }
        if (p != null) {
            init(p);
        } else if (templateCard != null) {
            createPatternFromCard(templateCard);
        } else if (track != null) {
            createPatternFromTrack(track);
        } else if (!StringUtils.isEmpty(trackCode)) {
            initTrackCode(trackCode);
        } else if (!StringUtils.isEmpty(toteId)) {
            initToteId(toteId);
        } else if (patternId != null) {
            PatternOutline dummy = new PatternOutline();
            dummy.setId(patternId);
            init(dummy);
        }
    }

    private void initTrackCode(String trackCode) {
        if (!StringUtils.isBlank(trackCode) && StringUtils.isNumeric(trackCode)) {
            Long tid = Long.parseLong(trackCode);
            if (tid != null) {
                this.trackId = tid;
                patternFilter.setTrackId(tid);
                Track t = trackService.get(tid);
                if (t != null) {
                    this.trackCode = t.getEquibaseId();
                    this.trackType = t.getTrackType();
                    patternFilter.setTrackCode(this.trackCode);
                    patternFilter.setTrackType(this.trackType);
                }
            }
        }
    }

    private void initToteId(String toteId) {
        patternFilter.setToteId(toteId);
    }

    private void init(PatternOutline selected) {
        if (selected != null) {
            this.selectedPattern = selected;
            loadDetail();
        }
    }

    public Integer getCurrentRaceOperatorIndex() {
        return currentRaceOperatorIndex;
    }

    public void setCurrentRaceOperatorIndex(Integer currentRaceOperatorIndex) {
        this.currentRaceOperatorIndex = currentRaceOperatorIndex;
    }

    public List<RuleCondition> getCurrentRules() {
        if (currentRules == null || currentRules.isEmpty()) {
            if (detail != null) {
                currentRules = detail.getRaceRules();
            }
        }
        return currentRules;
    }

    public void setCurrentRules(List<RuleCondition> currentRules) {
        this.currentRules = currentRules;
    }

    public Integer getCurrentRaceFieldIndex() {
        return currentRaceFieldIndex;
    }

    public void setCurrentRaceFieldIndex(Integer currentRaceFieldIndex) {
        this.currentRaceFieldIndex = currentRaceFieldIndex;
    }

    public Integer getCurrentActionIndex() {
        return currentActionIndex;
    }

    public void setCurrentActionIndex(Integer currentActionIndex) {
        this.currentActionIndex = currentActionIndex;
    }

    public ListModel<PatternOutline> getPatterns() {
        getPatternList(patternFilter);
        return new ListModelList<PatternOutline>(patternList);
    }

    public String getRuleParams() {
        return ruleParams;
    }

    public void setRuleParams(String ruleParams) {
        this.ruleParams = ruleParams;
    }

    public List<PatternOutline> getPatternList(PatternFilter filter) {
        SecurityToken token = getToken();
        PatternOutline[] query = new PatternOutline[1];
        query[0] = new PatternOutline();
        if (!StringUtils.isBlank(filter.getDescription())) {
            query[0].setDescription(filter.getDescription() + "%");
        }
        if (filter.getTrackId() != null) {
            query[0].setTrackId(filter.getTrackId());
        }
        query[0].setRaceCount(filter.getRaceCount());
        query[0].setTrackEquibaseId(filter.getTrackCode());
        query[0].setLoadCode(filter.getLoadCode());
        PageParams pageParams = new PageParams();
        pageParams.setFirstResult(0);
        pageParams.setMaxResult(Integer.MAX_VALUE);
        PatternListResponse result = patternService.find(token, query, true, pageParams);
        if (result.getStatus() == 0) {
            patternList = result.getPatterns();
            if (!StringUtils.isEmpty(filter.getToteId())) {
                int toteId = Integer.parseInt(filter.getToteId());
                ArrayList<PatternOutline> totePatterns = new ArrayList<PatternOutline>();
                for (PatternOutline p : patternList) {
                    Pattern pattern = patternService.getPattern(token, p.getId()).getPattern();
                    boolean found = false;
                    if (pattern.getTotes() != null) {
                        for (PatternTote pt : pattern.getTotes()) {
                            if (pt.getToteId() == toteId) {
                                found = true;
                                break;
                            }
                        }
                        if (found) {
                            totePatterns.add(p);
                        }
                    }
                }
                patternList = totePatterns;
            }
        } else {
            patternList = new ArrayList<PatternOutline>();
        }
        return patternList;
    }

    public List<Tote> getTotes() {
        List<Tote> totes = new ArrayList<Tote>();
        if (detail != null) {
            SecurityToken token = getToken();
            Collection<PatternTote> pts = detail.getTotes();
            for (PatternTote tote : pts) {
                Integer toteId = tote.getToteId();
                ToteResponse tr = toteService.get(token, toteId.longValue());
                if (tr.getStatus() == 0) {
                    totes.add(tr.getTote());
                }
            }
        }
        return totes;
    }

    public List<ToteOutline> getAllTotes() {
        SecurityToken token = getToken();
        ToteOutline[] query = new ToteOutline[1];
        query[0] = new ToteOutline();
        PageParams pageParams = new PageParams();
        pageParams.setFirstResult(0);
        pageParams.setMaxResult(Integer.MAX_VALUE);
        ToteListResponse result = toteService.find(token, query, null, pageParams);
        if (result.getStatus() == 0) {
            return result.getTotes();
        } else {
            return new ArrayList<ToteOutline>();
        }
    }

    public PatternOutline getSelectedPattern() {
        return selectedPattern;
    }

    @NotifyChange({ "totes" })
    public void setSelectedPattern(PatternOutline selectedPattern) {
        this.selectedPattern = selectedPattern;
        loadDetail();
    }

    public Integer getMergeType() {
        return mergeType;
    }

    public void setMergeType(Integer mergeType) {
        this.mergeType = mergeType;
    }

    public void setPatternList(List<PatternOutline> patternList) {
        this.patternList = patternList;
    }

    public PatternFilter getPatternFilter() {
        return patternFilter;
    }

    public void setPatternFilter(PatternFilter patternFilter) {
        this.patternFilter = patternFilter;
    }

    public Pattern getDetail() {
        return detail;
    }

    public void setDetail(Pattern detail) {
        this.detail = detail;
        if (detail != null) {
            setMergeType(detail.getMergeType());
        }
    }

    public String getPickedTab() {
        return pickedTab;
    }

    public void setPickedTab(String pickedTab) {
        this.pickedTab = pickedTab;
    }

    public String[] getTabNames() {
        return tabNames;
    }

    public RuleField[] getRaceFields() {
        RuleField[] fields = RuleDefinitions.getFields();
        return fields;
    }

    public RelationOperatorType[] getRaceOperators() {
        return raceOperators;
    }

    public boolean isDisplayPools() {
        if (getMergeType() != null && getMergeType() == PoolMergeType.HARD_CARD_ONLY.getId()) {
            return false;
        }
        return true;
    }

    public String getRuleStringValue() {
        return ruleStringValue;
    }

    public void setRuleStringValue(String ruleStringValue) {
        this.ruleStringValue = ruleStringValue;
    }

    public Date getRuleTimeValue() {
        return ruleTimeValue;
    }

    public void setRuleTimeValue(Date ruleTimeValue) {
        this.ruleTimeValue = ruleTimeValue;
    }

    public String[] getActionNames() {
        return RuleDefinitions.getActionNames();
    }

    private void loadDetail() {
        if (selectedPattern != null) {
            long id = selectedPattern.getId();
            SecurityToken token = getToken();
            PatternResponse p = patternService.getPattern(token, id);
            if (p.getStatus() == 0) {
                setDetail(p.getPattern());
            }
        }
    }

    private void createPatternFromCard(HardCard c) {

        Pattern p = new Pattern();
        p.setTotes(new HashSet<PatternTote>());
        p.setDescription(c.getTrack().getName());
        p.setMergeType(PoolMergeType.HARD_CARD_ONLY.getId());
        p.setRaceCount(c.getRaces().size());
        Track track = trackService.get(c.getTrack().getId());
        p.setTrack(track);
        p.setTrackId(track.getId());
        Set<PatternPool> pools = new HashSet<PatternPool>();
        for (HardPool pool : c.getPools()) {
            PatternPool pp = new PatternPool();
            pp.setParent(p);
            pp.setPoolName(pool.getPoolName());
            pp.setPoolTypeId(pool.getPoolTypeId());
            pp.setRaceBitmap(pool.getRaceBitmap());
            pp.setPoolCode(pool.getPoolCode());
            pools.add(pp);
        }
        p.setPools(pools);
        setDetail(p);
    }

    private void createPatternFromTrack(Track track) {
        Pattern p = new Pattern();
        p.setTotes(new HashSet<PatternTote>());
        p.setDescription(track.getName());
        p.setMergeType(PoolMergeType.HARD_CARD_ONLY.getId());
        p.setRaceCount(0);
        p.setTrack(track);
        p.setTrackId(track.getId());
        Set<PatternPool> pools = new HashSet<PatternPool>();
        p.setPools(pools);
        setDetail(p);
    }

    @Command
    @NotifyChange({ "displayPools" })
    public void changeMergeType() {
        if (detail != null) {
            detail.setMergeType(mergeType);
        }
    }

    @Command
    @NotifyChange({ "patterns" })
    public void changeFilter() {
        patternList = getPatternList(patternFilter);
    }

    @Command
    public void editPattern(@BindingParam("pattern") PatternOutline c) {
        Map<String, Object> arg = new HashMap<String, Object>();
        arg.put("pattern", c);
        Window window = (Window) Executions.createComponents("patterndetail.zul", null, arg);
        window.doModal();
    }

    @Command
    public void editTotes(@BindingParam("pattern") PatternOutline c) {
        Map<String, Object> arg = new HashMap<String, Object>();
        arg.put("pattern", c);
        arg.put("selectedTab", "Totes");
        Window window = (Window) Executions.createComponents("patterndetail.zul", null, arg);
        window.doModal();
    }

    @Command
    @NotifyChange({ "patterns" })
    public void addPattern() {
        Map<String, Object> arg = new HashMap<String, Object>();
        arg.put("trackId", trackId);
        Window window = (Window) Executions.createComponents("addpattern.zul", null, arg);
        window.doModal();
    }

    @Command
    @NotifyChange("patterns")
    public void submit(@ContextParam(ContextType.VIEW) Window comp) {
        SecurityToken token = getToken();
        if (detail != null) {
            if (detail.getRaceCount() == null) {
                detail.setRaceCount(0);
            }
            if (detail.getTotes() == null || detail.getTotes().size() == 0) {
                String errorMessage = "Pattern does not have any totes.";
                Messagebox.show(errorMessage, "Save Pattern Error", Messagebox.OK, Messagebox.ERROR);
                return;
            }
            if (StringUtils.isBlank(detail.getLoadCode())) {
                String errorMessage = "Pattern load code can not be empty.";
                Messagebox.show(errorMessage, "Save Pattern Error", Messagebox.OK, Messagebox.ERROR);
                return;
            }
            GenericResponse response;
            if (detail.getId() == null) {
                response = patternService.addPattern(token, detail);
            } else {
                response = patternService.updatePattern(token, detail.getId(), detail);
            }
            if (response.getStatus() == 0) {
                comp.detach();
            } else {
                String errorMessage = StringUtils.join(response.getErrors(), " ");
                Messagebox.show(errorMessage, "Save Pattern Error", Messagebox.OK, Messagebox.ERROR);
            }
        }
    }

    @Command
    @NotifyChange("patterns")
    public void deletePattern(@ContextParam(ContextType.VIEW) Window comp) {
        SecurityToken token = getToken();
        if (detail != null) {
            GenericResponse response;
            log.info("Deleting pattern: " + detail.getId());
            response = patternService.deletePattern(token, detail.getId());
            if (response.getStatus() == 0) {
                comp.detach();
            } else {
                Messagebox.show("Can not delete: Load cards from this pattern exist.");
            }
        }
    }

    @NotifyChange("pickedTab")
    @Command
    public void select(@BindingParam("tab") String tab) {
        pickedTab = tab;
    }

    @GlobalCommand
    @NotifyChange("patterns")
    public void refreshPatterns() {
        getPatternList(patternFilter);
    }

    @Command
    @NotifyChange("raceOperators")
    public void refreshOperators(@ContextParam(ContextType.BIND_CONTEXT) BindContext ctx) {
        Event event = ctx.getTriggerEvent();
        @SuppressWarnings("unchecked")
        SelectEvent<Component, RuleField> se = (SelectEvent<Component, RuleField>) event;
        if (se != null) {
            Set<RuleField> s = se.getSelectedObjects();
            for (RuleField f : s) {
                raceOperators = RuleDefinitions.getOperators(f);
                break;
            }
        }
    }

    @Command
    @NotifyChange({ "editingRule", "currentRules" })
    public void addRaceRule() {
        if ((getCurrentRaceFieldIndex() == null) || (this.getCurrentRaceOperatorIndex() == null)
                || (StringUtils.isBlank(ruleStringValue) && ruleTimeValue == null)) {
            Messagebox.show("Rule can not be empty.");
            return;
        }
        RuleCondition editingRule = new RuleCondition();
        editingRule.setRuleType(RuleType.Race);
        editingRule.setField(getRaceFields()[this.getCurrentRaceFieldIndex()]);
        editingRule.setRelationOperator(raceOperators[this.getCurrentRaceOperatorIndex()]);
        editingRule.setValue(ruleStringValue);
        editingRule.setActionName(getActionNames()[this.getCurrentActionIndex()]);
        editingRule.setParams(ruleParams);
        if (ruleTimeValue != null) {
            SimpleDateFormat format = new SimpleDateFormat("HH:mm");
            String timeValue = format.format(ruleTimeValue);
            if (StringUtils.endsWithIgnoreCase("true", ruleStringValue)) {
                timeValue += " (Next Day)";
            }
            editingRule.setValue(timeValue);
        }
        currentRules.add(editingRule);
        if (detail != null) {
            detail.setRaceRules(currentRules);
        }
    }

    @Command
    @NotifyChange({ "currentRules" })
    public void deleteRaceRule(@BindingParam("racerule") RuleCondition rule) {
        for (RuleCondition r : currentRules) {
            if (RaceRuleHelper.equals(r, rule)) {
                currentRules.remove(r);
                break;
            }
        }
        if (detail != null) {
            detail.setRaceRules(currentRules);
        }
    }

}
