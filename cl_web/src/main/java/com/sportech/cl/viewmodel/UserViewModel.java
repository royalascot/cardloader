package com.sportech.cl.viewmodel;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.DependsOn;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zul.Messagebox;

import com.sportech.cl.global.utils.config.ConfigLoader;
import com.sportech.cl.model.common.PageParams;
import com.sportech.cl.model.database.Group;
import com.sportech.cl.model.database.User;
import com.sportech.cl.model.response.GenericResponse;
import com.sportech.cl.model.security.SecurityToken;
import com.sportech.cl.services.security.UsersRemote;
import com.sportech.cl.services.security.UsersRemoteLocator;

public class UserViewModel extends BaseViewModel {

	public class PasswordUser {
		String identifier;
		Long id;
		String password;
		String retypePassword;

		public String getIdentifier() {
			return identifier;
		}

		public void setIdentifier(String identifier) {
			this.identifier = identifier;
		}

		public Long getId() {
			return id;
		}

		public void setId(Long id) {
			this.id = id;
		}

		public String getPassword() {
			return password;
		}

		public void setPassword(String password) {
			this.password = password;
		}

		public String getRetypePassword() {
			return retypePassword;
		}

		public void setRetypePassword(String retypePassword) {
			this.retypePassword = retypePassword;
		}

	};

	private UsersRemote userService = UsersRemoteLocator.getRemoteIface(ConfigLoader.getCfg());

	private User detail = null;

	private PasswordUser passwordUser;

	private String retypedPassword = null;

	private String firstPassword = null;

	public String getRetypedPassword() {
		return retypedPassword;
	}

	public void setRetypedPassword(String retypedPassword) {
		this.retypedPassword = retypedPassword;
	}

	public String getFirstPassword() {
		return firstPassword;
	}

	public void setFirstPassword(String firstPassword) {
		this.firstPassword = firstPassword;
	}

	public PasswordUser getPasswordUser() {
		return passwordUser;
	}

	public void setPasswordUser(PasswordUser passwordUser) {
		this.passwordUser = passwordUser;
	}

	@DependsOn("detail")
	public String getUserGroupList() {
		if (detail != null && detail.getGroups() != null) {
			ArrayList<String> groups = new ArrayList<String>();
			for (Group g : detail.getGroups()) {
				groups.add(g.getName());
			}
			return StringUtils.join(groups, ",");
		}
		return "";
	}
	
	public User getDetail() {
		return detail;
	}

	public void setDetail(User detail) {
		this.detail = detail;
	}

	public List<User> getUsers() {
		return getFilteredUsers(null);
	}

	private List<User> getFilteredUsers(String name) {
		PageParams pageParams = new PageParams();
		pageParams.setFirstResult(0);
		pageParams.setMaxResult(Integer.MAX_VALUE);
		User[] query = new User[1];
		query[0] = new User();
		if (name != null) {
			query[0].setIdentifier(name);
		} else {
			query[0].setSalt("OPERATORS");
		}
		List<User> users = userService.find(getToken(), query, false, null);
		return users;
	}

	@Command
	@NotifyChange({ "users", "detail" })
	public void addUser() {
		User u = new User();
		u.setActive(true);
		u.setActorTypeId(2);
		u.setRoleId(1);
		u.setActorId(1);
		u.setSalt("OPERATORS");
		detail = u;
	}

	@Command
	@NotifyChange({ "passwordUser" })
	public void changePassword(@BindingParam("user") User p) {
		if (p != null) {
			passwordUser = new PasswordUser();
			passwordUser.setIdentifier(p.getIdentifier());
			passwordUser.setId(p.getId());
		}
	}

	@Command
	@NotifyChange({ "users", "detail" })
	public void viewUser(@BindingParam("user") User p) {
		if (p != null) {
			detail = userService.getUser(p.getId());
		}
	}

	@Command
	@NotifyChange({ "users", "detail" })
	public void submit() {
		SecurityToken token = getToken();
		if (detail != null) {
			List<User> existingUsers = getFilteredUsers(detail.getIdentifier());
			if (existingUsers != null && existingUsers.size() > 0) {
				if ((detail.getId() == null) || (detail.getId().longValue() != existingUsers.get(0).getId().longValue())) {
					Messagebox.show("Login name exists. Please use another login name.", "Error Saving User", Messagebox.OK,
					        Messagebox.ERROR);
					return;
				}
			}

			GenericResponse response;
			if (detail.getId() == null) {
				response = userService.add(token, detail);
			} else {
				response = userService.update(token, detail.getId(), detail);
			}
			if (response.getStatus() == 0) {
				if (!StringUtils.isEmpty(firstPassword)) {
					if (!StringUtils.equals(firstPassword, retypedPassword)) {
						Messagebox.show("Your passwords do not match.", "Validation Error", Messagebox.OK, Messagebox.ERROR);
						return;
					}
					response = userService.setUserPassword(token, detail.getIdentifier(), firstPassword);
				}
			}
			if (response.getStatus() == 0) {
				detail = null;
			} else {
				String errorMessage = StringUtils.join(response.getErrors(), " ");
				Messagebox.show(errorMessage, "Error Saving User", Messagebox.OK, Messagebox.ERROR);
			}
		}
	}

	@Command
	@NotifyChange({ "users", "detail" })
	public void deleteUser() {
		SecurityToken token = getToken();
		if (detail != null) {
			GenericResponse response;
			response = userService.delete(token, detail.getId());
			if (response.getStatus() == 0) {
				detail = null;
			} else {
				Messagebox.show("Can not delete user in use.", "Error Deleting User", Messagebox.OK, Messagebox.ERROR);
			}
		}
	}

	@Command
	@NotifyChange({ "users", "passwordUser" })
	public void submitPassword() {
		if (passwordUser != null) {
			if (!StringUtils.equals(passwordUser.getPassword(), passwordUser.getRetypePassword())) {
				Messagebox.show("Passwords don't match.", "Error Changing User Password", Messagebox.OK, Messagebox.ERROR);
				return;
			}
			GenericResponse response;
			response = userService.setUserPassword(getToken(), passwordUser.getIdentifier(), passwordUser.getPassword());
			if (response.getStatus() == 0) {
				passwordUser = null;
			} else {
				String errorMessage = StringUtils.join(response.getErrors(), " ");
				Messagebox.show(errorMessage, "Error Changing User Password", Messagebox.OK, Messagebox.ERROR);
			}
		}
	}

	@Command
	@NotifyChange({ "passwordUser" })
	public void closePasswordDialog() {
		passwordUser = null;
	}

	@Command
	@NotifyChange({ "detail" })
	public void closeDialog() {
		detail = null;
	}

}
