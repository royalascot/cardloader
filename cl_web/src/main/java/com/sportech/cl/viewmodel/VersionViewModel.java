package com.sportech.cl.viewmodel;

import java.util.ArrayList;
import java.util.List;

import com.sportech.cl.global.utils.config.ConfigLoader;
import com.sportech.cl.model.common.CardLoaderVersion;
import com.sportech.cl.model.common.Version;
import com.sportech.cl.model.response.VersionResponse;
import com.sportech.cl.services.common.VersionsInterface;
import com.sportech.cl.services.common.VersionsRemoteLocator;

public class VersionViewModel {

	private VersionsInterface worker = VersionsRemoteLocator.getRemoteIface(ConfigLoader.getCfg());

	public List<Version> getVersions() {
		VersionResponse rsp = worker.getVersions();
		CardLoaderVersion v = null;
		if (rsp.isOK()) {
			v = rsp.getVersion();
			return v.getComponents();
		}

		return new ArrayList<Version>();
	}

}
