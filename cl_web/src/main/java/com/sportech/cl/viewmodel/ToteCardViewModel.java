package com.sportech.cl.viewmodel;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.DependsOn;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zul.ListModel;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Window;

import com.sportech.cl.global.utils.config.ConfigLoader;
import com.sportech.cl.model.common.PageParams;
import com.sportech.cl.model.database.SoftCard;
import com.sportech.cl.model.database.SoftCardOutline;
import com.sportech.cl.model.database.ToteOutline;
import com.sportech.cl.model.response.GenericResponse;
import com.sportech.cl.model.response.SoftCardListResponse;
import com.sportech.cl.model.response.SoftCardResponse;
import com.sportech.cl.model.response.ToteListResponse;
import com.sportech.cl.model.security.SecurityToken;
import com.sportech.cl.services.cards.SoftCardsRemote;
import com.sportech.cl.services.cards.SoftCardsRemoteLocator;
import com.sportech.cl.services.dictionaries.TotesRemote;
import com.sportech.cl.services.dictionaries.TotesRemoteLocator;

public class ToteCardViewModel extends BaseViewModel {

	private SoftCardsRemote softCardService = SoftCardsRemoteLocator.getRemoteIface(ConfigLoader.getCfg());

	private TotesRemote toteService = TotesRemoteLocator.getRemoteIface(ConfigLoader.getCfg());
	
	private SoftCardFilter cardFilter = new SoftCardFilter();

	private List<SoftCardOutline> selectedCards;

	private List<SoftCardOutline> softCards = new ArrayList<SoftCardOutline>();

	private SoftCard detail;

	private ToteOutline tote;
	
	private List<ToteOutline> toteList;

	@Init
	public void init(@BindingParam("tote") ToteOutline tote) {
		this.tote = tote;
	}

	public List<ToteOutline> getToteList(ToteFilter filter) {
		SecurityToken token = getToken();
		ToteOutline[] query = new ToteOutline[1];
		query[0] = new ToteOutline();
		PageParams pageParams = new PageParams();
		pageParams.setFirstResult(0);
		pageParams.setMaxResult(Integer.MAX_VALUE);

		ToteListResponse result = toteService.find(token, query, null, pageParams);
		
		if (result.getStatus() == 0) {
			toteList = result.getTotes();
		} else {
			toteList = new ArrayList<ToteOutline>();
		}
		return toteList;
	}

	public List<ToteOutline> getTotes() {
		toteList = getToteList(null);
		List<ToteOutline> totes = new ArrayList<ToteOutline>();
		for (ToteOutline t : toteList) {
			if (t.getFlag() != null && t.getFlag()) {
				totes.add(t);
			}
		}
		if (totes.size() > 0) {
			tote = totes.get(0);
		}
		return totes;
	}
	
	public ToteOutline getTote() {
		return tote;
	}

	public void setTote(ToteOutline tote) {
		this.tote = tote;
	}

	public ListModel<SoftCardOutline> getCards() {
		softCards = new ArrayList<SoftCardOutline>();
		List<SoftCardOutline> cards = getFilterCards(cardFilter);
		if (tote != null) {
			for (SoftCardOutline s : cards) {
				if (StringUtils.contains(s.getToteName(), tote.getToteCode())) {
					if (StringUtils.contains(s.getToteApprovalList(), tote.getToteCode())) {
						s.setToteApproved(true);
					}
					softCards.add(s);
				}
			}
		} else {
			softCards.addAll(cards);
		}
		ListModelList<SoftCardOutline> cardsList = new ListModelList<SoftCardOutline>(softCards);
		cardsList.setMultiple(true);
		return cardsList;
	}

	public List<SoftCardOutline> getFilterCards(SoftCardFilter filter) {
		SecurityToken token = getToken();
		SoftCardOutline[] query = new SoftCardOutline[1];
		query[0] = new SoftCardOutline();
		PageParams pageParams = new PageParams();
		pageParams.setFirstResult(0);
		pageParams.setMaxResult(Integer.MAX_VALUE);

		Date startDate = getWorkingDate();
		Date endDate = addDate(startDate, 0);
		if (filter.getCardDate() != null) {
			startDate = filter.getCardDate();
			endDate = filter.getCardDate();
		}

		if (!StringUtils.isBlank(filter.getDescription())) {
			query[0].setDescription(filter.getDescription() + "%");
		}

		query[0].setApproved(true);

		if (!StringUtils.isBlank(cardFilter.getLoadCode())) {
			query[0].setLoadCode(cardFilter.getLoadCode());
		}

		if (tote != null) {
			query[0].setToteName("%" + tote.getToteCode() + "%");
		}
		else {
			query[0].setToteName("EMPTY");
		}
		SoftCardListResponse result = softCardService.find(token, query, startDate, endDate, true, pageParams);
		if (result.getStatus() == 0) {
			return result.getCards();
		} else {
			return new ArrayList<SoftCardOutline>();
		}
	}

	public SoftCard getSingleCard(SoftCardOutline hco) {
		if (hco != null) {
			long id = hco.getId();
			SecurityToken token = getToken();
			SoftCardResponse hcr = softCardService.get(token, id);
			if (hcr.getStatus() == 0) {
				return hcr.getCard();
			}
		}
		return null;
	}

	public SoftCardFilter getCardFilter() {
		return cardFilter;
	}

	public void setCardFilter(SoftCardFilter cardFilter) {
		this.cardFilter = cardFilter;
	}

	public List<SoftCardOutline> getSelectedCards() {
		return selectedCards;
	}

	public void setSelectedCards(List<SoftCardOutline> selectedCards) {
		this.selectedCards = selectedCards;
	}

	public SoftCard getDetail() {
		return detail;
	}

	public void setDetail(SoftCard detail) {
		this.detail = detail;
	}

	@Command
	@NotifyChange({ "cards" })
	public void changeFilter() {
	}

	@Command
	public void editCard(@BindingParam("card") SoftCardOutline c) {
		Map<String, Object> arg = new HashMap<String, Object>();
		arg.put("selectedCard", c);
		Window window = (Window) Executions.createComponents("loadcarddetail.zul", null, arg);
		window.doModal();
	}

	@DependsOn("selectedCards")
	public boolean isPendingLoadCardButtonDisabled() {
		return (selectedCards == null || selectedCards.size() == 0);
	}

	@DependsOn("selectedCards")
	public String getPendingLoadCardButtonTitle() {
		if (isPendingLoadCardButtonDisabled()) {
			return "Approve Selected";
		} else {
			int size = selectedCards.size();
			return "Approve " + size + " Selected";
		}
	}

	@Command
	@NotifyChange({ "cards", "selectedCards" })
	public void approveCards() {
		if (selectedCards != null) {
			SecurityToken token = getToken();
			for (SoftCardOutline h : selectedCards) {
				SoftCard sc = getSingleCard(h);
				String approved = sc.getToteApprovalList();
				if (approved == null) {
					approved = tote.getToteCode();
				} else if (!StringUtils.contains(approved, tote.getToteCode())) {
					approved = approved + "," + tote.getToteCode();
				}
				sc.setToteApprovalList(approved);
				softCardService.update(token, sc.getId(), sc);
			}
			Messagebox.show("Successfully approved " + selectedCards.size() + " load cards for tote " + tote.getName() + ".",
			        "Approving Load Cards for Tote", Messagebox.OK, Messagebox.INFORMATION);
			selectedCards = null;
		}
	}

	@Command
	public void closeCardEditWindow(@ContextParam(ContextType.VIEW) Window comp) {
		comp.detach();
	}

	@Command
	@NotifyChange({ "cards", "selectedCards" })
	public void approveLoadCard(@ContextParam(ContextType.VIEW) Window comp) {
		if (detail != null) {
			SecurityToken token = getToken();
			GenericResponse response = softCardService.approve(token, detail.getId(), true);
			if (response.getStatus() == 0) {
				comp.detach();
			} else {
				Messagebox.show("Failed to approve load card.");
			}
		}
	}

	@Command
	public void viewPattern(@BindingParam("patternId") Long patternId) {
		Map<String, Object> arg = new HashMap<String, Object>();
		arg.put("patternId", patternId);
		Window window = (Window) Executions.createComponents("patterndetail.zul", null, arg);
		window.doModal();
	}

	@Command
	@NotifyChange({"tote", "cards"})
	public void refreshSelectedTote() {
		
	}
}
