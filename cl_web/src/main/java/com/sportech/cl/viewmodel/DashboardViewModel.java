package com.sportech.cl.viewmodel;

import java.io.ByteArrayOutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.DependsOn;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zhtml.Filedownload;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zul.ListModel;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Window;

import com.sportech.cl.global.utils.config.ConfigLoader;
import com.sportech.cl.model.common.PageParams;
import com.sportech.cl.model.database.HardCard;
import com.sportech.cl.model.database.HardCardOutline;
import com.sportech.cl.model.database.SoftCardOutline;
import com.sportech.cl.model.response.GenericResponse;
import com.sportech.cl.model.response.HardCardListResponse;
import com.sportech.cl.model.response.HardCardResponse;
import com.sportech.cl.model.response.SoftCardListResponse;
import com.sportech.cl.model.security.SecurityToken;
import com.sportech.cl.services.cards.HardCardsRemote;
import com.sportech.cl.services.cards.HardCardsRemoteLocator;
import com.sportech.cl.services.cards.SoftCardsRemote;
import com.sportech.cl.services.cards.SoftCardsRemoteLocator;
import com.sportech.cl.utils.exporter.BetfredExporter;
import com.sportech.common.util.DateHelper;

public class DashboardViewModel extends BaseViewModel {

	private HardCardsRemote hardCardService = HardCardsRemoteLocator.getRemoteIface(ConfigLoader.getCfg());
	private SoftCardsRemote softCardService = SoftCardsRemoteLocator.getRemoteIface(ConfigLoader.getCfg());

	private List<HardCardOutline> hardCards = new ArrayList<HardCardOutline>();
	private List<HardCardOutline> approvedHardCards = null;
	private List<SoftCardOutline> softCards = new ArrayList<SoftCardOutline>();
	private List<SoftCardOutline> publishedSoftCards = new ArrayList<SoftCardOutline>();

	private Set<HardCardOutline> selectedHardCards = null;
	private Set<SoftCardOutline> selectedSoftCards = null;
	private Set<HardCardOutline> selectedApprovedHardCards = null;
	private Set<SoftCardOutline> selectedEnabledLoadCards = null;

	@Init
	public void init() {
	}

	public boolean isShowHostCardTabs() {
		return isUserInRole("Host Card Viewer,Host Card Editor");
	}

	public boolean isSelectHostCardTabs() {
		return isShowHostCardTabs();
	}

	public boolean isShowLoadCardTabs() {
		return isUserInRole("Load Card Viewer,Load Card Editor");
	}

	public boolean isSelectLoadCardTabs() {
		return (!isShowHostCardTabs()) && isShowLoadCardTabs();
	}

	public ListModel<HardCardOutline> getHardCards() {
		getPendingHardCards();
		ListModelList<HardCardOutline> model = new ListModelList<HardCardOutline>(hardCards);
		model.setMultiple(true);
		return model;
	}

	public ListModel<HardCardOutline> getApprovedHardCards() {
		loadApprovedHardCards();
		ListModelList<HardCardOutline> model = new ListModelList<HardCardOutline>(approvedHardCards);
		model.setMultiple(true);
		return model;
	}

	public ListModel<SoftCardOutline> getSoftCards() {
		getPendingSoftCards();
		ListModelList<SoftCardOutline> model = new ListModelList<SoftCardOutline>(softCards);
		model.setMultiple(true);
		return model;
	}

	public List<HardCardOutline> getPendingHardCards() {
		SecurityToken token = getToken();
		HardCardOutline[] query = new HardCardOutline[1];
		query[0] = new HardCardOutline();
		query[0].setApproved(false);
		PageParams pageParams = new PageParams();
		pageParams.setFirstResult(0);
		pageParams.setMaxResult(Integer.MAX_VALUE);

		Date startDate = getWorkingDate();
		Date endDate = addDate(startDate, 0);
		HardCardListResponse result = hardCardService.find(token, query, startDate, endDate, true, pageParams);
		if (result.getStatus() == 0) {
			hardCards = result.getCards();
		} else {
			hardCards = new ArrayList<HardCardOutline>();
		}
		return hardCards;
	}

	public ListModel<HardCardOutline> getAllHardCards() {
		SecurityToken token = getToken();
		HardCardOutline[] query = new HardCardOutline[1];
		query[0] = new HardCardOutline();
		PageParams pageParams = new PageParams();
		pageParams.setFirstResult(0);
		pageParams.setMaxResult(Integer.MAX_VALUE);

		Date startDate = getWorkingDate();
		Date endDate = addDate(startDate, 0);
		HardCardListResponse result = hardCardService.find(token, query, startDate, endDate, true, pageParams);
		if (result.getStatus() == 0) {
			hardCards = result.getCards();
		} else {
			hardCards = new ArrayList<HardCardOutline>();
		}
		ListModelList<HardCardOutline> model = new ListModelList<HardCardOutline>(hardCards);
		model.setMultiple(true);
		return model;

	}

	public Set<SoftCardOutline> getSelectedEnabledLoadCards() {
		return selectedEnabledLoadCards;
	}

	public void setSelectedEnabledLoadCards(Set<SoftCardOutline> selectedEnabledLoadCards) {
		this.selectedEnabledLoadCards = selectedEnabledLoadCards;
	}

	public List<SoftCardOutline> getPendingSoftCards() {
		softCards = getSoftCards(false);
		return softCards;
	}

	public List<SoftCardOutline> getPublishedSoftCards() {
		publishedSoftCards = getSoftCards(true);
		return publishedSoftCards;
	}

	public Set<HardCardOutline> getSelectedHardCards() {
		return selectedHardCards;
	}

	public void setSelectedHardCards(Set<HardCardOutline> selectedHardCards) {
		this.selectedHardCards = selectedHardCards;
	}

	public Set<SoftCardOutline> getSelectedSoftCards() {
		return selectedSoftCards;
	}

	public void setSelectedSoftCards(Set<SoftCardOutline> selectedSoftCards) {
		this.selectedSoftCards = selectedSoftCards;
	}

	public Set<HardCardOutline> getSelectedApprovedHardCards() {
		return selectedApprovedHardCards;
	}

	public void setSelectedApprovedHardCards(Set<HardCardOutline> selectedApprovedHardCards) {
		this.selectedApprovedHardCards = selectedApprovedHardCards;
	}

	@DependsOn("selectedHardCards")
	public boolean isPendingHostCardButtonDisabled() {
		return (selectedHardCards == null || selectedHardCards.size() == 0);
	}

	@DependsOn("selectedHardCards")
	public String getPendingHostCardButtonTitle() {
		if (isPendingHostCardButtonDisabled()) {
			return "Mark Selected Verified";
		} else {
			int size = selectedHardCards.size();
			return "Mark " + size + " Selected Verified";
		}
	}

	@DependsOn("selectedApprovedHardCards")
	public boolean isApprovedHostCardButtonDisabled() {
		return (selectedApprovedHardCards == null || selectedApprovedHardCards.size() == 0);
	}

	@DependsOn("selectedApprovedHardCards")
	public String getApprovedHostCardButtonTitle() {
		if (isApprovedHostCardButtonDisabled()) {
			return "Create Load Cards from Selected";
		} else {
			int size = selectedApprovedHardCards.size();
			return "Create Load Cards from " + size + " Selected";
		}
	}

	@DependsOn("selectedSoftCards")
	public boolean isPendingLoadCardButtonDisabled() {
		return (selectedSoftCards == null || selectedSoftCards.size() == 0);
	}

	@DependsOn("selectedEnabledLoadCards")
	public boolean isDisableLoadCardButtonDisabled() {
		return (selectedEnabledLoadCards == null || selectedEnabledLoadCards.size() == 0);
	}

	@DependsOn("selectedSoftCards")
	public String getPendingLoadCardButtonTitle() {
		if (isPendingLoadCardButtonDisabled()) {
			return "Activate Selected";
		} else {
			int size = selectedSoftCards.size();
			return "Activate " + size + " Selected";
		}
	}

	@Command
	public void editHardCard(@BindingParam("hardCard") HardCardOutline c) {
		Map<String, Object> arg = new HashMap<String, Object>();
		arg.put("selectedCard", c);
		Window window = (Window) Executions.createComponents("hostcarddetail.zul", null, arg);
		window.doModal();
	}

	@Command
	public void editSoftCard(@BindingParam("softCard") SoftCardOutline c) {
		Map<String, Object> arg = new HashMap<String, Object>();
		arg.put("selectedCard", c);
		Window window = (Window) Executions.createComponents("loadcarddetail.zul", null, arg);
		window.doModal();
	}

	@Command
	@NotifyChange({ "approvedHardCards", "hardCards", "softCards", "selectedHardCards" })
	public void approveHardCards() {
		if (selectedHardCards != null) {
			SecurityToken token = getToken();
			int success = 0;
			int failure = 0;
			for (HardCardOutline h : selectedHardCards) {
				Long id = h.getId();
				GenericResponse response = hardCardService.approve(token, id, true);
				if (response.getStatus() == 0) {
					success++;
				} else {
					failure++;
				}
			}
			String message = "";
			if (success > 0) {
				message += "Successfully marked " + success + " out of " + selectedHardCards.size() + " host card(s) 'verified'. ";
			}
			if (failure > 0) {
				message += "Could not verify " + failure + " invalid card(s). Please check card's CDM details.";
			}
			Messagebox.show(message, "Verifying Host Cards", Messagebox.OK, Messagebox.INFORMATION);
			selectedHardCards = null;
		}
	}

	@Command
	@NotifyChange({ "softCards", "publishedSoftCards", "selectedSoftCards" })
	public void approveSoftCards() {
		if (selectedSoftCards != null) {
			SecurityToken token = getToken();
			for (SoftCardOutline h : selectedSoftCards) {
				Long id = h.getId();
				softCardService.approve(token, id, true);
			}
			Messagebox.show("Successfully activated " + selectedSoftCards.size() + " load card(s).", "Activating Load Cards", Messagebox.OK, Messagebox.INFORMATION);
			selectedSoftCards = null;
		}
	}

	@Command
	@NotifyChange({ "softCards", "publishedSoftCards", "selectedEnabledLoadCards" })
	public void disableLoadCards() {
		if (selectedEnabledLoadCards != null) {
			SecurityToken token = getToken();
			for (SoftCardOutline h : selectedEnabledLoadCards) {
				Long id = h.getId();
				softCardService.approve(token, id, false);
			}
			Messagebox.show("Successfully deactivated " + selectedEnabledLoadCards.size() + " load card(s).", "Deactivating Load Cards", Messagebox.OK, Messagebox.INFORMATION);
			selectedEnabledLoadCards = null;
		}
	}

	@Command
	public void createLoadCards(@ContextParam(ContextType.VIEW) Window comp, @BindingParam("card") HardCardOutline c) {
		Map<String, Object> arg = new HashMap<String, Object>();
		arg.put("selectedCard", c);
		Window window = (Window) Executions.createComponents("createloadcards.zul", null, arg);
		window.doModal();
	}

	@Command
	@NotifyChange({ "approvedHardCards", "softCards", "selectedApprovedHardCards" })
	public void batchCreateLoadCards() {
		if (selectedApprovedHardCards != null) {
			int count = 0;
			for (HardCardOutline h : selectedApprovedHardCards) {
				count += hardCardService.generateSoftCards(h.getId());
			}
			if (count > 0) {
				Messagebox.show("Successfully created " + count + " load card(s).", "Creating Load Cards", Messagebox.OK, Messagebox.INFORMATION);
				selectedApprovedHardCards = null;
			}
		}
	}

	@Command
	public void viewPatterns(@BindingParam("card") HardCardOutline c) {
		String url = "patterns.zul?trackcode=" + c.getTrackId();
		Executions.getCurrent().sendRedirect(url);
	}

	@Command
	public void closeCardEditWindow(@ContextParam(ContextType.VIEW) Window comp) {
		comp.detach();
	}

	@Command
	@NotifyChange({ "publishedSoftCards" })
	public void reloadSoftCards() {
	}

	@Command
	public void downloadRif() {
		Date d = getWorkingDate();
		SimpleDateFormat sdf = new SimpleDateFormat("MMdd");
		String fileName = sdf.format(d) + ".rif";
		String rif = hardCardService.generateRif(d);
		if (rif != null) {
			Filedownload.save(rif, "text/html", fileName);
		} else {
			Messagebox.show("Error generating RIF file.");
		}
	}

	@GlobalCommand
	@NotifyChange({ "approvedHardCards", "hardCards", "softCards", "publishedSoftCards" })
	public void refreshDashboard() {
	}

	@GlobalCommand
	@NotifyChange({ "approvedHardCards", "hardCards" })
	public void refreshHostCard() {
	}

	@Command
	@NotifyChange({ "selectedHardCards" })
	public void exportRmsCards() {
		if (selectedHardCards != null) {
			SecurityToken token = getToken();
			List<HardCard> cards = new ArrayList<HardCard>();
			for (HardCardOutline h : selectedHardCards) {
				Long id = h.getId();
				HardCardResponse hcp = hardCardService.get(token, id);
				cards.add(hcp.getCard());
			}
			selectedHardCards = null;
			if (cards.size() > 0) {
				Date d = getWorkingDate();
				String fileName = DateHelper.dateToStr(d) + "card.xml";
				ByteArrayOutputStream bos = new ByteArrayOutputStream();
				BetfredExporter exporter = new BetfredExporter();
				exporter.export(bos, cards);
				Filedownload.save(bos.toByteArray(), "text/xml", fileName);
			}
		}
	}

	private List<HardCardOutline> loadApprovedHardCards() {
		SecurityToken token = getToken();
		HardCardOutline[] query = new HardCardOutline[1];
		query[0] = new HardCardOutline();
		query[0].setApproved(true);
		PageParams pageParams = new PageParams();
		pageParams.setFirstResult(0);
		pageParams.setMaxResult(Integer.MAX_VALUE);

		Date startDate = getWorkingDate();
		Date endDate = addDate(startDate, 0);
		HardCardListResponse result = hardCardService.find(token, query, startDate, endDate, true, pageParams);
		approvedHardCards = new ArrayList<HardCardOutline>();
		if (result.getStatus() == 0) {
			for (HardCardOutline hc : result.getCards()) {
				if (hc.getSoftCards() == null || hc.getSoftCards().size() == 0) {
					approvedHardCards.add(hc);
				}
			}
		}
		return approvedHardCards;
	}

	private List<SoftCardOutline> getSoftCards(boolean approved) {
		List<SoftCardOutline> softCards = null;
		SecurityToken token = getToken();
		SoftCardOutline[] query = new SoftCardOutline[1];
		query[0] = new SoftCardOutline();
		query[0].setApproved(approved);
		PageParams pageParams = new PageParams();
		pageParams.setFirstResult(0);
		pageParams.setMaxResult(Integer.MAX_VALUE);

		Date startDate = getWorkingDate();
		Date endDate = addDate(startDate, 0);
		SoftCardListResponse result = softCardService.find(token, query, startDate, endDate, true, pageParams);
		if (result.getStatus() == 0) {
			softCards = result.getCards();
		} else {
			softCards = new ArrayList<SoftCardOutline>();
		}
		return softCards;
	}

}
