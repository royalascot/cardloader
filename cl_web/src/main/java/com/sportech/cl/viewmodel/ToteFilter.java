package com.sportech.cl.viewmodel;

import java.util.Set;

public class ToteFilter {
	
	private Set<Integer> toteIds;
	
	private String toteName;
	
	private String toteCode;
	
	private String toteLogin;

	public String getToteName() {
		return toteName;
	}

	public void setToteName(String toteName) {
		this.toteName = toteName;
	}

	public String getToteCode() {
		return toteCode;
	}

	public void setToteCode(String toteCode) {
		this.toteCode = toteCode;
	}

	public String getToteLogin() {
		return toteLogin;
	}

	public void setToteLogin(String toteLogin) {
		this.toteLogin = toteLogin;
	}

	public Set<Integer> getToteIds() {
		return toteIds;
	}
	
	public void setToteIds(Set<Integer> toteIds) {
		this.toteIds = toteIds;
	}
	
}
