package com.sportech.cl.viewmodel;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.Init;
import org.zkoss.zul.Window;

import com.sportech.common.model.PoolType;

public class PoolTypeViewModel extends BaseViewModel {

	private PoolViewModel poolViewModel;
	
	private PoolType selectedPoolType;

	private List<PoolType> selectedPoolTypes;
	
	private Boolean addRaces = true;
	
	@Init
	public void init(@BindingParam("pool") PoolViewModel pool) {
		poolViewModel = pool;
	}
 
	public List<PoolType> getAvailablePoolTypes() {
		List<PoolType> pools = getPoolTypes();
		List<PoolType> availablePools = new ArrayList<PoolType>();
		Set<String> currentNames = new HashSet<String>();
		currentNames.addAll(poolViewModel.getPoolNames());
		for (PoolType pt : pools) {
			if (!currentNames.contains(pt.getName())) {
				availablePools.add(pt);
			}
		}
		return availablePools;
	}

	public PoolType getSelectedPoolType() {
		return selectedPoolType;
	}

	public void setSelectedPoolType(PoolType selectedPoolType) {
		this.selectedPoolType = selectedPoolType;
	}

	public Boolean getAddRaces() {
		return addRaces;
	}

	public void setAddRaces(Boolean addRaces) {
		this.addRaces = addRaces;
	}

	public List<PoolType> getSelectedPoolTypes() {
		return selectedPoolTypes;
	}

	public void setSelectedPoolTypes(List<PoolType> selectedPoolTypes) {
		this.selectedPoolTypes = selectedPoolTypes;
	}

	@Command
	public void selectPoolType(@ContextParam(ContextType.VIEW) Window comp) {
		comp.detach();
	}

}
