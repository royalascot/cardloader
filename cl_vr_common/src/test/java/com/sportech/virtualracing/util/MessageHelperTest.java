package com.sportech.virtualracing.util;

import org.apache.commons.lang3.StringUtils;
import org.junit.Assert;
import org.junit.Test;

public class MessageHelperTest {

	@Test
	public void testSpanish() {
		String race = LocaleHelper.getCaption("race");
		Assert.assertTrue(StringUtils.equals(race,  "Carrera"));
	}
	
}
