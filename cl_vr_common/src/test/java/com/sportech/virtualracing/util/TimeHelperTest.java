package com.sportech.virtualracing.util;

import java.util.TimeZone;

import org.junit.Assert;
import org.junit.Test;

public class TimeHelperTest {

    @Test
    public void testDST() {
        long t = 1433516389026L;
        TimeZone tz = TimeZone.getTimeZone("America/New_York");
        int offset = tz.getOffset(t);
        Assert.assertTrue(offset == 3600000 * -4);
    }
    
}
