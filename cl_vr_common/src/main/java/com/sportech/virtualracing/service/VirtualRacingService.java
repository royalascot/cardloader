package com.sportech.virtualracing.service;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

public interface VirtualRacingService {
	@GET
	@Path("/VseGameServer/DataService/Event/{id}")
	@Produces("text/plain")
	public String getEvent(@PathParam("id") String id);

	@GET
	@Path("/VseGameServer/DataService/Result/{id}")
	@Produces("text/plain")
	public String getResult(@PathParam("id") String id);

	@GET
	@Path("/VseGameServer/DataService/UpcomingEvents")
	@Produces("text/plain")
	public String queryEvents();
}
