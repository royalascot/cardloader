/*
 * jaw 2014-20-20 modified to use with kironTrackId
 * 
 */
package com.sportech.virtualracing.service;

import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.TimeZone;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.stream.StreamSource;

import org.apache.commons.lang3.StringUtils;
import org.apache.http.client.utils.URIUtils;
import org.apache.log4j.Logger;
import org.jboss.resteasy.client.ClientRequest;

import com.sportech.cl.model.database.HardCard;
import com.sportech.cl.model.database.HardRace;
import com.sportech.cl.model.database.Runner;
import com.sportech.cl.model.database.Track;
import com.sportech.cl.model.database.utils.DateUtils;
import com.sportech.common.model.CardType;
import com.sportech.common.model.PerformanceType;
import com.sportech.common.util.PropertyHelper;
import com.sportech.virtualracing.dao.GeneratorDao;
import com.sportech.virtualracing.dao.VirtualTrackDao;
import com.sportech.virtualracing.entity.CardConfiguration;
import com.sportech.virtualracing.entity.RequestLog;
import com.sportech.virtualracing.entity.VirtualTrack;
import com.sportech.virtualracing.messaging.MessageHelper;
import com.sportech.virtualracing.model.schema.CreateEvent.CreateEventType;
import com.sportech.virtualracing.model.schema.CreateEvent.EntryType;
import com.sportech.virtualracing.model.schema.CreateEvent.RaceEventType;
import com.sportech.virtualracing.util.ConfigHelper;
import com.sportech.virtualracing.util.LocaleHelper;

public class CardGenerator {

	static private final Logger log = Logger.getLogger(CardGenerator.class);

	private GeneratorDao dao = new GeneratorDao();
	
	private VirtualTrackDao trackDao = new VirtualTrackDao();
	
	private String serverIp = null;
	
	public CardGenerator() {
	    serverIp = PropertyHelper.getJndiProperty("java:global/virtualracing/kironserver");
	}
	
	public boolean generateCard(CardConfiguration config) {
        if (StringUtils.isEmpty(serverIp)) {
            log.info("game server is not configured.");
            return false;
        }
        TimeZone tz = ConfigHelper.getTimeZone();
        
		RequestLog entry = new RequestLog();
		entry.setTimestamp(new Date());
		entry.setAction("generate card");
		dao.writeLog(entry);
		HardCard eventData = new HardCard();
		eventData.setRaces(new HashSet<HardRace>());
		Date cardDate = config.getCardDate();
		eventData.setCardDate(cardDate);
		
		Track track = new Track();
		Date firstRaceTime = config.getFirstRaceTime();
		eventData.setFirstRaceTime(firstRaceTime);
		Date raceTime = DateUtils.combineDateAndTime(cardDate, firstRaceTime);
		raceTime = DateUtils.getUTCTime(tz, raceTime);
		VirtualTrack vt = config.getVirtualTrack();
		if (vt == null) {
			vt = trackDao.findVirtualTrack(config.getVirtualRaceType());
		}
		track.setEquibaseId(vt.getCode());
		track.setTrackCode(vt.getCode());
		track.setCountryCode("VR");
		track.setName(vt.getName());
		if (StringUtils.equals(config.getVirtualRaceType(), CardConfiguration.DOG_RACE_TYPE)) {
			eventData.setCardType(CardType.VirtualDog);	
		} else if(StringUtils.equals(config.getVirtualRaceType(), CardConfiguration.HARNESS_RACE_TYPE)) {
			eventData.setCardType(CardType.VirtualHarness);
		} else {
			eventData.setCardType(CardType.VirtualHorse);
		}
		track.setToteTrackId("VirtualR" + track.getEquibaseId());
		track.setPerfType(PerformanceType.Matinee);
		track.setTrackType(eventData.getCardType());
		track.setTimeZone(tz);
		track.setActive(true);
		eventData.setTrack(track);
		eventData.setProvider("Virtual Racing");
		boolean success = true;
		String eventName = vt.getName();
		String race = LocaleHelper.getCaption("race");
		eventData.setDescription(eventName);
		for (int i = 1; i <= config.getRaceCount(); i++) {
			config.setDescription(eventName + " Race " + i);
			config.setDisplayName(eventName + " " + race + " " + i);
			success = createEvent(config, eventData, raceTime, i);
			if (!success) {
				break;
			}
		}
		if (success) {
			MessageHelper.addCardMessage(eventData);
		}
		return success;
	}

	private boolean createEvent(CardConfiguration config, HardCard eventData, Date utcStartTime, int raceNumber) {
		RequestLog entry = new RequestLog();
		entry.setTimestamp(new Date());
		entry.setAction("createEvent");
		try {
			Date raceTime = DateUtils.addMinute(utcStartTime, (raceNumber - 1) * config.getRaceInterval());
			URI uri = generateUrl(raceTime, config);
			String url = uri.toString();
			log.info("Send request:" + url);
			entry.setRequest(url);
			ClientRequest cr = new ClientRequest(url);
			String result = cr.get(String.class).getEntity();
			entry.setResponse(result);
			if (StringUtils.startsWith(result, "<?xml version=\"1.0\" encoding=\"utf-8\"?><Error")) {
				log.error("Error:" + result);
				entry.setStatus("Failure");
				dao.writeLog(entry);
				return false;
			}
			entry.setStatus("Success");
			dao.writeLog(entry);
			log.info("Result:" + result);
			CreateEventType o = parse(result);
			log.info("Created new events at " + o);
			getRace(eventData, o, utcStartTime, raceNumber);
		} catch (Exception e) {
			entry.setStatus("Error");
			entry.setResponse(e.toString());
			dao.writeLog(entry);
			log.error("Error creating new event: ", e);
			return false;
		}
		return true;
	}

	private URI generateUrl(Date d, CardConfiguration config) throws URISyntaxException, UnsupportedEncodingException {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd%20HH:mm");
		String startTime = sdf.format(d);
		URI uri = URIUtils.createURI(
		        "http",
		        serverIp,
		        8023,
		        "/VseGameServer/DataServiceOnDemand/CreateEvent",
		        "eventtype=" + config.getVirtualTrack().getKironEventType() + "" + "&name="
		                + URLEncoder.encode(config.getDisplayName(), "UTF-8") + "&start=" + startTime + ":00Z" + "&distance="
		                + config.getDistance() + "&runners=" + config.getRunnerCount() + "&trackid=" + config.getVirtualTrack().getKironTrackId(), null);
		return uri;
	}

	private CreateEventType parse(String s) {
		try {
			JAXBContext jc = JAXBContext.newInstance(CreateEventType.class);
			Unmarshaller unmarshaller = jc.createUnmarshaller();
			StringReader reader = new StringReader(s);
			JAXBElement<CreateEventType> o = unmarshaller.unmarshal(new StreamSource(reader), CreateEventType.class);
			return o.getValue();
		} catch (Exception e) {
			log.error(e);
		}
		return null;
	}

	private void getRace(HardCard eventData, CreateEventType event, Date utcStartTime, long number) {
		RaceEventType race = event.getRaceEvent();
		if (StringUtils.equals("DashingDerby", race.getEventType()) || 
				StringUtils.equals("PlatinumHounds", race.getEventType()) ||
				StringUtils.equals("HarnessRacing",race.getEventType())
				) {
			HardRace r = new HardRace();
			r.setNumber(number);
			r.setParent(eventData);
			r.setRaceNameShort("Race " + r.getNumber());
			r.setRaceNameLong("Virtual Racing Race " + r.getNumber());
			r.setEventId(race.getID().longValue());
			r.setPostTime(utcStartTime);
			r.setDistance("" + race.getDistance());
			r.setDistanceText("" + race.getDistance() + "Yards");
			r.setEvening(false);

			List<EntryType> entries = race.getEntry();
			r.setRunners(new ArrayList<Runner>());
			for (EntryType e : entries) {
				Runner runner = new Runner();
				runner.setParent(r);
				runner.setName(e.getName());
				runner.setPosition(e.getDraw().longValue());
				runner.setProgramNumber("" + e.getDraw());
				runner.setMornlineOdds(e.getOddsFractional());
				r.getRunners().add(runner);
			}
			eventData.getRaces().add(r);
		}
	}

}
