package com.sportech.virtualracing.util;

import java.io.IOException;
import java.net.URL;
import java.util.Enumeration;
import java.util.jar.Attributes;
import java.util.jar.Manifest;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

public class VersionHelper {
	
	static private final Logger log = Logger.getLogger(VersionHelper.class);
	
	public String getVersion() {
		
		try {
			Enumeration<URL> resources = getClass().getClassLoader().getResources("META-INF/MANIFEST.MF");
			while (resources.hasMoreElements()) {
				try {
					Manifest manifest = new Manifest(resources.nextElement().openStream());
					Attributes attrs = manifest.getMainAttributes(); // "Application-Version");
					if (attrs != null) {
						String version = attrs.getValue("Application-Version");
						String buildNumber = attrs.getValue("Build-Number");
						if (!StringUtils.isEmpty(version)) {
							if (!StringUtils.isEmpty(buildNumber)) {
								version += " build " + buildNumber;
							}
							return version;
						}					
					}

				} catch (IOException e) {
					log.error("IOException caught: ", e);
				}
			}
		} catch (Exception ex) {
			log.error("Exception caught: ", ex);
		}
		
		return "0.1";
		
	}

}
