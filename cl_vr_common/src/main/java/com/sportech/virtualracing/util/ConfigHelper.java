package com.sportech.virtualracing.util;

import java.util.TimeZone;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import com.sportech.common.util.PropertyHelper;

public class ConfigHelper {
    
    static private final Logger log = Logger.getLogger(ConfigHelper.class);
    
    public static TimeZone getTimeZone() {
        String timeZoneName = null;
        timeZoneName = PropertyHelper.getJndiProperty("java:global/virtualracing/timezone");
        TimeZone tz = TimeZone.getDefault();
        if (StringUtils.isEmpty(timeZoneName)) {
            log.info("empty time zone configuration. use server default:" + tz.getID());
        }
        else {
            tz = TimeZone.getTimeZone(timeZoneName);
            if (tz == null) {
                tz = TimeZone.getDefault();             
            }           
            log.info("Use time zone:" + tz.getID());
        }
        return tz;
    }
    
}
