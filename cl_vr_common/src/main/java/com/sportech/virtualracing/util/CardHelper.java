package com.sportech.virtualracing.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.time.DateUtils;

import com.sportech.virtualracing.entity.CardConfiguration;
import com.sportech.virtualracing.entity.CardSchedule;
import com.sportech.virtualracing.entity.CardStatus;
import com.sportech.virtualracing.entity.ScheduleSequence;

public class CardHelper {
	
	public static CardConfiguration createConfig(CardSchedule s) {
		CardConfiguration config = new CardConfiguration();
		config.setCardDate(s.getCardDate());
		config.setFirstRaceTime(s.getFirstPostTime());
		config.setRaceInterval(s.getRaceInterval());
		config.setRunnerCount(s.getRunnerCount());
		config.setRaceCount(s.getRaceCount());
		config.setVirtualRaceType(s.getVirtualRaceType());
		config.setDescription(s.getName());
		config.setDistance(s.getDistance());
		config.setVirtualTrack(s.getVirtualTrack());
		return config;
	}
	
	public static List<CardSchedule> convertSequences(Collection<ScheduleSequence> sequences) {
		List<CardSchedule> cards = new ArrayList<CardSchedule>();
		if (sequences != null) {
			for (ScheduleSequence s : sequences) {
				Date startDate = s.getStartDate();
				Date endDate = s.getEndDate();
				Date workingDate = startDate;
				while (workingDate.getTime() < endDate.getTime()) {
					CardSchedule card = new CardSchedule();
					cloneCardFromSequence(s, card);
					card.setCardDate(workingDate);
					cards.add(card);
					workingDate = DateUtils.addDays(workingDate,  1);
				}
			}
		}
		return cards;
	}
	
	public static void cloneCardFromSequence(ScheduleSequence s, CardSchedule card) {
		card.setDisplayColor(s.getDisplayColor());
		card.setName(s.getName());
		card.setRaceCount(s.getRaceCount());
		card.setRunnerCount(s.getRunnerCount());
		card.setRaceInterval(s.getRaceInterval());
		card.setFirstPostTime(s.getFirstPostTime());		
		card.setDistance(s.getDistance());
		card.setVirtualRaceType(s.getVirtualRaceType());
		card.setVirtualTrack(s.getVirtualTrack());
		card.setCardDate(s.getStartDate());
		card.setScheduleTime(s.getScheduleTime());		
		card.setStatus(CardStatus.Scheduled);
	}

}
