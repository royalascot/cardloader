package com.sportech.virtualracing.util;

import java.util.Locale;
import java.util.ResourceBundle;

public class LocaleHelper {
	
	public static String getCaption(String key) {
		Locale currentLocale = new Locale("es", "DO");
		ResourceBundle messages = ResourceBundle.getBundle("MessagesBundle", currentLocale);
		return messages.getString(key);
	}
	
}
