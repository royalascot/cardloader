package com.sportech.virtualracing.messaging;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.JMSException;
import javax.jms.MessageProducer;
import javax.jms.ObjectMessage;
import javax.jms.Queue;
import javax.jms.Session;
import javax.naming.InitialContext;

import org.apache.log4j.Logger;

import com.sportech.cl.model.database.HardCard;
import com.sportech.cl.model.messaging.CommandMessage;

public class MessageHelper {

	static private final Logger log = Logger.getLogger(MessageHelper.class);

	public static void addCardMessage(HardCard eventData) {
		try {
			CommandMessage cm = new CommandMessage();
			cm.setCommand(CommandMessage.ADDCARD);
			cm.setCard(eventData);
			queueMessage(cm);
		} catch (Exception e) {
			log.error("Error sending card", e);
		}
	}

	private static void queueMessage(CommandMessage cm) {
		Connection connection = null;
		try {
			InitialContext ic = new InitialContext();
			ConnectionFactory cf = (ConnectionFactory) ic.lookup("/ConnectionFactory");
			Queue orderQueue = (Queue) ic.lookup("/queue/cardloader");
			connection = cf.createConnection();
			Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
			MessageProducer producer = session.createProducer(orderQueue);
			connection.start();
			ObjectMessage message = session.createObjectMessage();
			message.setObject(cm);
			producer.send(message);
			session.close();
		} catch (Exception e) {
			log.error("Error creating queue", e);
		} finally {
			if (connection != null) {
				try {
					connection.close();
				} catch (JMSException e) {
					log.error("Error closing connection", e);
				}
			}
		}
	}

}
