package com.sportech.virtualracing.dao;

import org.hibernate.Session;
import org.hibernate.Transaction;

import com.sportech.virtualracing.entity.RequestLog;

public class GeneratorDao {

	public void writeLog(RequestLog logEntry) {
		if (logEntry != null) {
			Session ss = HibernateHelper.getCurrentSession();
			Transaction tran = ss.beginTransaction();
			ss.save(logEntry);
			ss.flush();
			tran.commit();
		}
	}

}
