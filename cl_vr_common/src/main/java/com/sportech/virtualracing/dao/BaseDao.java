package com.sportech.virtualracing.dao;

import java.lang.reflect.ParameterizedType;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;

@SuppressWarnings("unchecked")
public class BaseDao<T> {

	private Class<?> valueClazz = (Class<?>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];

	public List<T> find() {
		Session ss = HibernateHelper.getCurrentSession();
		ss.getTransaction().begin();
		Criteria c = ss.createCriteria(valueClazz);
		List<T> result = (List<T>) c.list();
		ss.getTransaction().commit();
		return result;
	}

	public T get(Long id) {
		Session ss = HibernateHelper.getCurrentSession();
		ss.getTransaction().begin();
		T s = (T) ss.get(valueClazz, id);
		ss.getTransaction().commit();
		return s;
	}

	public void save(T entry) {
		if (entry != null) {
			Session ss = HibernateHelper.getCurrentSession();
			ss.getTransaction().begin();
			ss.saveOrUpdate(entry);
			ss.flush();
			ss.getTransaction().commit();
		}
	}

	public void delete(T entry) {
		if (entry != null) {
			Session ss = HibernateHelper.getCurrentSession();
			ss.getTransaction().begin();
			ss.delete(entry);
			ss.flush();
			ss.getTransaction().commit();
		}
	}
	
}
