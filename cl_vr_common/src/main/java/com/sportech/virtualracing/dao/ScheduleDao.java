package com.sportech.virtualracing.dao;

import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import com.sportech.virtualracing.entity.CardSchedule;

@SuppressWarnings("unchecked")
public class ScheduleDao {

	public List<CardSchedule> findCardsInRange(Date startDate, Date endDate) {
		Session ss = HibernateHelper.getCurrentSession();	
		ss.getTransaction().begin();
		Criteria c = ss.createCriteria(CardSchedule.class);
		c.add(Restrictions.ge("scheduleTime", startDate));
		c.add(Restrictions.le("scheduleTime", endDate));
		List<CardSchedule> result = (List<CardSchedule>) c.list();
		ss.getTransaction().commit();
		return result;		
	}

	public List<CardSchedule> find() {
		Session ss = HibernateHelper.getCurrentSession();
		ss.getTransaction().begin();
		Criteria c = ss.createCriteria(CardSchedule.class);
		List<CardSchedule> result = (List<CardSchedule>) c.list();
		ss.getTransaction().commit();
		return result;
	}

	public CardSchedule get(Long id) {
		Session ss = HibernateHelper.getCurrentSession();
		ss.getTransaction().begin();
		CardSchedule s = (CardSchedule) ss.get(CardSchedule.class, id);
		ss.getTransaction().commit();
		return s;
	}
	
	public void save(CardSchedule entry) {
		if (entry != null) {
			Session ss = HibernateHelper.getCurrentSession();
			ss.getTransaction().begin();
			ss.saveOrUpdate(entry);
			ss.flush();
			ss.getTransaction().commit();
		}
	}

	public void delete(CardSchedule entry) {
		if (entry != null) {
			Session ss = HibernateHelper.getCurrentSession();
			ss.getTransaction().begin();
			ss.delete(entry);
			ss.flush();
			ss.getTransaction().commit();
		}	
	}
}
