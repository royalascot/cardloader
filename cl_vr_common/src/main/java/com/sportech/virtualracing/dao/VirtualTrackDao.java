package com.sportech.virtualracing.dao;

import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import com.sportech.virtualracing.entity.VirtualTrack;

@SuppressWarnings("unchecked")
public class VirtualTrackDao extends BaseDao<VirtualTrack> {

	public List<VirtualTrack> findVirtualTracks(String category) {
		Session ss = HibernateHelper.getCurrentSession();
		ss.getTransaction().begin();
		Criteria c = ss.createCriteria(VirtualTrack.class);
		c.add(Restrictions.eq("category", category));
		List<VirtualTrack> result = (List<VirtualTrack>) c.list();
		ss.getTransaction().commit();
		return result;
	}

	public VirtualTrack findVirtualTrack(String category) {
		Session ss = HibernateHelper.getCurrentSession();
		ss.getTransaction().begin();
		Criteria c = ss.createCriteria(VirtualTrack.class);
		c.add(Restrictions.eq("category", category));
		c.addOrder(Order.asc("lastUsedTime"));
		c.setMaxResults(1);
		VirtualTrack t = (VirtualTrack) c.uniqueResult();
		t.setLastUsedTime(new Date());
		ss.saveOrUpdate(t);
		ss.getTransaction().commit();
		return t;
	}

	public List<VirtualTrack> getVirtualTracks(String category) {
		Session ss = HibernateHelper.getCurrentSession();
		ss.getTransaction().begin();
		Criteria c = ss.createCriteria(VirtualTrack.class);
		c.add(Restrictions.eq("category", category));
		c.addOrder(Order.asc("name"));
		List<VirtualTrack> result = c.list();
		ss.getTransaction().commit();
		return result;
	}

}