package com.sportech.virtualracing.dao;

import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import com.sportech.virtualracing.entity.ScheduleSequence;

public class SequenceDao extends BaseDao<ScheduleSequence> {
	
	@SuppressWarnings("unchecked")
    public List<ScheduleSequence> findPendingSequences(Date startDate, Date endDate) {
		Session ss = HibernateHelper.getCurrentSession();	
		ss.getTransaction().begin();
		Criteria c = ss.createCriteria(ScheduleSequence.class);
		c.add(Restrictions.le("startDate", startDate));
		c.add(Restrictions.gt("endDate",  endDate));
		List<ScheduleSequence> result = (List<ScheduleSequence>) c.list();
		ss.getTransaction().commit();
		return result;		
	}
	
}
