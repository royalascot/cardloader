package com.sportech.virtualracing.entity;

public enum CardStatus {
	Created,
	Done,
	Pending,
	Scheduled,
	Deleted,
	Error
}
