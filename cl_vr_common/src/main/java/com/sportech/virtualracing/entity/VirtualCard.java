/*
 * jaw 2014-20-20 modified to use with kironTrackId
 */
package com.sportech.virtualracing.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;

@MappedSuperclass
public class VirtualCard {

    private Long id;
    private Date firstPostTime;
    private Date cardDate;
    private Date scheduleTime;
    private String name;
    private CardStatus status;

    private Integer raceCount;

    private Integer raceInterval;

    private VirtualTrack virtualTrack;

    private String virtualRaceType;

    private Integer runnerCount;

    private String distance = null;

    private String displayColor;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "status")
    public CardStatus getStatus() {
        return status;
    }

    public void setStatus(CardStatus status) {
        this.status = status;
    }

    @Column(name = "first_post_time")
    public Date getFirstPostTime() {
        return firstPostTime;
    }

    public void setFirstPostTime(Date firstPostTime) {
        this.firstPostTime = firstPostTime;
    }

    @Column(name = "name", length=100)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name = "card_date")
    public Date getCardDate() {
        return cardDate;
    }

    public void setCardDate(Date cardDate) {
        this.cardDate = cardDate;
    }

    @Column(name = "schedule_time")
    public Date getScheduleTime() {
        return scheduleTime;
    }

    public void setScheduleTime(Date scheduleTime) {
        this.scheduleTime = scheduleTime;
    }

    @Column(name = "race_interval")
    public Integer getRaceInterval() {
        return raceInterval;
    }

    public void setRaceInterval(Integer raceInterval) {
        this.raceInterval = raceInterval;
    }

    @ManyToOne
    @JoinColumn(name = "track_id")
    public VirtualTrack getVirtualTrack() {
        return virtualTrack;
    }

    public void setVirtualTrack(VirtualTrack virtualTrack) {
        this.virtualTrack = virtualTrack;
    }

    @Column(name = "race_type", length=50)
    public String getVirtualRaceType() {
        return virtualRaceType;
    }

    public void setVirtualRaceType(String virtualRaceType) {
        this.virtualRaceType = virtualRaceType;
    }

    @Column(name = "runner_count")
    public Integer getRunnerCount() {
        return runnerCount;
    }

    public void setRunnerCount(Integer runnerCount) {
        this.runnerCount = runnerCount;
    }

    @Column(name = "distance", length=100)
    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    @Column(name = "race_count")
    public Integer getRaceCount() {
        return raceCount;
    }

    public void setRaceCount(Integer raceCount) {
        this.raceCount = raceCount;
    }

    @Column(name = "display_color", length=100)
    public String getDisplayColor() {
        return displayColor;
    }

    public void setDisplayColor(String displayColor) {
        this.displayColor = displayColor;
    }

}
