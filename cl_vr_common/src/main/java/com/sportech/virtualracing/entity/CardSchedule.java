package com.sportech.virtualracing.entity;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.apache.commons.lang3.time.DateUtils;

@Entity
@Table(name = "card_schedule")
public class CardSchedule extends VirtualCard implements CalendarItem {

	@Transient
	public Date getStartDate() {
		return getCardDate();
	}
	
	@Transient
	public Date getEndDate() {
		Integer count = getRaceCount();
		Integer interval = getRaceInterval();
		if (count != null && interval != null) {
			return DateUtils.addMinutes(getCardDate(), count * interval);
		}
		return DateUtils.addMinutes(getCardDate(), 60);
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((getVirtualRaceType() == null) ? 0 : getVirtualRaceType().hashCode());
		result = prime * result + ((getCardDate() == null) ? 0 : getCardDate().hashCode());
		result = prime * result + ((getFirstPostTime() == null) ? 0 : getFirstPostTime().hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CardSchedule other = (CardSchedule) obj;
		if (getVirtualRaceType() == null) {
			if (other.getVirtualRaceType() != null)
				return false;
		} else if (!getVirtualRaceType().equals(other.getVirtualRaceType()))
			return false;
		if (getCardDate() == null) {
			if (other.getCardDate() != null)
				return false;
		} else if (!getCardDate().equals(other.getCardDate()))
			return false;
		if (getFirstPostTime() == null) {
			if (other.getFirstPostTime() != null)
				return false;
		} else if (!getFirstPostTime().equals(other.getFirstPostTime()))
			return false;		
		return true;
	}
}
