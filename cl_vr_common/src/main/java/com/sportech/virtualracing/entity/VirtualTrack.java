/*
 * jaw - 2014-10-20 added kironTrackId to identify
 * new horse track #2 and allow for future expansion
 * of dog, horse, harness. 
 * Added kironEventType to DB
 * 
 */
package com.sportech.virtualracing.entity;

import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "virtual_track")
public class VirtualTrack {

    private Long id;
    private String name;
    private String code;
    private String category;
    private Date lastUsedTime;
    private Integer kironTrackId;
    private String kironEventType;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "name", length=100)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name = "track_code", length=100)
    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @Column(name = "category", length=100)
    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    @Column(name = "last_used_time")
    public Date getLastUsedTime() {
        return lastUsedTime;
    }

    public void setLastUsedTime(Date lastUsedTime) {
        this.lastUsedTime = lastUsedTime;
    }

    @Column(length=100)
    public String getKironEventType() {
        return kironEventType;
    }

    public void setKironEventType(String kironEventType) {
        this.kironEventType = kironEventType;
    }

    @Basic
    public Integer getKironTrackId() {
        return kironTrackId;
    }

    public void setKironTrackId(Integer kironTrackId) {
        this.kironTrackId = kironTrackId;
    }

}
