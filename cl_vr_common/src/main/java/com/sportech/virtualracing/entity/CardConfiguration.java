package com.sportech.virtualracing.entity;

import com.sportech.cl.model.database.HardCard;

public class CardConfiguration extends HardCard {
	
    private static final long serialVersionUID = 1L;
	public static final String DOG_RACE_TYPE = "Dog";
	public static final String HORSE_RACE_TYPE1 = "Horse1";
	public static final String HORSE_RACE_TYPE2 = "Horse2";
	public static final String HARNESS_RACE_TYPE = "Harness";
	
	private int raceInterval = 10;
	
	private int raceCount = 8;
	
	private VirtualTrack virtualTrack;
	
	private String virtualRaceType = HORSE_RACE_TYPE1;

	private int runnerCount = 10;
	
	private String distance = null;
	
	private String displayName;
	
	public int getRaceInterval() {
		return raceInterval;
	}

	public void setRaceInterval(int raceInterval) {
		this.raceInterval = raceInterval;
	}

	public VirtualTrack getVirtualTrack() {
		return virtualTrack;
	}

	public void setVirtualTrack(VirtualTrack virtualTrack) {
		this.virtualTrack = virtualTrack;
	}

	public String getVirtualRaceType() {
		return virtualRaceType;
	}

	public void setVirtualRaceType(String virtualRaceType) {
		this.virtualRaceType = virtualRaceType;
	}

	public int getRunnerCount() {
		return runnerCount;
	}

	public void setRunnerCount(int runnerCount) {
		this.runnerCount = runnerCount;
	}

	public String getDistance() {
		return distance;
	}

	public void setDistance(String distance) {
		this.distance = distance;
	}

	public Integer getRaceCount() {
		return raceCount;
	}

	public void setRaceCount(Integer raceCount) {
		this.raceCount = raceCount;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}
	
}
