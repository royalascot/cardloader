package com.sportech.virtualracing.entity;

import java.util.Date;

public interface CalendarItem {
	public CardStatus getStatus();
	public Date getStartDate();
	public Date getEndDate();
}
