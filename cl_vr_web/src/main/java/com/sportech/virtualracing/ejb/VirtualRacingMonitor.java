package com.sportech.virtualracing.ejb;

import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.annotation.PostConstruct;
import javax.ejb.AccessTimeout;
import javax.ejb.Lock;
import javax.ejb.LockType;
import javax.ejb.Schedule;
import javax.ejb.Singleton;
import javax.ejb.Startup;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.apache.log4j.Logger;

import com.sportech.common.util.PropertyHelper;
import com.sportech.virtualracing.dao.ScheduleDao;
import com.sportech.virtualracing.entity.CardSchedule;
import com.sportech.virtualracing.entity.CardStatus;
import com.sportech.virtualracing.service.CardGenerator;
import com.sportech.virtualracing.util.CardHelper;

@Singleton
@Startup
public class VirtualRacingMonitor {

    static private final Logger log = Logger.getLogger(VirtualRacingMonitor.class);

    private boolean enabled = true;

    private int scannerLeadTime = 60;

    private String serverIp;

    @PostConstruct
    public void init() {
        serverIp = PropertyHelper.getJndiProperty("java:global/virtualracing/kironserver");
        if (StringUtils.isEmpty(serverIp)) {
            enabled = false;
            log.info("Kiron server IP is not configured. VR Sequence scheduler is disabled.");
        }
        else {
            log.info("Using Kiron server:" + serverIp);
        }
        String sLead = PropertyHelper.getJndiProperty("java:global/virtualracing/scanner_lead_time");
        if (sLead != null && StringUtils.isNumeric(sLead)) {
            scannerLeadTime = Integer.parseInt(sLead);
            log.info("Scan calendar with " + scannerLeadTime + " minutes ahead.");
        } else {
            scannerLeadTime = 60;
            log.info("Use default 60 minutes");
        }
    }

    @Schedule(minute = "*", hour = "*", persistent = false)
    @Lock(LockType.WRITE)
    @AccessTimeout(value = 10, unit = TimeUnit.MINUTES)
    public void checkVirtualEvents() {
        if (!enabled) {
            return;
        }
        Date currentDate = new Date();
        Date cardDate = DateUtils.addHours(currentDate, -2);
        Date upperDate = DateUtils.addMinutes(currentDate, scannerLeadTime);
        ScheduleDao dao = new ScheduleDao();
        List<CardSchedule> schedules = dao.findCardsInRange(cardDate, upperDate);
        for (CardSchedule s : schedules) {
            if (s.getStatus() == CardStatus.Pending) {
                log.info("Generating card for " + s.getScheduleTime());
                CardGenerator generator = new CardGenerator();
                if (generator.generateCard(CardHelper.createConfig(s))) {
                    s.setStatus(CardStatus.Done);
                } else {
                    s.setStatus(CardStatus.Error);
                }
                dao.save(s);
            }
        }
    }
}
