package com.sportech.virtualracing.ejb;

import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.annotation.PostConstruct;
import javax.ejb.AccessTimeout;
import javax.ejb.Lock;
import javax.ejb.LockType;
import javax.ejb.Schedule;
import javax.ejb.Singleton;
import javax.ejb.Startup;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.apache.log4j.Logger;

import com.sportech.common.util.PropertyHelper;
import com.sportech.virtualracing.dao.ScheduleDao;
import com.sportech.virtualracing.dao.SequenceDao;
import com.sportech.virtualracing.entity.CardSchedule;
import com.sportech.virtualracing.entity.CardStatus;
import com.sportech.virtualracing.entity.ScheduleSequence;
import com.sportech.virtualracing.util.CardHelper;

@Singleton
@Startup
public class SequenceMonitor {

	static private final Logger log = Logger.getLogger(SequenceMonitor.class);

	private boolean enabled = true;
	
	@PostConstruct
	public void init() {	    
	    String serverIp = PropertyHelper.getJndiProperty("java:global/virtualracing/kironserver");
	    if (StringUtils.isEmpty(serverIp)) {
	        enabled = false;
	        log.info("Kiron server IP is not configured. VR Sequence scheduler is disabled.");
	    }
	}

	@Schedule(minute = "*/10", hour = "*", persistent = false)
	@Lock(LockType.WRITE)
	@AccessTimeout(value = 10, unit = TimeUnit.MINUTES)
	public void checkSequences() {
	    if (!enabled) {
	        return;
	    }
		Date currentDate = new Date();
		Date cardDate = DateUtils.addHours(currentDate, 24);
		Date upperDate = DateUtils.addHours(cardDate, 24);
		SequenceDao dao = new SequenceDao();
		ScheduleDao scheduleDao = new ScheduleDao();
		List<ScheduleSequence> schedules = dao.findPendingSequences(cardDate, upperDate);
		List<CardSchedule> cards = CardHelper.convertSequences(schedules);
		HashSet<CardSchedule> existingCards = new HashSet<CardSchedule>();
		existingCards.addAll(scheduleDao.findCardsInRange(currentDate, upperDate));
		for (CardSchedule card : cards) {
			if (DateUtils.isSameDay(cardDate, card.getCardDate())) {
				if (!existingCards.contains(card)) {
					log.info("Scheduling card for " + card.getFirstPostTime());
					card.setStatus(CardStatus.Pending);
					scheduleDao.save(card);
					existingCards.add(card);
				}
			}
		}
	}
}
