/*
 * jaw 2014-20-20 modified to use with kironTrackId
 */
package com.sportech.virtualracing.web.viewmodel;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.lang3.StringUtils;
import org.zkoss.bind.BindContext;
import org.zkoss.bind.annotation.AfterCompose;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.calendar.Calendars;
import org.zkoss.calendar.api.CalendarEvent;
import org.zkoss.calendar.event.CalendarsEvent;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.SelectEvent;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Window;

import com.sportech.cl.model.database.utils.DateUtils;
import com.sportech.virtualracing.dao.SequenceDao;
import com.sportech.virtualracing.entity.CardConfiguration;
import com.sportech.virtualracing.entity.ScheduleSequence;
import com.sportech.virtualracing.web.util.WebHelper;

@Named
public class SequenceViewModel extends BaseViewModel {

	private static String[] raceTypes = new String[] { 
		CardConfiguration.DOG_RACE_TYPE, 
		CardConfiguration.HORSE_RACE_TYPE1,
		CardConfiguration.HORSE_RACE_TYPE2,
		CardConfiguration.HARNESS_RACE_TYPE};

	private static String[] dogDistances = new String[] { "375", "725" };

	private static String[] horseDistances = new String[] { "1000", "1600", "2000" };
	
	private static String[] harnessDistances = new String[] { "800" };

	private static Integer[] dogRunnerCounts = new Integer[] { 6, 8 };

	private static Integer[] horseRunnerCounts = new Integer[] { 8, 9, 10, 11, 12, 13, 14 };

	private static Integer[] harnessRunnerCounts = new Integer[] { 10 };

	private VirtualRacingEventModel eventModel = new VirtualRacingEventModel();

	private SequenceDao dao = new SequenceDao();

	private ScheduleSequence detail;

	private static SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm");
	
    @PersistenceContext(unitName = "vr")
    EntityManager em;

	@Wire("#calendars")
	Calendars calendars;

	@AfterCompose
	public void afterCompose(@ContextParam(ContextType.VIEW) Component view) {
		Selectors.wireComponents(view, this, false);
	}

	@Init
	public void init(@BindingParam("detail") ScheduleSequence schedule) {
		if (schedule != null) {
			detail = schedule;
			return;
		}
	}

	public VirtualRacingEventModel getEventModel() {
		eventModel = new VirtualRacingEventModel();
		List<ScheduleSequence> schedules = dao.find();
		for (ScheduleSequence s : schedules) {
			VirtualRacingEvent vre = new VirtualRacingEvent();
			vre.setBeginDate(s.getStartDate());
			vre.setEndDate(s.getEndDate());
			String name = s.getSequenceName() + "-" + 
			s.getRaceCount() + " " + s.getVirtualRaceType() + " Races/" + s.getRaceInterval() + " @ " + timeFormat.format(s.getFirstPostTime());
			vre.setTitle(name);
			vre.setContent(name);
			vre.setContentColor(s.getDisplayColor());			
			vre.setSequenceId(s.getId());
			eventModel.add(vre);
		}
		return eventModel;
	}

	public void setEventModel(VirtualRacingEventModel eventModel) {
		this.eventModel = eventModel;
	}

	public ScheduleSequence getDetail() {
		if (detail == null) {
			createDetail(new Date());
		}
		return detail;
	}

	public void setDetail(ScheduleSequence detail) {
		this.detail = detail;
	}

	@SuppressWarnings("serial")
	private static HashMap<String, Integer[]> runnerCountMap = new HashMap<String, Integer[]>() {
		{
			put(CardConfiguration.DOG_RACE_TYPE, dogRunnerCounts);
			put(CardConfiguration.HORSE_RACE_TYPE1, horseRunnerCounts);
			put(CardConfiguration.HORSE_RACE_TYPE2, horseRunnerCounts);
			put(CardConfiguration.HARNESS_RACE_TYPE, harnessRunnerCounts);
		}
	};

	@SuppressWarnings("serial")
	private static HashMap<String, String[]> distanceMap = new HashMap<String, String[]>() {
		{
			put(CardConfiguration.DOG_RACE_TYPE, dogDistances);
			put(CardConfiguration.HORSE_RACE_TYPE1, horseDistances);
			put(CardConfiguration.HORSE_RACE_TYPE2, horseDistances);
			put(CardConfiguration.HARNESS_RACE_TYPE, harnessDistances);
		}
	};

	public String[] getRaceTypes() {
		return raceTypes;
	}

	public Integer[] getCounts(String v) {
		Integer[] counts = runnerCountMap.get(v);
		return counts;
	}

	public ListModelList<String> getDistances() {
		return new ListModelList<String>(distanceMap.get(getDetail().getVirtualRaceType()));
	}

	@Command
	@NotifyChange({ "distances", "detail", "virtualTracks","runnerCounts" })
	public void refreshRaceType(@ContextParam(ContextType.BIND_CONTEXT) BindContext ctx) {
		Event event = ctx.getTriggerEvent();
		@SuppressWarnings("unchecked")
		SelectEvent<Component, String> se = (SelectEvent<Component, String>) event;
		if (se != null) {
			Set<String> s = se.getSelectedObjects();
			for (String f : s) {
				if (detail != null) {
					detail.setVirtualRaceType(f);
					virtualRaceType = f;
					detail.setVirtualTrack(null);
					detail.setDistance(getDistances().get(0));
				}
			}
		}
	}

	@Command
	@NotifyChange({ "detail" })
	public void refreshDistance(@ContextParam(ContextType.BIND_CONTEXT) BindContext ctx) {
		Event event = ctx.getTriggerEvent();
		@SuppressWarnings("unchecked")
		SelectEvent<Component, String> se = (SelectEvent<Component, String>) event;
		if (se != null) {
			Set<String> s = se.getSelectedObjects();
			for (String f : s) {
				if (detail != null) {
					detail.setDistance(f);
				}
			}
		}
	}

	@Command
	public void closeDialog(@ContextParam(ContextType.VIEW) Window comp) {
		comp.detach();
	}

	@Command
	@NotifyChange("EventModel")
	public void saveSequence(@ContextParam(ContextType.VIEW) Window comp) {
		Integer runnerCount = detail.getRunnerCount();
		Integer[] counts = getCounts(detail.getVirtualRaceType());
		if (!Arrays.asList(counts).contains(runnerCount)) {
			Messagebox.show("Invalid runner count.");
			return;
		}
		if (StringUtils.isBlank(detail.getDistance())) {
			Messagebox.show("Invalid distance.");
			return;
		}
		Date current = new Date();
		if (detail.getStartDate() != null && detail.getStartDate().getTime() < current.getTime()) {
			Messagebox.show("Invalid start date.");
			return;
		}
		if (detail.getEndDate() != null && detail.getEndDate().getTime() < detail.getStartDate().getTime()) {
			Messagebox.show("Invalid end date.");
			return;
		}
		if (detail.getVirtualTrack() != null) {
			detail.setName(detail.getVirtualTrack().getName());
		}
		dao.save(detail);
		comp.detach();
	}

	@Command
	@NotifyChange({ "eventModel" })
	public void deleteSequence(@ContextParam(ContextType.VIEW) Window comp) {
		dao.delete(detail);
		comp.detach();
	}

	@Command
	@NotifyChange({ "eventModel" })
	public void editSequence(@ContextParam(ContextType.BIND_CONTEXT) BindContext ctx) {
		Event event = ctx.getTriggerEvent();
		CalendarsEvent se = (CalendarsEvent) event;
		if (se != null) {
			CalendarEvent ce = se.getCalendarEvent();
			VirtualRacingEvent vre = (VirtualRacingEvent) ce;
			Long id = vre.getSequenceId();
			if (id != null) {
				ScheduleSequence s = dao.get(id);
				if (s != null) {
					detail = s;
					showDetail();
				}
			}
		}
	}

	@Command
	@NotifyChange("eventModel")
	public void newSequence(@ContextParam(ContextType.BIND_CONTEXT) BindContext ctx) {
		Event event = ctx.getTriggerEvent();
		CalendarsEvent se = (CalendarsEvent) event;
		if (se != null) {
			Date startDate = se.getBeginDate();
			Date endDate = se.getEndDate();
			createDetail(startDate);
			detail.setStartDate(startDate);
			detail.setEndDate(endDate);
			showDetail();
		}
	}
	
	@Command
	@NotifyChange({ "eventModel" })
	public void addSequence(@ContextParam(ContextType.VIEW) Window comp) {
		createDetail(new Date());
		showDetail();
	}

	/**
	 * default values
	 * @param postTime
	 */
	private void createDetail(Date postTime) {
		detail = new ScheduleSequence();
		Date cardDate = DateUtils.addDate(new Date(), 1);
		detail.setCardDate(cardDate);
		detail.setStartDate(cardDate);
		detail.setEndDate(DateUtils.addDate(detail.getStartDate(), 7));
		detail.setName("Card Series");
		detail.setFirstPostTime(postTime);
		detail.setVirtualRaceType(CardConfiguration.HORSE_RACE_TYPE1);
		detail.setVirtualTrack(null);
		detail.setDistance("1000");	
		detail.setRunnerCount(10);
		detail.setRaceInterval(10);
		detail.setRaceCount(8);
		detail.setDisplayColor(WebHelper.getRandomColor());
	}
	
	private void showDetail() {
		Map<String, Object> arg = new HashMap<String, Object>();
		arg.put("sequence", detail);
		Window window = (Window) Executions.createComponents("sequenceeditor.zul", null, arg);
		window.doModal();
	}

	@GlobalCommand
	@NotifyChange({ "eventModel" })
	public void refreshCalendar() {
	}

	@Command
	@NotifyChange("currentRange")
	public void backCalendar(@ContextParam(ContextType.BIND_CONTEXT) BindContext ctx) {
		calendars.previousPage();
	}

	@Command
	@NotifyChange("currentRange")
	public void forwardCalendar(@ContextParam(ContextType.BIND_CONTEXT) BindContext ctx) {
		calendars.nextPage();
	}

	public String getCurrentRange() {
		Date b = calendars.getBeginDate();
		Date e = calendars.getEndDate();
		SimpleDateFormat sdfV = new SimpleDateFormat("MMM/dd/yyyy");
		return sdfV.format(b) + " - " + sdfV.format(e);
	}
	
}
