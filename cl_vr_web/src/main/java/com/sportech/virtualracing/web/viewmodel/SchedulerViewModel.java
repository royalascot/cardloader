/*
 * added second horse track and new 
 * harness track * 
 */
package com.sportech.virtualracing.web.viewmodel;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.zkoss.bind.BindContext;
import org.zkoss.bind.annotation.AfterCompose;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.calendar.Calendars;
import org.zkoss.calendar.api.CalendarEvent;
import org.zkoss.calendar.event.CalendarsEvent;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.SelectEvent;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Window;

import com.sportech.virtualracing.dao.ScheduleDao;
import com.sportech.virtualracing.dao.SequenceDao;
import com.sportech.virtualracing.entity.CardConfiguration;
import com.sportech.virtualracing.entity.CardSchedule;
import com.sportech.virtualracing.entity.CardStatus;
import com.sportech.virtualracing.service.CardGenerator;
import com.sportech.virtualracing.util.CardHelper;

@Named
public class SchedulerViewModel extends BaseViewModel {

    private static final String PROCESSED_COLOR = "#668CD9";
    private static final String NEW_COLOR = "#D98C66";
    private static final String PENDING_DOG_COLOR = "#B373B3";
    private static final String PENDING_HORSE_COLOR = "#D96666";

    private static String[] raceTypes = new String[] { CardConfiguration.DOG_RACE_TYPE, CardConfiguration.HORSE_RACE_TYPE1,
            CardConfiguration.HORSE_RACE_TYPE2, CardConfiguration.HARNESS_RACE_TYPE

    };

    private static String[] dogDistances = new String[] { "375", "725" };

    private static String[] horseDistances = new String[] { "1000", "1600", "2000" };

    private static String[] harnessDistances = new String[] { "800" };

    private static Integer[] dogRunnerCounts = new Integer[] { 6, 8 };

    private static Integer[] horseRunnerCounts = new Integer[] { 8, 9, 10, 11, 12, 13, 14 };

    private static Integer[] harnessRunnerCounts = new Integer[] { 10 };

    private VirtualRacingEventModel eventModel = new VirtualRacingEventModel();

    private static SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");

    private static SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm");

    private ScheduleDao dao = new ScheduleDao();

    private SequenceDao sequenceDao = new SequenceDao();

    private CardSchedule detail;

    @PersistenceContext(unitName = "vr")
    EntityManager em;

    @Wire("#calendars")
    Calendars calendars;

    @AfterCompose
    public void afterCompose(@ContextParam(ContextType.VIEW) Component view) {
        Selectors.wireComponents(view, this, false);
    }

    @Init
    public void init(@BindingParam("detail") CardSchedule schedule) {
        if (schedule != null) {
            detail = schedule;
            return;
        }
    }

    public VirtualRacingEventModel getEventModel() {
        eventModel = new VirtualRacingEventModel();
        HashSet<CardSchedule> schedules = new HashSet<CardSchedule>();
        schedules.addAll(dao.find());
        schedules.addAll(CardHelper.convertSequences(sequenceDao.find()));
        for (CardSchedule s : schedules) {
            if (s.getStatus() == CardStatus.Deleted) {
                continue;
            }
            VirtualRacingEvent vre = new VirtualRacingEvent();
            Date beginDate = s.getCardDate();
            Calendar postTime = DateUtils.toCalendar(s.getFirstPostTime());
            beginDate = DateUtils.setHours(beginDate, postTime.get(Calendar.HOUR_OF_DAY));
            beginDate = DateUtils.setMinutes(beginDate, postTime.get(Calendar.MINUTE));
            beginDate = DateUtils.setSeconds(beginDate, 0);
            vre.setBeginDate(beginDate);
            vre.setEndDate(getEndDateFromCard(beginDate, s));
            vre.setTitle(s.getName() + ":" + s.getStatus());
            vre.setContent("" + s.getRaceCount() + " " + s.getVirtualRaceType() + " Races/" + s.getRaceInterval() + " @ "
                    + timeFormat.format(beginDate));
            vre.setEventItem(s);
            setContentColor(s, vre);
            eventModel.add(vre);
        }
        return eventModel;
    }

    public void setEventModel(VirtualRacingEventModel eventModel) {
        this.eventModel = eventModel;
    }

    public CardSchedule getDetail() {
        if (detail == null) {
            detail = createCard(new Date());
        }
        return detail;
    }

    public void setDetail(CardSchedule detail) {
        this.detail = detail;
    }

    @SuppressWarnings("serial")
    private static HashMap<String, Integer[]> runnerCountMap = new HashMap<String, Integer[]>() {
        {
            put(CardConfiguration.DOG_RACE_TYPE, dogRunnerCounts);
            put(CardConfiguration.HORSE_RACE_TYPE1, horseRunnerCounts);
            put(CardConfiguration.HORSE_RACE_TYPE2, horseRunnerCounts);
            put(CardConfiguration.HARNESS_RACE_TYPE, harnessRunnerCounts);
        }
    };

    @SuppressWarnings("serial")
    private static HashMap<String, String[]> distanceMap = new HashMap<String, String[]>() {
        {
            put(CardConfiguration.DOG_RACE_TYPE, dogDistances);
            put(CardConfiguration.HORSE_RACE_TYPE1, horseDistances);
            put(CardConfiguration.HORSE_RACE_TYPE2, horseDistances);
            put(CardConfiguration.HARNESS_RACE_TYPE, harnessDistances);
        }
    };

    public String[] getRaceTypes() {
        return raceTypes;
    }

    public ListModelList<Integer> getRunnerCounts() {
        return new ListModelList<Integer>(runnerCountMap.get(getDetail().getVirtualRaceType()));
    }

    public ListModelList<String> getDistances() {
        return new ListModelList<String>(distanceMap.get(getDetail().getVirtualRaceType()));
    }

    public boolean isReadonly() {
        if (detail != null
                && (detail.getStatus() == CardStatus.Created || detail.getStatus() == CardStatus.Pending || detail.getStatus() == CardStatus.Error)) {
            return false;
        }
        return true;
    }

    @Command
    @NotifyChange({ "distances", "detail", "virtualTracks", "runnerCounts" })
    public void refreshRaceType(@ContextParam(ContextType.BIND_CONTEXT) BindContext ctx) {
        Event event = ctx.getTriggerEvent();
        @SuppressWarnings("unchecked")
        SelectEvent<Component, String> se = (SelectEvent<Component, String>) event;
        if (se != null) {
            Set<String> s = se.getSelectedObjects();
            for (String f : s) {
                if (detail != null) {
                    detail.setVirtualRaceType(f);
                    this.virtualRaceType = f;
                    detail.setVirtualTrack(getVirtualTracks().get(0));                    
                    detail.setDistance(getDistances().get(0));
                    detail.setRunnerCount(getRunnerCounts().get(0));
                }
            }
        }
    }

    @Command
    @NotifyChange({ "detail" })
    public void refreshDistance(@ContextParam(ContextType.BIND_CONTEXT) BindContext ctx) {
        Event event = ctx.getTriggerEvent();
        @SuppressWarnings("unchecked")
        SelectEvent<Component, String> se = (SelectEvent<Component, String>) event;
        if (se != null) {
            Set<String> s = se.getSelectedObjects();
            for (String f : s) {
                if (detail != null) {
                    detail.setDistance(f);
                }
            }
        }
    }

    /**
     * weekly view default parameters
     * 
     * @param d
     * @return
     */
    private CardSchedule createCard(Date d) {
        CardSchedule s = new CardSchedule();
        s.setVirtualRaceType(CardConfiguration.HORSE_RACE_TYPE1);
        this.virtualRaceType = CardConfiguration.HORSE_RACE_TYPE1;
        s.setDistance("1000");
        s.setStatus(CardStatus.Created);
        s.setRaceInterval(10);
        s.setRunnerCount(10);
        s.setRaceCount(8);
        s.setName("Virtual Racing");
        s.setVirtualTrack(getVirtualTracks().get(0));
        s.setCardDate(d);
        s.setScheduleTime(d);
        s.setFirstPostTime(d);
        s.setName(sdf.format(d));
        return s;
    }

    @Command
    @NotifyChange("eventModel")
    public void scheduleCard(@ContextParam(ContextType.BIND_CONTEXT) BindContext ctx) {
        Event event = ctx.getTriggerEvent();
        CalendarsEvent se = (CalendarsEvent) event;
        if (se != null) {
            Date currentDate = new Date();
            Date cardDate = DateUtils.addHours(currentDate, -1);
            if (se.getBeginDate().getTime() < currentDate.getTime()) {
                return;
            }
            if (se.getBeginDate().getTime() < cardDate.getTime()) {
                Messagebox.show("Can not schedule a card for past time.");
                return;
            }
            CardSchedule s = createCard(se.getBeginDate());
            editCard(s);
        }
    }

    @Command
    @NotifyChange("eventModel")
    public void editSchedule(@ContextParam(ContextType.BIND_CONTEXT) BindContext ctx) {
        Event event = ctx.getTriggerEvent();
        CalendarsEvent se = (CalendarsEvent) event;
        if (se != null) {
            CalendarEvent ce = se.getCalendarEvent();
            VirtualRacingEvent vre = (VirtualRacingEvent) ce;
            CardSchedule item = vre.getEventItem();
            if (item != null) {
                editCard(item);
            }
        }
    }

    @Command
    public void closeDialog(@ContextParam(ContextType.VIEW) Window comp) {
        comp.detach();
    }

    private void editCard(CardSchedule schedule) {
        Map<String, Object> arg = new HashMap<String, Object>();
        arg.put("schedule", schedule);
        Window window = (Window) Executions.createComponents("addcard.zul", null, arg);
        window.doModal();
    }

    @Command
    public void saveCard(@ContextParam(ContextType.VIEW) Window comp) {
        Integer runnerCount = detail.getRunnerCount();

        if (!getRunnerCounts().contains(runnerCount)) {
            Messagebox.show("Invalid runner count.");
            return;
        }
        if (StringUtils.isBlank(detail.getDistance())) {
            Messagebox.show("Invalid distance.");
            return;
        }
        if (detail.getStatus() == CardStatus.Created) {
            detail.setStatus(CardStatus.Pending);
        }
        if (detail.getVirtualTrack() != null) {
            detail.setName(detail.getVirtualTrack().getName());
        }
        dao.save(detail);
        comp.detach();
    }

    @Command
    public void deleteCard(@ContextParam(ContextType.VIEW) Window comp) {
        if (detail != null) {
            detail.setStatus(CardStatus.Deleted);
            dao.save(detail);
        }
        detail = null;
        comp.detach();
    }

    @GlobalCommand
    @NotifyChange({ "eventModel" })
    public void refreshCalendar() {
    }

    @Command
    public void generateCard(@ContextParam(ContextType.COMPONENT) Component comp) {
        int runnerCount = detail.getRunnerCount();

        if (getRunnerCounts().contains(runnerCount)) {
            Messagebox.show("Invalid runner count.");
            return;
        }
        if (StringUtils.isBlank(detail.getDistance())) {
            Messagebox.show("Invalid distance.");
            return;
        }
        CardGenerator generator = new CardGenerator();
        generator.generateCard(CardHelper.createConfig(detail));
        Messagebox.show("New virtual racing card generated.");
    }

    @Command
    public void cancelGeneration() {
        String url = "scheduler.zul";
        Executions.getCurrent().sendRedirect(url);
    }

    @Command
    @NotifyChange("currentRange")
    public void backCalendar(@ContextParam(ContextType.BIND_CONTEXT) BindContext ctx) {
        calendars.previousPage();
    }

    @Command
    @NotifyChange("currentRange")
    public void forwardCalendar(@ContextParam(ContextType.BIND_CONTEXT) BindContext ctx) {
        calendars.nextPage();
    }

    public String getCurrentRange() {
        Date b = calendars.getBeginDate();
        Date e = calendars.getEndDate();
        SimpleDateFormat sdfV = new SimpleDateFormat("MMM/dd/yyyy");
        return sdfV.format(b) + " - " + sdfV.format(e);
    }

    private static void setContentColor(CardSchedule s, VirtualRacingEvent vre) {
        switch (s.getStatus()) {
        case Pending:
            if (StringUtils.equals(s.getVirtualRaceType(), CardConfiguration.HORSE_RACE_TYPE1)) {
                vre.setContentColor(PENDING_HORSE_COLOR);
            } else {
                vre.setContentColor(PENDING_DOG_COLOR);
            }
            break;
        case Done:
            vre.setContentColor(PROCESSED_COLOR);
            break;
        case Created:
            vre.setContentColor(NEW_COLOR);
            break;
        case Scheduled:
            vre.setContentColor(s.getDisplayColor());
            break;
        case Deleted:
        case Error:
            vre.setContentColor("Red");
            break;
        }

    }

    private static Date getEndDateFromCard(Date beginDate, CardSchedule s) {
        if (beginDate == null) {
            return null;
        }
        if (s == null || s.getRaceInterval() == null || s.getRaceCount() == null) {
            return DateUtils.addMinutes(beginDate, 60);
        }
        Date endDate = DateUtils.addMinutes(beginDate, s.getRaceInterval() * (s.getRaceCount() - 1) + 20);
        return endDate;
    }

}
