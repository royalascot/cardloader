package com.sportech.virtualracing.web.viewmodel;

import org.zkoss.calendar.impl.SimpleCalendarEvent;

import com.sportech.virtualracing.entity.CardSchedule;

@SuppressWarnings("serial")
public class VirtualRacingEvent extends SimpleCalendarEvent {
	
	private CardSchedule eventItem;

	private Long sequenceId;
	
	public CardSchedule getEventItem() {
		return eventItem;
	}

	public void setEventItem(CardSchedule eventItem) {
		this.eventItem = eventItem;
	}

	public Long getSequenceId() {
		return sequenceId;
	}

	public void setSequenceId(Long sequenceId) {
		this.sequenceId = sequenceId;
	}	
	
}
