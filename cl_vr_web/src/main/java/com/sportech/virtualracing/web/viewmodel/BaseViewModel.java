/*
 * jaw 2014-20-20 modified to use with kironTrackId
 */
package com.sportech.virtualracing.web.viewmodel;

import java.util.List;
import java.util.TimeZone;

import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.zkoss.zul.ListModelList;

import com.sportech.virtualracing.dao.VirtualTrackDao;
import com.sportech.virtualracing.entity.CardConfiguration;
import com.sportech.virtualracing.entity.VirtualTrack;
import com.sportech.virtualracing.util.ConfigHelper;
import com.sportech.virtualracing.util.VersionHelper;

@Named
public class BaseViewModel {

    @PersistenceContext(unitName = "vr")
    EntityManager em;

	private VirtualTrackDao trackDao = new VirtualTrackDao();

	protected String virtualRaceType = CardConfiguration.HORSE_RACE_TYPE1;

	public String getBuildVersion() {
		VersionHelper v = new VersionHelper();
		return v.getVersion();
	}

	public ListModelList<VirtualTrack> getVirtualTracks() {
		List<VirtualTrack> tracks = trackDao.getVirtualTracks(virtualRaceType);
		return new ListModelList<VirtualTrack>(tracks);
	}

	public TimeZone getTimeZone() {
	    return ConfigHelper.getTimeZone();
	}

	public String getTimeProps() {
	    TimeZone tz = ConfigHelper.getTimeZone();
	    String value = String.format("Name: %s, Offset: %d, DST Offset: %d, DST: %b", tz.getDisplayName(), tz.getRawOffset(), tz.getDSTSavings(), tz.observesDaylightTime());
	    return value;
	}
	
}
