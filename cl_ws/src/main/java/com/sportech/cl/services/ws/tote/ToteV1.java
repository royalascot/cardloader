package com.sportech.cl.services.ws.tote;

import javax.annotation.Resource;
import javax.annotation.PostConstruct;
import javax.jws.HandlerChain;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.ws.WebServiceContext;

import com.sportech.cl.model.response.GenericResponse;
import com.sportech.cl.model.response.LoginResponce;
import com.sportech.cl.model.response.ToteCardListResponse;
import com.sportech.cl.model.response.ToteCardResponse;
import com.sportech.cl.services.ws.utils.HttpAuthentication;
import com.sportech.cl.services.tote.ToteInterface;
import com.sportech.cl.services.tote.ToteRemoteLocator;
import com.sportech.cl.services.security.AuthenticateInterface;
import com.sportech.cl.services.security.AuthenticateRemoteLocator;
import com.sportech.cl.global.utils.config.ConfigLoader;

@WebService(targetNamespace="http://www.sportech.net/cl/services/tote/v1/")
@HandlerChain(file="../utils/soap_handlers_chain.xml")
public class ToteV1 {
    @Resource
    WebServiceContext wsctx;

    @WebMethod(exclude=true)
    @PostConstruct
    protected void init()
    {
        if( worker == null )
        {
            worker = ToteRemoteLocator.getRemoteIface( ConfigLoader.getCfg() );
        }
        
        if( secProvider == null )
        {
            secProvider = AuthenticateRemoteLocator.getRemoteIface( ConfigLoader.getCfg() );
        }
    }
    
    @WebMethod()
    public ToteCardListResponse     findCurrentCards()
    {
        // Authentication first
        LoginResponce authResp = HttpAuthentication.doAuthentication(wsctx, secProvider);

        if( !authResp.isOK()) {
            return new ToteCardListResponse(authResp);
        }
        else {
            return worker.findCurrentCards(authResp.getToken());
        }
    }
    
    @WebMethod()
    public ToteCardResponse         getCard(@XmlElement(required=true)  @WebParam(name="id") Long id)
    {
        // Authentication first
        LoginResponce authResp = HttpAuthentication.doAuthentication(wsctx, secProvider);

        if( !authResp.isOK()) {
            return new ToteCardResponse(authResp);
        }
        else {
            return worker.getCard(authResp.getToken(), id);
        }
    }
    
    public GenericResponse          setCardInUse(@XmlElement(required=true) @WebParam(name="id") Long id, 
                                                 @XmlElement(required=true) @WebParam(name="isInUse") Boolean isInUse )
    {
        // Authentication first
        LoginResponce authResp = HttpAuthentication.doAuthentication(wsctx, secProvider);

        if( !authResp.isOK()) {
            return new GenericResponse(authResp);
        }
        else {
            return worker.setCardInUse(authResp.getToken(), id, isInUse);
        }
    }
    
    private ToteInterface worker = null;
    private AuthenticateInterface secProvider = null;
}
