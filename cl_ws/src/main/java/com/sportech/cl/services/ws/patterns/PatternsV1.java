package com.sportech.cl.services.ws.patterns;

import javax.annotation.Resource;
import javax.annotation.PostConstruct;
import javax.jws.HandlerChain;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.ws.WebServiceContext;


import com.sportech.cl.model.common.PageParams;
import com.sportech.cl.model.database.Pattern;
import com.sportech.cl.model.database.PatternOutline;
import com.sportech.cl.model.response.AddEntityResponse;
import com.sportech.cl.model.response.GenericResponse;
import com.sportech.cl.model.response.LoginResponce;
import com.sportech.cl.model.response.PagedResponse;
import com.sportech.cl.model.response.PatternResponse;
import com.sportech.cl.model.response.PatternListResponse;
import com.sportech.cl.model.response.PatternUsedListResponse;
import com.sportech.cl.services.ws.utils.HttpAuthentication;
import com.sportech.cl.services.patterns.PatternsInterface;
import com.sportech.cl.services.patterns.PatternsRemoteLocator;
import com.sportech.cl.services.security.AuthenticateInterface;
import com.sportech.cl.services.security.AuthenticateRemoteLocator;
import com.sportech.cl.global.utils.config.ConfigLoader;

@WebService(targetNamespace="http://www.sportech.net/cl/services/patterns/v1/")
@HandlerChain(file="../utils/soap_handlers_chain.xml")
public class PatternsV1 {
	@Resource
	WebServiceContext wsctx;

	@WebMethod(exclude=true)
	@PostConstruct
	protected void init()
	{
		if( worker == null )
		{
			worker = PatternsRemoteLocator.getRemoteIface( ConfigLoader.getCfg() );
		}
		
		if( secProvider == null )
		{
			secProvider = AuthenticateRemoteLocator.getRemoteIface( ConfigLoader.getCfg() );
		}
	}
	
	@WebMethod()
	public PatternResponse	getPattern(@XmlElement(required=true) @WebParam(name="id") Long id)
	{
		// Authentication first
		LoginResponce authResp = HttpAuthentication.doAuthentication(wsctx, secProvider);

		if( !authResp.isOK()) {
			return new PatternResponse(authResp);
		}
		else {
			return worker.getPattern(authResp.getToken(), id);
		}
	}

    @WebMethod()
    public PagedResponse<PatternListResponse> findPatterns(@WebParam(name="query")  PatternOutline[] query,
                                                           @WebParam(name="like")  Boolean like,
                                                           @WebParam(name="pages") PageParams pages)
    {
        // Authentication first
        LoginResponce authResp = HttpAuthentication.doAuthentication(wsctx, secProvider);

        if( !authResp.isOK()) {
            return new PagedResponse<PatternListResponse>( new PatternListResponse(authResp) );
        }
        else {
            PatternListResponse resp = worker.find(authResp.getToken(), query, like, pages);
            return new PagedResponse<PatternListResponse>( resp, pages );
        }
    }

    @WebMethod()
    public PatternUsedListResponse findPatterns4HardCard(@XmlElement(required=true) @WebParam(name="hardCardId") Long    hardCardId )
    {
        // Authentication first
        LoginResponce authResp = HttpAuthentication.doAuthentication(wsctx, secProvider);

        if( !authResp.isOK()) {
            return new PatternUsedListResponse(authResp);
        }
        else {
            return worker.find4HardCard(authResp.getToken(), hardCardId);
        }
    }
    
	@WebMethod()
	public AddEntityResponse	addPattern(@XmlElement(required=true) @WebParam(name="pattern")Pattern 		pattern)
	{
		// Authentication first
		LoginResponce authResp = HttpAuthentication.doAuthentication(wsctx, secProvider);
		
		if( !authResp.isOK()) {
			return new AddEntityResponse(authResp);
		}
		
		else {
			return worker.addPattern(authResp.getToken(), pattern);
		}
		
	}
	
	@WebMethod()
	public GenericResponse		updatePattern(@XmlElement(required=true) @WebParam(name="id")Long id, 
	                                          @XmlElement(required=true) @WebParam(name="pattern")Pattern pattern)
	{
		// Authentication first
		LoginResponce authResp = HttpAuthentication.doAuthentication(wsctx, secProvider);
		
		if( !authResp.isOK()) {
			return authResp;
		}
		
		else {
			return worker.updatePattern(authResp.getToken(), id, pattern);
		}
	}

	@WebMethod()
	public GenericResponse	deletePattern(@XmlElement(required=true) @WebParam(name="id") Long id)
	{
		// Authentication first
		LoginResponce authResp = HttpAuthentication.doAuthentication(wsctx, secProvider);

		if( !authResp.isOK()) {
			return new GenericResponse(authResp);
		}
		else {
			return worker.deletePattern(authResp.getToken(), id);
		}
	}

	private PatternsInterface worker = null;
	private AuthenticateInterface secProvider = null;
}
