package com.sportech.cl.services.ws.utils;

import java.util.Set;
import java.io.StringWriter; 

import javax.xml.namespace.QName;
import javax.xml.soap.SOAPPart;
import javax.xml.ws.handler.MessageContext;
import javax.xml.ws.handler.soap.SOAPHandler;
import javax.xml.ws.handler.soap.SOAPMessageContext;
import javax.servlet.http.HttpServletRequest;

import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Node;
import org.apache.log4j.Logger;

import com.sportech.cl.global.utils.config.ConfigLoader;

public class SOAPLoggingHandler implements SOAPHandler<SOAPMessageContext> {
    Logger logger = Logger.getLogger(SOAPLoggingHandler.class);
    
    @Override
    public Set<QName> getHeaders() 
    {
        return null;
    }

    @Override
    public void close(MessageContext ctx) 
    {
    }
    
    @Override
    public boolean handleFault(SOAPMessageContext ctx) 
    {
        try {
            if( ConfigLoader.getCfg().getCommonConfig().getLogSOAP() )
                logMessage( "SOAP fault", true, ctx );
        }
        catch( Exception e ) {
            logger.error("Exception in handleFault():\n", e );
        }

        return true;
    }

    @Override
    public boolean handleMessage(SOAPMessageContext ctx) 
    {
        try {
            if( ConfigLoader.getCfg().getCommonConfig().getLogSOAP() )
            {
                Boolean outMsg = (Boolean)ctx.get(MessageContext.MESSAGE_OUTBOUND_PROPERTY);
                
                if(!outMsg) {
                    logMessage( "SOAP request", false, ctx );
                } else {
                    logMessage( "SOAP response", false, ctx );
                }
            }
        }
        catch( Exception e ) {
            logger.error("Exception in handleMessage():\n", e );
        }

        return true;
    }

    /*-------------------------------------------------------------------------
     * Some private logging helpers
     *-----------------------------------------------------------------------*/

    private void logMessage(String prefix, boolean isError, SOAPMessageContext ctx)
                            throws  Exception
    {
        HttpServletRequest  req = (HttpServletRequest)ctx.get(MessageContext.SERVLET_REQUEST);
        String              clientAddr = req.getRemoteAddr() + ":" + req.getRemotePort();

        HttpAuthentication.AuthDecodeResponse rsp = HttpAuthentication.getAuthenticationDetails( req );                 

        SOAPPart            soap = ctx.getMessage().getSOAPPart();
        String              msg2log = prefix + "\n";
        
        msg2log += "Client : " + clientAddr + "\n";
        
        if( rsp.isOK() && rsp.getUsername() != null )
            msg2log += "User : " + rsp.getUsername() + "\n";
            
        msg2log += "Body : \n" + doc2str(soap);
        
        if( isError )
            logger.error(msg2log);
        else
            logger.info(msg2log);
    }
    
    private static String doc2str(Node doc)
    throws Exception
    {
        String rawXML = null;

        DOMSource domSource = new DOMSource(doc);
        StringWriter wrt = new StringWriter();
        StreamResult streamResult = new StreamResult(wrt);
        TransformerFactory tf = TransformerFactory.newInstance();
        Transformer serializer = tf.newTransformer();

        serializer.setOutputProperty(OutputKeys.METHOD, "xml");
        serializer.setOutputProperty(OutputKeys.INDENT, "yes");
        serializer.transform(domSource, streamResult);

        rawXML = wrt.toString();

        return rawXML;
    }
}
