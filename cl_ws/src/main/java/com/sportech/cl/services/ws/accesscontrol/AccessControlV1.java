package com.sportech.cl.services.ws.accesscontrol;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.jws.HandlerChain;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.ws.WebServiceContext;

import com.sportech.cl.global.utils.config.ConfigLoader;
import com.sportech.cl.model.database.Ace;
import com.sportech.cl.model.response.AceListResponse;
import com.sportech.cl.model.response.GenericResponse;
import com.sportech.cl.model.response.LoginResponce;
import com.sportech.cl.services.security.AccessControlInterface;
import com.sportech.cl.services.security.AccessControlRemoteLocator;
import com.sportech.cl.services.security.AuthenticateRemote;
import com.sportech.cl.services.security.AuthenticateRemoteLocator;
import com.sportech.cl.services.ws.utils.HttpAuthentication;

@WebService(targetNamespace="http://www.sportech.net/cl/services/accesscontrol/v1/")
@HandlerChain(file="../utils/soap_handlers_chain.xml")
public class AccessControlV1 {
    @Resource
    WebServiceContext wsctx;

    @WebMethod(exclude=true)
    @PostConstruct
    protected void init()
    {
        if( worker == null )
        {
            worker = AccessControlRemoteLocator.getRemoteIface( ConfigLoader.getCfg() );
        }
        
        if( secProvider == null )
        {
            secProvider = AuthenticateRemoteLocator.getRemoteIface( ConfigLoader.getCfg() );
        }
    }
    
    @WebMethod()
    public AceListResponse      getObjectACL(@XmlElement(required=true)  @WebParam(name="objTypeId") Integer objTypeId, 
                                             @XmlElement(required=true)  @WebParam(name="objId")     Long objId)
    {
        // Authentication first
        LoginResponce authResp = HttpAuthentication.doAuthentication(wsctx, secProvider);

        if( !authResp.isOK()) {
            return new AceListResponse(authResp);
        }
        else {
            return worker.getObjectACL(authResp.getToken(), objTypeId, objId);
        }
    }
    
    @WebMethod()
    public GenericResponse      setObjectACL( @XmlElement(required=true)  @WebParam(name="objTypeId") Integer objTypeId, 
                                              @XmlElement(required=true)  @WebParam(name="objId")     Long objId,
                                              @XmlElement(required=true)  @WebParam(name="ace")       List<Ace> acl )
    {
        // Authentication first
        LoginResponce authResp = HttpAuthentication.doAuthentication(wsctx, secProvider);

        if( !authResp.isOK()) {
            return new GenericResponse(authResp);
        }
        else {
            return worker.setObjectACL(authResp.getToken(), objTypeId, objId, acl);
        }
    }

    @WebMethod()
    public AceListResponse      getObjectTypeACL(@XmlElement(required=true)  @WebParam(name="objTypeId") Integer objTypeId) 
    {
        // Authentication first
        LoginResponce authResp = HttpAuthentication.doAuthentication(wsctx, secProvider);

        if( !authResp.isOK()) {
            return new AceListResponse(authResp);
        }
        else {
            return worker.getObjectTypeACL(authResp.getToken(), objTypeId);
        }
    }
    
    @WebMethod()
    public GenericResponse      setObjectTypeACL( @XmlElement(required=true)  @WebParam(name="objTypeId") Integer objTypeId, 
                                                  @XmlElement(required=true)  @WebParam(name="ace")       List<Ace> acl )
    {
        // Authentication first
        LoginResponce authResp = HttpAuthentication.doAuthentication(wsctx, secProvider);

        if( !authResp.isOK()) {
            return new GenericResponse(authResp);
        }
        else {
            return worker.setObjectTypeACL(authResp.getToken(), objTypeId, acl);
        }
    }

    @WebMethod()
    public AceListResponse      getDefaultACL(@XmlElement(required=true)  @WebParam(name="objTypeId") Integer objTypeId) 
    {
        // Authentication first
        LoginResponce authResp = HttpAuthentication.doAuthentication(wsctx, secProvider);

        if( !authResp.isOK()) {
            return new AceListResponse(authResp);
        }
        else {
            return worker.getDefaultACL(authResp.getToken(), objTypeId);
        }
    }
    
    @WebMethod()
    public GenericResponse      setDefaultACL( @XmlElement(required=true)  @WebParam(name="objTypeId") Integer objTypeId, 
                                                  @XmlElement(required=true)  @WebParam(name="ace")       List<Ace> acl )
    {
        // Authentication first
        LoginResponce authResp = HttpAuthentication.doAuthentication(wsctx, secProvider);

        if( !authResp.isOK()) {
            return new GenericResponse(authResp);
        }
        else {
            return worker.setDefaultACL(authResp.getToken(), objTypeId, acl);
        }
    }
    
    private AccessControlInterface worker = null;
    @EJB
    AuthenticateRemote secProvider;
    //private AuthenticateInterface secProvider = null;    
}
