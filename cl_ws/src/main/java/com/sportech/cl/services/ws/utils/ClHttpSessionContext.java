package com.sportech.cl.services.ws.utils;

import java.io.Serializable;

import com.sportech.cl.model.security.SecurityToken;

public class ClHttpSessionContext implements Serializable 
{
	private static final long serialVersionUID = -1L;

	// This constant defines session inactivity interval after which 
	// credentials are checked again 
	public static final int	INACTIVITY_INTERVAL_SEC	=	300;

	public ClHttpSessionContext() {
		this.token = null;
		this.lastOperTimeMs = System.currentTimeMillis();
	}

	public ClHttpSessionContext(SecurityToken	_token) 
	{
		this.token = _token;
		this.lastOperTimeMs = System.currentTimeMillis();
	}
	
	/*
	 * Data accessors (must be to make class serializable)
	 */
	public SecurityToken getToken() {
		return token;
	}

	public void setToken(SecurityToken token) {
		this.token = token;
	}

	public long getLastOperTimeMs() {
		return lastOperTimeMs;
	}
	public void setLastOperTimeMs(long lastOperTimeMs) {
		this.lastOperTimeMs = lastOperTimeMs;
	}
	
	public String toString() {
		String str = "";
		if(token != null) {
			str += token.toString();
		}
		else {
			str += "NONE";
		}
		
		str += ", lastOperTimeMs:" + lastOperTimeMs;
		
		return str;
	}
	
	/*
	 * Data members
	 */
	
	// Client security token which was used for initial call
	private SecurityToken	token;
	
	// Last call system time in milliseconds
	private long lastOperTimeMs; 

}
