package com.sportech.cl.services.ws.tote;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.jws.HandlerChain;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.ws.WebServiceContext;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import com.sportech.cl.global.utils.config.ConfigLoader;
import com.sportech.cl.model.common.ErrorCode;
import com.sportech.cl.model.database.Runner;
import com.sportech.cl.model.database.ToteCard;
import com.sportech.cl.model.database.TotePool;
import com.sportech.cl.model.database.ToteRace;
import com.sportech.cl.model.database.utils.PoolHelper;
import com.sportech.cl.model.database.utils.PropertyHelper;
import com.sportech.cl.model.entity.Card;
import com.sportech.cl.model.entity.Pool;
import com.sportech.cl.model.entity.Race;
import com.sportech.cl.model.response.GenericResponse;
import com.sportech.cl.model.response.LoginResponce;
import com.sportech.cl.model.response.ToteCardListResponse;
import com.sportech.cl.model.response.ToteCardResponse;
import com.sportech.cl.model.response.ToteGetCardResponse;
import com.sportech.cl.services.security.AuthenticateInterface;
import com.sportech.cl.services.security.AuthenticateRemoteLocator;
import com.sportech.cl.services.tote.ToteInterface;
import com.sportech.cl.services.tote.ToteRemoteLocator;
import com.sportech.cl.services.ws.utils.HttpAuthentication;
import com.sportech.common.model.PoolType;
import com.sportech.common.util.DateHelper;

@WebService(targetNamespace = "http://www.sportech.net/cl/services/tote/v2/")
@HandlerChain(file = "../utils/soap_handlers_chain.xml")
public class ToteV2 {

    static private final Logger log = Logger.getLogger(ToteV2.class);

    @Resource
    private WebServiceContext wsctx;

    private ToteInterface worker = null;
    private AuthenticateInterface secProvider = null;

    @WebMethod(exclude = true)
    @PostConstruct
    protected void init() {
        if (worker == null) {
            worker = ToteRemoteLocator.getRemoteIface(ConfigLoader.getCfg());
        }

        if (secProvider == null) {
            secProvider = AuthenticateRemoteLocator.getRemoteIface(ConfigLoader.getCfg());
        }
    }

    @WebMethod()
    public ToteCardListResponse findCurrentCards() {
        // Authentication first
        LoginResponce authResp = HttpAuthentication.doAuthentication(wsctx, secProvider);

        if (!authResp.isOK()) {
            return new ToteCardListResponse(authResp);
        } else {
            return worker.findCurrentCards(authResp.getToken());
        }
    }

    @WebMethod()
    public ToteGetCardResponse getCard(@XmlElement(required = true) @WebParam(name = "id") Long id) {
        ToteGetCardResponse response = new ToteGetCardResponse();
        LoginResponce authResp = HttpAuthentication.doAuthentication(wsctx, secProvider);

        if (!authResp.isOK()) {
            response.setResult(ErrorCode.E_AUTH_INV_USER_PWD, "Error authenticating.");
        } else {
            ToteCardResponse cardResponse = worker.getCard(authResp.getToken(), id);
            if (cardResponse.isOK()) {
                Card card = new Card();
                populateCard(card, cardResponse.getCard());
                response.setCard(card);
            }
        }
        return response;
    }

    public GenericResponse setCardInUse(@XmlElement(required = true) @WebParam(name = "id") Long id,
            @XmlElement(required = true) @WebParam(name = "isInUse") Boolean isInUse) {
        // Authentication first
        LoginResponce authResp = HttpAuthentication.doAuthentication(wsctx, secProvider);

        if (!authResp.isOK()) {
            return new GenericResponse(authResp);
        } else {
            return worker.setCardInUse(authResp.getToken(), id, isInUse);
        }
    }

    private void populateCard(Card card, ToteCard tc) {
        try {
            PropertyHelper.populateValues(card, tc, new String[] { "cardDate", "eventCode" });
        } catch (Exception e) {
            log.error("Error populating card", e);
        }
        card.setId(tc.getId());
        card.setCardDate(DateHelper.dateToStr(tc.getCardDate()));
        card.setItspEventCode(tc.getEventCode());
        card.setPoolMinimum(tc.getPoolMinimum());
        card.setSource(tc.getSource());
        if (tc.getTrack() != null) {
            card.setCountryCode(tc.getTrack().getCountryCode());
            card.setName(tc.getTrack().getName());
            card.setTrackCode(tc.getTrack().getEquibaseId());
            card.setTrackName(tc.getTrack().getName());
            card.setMeetingCodeUK(tc.getTrack().getToteTrackId());
        }
        if (tc.getRaces() != null) {
            Set<Race> races = new HashSet<Race>();
            Map<Long, Race> raceMap = new HashMap<Long, Race>();
            card.setRaces(races);
            for (ToteRace tr : tc.getRaces()) {

                Race race = new Race();
                race.setPools(new HashSet<Pool>());
                race.setCardDate(tr.getCardDate());
                race.setBreed(tr.getBreed());
                race.setCourse(tr.getCourse());
                race.setDescription(tr.getRaceNameLong());
                race.setDistanceText(tr.getDistanceText());
                race.setDistance(tr.getDistance());
                race.setDistanceUnit(tr.getDistanceUnit());
                race.setEnabled(true);
                race.setEventIdentifier(tr.getEventId());
                race.setHandicap(tr.getHandicap());
                race.setPostTime(tr.getPostTime());
                race.setPurse(tr.getPurse());
                race.setRaceName(tr.getRaceNameShort());
                race.setRaceNumber(tr.getNumber());
                race.setRaceType(tr.getRaceType());
                race.setTimeZone(tr.getTimeZone());
                race.setTrackTimeZone(tr.getTrackTimeZone());
                race.setMirrorFrom(tr.getMirrorFrom());
                race.setNumberPositions(tr.getRace().getNumberPositions());
                Set<Runner> runners = new HashSet<Runner>();
                for (Runner runner : tr.getRunners()) {
                    runners.add(runner);
                }
                race.setRunners(runners);

                races.add(race);
                raceMap.put(race.getRaceNumber(), race);
            }
            if (tc.getPools() != null) {
                for (TotePool tp : tc.getPools()) {
                    int poolId = tp.getPoolTypeId();
                    PoolType poolType = PoolType.fromId(poolId);
                    if (poolType != null) {
                        Set<Long> raceSet = new HashSet<Long>();
                        Long first = PoolHelper.getRacesFromBitmap(tp.getRaceBitmap(), raceSet);

                        if (poolType.isMultiRace()) {
                            Pool pool = createPool(raceMap, first, poolType, tp);
                            pool.setRaceNumbers(StringUtils.join(raceSet, ","));
                            raceMap.get(first).getPools().add(pool);
                        } else {
                            for (Long l : raceSet) {
                                Pool pool = createPool(raceMap, l, poolType, tp);
                                pool.setRaceNumbers("" + l);
                                raceMap.get(l).getPools().add(pool);
                            }
                        }
                    }
                }
            }
        }
    }

    private static Pool createPool(Map<Long, Race> raceMap, Long raceNumber, PoolType poolType, TotePool tp) {
        Pool pool = new Pool();
        try {
            PropertyUtils.copyProperties(pool, tp);
        } catch (Exception e) {
            log.error("Error populating pool fields", e);
        }
        pool.setCardLoaderPoolTypeId(poolType.getId());
        pool.setTotePoolTypeId(poolType.getToteId());
        pool.setSpecialPoolCode(tp.getPoolCode());
        pool.setSpecialPoolName(tp.getPoolName());

        if (poolType == PoolType.PLC) {
            pool.setNumberPositions(2);
            Race race = raceMap.get(raceNumber);
            if (race != null && race.getNumberPositions() != null) {
                pool.setNumberPositions(race.getNumberPositions());
            }
        }

        return pool;
    }

}
