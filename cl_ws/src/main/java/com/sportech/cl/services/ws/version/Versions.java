package com.sportech.cl.services.ws.version;

import javax.annotation.PostConstruct;
import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.HandlerChain;

import com.sportech.cl.global.utils.config.ConfigLoader;
import com.sportech.cl.model.common.Version;
import com.sportech.cl.model.response.VersionResponse;
import com.sportech.cl.services.common.VersionsInterface;
import com.sportech.cl.services.common.VersionsRemoteLocator;

@WebService(targetNamespace="http://www.sportech.net/cl/services/version")
@HandlerChain(file="../utils/soap_handlers_chain.xml")
public class Versions
{
    private static final String FRONT_END_VERSION = "1.0.0.01"; 
    
    @WebMethod(exclude=true)
    @PostConstruct
    protected void init()
    {
        if( worker == null )
        {
            worker = VersionsRemoteLocator.getRemoteIface( ConfigLoader.getCfg() );
        }
    }
    
    
    @WebMethod()
    public VersionResponse     getVersions()
    {
        VersionResponse rsp = worker.getVersions();
        
        if( rsp.isOK() )
        {
            rsp.getVersion().addComponent(new Version("Middleware Front-End", FRONT_END_VERSION));
        }
        
        return rsp;
    }
    
    private VersionsInterface worker = null;
    
}
