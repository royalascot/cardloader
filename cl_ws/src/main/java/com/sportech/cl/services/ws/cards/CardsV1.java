package com.sportech.cl.services.ws.cards;

import java.util.Date;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.jws.HandlerChain;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.ws.WebServiceContext;

import com.sportech.cl.global.utils.config.ConfigLoader;
import com.sportech.cl.model.common.PageParams;
import com.sportech.cl.model.database.HardCard;
import com.sportech.cl.model.database.HardCardOutline;
import com.sportech.cl.model.database.SoftCard;
import com.sportech.cl.model.database.SoftCardOutline;
import com.sportech.cl.model.response.AddEntityResponse;
import com.sportech.cl.model.response.GenericResponse;
import com.sportech.cl.model.response.HardCardListResponse;
import com.sportech.cl.model.response.HardCardResponse;
import com.sportech.cl.model.response.LoginResponce;
import com.sportech.cl.model.response.PagedResponse;
import com.sportech.cl.model.response.SoftCardListResponse;
import com.sportech.cl.model.response.SoftCardResponse;
import com.sportech.cl.model.security.SecurityToken;
import com.sportech.cl.services.cards.HardCardsInterface;
import com.sportech.cl.services.cards.HardCardsRemoteLocator;
import com.sportech.cl.services.cards.SoftCardsInterface;
import com.sportech.cl.services.cards.SoftCardsRemoteLocator;
import com.sportech.cl.services.security.AuthenticateInterface;
import com.sportech.cl.services.security.AuthenticateRemoteLocator;
import com.sportech.cl.services.ws.utils.HttpAuthentication;

@WebService(targetNamespace = "http://www.sportech.net/cl/services/cards/v1/")
@HandlerChain(file = "../utils/soap_handlers_chain.xml")
public class CardsV1 {
	@Resource
	WebServiceContext wsctx;

	@WebMethod(exclude = true)
	@PostConstruct
	protected void init() {
		if (hardCardWorker == null) {
			hardCardWorker = HardCardsRemoteLocator.getRemoteIface(ConfigLoader.getCfg());
		}

		if (softCardWorker == null) {
			softCardWorker = SoftCardsRemoteLocator.getRemoteIface(ConfigLoader.getCfg());
		}

		if (secProvider == null) {
			secProvider = AuthenticateRemoteLocator.getRemoteIface(ConfigLoader.getCfg());
		}
	}

	@WebMethod()
	public HardCardResponse getHardCard(@XmlElement(required = true) @WebParam(name = "id") Long id) {
		// Authentication first
		LoginResponce authResp = HttpAuthentication.doAuthentication(wsctx, secProvider);

		if (!authResp.isOK()) {
			return new HardCardResponse(authResp);
		} else {
			return hardCardWorker.get(authResp.getToken(), id);
		}
	}

	@WebMethod()
	public PagedResponse<HardCardListResponse> findHardCards(@WebParam(name = "query") HardCardOutline[] query,
	        @WebParam(name = "startDate") Date startDate, @WebParam(name = "endDate") Date endDate,
	        @WebParam(name = "like") Boolean like, @WebParam(name = "pages") PageParams pages) {
		// Authentication first
		LoginResponce authResp = HttpAuthentication.doAuthentication(wsctx, secProvider);

		if (!authResp.isOK()) {
			return new PagedResponse<HardCardListResponse>(new HardCardListResponse(authResp));
		} else {
			HardCardListResponse resp = hardCardWorker.find(authResp.getToken(), query, startDate, endDate, like, pages);
			return new PagedResponse<HardCardListResponse>(resp, pages);
		}
	}

	@WebMethod()
	public AddEntityResponse addHardCard(@XmlElement(required = true) @WebParam(name = "card") HardCard card) {
		return hardCardWorker.add(SecurityToken.getDummyToken(), card);
	}

	@WebMethod()
	public GenericResponse updateHardCard(@XmlElement(required = true) @WebParam(name = "id") Long id,
	        @XmlElement(required = true) @WebParam(name = "card") HardCard new_card) {
		// Authentication first
		LoginResponce authResp = HttpAuthentication.doAuthentication(wsctx, secProvider);

		if (!authResp.isOK()) {
			return authResp;
		}

		else {
			return hardCardWorker.update(authResp.getToken(), id, new_card);
		}
	}

	@WebMethod()
	public GenericResponse deleteHardCard(@XmlElement(required = true) @WebParam(name = "id") Long id) {
		// Authentication first
		LoginResponce authResp = HttpAuthentication.doAuthentication(wsctx, secProvider);

		if (!authResp.isOK()) {
			return new GenericResponse(authResp);
		} else {
			return hardCardWorker.delete(authResp.getToken(), id);
		}
	}

	@WebMethod()
	public SoftCardResponse getSoftCard(@XmlElement(required = true) @WebParam(name = "id") Long id) {
		// Authentication first
		LoginResponce authResp = HttpAuthentication.doAuthentication(wsctx, secProvider);

		if (!authResp.isOK()) {
			return new SoftCardResponse(authResp);
		} else {
			return softCardWorker.get(authResp.getToken(), id);
		}
	}

	@WebMethod()
	public PagedResponse<SoftCardListResponse> findSoftCards(@WebParam(name = "query") SoftCardOutline[] query,
	        @WebParam(name = "startDate") Date startDate, @WebParam(name = "endDate") Date endDate,
	        @WebParam(name = "like") Boolean like, @WebParam(name = "pages") PageParams pages) {
		// Authentication first
		LoginResponce authResp = HttpAuthentication.doAuthentication(wsctx, secProvider);

		if (!authResp.isOK()) {
			return new PagedResponse<SoftCardListResponse>(new SoftCardListResponse(authResp));
		} else {
			SoftCardListResponse resp = softCardWorker.find(authResp.getToken(), query, startDate, endDate, like, pages);
			return new PagedResponse<SoftCardListResponse>(resp, pages);
		}
	}

	@WebMethod()
	public AddEntityResponse addSoftCard(@XmlElement(required = true) @WebParam(name = "card") SoftCard card) {
		// Authentication first
		LoginResponce authResp = HttpAuthentication.doAuthentication(wsctx, secProvider);

		if (!authResp.isOK()) {
			return new AddEntityResponse(authResp);
		}

		else {
			return softCardWorker.add(authResp.getToken(), card);
		}

	}

	@WebMethod()
	public GenericResponse updateSoftCard(@XmlElement(required = true) @WebParam(name = "id") Long id,
	        @XmlElement(required = true) @WebParam(name = "card") SoftCard new_card) {
		// Authentication first
		LoginResponce authResp = HttpAuthentication.doAuthentication(wsctx, secProvider);

		if (!authResp.isOK()) {
			return authResp;
		}

		else {
			return softCardWorker.update(authResp.getToken(), id, new_card);
		}
	}

	@WebMethod()
	public GenericResponse deleteSoftCard(@XmlElement(required = true) @WebParam(name = "id") Long id) {
		// Authentication first
		LoginResponce authResp = HttpAuthentication.doAuthentication(wsctx, secProvider);

		if (!authResp.isOK()) {
			return new GenericResponse(authResp);
		} else {
			return softCardWorker.delete(authResp.getToken(), id);
		}
	}

	@WebMethod()
	public GenericResponse approveSoftCard(@XmlElement(required = true) @WebParam(name = "id") Long id,
	        @XmlElement(required = true) @WebParam(name = "approved") Boolean approved) {
		// Authentication first
		LoginResponce authResp = HttpAuthentication.doAuthentication(wsctx, secProvider);

		if (!authResp.isOK()) {
			return authResp;
		}

		else {
			return softCardWorker.approve(authResp.getToken(), id, approved);
		}
	}

	@WebMethod()
	public GenericResponse approveHardCard(@XmlElement(required = true) @WebParam(name = "id") Long id) {
		// Authentication first
		LoginResponce authResp = HttpAuthentication.doAuthentication(wsctx, secProvider);

		if (!authResp.isOK()) {
			return authResp;
		}

		else {
			return hardCardWorker.approve(authResp.getToken(), id, true);
		}
	}

	@WebMethod()
	public AddEntityResponse createFromHardCard(@XmlElement(required = true) @WebParam(name = "hardCardId") Long hardCardId,
	        @WebParam(name = "patternId") Long patternId, @WebParam(name = "mergeTypeId") Integer mergeTypeId,
	        @WebParam(name = "onlyPatternRaces") Boolean onlyPatternRaces) {
		// Authentication first
		LoginResponce authResp = HttpAuthentication.doAuthentication(wsctx, secProvider);

		if (!authResp.isOK()) {
			return new AddEntityResponse(authResp);
		} else {
			return softCardWorker.createFromHardCard(authResp.getToken(), hardCardId, patternId, mergeTypeId, onlyPatternRaces,
			        false);
		}
	}

	@WebMethod()
	public AddEntityResponse publishFromHardCard(@XmlElement(required = true) @WebParam(name = "hardCardId") Long hardCardId,
	        @WebParam(name = "patternId") Long patternId, @WebParam(name = "mergeTypeId") Integer mergeTypeId,
	        @WebParam(name = "onlyPatternRaces") Boolean onlyPatternRaces) {
		// Authentication first
		LoginResponce authResp = HttpAuthentication.doAuthentication(wsctx, secProvider);

		if (!authResp.isOK()) {
			return new AddEntityResponse(authResp);
		} else {
			return softCardWorker.createFromHardCard(authResp.getToken(), hardCardId, patternId, mergeTypeId, onlyPatternRaces,
			        true);
		}
	}

	private HardCardsInterface hardCardWorker = null;
	private SoftCardsInterface softCardWorker = null;
	private AuthenticateInterface secProvider = null;

}
