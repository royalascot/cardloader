package com.sportech.cl.services.ws.utils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.xml.ws.WebServiceContext;
import javax.xml.ws.handler.MessageContext;

import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.Logger;

import com.sportech.cl.model.common.ErrorCode;
import com.sportech.cl.model.response.GenericResponse;
import com.sportech.cl.model.response.LoginResponce;
import com.sportech.cl.services.security.AuthenticateInterface;

public class HttpAuthentication {

	static private final Logger log = Logger.getLogger(HttpAuthentication.class);
	
	private static final String CL_SESS_CTX_ATTR_NAME = "ClSessCtx";

	/**
	 * This method is used by all Web Services to do authentication for the
	 * call.
	 * 
	 * @param wsctx
	 *            - Web Service context
	 * @param securityProvider
	 *            - Security provider to validate credentials passed
	 * @return - operation result.
	 * 
	 *         TODO: Cleanup logging
	 */
	public static LoginResponce doAuthentication(HttpServletRequest rctx, AuthenticateInterface securityProvider) {
		boolean loginRequired = false;
		ClHttpSessionContext sessCtx = null;
		LoginResponce result = new LoginResponce(ErrorCode.SUCCESS);

		//
		// Get HTTPSession first and check if inactivity interval is still less
		// than
		// required for a new login... If HttpSessionContext objects wasn't yet
		// set
		// in HttpSession or maximum inactivity interval is reached than do
		// login
		// sequence, otherwise treat session still valid and do not login
		// again...
		//
		HttpSession session = rctx.getSession();
		if (session != null) {
			log.debug("HTTP session found!!! - " + session.getId());
			sessCtx = (ClHttpSessionContext) session.getAttribute(CL_SESS_CTX_ATTR_NAME);
			log.debug("sessCtx = " + (sessCtx == null ? "null" : sessCtx.toString()));
			if (sessCtx == null
			        || (System.currentTimeMillis() - sessCtx.getLastOperTimeMs()) > ClHttpSessionContext.INACTIVITY_INTERVAL_SEC * 1000) {
				loginRequired = true;
				sessCtx = new ClHttpSessionContext();
			} else {
				sessCtx.setLastOperTimeMs(System.currentTimeMillis());
				result.setToken(sessCtx.getToken());
			}
		} else {
			loginRequired = true;
		}

		//
		// Do login sequence if required.
		// Get Authorization header first with encoded username/password
		//
		if (loginRequired) {
			log.debug("LOGIN REQUIRED!!!");

			AuthDecodeResponse rsp = getAuthenticationDetails(rctx);

			if (rsp.isOK()) {
				try {
					log.info("Tote Login as user:" + rsp.getUsername());
					result = securityProvider.loginTote(rsp.getUsername(), rsp.getPassword());
				} catch (Exception e) {
					e.printStackTrace();
					result.setResult(ErrorCode.FAIL);
				}

				if (result.isOK()) {
					if (sessCtx != null) {
						sessCtx.setToken(result.getToken());
					}
				}
			} else {
				result = new LoginResponce(rsp);
			}
		}

		// Save session context in session in case of success
		if (result.isOK() && (session != null) && (sessCtx != null)) {
			log.debug("Context saved!!!");
			session.setAttribute(CL_SESS_CTX_ATTR_NAME, sessCtx);
			log.info("Tote logged in successfully as user " + result.getToken().getLoginName());
		}

		return result;
	}

	public static LoginResponce doAuthentication(WebServiceContext wsctx, AuthenticateInterface securityProvider) {
		return doAuthentication((HttpServletRequest) wsctx.getMessageContext().get(MessageContext.SERVLET_REQUEST),
		        securityProvider);
	}

	public static class AuthDecodeResponse extends GenericResponse {
		protected String username;
		protected String password;

		public AuthDecodeResponse() {
		}

		public AuthDecodeResponse(ErrorCode code, String... _errors) {
			super(code, _errors);
		}

		public AuthDecodeResponse(String _username, String _password) {
			username = _username;
			password = _password;
		}

		public String getUsername() {
			return username;
		}

		public String getPassword() {
			return password;
		}

		private static final long serialVersionUID = -1L;
	}

	public static AuthDecodeResponse getAuthenticationDetails(HttpServletRequest ctx) {
		String authHdr = ctx.getHeader("Authorization");

		if (authHdr == null) {
			// No authentication headers found
			return new AuthDecodeResponse(ErrorCode.E_AUTH_NO_HEADERS);
		} else {
			//
			// Authorization header must have the following format:
			// "Basic <base64Encode(username:password)>"
			// Take username/password substring and decode it
			//
			String userPass = authHdr;
			userPass = userPass.substring(5);
			byte[] buf = Base64.decodeBase64(userPass.getBytes());
			String credentials = new String(buf);
			int p = credentials.indexOf(":");
			if (p > -1) {
				String username = credentials.substring(0, p);
				String password = credentials.substring(p + 1);

				return new AuthDecodeResponse(username, password);
			} else
				return new AuthDecodeResponse(ErrorCode.E_AUTH_UNABLE_TO_DECODE);
		}
	}

}
