package com.sportech.cl.internal.businesslogic;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.hibernate.Session;

import com.sportech.cl.internal.utils.PoolsHelper;
import com.sportech.cl.model.common.PoolMergeType;
import com.sportech.cl.model.comparator.SoftRaceComparator;
import com.sportech.cl.model.database.HardCard;
import com.sportech.cl.model.database.HardPool;
import com.sportech.cl.model.database.HardRace;
import com.sportech.cl.model.database.Pattern;
import com.sportech.cl.model.database.PatternPool;
import com.sportech.cl.model.database.PatternTote;
import com.sportech.cl.model.database.PoolTableCell;
import com.sportech.cl.model.database.PoolTableRow;
import com.sportech.cl.model.database.SoftCard;
import com.sportech.cl.model.database.SoftPool;
import com.sportech.cl.model.database.SoftRace;
import com.sportech.cl.model.database.SoftTote;
import com.sportech.cl.model.database.utils.PoolHelper;
import com.sportech.cl.model.rule.RuleCondition;
import com.sportech.cl.model.rule.RuleContext;
import com.sportech.cl.model.rule.RuleExecutor;

public class SoftcardGenerator {

	static private final Logger log = Logger.getLogger(SoftcardGenerator.class);
	
	public static SoftCard generateMirrorCard(Session ss, HardCard hc, Pattern pt) {
	    log.info("Generating mirror card for " + hc.getDescription());
        SoftCard detail = new SoftCard();
        detail.setApproved(false);
        detail.setDescription("Mirror Card " + hc.getDescription());
        detail.setHardCardId(hc.getId());
        detail.setPatternId(pt.getId());
        detail.setPatternDescription(pt.getDescription());
        detail.setCardDate(hc.getCardDate());
        detail.setTrack(hc.getTrack());
        detail.setCardType(hc.getCardType());
        detail.setPerfType(hc.getPerfType());
        detail.setConfig(hc.getConfig());
        
        detail.setLoadCode(pt.getLoadCode());
        
        detail.setRaces(new TreeSet<SoftRace>(new SoftRaceComparator()));
        detail.setPools(new HashSet<SoftPool>());
        HashSet<Long> sourceCards = new HashSet<Long>();
        for (HardRace hr : hc.getRaces()) {
            if (StringUtils.isEmpty(hr.getCloneSource())) {
                log.error("Can not find mirror source for race " + hr.getNumber());
                return null;
            }
            String[] parts = hr.getCloneSource().split("\\|");
            if (parts.length < 3 || StringUtils.isEmpty(parts[1]) || StringUtils.isEmpty(parts[2])) {
                log.error("Can not find mirror source for race " + hr.getNumber());
                return null;               
            }
            
            long cid = Long.parseLong(parts[1]);
            long raceNumber = Long.parseLong(parts[2]);
            List<SoftCard> cards = findSoftCards(ss, cid);
            if (cards == null || cards.size() == 0) {
                log.error("Can not find source load card " + cid);
                return null;                               
            }
            SoftCard sourceCard = cards.get(0);
            sourceCards.add(sourceCard.getId());
            for (SoftRace sr : sourceCard.getRaces()) {
                if (sr.getNumber().longValue() == raceNumber) {
                    SoftRace r = new SoftRace();
                    r.setNumber(hr.getNumber());
                    r.setParentRace(sr);
                    r.setSource("" + hr.getNumber() + "-(" + sourceCard.getLoadCode() + ")-" + raceNumber);
                    r.setParent(detail);
                    detail.getRaces().add(r);
                    detail.setRaceCount(detail.getRaces().size());
                    break;
                }
            }            
        }
        // Create Pools
        for (HardPool hp : hc.getPools()) {
            SoftPool sp = new SoftPool(hp);
            sp.setParent(detail);
            detail.getPools().add(sp);
        }

        // Copy 'allowed Totes'
        if (pt != null) {
            Set<SoftTote> totes = new HashSet<SoftTote>();
            for (PatternTote pt_t : pt.getTotes()) {
                SoftTote sc_t = new SoftTote();

                sc_t.setToteId(pt_t.getToteId());

                totes.add(sc_t);
            }

            detail.setTotes(totes);
            detail.setToteName(pt.getToteList());
        }
        detail.setSource(StringUtils.join(sourceCards, ","));
        detail.setChildParents();
        return detail;
    } 
	
	public static SoftCard generateSoftCard(HardCard hc, Pattern pt, PoolMergeType mergeType,
	        boolean onlyPatternRaces) {
		PoolsHelper pools_helper = new PoolsHelper();
		if (mergeType == PoolMergeType.HARD_CARD_ONLY) {
			onlyPatternRaces = false;
		}

		log.debug("Generating load cards.");

		int maxRaceNum = onlyPatternRaces ? pt.getRaceCount() : hc.getRaces().size();
		long maxRaceBitmask = 0xFFFFFFFFFFFFFFFFL >>> (Long.SIZE - maxRaceNum);

		//
		// Pre-processing: remove unwanted races from Hard Card
		//
		Set<HardRace> races_to_remove = new HashSet<HardRace>();

		for (HardRace hr : hc.getRaces()) {
			if (hr.getNumber() > maxRaceNum)
				races_to_remove.add(hr);
		}

		hc.getRaces().removeAll(races_to_remove);

		//
		// Pre-processing: remove unwanted pools from Hard Card
		//
		Set<HardPool> hp_to_remove = new HashSet<HardPool>();

		for (HardPool hp : hc.getPools()) {
			long bitmask = hp.getRaceBitmap();

			if (!pools_helper.isMultiRacePool(hp.getPoolTypeId()))
				hp.setRaceBitmap(bitmask & maxRaceBitmask);
			else if ((bitmask & maxRaceBitmask) != bitmask)
				hp_to_remove.add(hp);
		}
		hc.getPools().removeAll(hp_to_remove);

		//
		// Pre-processing: remove unwanted pools from Pattern
		//
		if (pt != null) {
			Set<PatternPool> pp_to_remove = new HashSet<PatternPool>();

			for (PatternPool pp : pt.getPools()) {
				long bitmask = pp.getRaceBitmap();

				if (!pools_helper.isMultiRacePool(pp.getPoolTypeId()))
					pp.setRaceBitmap(bitmask & maxRaceBitmask);
				else if ((bitmask & maxRaceBitmask) != bitmask)
					pp_to_remove.add(pp);
			}
			pt.getPools().removeAll(pp_to_remove);
		}

		//
		// Create Soft Card, copy 'basic' properties
		//
		SoftCard sc = new SoftCard();

		String description = pt.getLoadCode();
		String hostDescription = hc.getDescription();
		String countryCode = pt.getTrack().getCountryCode();
		sc.setDescription(StringUtils.abbreviate(description + "(" + countryCode + "):" + hostDescription, 40));
		sc.setFeatureRace(hc.getFeatureRace());
		sc.setSparseRaces(false);
		sc.setSparsePools(false);
		sc.setHardCardId(hc.getId());
		sc.setCardType(hc.getCardType());
		sc.setPerfType(hc.getPerfType());
		sc.setConfig(hc.getConfig());
		sc.setTrack(hc.getTrack());
		sc.setCardDate(hc.getCardDate());
		sc.setSource(null);
		
		if (pt != null) {
			sc.setPatternId(pt.getId());
			sc.setLoadCode(pt.getLoadCode());
		}

		sc.setApproved(false);

		// Create soft races

		Set<SoftRace> s_races = new HashSet<SoftRace>();

		for (HardRace hr : hc.getRaces()) {
			SoftRace sr = new SoftRace();
			sr.setHardRace(hr);
			sr.setNumber(hr.getNumber());
			s_races.add(sr);
		}

		sc.setRaces(s_races);

		/*
		 * Process pools.
		 */
		Set<SoftPool> s_pools = new HashSet<SoftPool>();

		if (mergeType == PoolMergeType.HARD_CARD_ONLY) {
			for (HardPool hp : hc.getPools())
				s_pools.add(new SoftPool(hp));
		} else if (mergeType == PoolMergeType.PATTERN_ONLY) {
			for (PatternPool pp : pt.getPools())
				s_pools.add(new SoftPool(pp));
		} else {
			// First of all, copy pools from Hard Card
			for (HardPool hp : hc.getPools())
				s_pools.add(new SoftPool(hp));

			if (pt != null) {
				Set<SoftPool> pools_to_add = new HashSet<SoftPool>();
				Set<SoftPool> pools_to_remove = new HashSet<SoftPool>();

				if (mergeType == PoolMergeType.HARD_CARD_PRIORITY || mergeType == PoolMergeType.PATTERN_PRIORITY) {
					for (PatternPool pp : pt.getPools()) {
						boolean addPatternPool = pools_helper.isMultiRacePool(pp.getPoolTypeId()) ? true : false;
						boolean found = false;
						for (SoftPool sp : s_pools) {
							if (sp.getPoolTypeId().equals(pp.getPoolTypeId())) {
								found = true;
								if ((!sp.getRaceBitmap().equals(pp.getRaceBitmap()))) {
									if (!pools_helper.isMultiRacePool(pp.getPoolTypeId())) {
										sp.setRaceBitmap(sp.getRaceBitmap() | pp.getRaceBitmap());
									} else {
										if (pools_helper
										        .poolsInConflict(sp.getRaceBitmap(), pp.getRaceBitmap(), pp.getPoolTypeId())) {
											if (mergeType == PoolMergeType.PATTERN_PRIORITY) {
												pools_to_remove.add(sp);
											} else {
												addPatternPool = false;
											}
										}
									}
								}
							}
						}
						if (!found) {
							addPatternPool = true;
						}
						if (addPatternPool) {
							pools_to_add.add(new SoftPool(pp));
						}
					}
				} else if (mergeType == PoolMergeType.ONLY_IN_PATTERN || mergeType == PoolMergeType.NOT_IN_PATTERN) {

					for (SoftPool sp : s_pools) {

						boolean matchFound = false;

						for (PatternPool pp : pt.getPools()) {
							if (sp.getPoolTypeId().equals(pp.getPoolTypeId())) {
								if (!pools_helper.isMultiRacePool(sp.getPoolTypeId())) { // Single-race
									                                                     // pools
									matchFound = true;
									if (mergeType == PoolMergeType.ONLY_IN_PATTERN)
										sp.setRaceBitmap(sp.getRaceBitmap() & pp.getRaceBitmap());
									else
										sp.setRaceBitmap(sp.getRaceBitmap() & (~pp.getRaceBitmap()));
								} else { // Multi-race pools
									if (sp.getRaceBitmap().equals(pp.getRaceBitmap())) {
										matchFound = true;
										break;
									}
								}
							}
						}

						if ((mergeType == PoolMergeType.ONLY_IN_PATTERN && !matchFound)
						        || (mergeType == PoolMergeType.NOT_IN_PATTERN && matchFound)) {
							pools_to_remove.add(sp);
						}

					}
				}

				s_pools.addAll(pools_to_add);
				s_pools.removeAll(pools_to_remove);
			}

		}

		sc.setPools(s_pools);

		filterRaces(hc, pt, sc);

		if (mergeType != PoolMergeType.PATTERN_ONLY) {
			if (sc.getPools() != null) {
				int cycleTimes = sc.getRaceCount() * sc.getPools().size();
				for (int i = 0; i < cycleTimes; i++) {
					if (!populatePoolNames(sc, pt)) {
						break;
					}
				}
			}
		}

		// Copy 'allowed Totes'
		if (pt != null) {
			Set<SoftTote> sc_totes = new HashSet<SoftTote>();
			for (PatternTote pt_t : pt.getTotes()) {
				SoftTote sc_t = new SoftTote();

				sc_t.setToteId(pt_t.getToteId());

				sc_totes.add(sc_t);
			}

			sc.setTotes(sc_totes);
			sc.setToteName(pt.getToteList());
		}

		// And, finally, save Soft Card
		sc.setChildParents();

		return sc;
	}

	private static void filterRaces(HardCard hc, Pattern p, SoftCard card) {
		if (p != null) {
			List<RuleCondition> rules = p.getRaceRules();
			if (rules != null) {
				for (RuleCondition r : rules) {
					applyRule(hc, r, card);
				}
			}
		}
	}

	
	private static void applyRule(HardCard hc, RuleCondition rule, SoftCard card) {
		if (card != null) {
			Set<SoftRace> races = card.getRaces();
			
			for (SoftRace r : races) {
			    RuleContext context = new RuleContext();
			    context.setCard(hc);
			    context.setRace(r.getHardRace());
			    context.setTarget(card);
			    RuleExecutor.executeRule(context, rule);
			}
		}
	}

	private static boolean populatePoolNames(SoftCard sc, Pattern pt) {
		PoolHelper cardHelper = new PoolHelper(sc);
		PoolHelper patternHelper = new PoolHelper(pt);
		cardHelper.setupSortedPools(sc.getPools());
		patternHelper.setupSortedPools(pt.getPools());
		if (pt.getRaceCount() > 0) {
			Collection<PoolTableRow> cardRows = cardHelper.getPoolTableRows(sc.getRaceCount());
			Collection<PoolTableRow> patternRows = patternHelper.getPoolTableRows(pt.getRaceCount());
			for (PoolTableRow row : patternRows) {
				long raceNumber = row.getRaceNumber();
				PoolTableRow cardRow = null;
				for (PoolTableRow cr : cardRows) {
					if (cr.getRaceNumber() == raceNumber) {
						cardRow = cr;
						break;
					}
				}
				if (cardRow != null) {
					for (Integer poolTypeId : row.getPoolTypeIds()) {
						Integer cardPoolTypeId = null;
						for (Integer ptid : cardRow.getPoolTypeIds()) {
							if (poolTypeId.intValue() == ptid.intValue()) {
								cardPoolTypeId = ptid;
								break;
							}
						}
						if (cardPoolTypeId != null) {
							PoolTableCell patternCell = row.getCell(cardPoolTypeId);
							PoolTableCell cardCell = cardRow.getCell(cardPoolTypeId);
							if (patternCell != null && cardCell != null && !StringUtils.isEmpty(patternCell.getPoolCode())) {
								if (!StringUtils.equals(patternCell.getPoolCode(), cardCell.getPoolCode())) {
									cardCell.setPoolCode(patternCell.getPoolCode());
									cardCell.setPoolName(patternCell.getPoolName());
									PoolHelper.updatePoolProperties(sc, cardCell);
									return true;
								}
							}
						}
					}

				}
			}
		}
		return false;
	}
	
	@SuppressWarnings("unchecked")
    private static List<SoftCard> findSoftCards(Session ss, Long id) {
	    List<SoftCard> cards = (List<SoftCard>) ss.createQuery("from SoftCard where hardCardId = :id").setLong("id", id).list();
	    return cards;
	}
	
}
