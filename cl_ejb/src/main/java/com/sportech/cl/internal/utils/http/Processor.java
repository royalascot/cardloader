package com.sportech.cl.internal.utils.http;

import java.io.IOException;
import java.io.InputStream;

public interface Processor {
	
	public void process(InputStream s) throws IOException;
	
}
