package com.sportech.cl.internal.validators;

import com.sportech.cl.model.response.GenericResponse;
import com.sportech.cl.model.database.utils.Persistent;

public interface Validator<T extends Persistent<T>>
{
    public boolean Validate( T obj, boolean create, GenericResponse rsp);
}
