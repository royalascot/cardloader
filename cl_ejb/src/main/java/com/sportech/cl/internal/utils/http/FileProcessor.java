package com.sportech.cl.internal.utils.http;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Date;

import org.apache.commons.io.IOUtils;
import org.hibernate.Session;

import com.sportech.cl.model.database.FtpImport;

public class FileProcessor implements Processor {

	private String fileName;
	private Session session;
	private String incomingPath;

	public FileProcessor(Session ss, String incomingPath, String name) {
		this.incomingPath = incomingPath;
		this.fileName = name;
		this.session = ss;
	}

	public void process(InputStream instream) throws IOException {
		File newFile = new File(incomingPath, fileName);
		FileOutputStream fos = new FileOutputStream(newFile);
		IOUtils.copy(instream, fos);
		fos.close();
		if (newFile.length() > 0) {
			FtpImport i = new FtpImport();
			i.setChecksum(0L);
			i.setFileDate(new Date());
			i.setFilename(fileName);
			i.setTimestamp(new Date());
			session.save(i);
			session.flush();
		} else {
			newFile.delete();
		}
	}
}
