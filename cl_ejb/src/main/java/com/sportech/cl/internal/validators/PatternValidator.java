package com.sportech.cl.internal.validators;


import org.apache.commons.lang3.StringUtils;

import com.sportech.cl.model.database.Pattern;
import com.sportech.cl.model.database.PatternTote;
import com.sportech.cl.model.response.GenericResponse;
import com.sportech.cl.model.common.ErrorCode;
import com.sportech.cl.internal.utils.PoolsHelper;

public class PatternValidator implements Validator<Pattern>
{
    public PatternValidator()                               { pools_helper = new PoolsHelper(); }  
    public PatternValidator( PoolsHelper _pools_helper )    { pools_helper = _pools_helper; }
    
    public boolean Validate( Pattern pat, boolean create, GenericResponse rsp)
    {
        if ( !create && (pat.getId() == null) ) { 
            rsp.setResult(ErrorCode.E_VAL_FAILED, "Pattern ID is empty");
        }
        if ( StringUtils.isEmpty(pat.getDescription())) {
            rsp.setResult(ErrorCode.E_VAL_FAILED, "Pattern description is empty");
        }
        if ( StringUtils.isEmpty(pat.getLoadCode())) {
        	rsp.setResult(ErrorCode.E_VAL_FAILED, "Pattern load code is empty");
        }
        
        for( PatternTote tote : pat.getTotes())
        {
            if( (tote.getToteId() == null) )
                rsp.setResult(ErrorCode.E_VAL_FAILED, "Tote ID is empty");
        }
        
        pools_helper.validateCompactPools(pat.getPools(), rsp);
        
        return rsp.isOK();
    }
    
    private PoolsHelper pools_helper;    
}
