package com.sportech.cl.internal.utils;

import org.apache.commons.codec.digest.DigestUtils;

import com.sportech.cl.model.security.ObjectType;
import com.sportech.cl.model.security.Privilege;
import com.sportech.cl.model.security.SecurityToken;

public class SecurityHelper {	
	
	public static String	getHashedPassword( String password )
	{
		return DigestUtils.sha256Hex(password);
	}
	
	public static boolean checkAccess(SecurityToken token, Privilege priv)
	{	
		return true;
	}

	public static boolean checkAccess(SecurityToken token, Privilege priv, Long objId, ObjectType objType, Long accessMask)
	{
		if( !checkAccess(token, priv))
			return false;
		
		return checkAccess(token, objId, objType, accessMask);
	}

	public static boolean checkAccess(SecurityToken token, Long objId, ObjectType objType, Long accessMask)
	{   
		return true;
	}
	
}
