package com.sportech.cl.internal.utils;

import java.net.URI;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.apache.log4j.Logger;
import org.hibernate.Session;

import com.sportech.cl.global.utils.config.FtpConfig;
import com.sportech.cl.internal.utils.http.FileProcessor;
import com.sportech.cl.internal.utils.http.HttpHelper;
import com.sportech.cl.model.database.utils.DateUtils;

public class AustraliaHelper {

	static private final Logger log = Logger.getLogger(AustraliaHelper.class);

	private static final String[] tracks = new String[] { "AUC", "AUS", "AUB" };
	private static final String prefix = "http://";
	private static final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");

	private FtpConfig config;

	public AustraliaHelper(FtpConfig config) {
		this.config = config;
	}

	public synchronized void checkNewFiles(Session ss) {
		try {
			log.info("Downloading new files from Australia track info");
			URI loginUri = new URI(prefix + config.getFtpServer() + "/pages/login/8.php");
			HttpHelper helper = new HttpHelper(config);
			helper.login(loginUri);
			Date today = new Date();
			for (int i = 0; i < 4; i++) {
				Date fileDate = DateUtils.addOffset(today, Calendar.DAY_OF_MONTH, i);
				String fileDateString = dateFormat.format(fileDate);
				for (String track : tracks) {
					String fileName = "SIMD" + fileDateString + track + "_AUS.XML";
					FileProcessor fp = new FileProcessor(ss, config.getDownloadPath(), fileName);
					URI downloadUri = new URI(prefix + config.getFtpServer() + "/data_download.php?url=" + fileName);
					helper.getWithCookies(downloadUri, fp);
				}
				// Download NZ file
				String fileName = "SIMD" + fileDateString + "AUS_NZ.XML";
				FileProcessor fp = new FileProcessor(ss, config.getDownloadPath(), fileName);
				URI downloadUri = new URI(prefix + config.getFtpServer() + "/data_download.php?url=" + fileName);
				helper.getWithCookies(downloadUri, fp);
			}
			helper.shutdown();
		} catch (Exception e) {
			log.error("Error login", e);
		}
	}

}
