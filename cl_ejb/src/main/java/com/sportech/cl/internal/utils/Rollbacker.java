package com.sportech.cl.internal.utils;

import javax.annotation.Resource;
import javax.ejb.SessionContext;
import javax.interceptor.AroundInvoke;
import javax.interceptor.InvocationContext;

import com.sportech.cl.model.response.GenericResponse;

public class Rollbacker
{
    @Resource SessionContext ctx;
    
    @AroundInvoke
    public Object   checkAndRollback( InvocationContext invctx ) throws Exception
    {
        Object retval = invctx.proceed();
        
        if( retval instanceof GenericResponse )
        {
            GenericResponse rsp = (GenericResponse)retval;
            
            if( !rsp.isOK() )
            {
                ctx.setRollbackOnly();
            }
        }
        
        return retval;
    }
}
