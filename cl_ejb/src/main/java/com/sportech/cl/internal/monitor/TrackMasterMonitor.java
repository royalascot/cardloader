package com.sportech.cl.internal.monitor;

import java.util.Date;
import java.util.concurrent.TimeUnit;

import javax.annotation.PostConstruct;
import javax.ejb.AccessTimeout;
import javax.ejb.Schedule;
import javax.ejb.Singleton;
import javax.persistence.PersistenceContext;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.hibernate.Session;

import com.sportech.cl.global.utils.config.ConfigLoader;
import com.sportech.cl.global.utils.config.FtpConfig;
import com.sportech.cl.internal.messages.MessagingHelper;
import com.sportech.cl.internal.utils.FtpHelper;
import com.sportech.cl.model.database.utils.DateUtils;
import com.sportech.cl.model.messaging.CommandMessage;

@Singleton
public class TrackMasterMonitor {

	static private final Logger log = Logger.getLogger(TrackMasterMonitor.class);

	private String checkTime;

	private boolean checkSite = false;

	private FtpHelper helper = null;

	@PersistenceContext
    private Session ss;
	
	@PostConstruct
	public void init() {
		FtpConfig config = ConfigLoader.getCfg().getTrackmasterConfig();
		if (config == null) {
			log.info("No Track Master configuration. Downloading from Track Master is disabled.");
			checkSite = false;
			return;
		}
		checkTime = config.getFtpCheckTime();
		if (StringUtils.isNotEmpty(checkTime)) {
			checkSite = true;
			log.info("Check Track Master FTP site at:" + checkTime);
			helper = new FtpHelper(config);
		} else {
			checkSite = false;
			log.info("Error or missing 'ftp-check-site-time' configuration. Downloading from Equibase is disabled.");
		}

	}

	@Schedule(minute = "*", hour = "*", persistent = false)
	@AccessTimeout(value = 10, unit = TimeUnit.MINUTES)
	public void checkFiles() {
		if (checkSite) {
			if (DateUtils.isLaunchTime(checkTime)) {
				MessagingHelper.addCommandMessage(CommandMessage.TrackMaster);
			}
		}
	}

	@javax.ejb.Lock(javax.ejb.LockType.WRITE)
	@AccessTimeout(value = 9, unit = TimeUnit.MINUTES)
	public void checkNewFiles() {
		Date currentTime = new Date();
		log.info("Starting downloading new files at " + currentTime);
		if (helper != null) {
			helper.checkNewFiles(ss);
		}
		log.info("Finished downloading new files");
	}

}
