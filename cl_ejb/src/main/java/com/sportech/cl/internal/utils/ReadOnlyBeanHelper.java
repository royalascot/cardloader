package com.sportech.cl.internal.utils;

import java.util.Date;

import org.apache.log4j.Logger;
import org.hibernate.Session;

import com.sportech.cl.model.common.ErrorCode;
import com.sportech.cl.model.common.PageParams;
import com.sportech.cl.model.database.utils.EntityWithId;
import com.sportech.cl.model.database.utils.SortOrder;
import com.sportech.cl.model.response.ObjectListResponse;
import com.sportech.cl.model.response.ObjectResponse;
import com.sportech.cl.model.security.ObjectType;
import com.sportech.cl.model.security.Permission;
import com.sportech.cl.model.security.Privilege;
import com.sportech.cl.model.security.SecurityToken;

public class ReadOnlyBeanHelper<EntityType extends EntityWithId, EntityOutlineType extends EntityWithId>
{
	protected Session ss;
	
    public
    ReadOnlyBeanHelper(Session ss,  
                        ObjectType                          _objType,
                        Privilege                           _readPriv,
                        Class<?>                            _typeClass,
                        Class<?>                            _outlineTypeClass,
                        Logger                              _log)
    {
    	this.ss = ss;
        this.objType = _objType;
        this.readPriv = _readPriv;
        this.typeClass = _typeClass;
        this.outlineTypeClass = _outlineTypeClass;
        this.log = _log;
        this.useLocalSecurity = false;
        this.hasObjectLevelACL = true;
    }

    public void setUseLocalSecurity(boolean useLocalSecurity)
    {
        this.useLocalSecurity = useLocalSecurity;
    }
    
    public void hasObjectLevelACL(boolean hasObjectLevelACL)
    {
        this.hasObjectLevelACL = hasObjectLevelACL;
    }

    public ObjectResponse<EntityType> get(SecurityToken token, Long id, ObjectResponse<EntityType> rsp) 
    {
        try {
           
            if( hasObjectLevelACL ? !SecurityHelper.checkAccess(token, readPriv, id, objType, Permission.READ_PERM) 
                                  : !SecurityHelper.checkAccess(token, readPriv) ) 
            {
                rsp.setResult(ErrorCode.E_ACCESS_DENIED);
            }
            else{
                @SuppressWarnings("unchecked")
                EntityType obj = (EntityType) ss.get( typeClass,id);
                
                if( obj != null )
                {
                    rsp.setResult(ErrorCode.SUCCESS);
                    rsp.setObject(obj);
                }
                else
                {
                    rsp.setResult(ErrorCode.E_NOT_FOUND);
                }
            }
            
        }
        catch( Exception e ) {
            log.error( "Exception caught: ", e );
            
            rsp.setResult( ErrorCode.FAIL);
        }
        return rsp;
    }   
    
    public ObjectListResponse<EntityOutlineType>        find(SecurityToken token, EntityOutlineType[] query, Boolean useLike, 
                                                             PageParams pages, ObjectListResponse<EntityOutlineType> rsp,
                                                             SortOrder[] orderby)
    {
        try {
            
            if( !SecurityHelper.checkAccess(token, readPriv) )
            {
                rsp.setResult(ErrorCode.E_ACCESS_DENIED);
            }
            else
            {
                return SearchHelper.findByExamples(ss, token, query, useLike, pages, outlineTypeClass, objType, rsp, orderby);
            }

        }
        catch( Exception e ) {
            log.error( "Exception caught: ", e );
            
            rsp.setResult( ErrorCode.FAIL);
        }
        
        return rsp;
    }

    public ObjectListResponse<EntityOutlineType>        findByDate(SecurityToken                         token, 
                                                                   EntityOutlineType[]                   query, 
                                                                   Date                                  startDate, 
                                                                   Date                                  endDate, 
                                                                   Boolean                               useLike, 
                                                                   PageParams                            pages,
                                                                   String                                datePropName,
                                                                   ObjectListResponse<EntityOutlineType> rsp,
                                                                   SortOrder[] orderby)
    {
        try {
            
            if( !SecurityHelper.checkAccess(token, readPriv) )
            {
                rsp.setResult(ErrorCode.E_ACCESS_DENIED);
            }
            else
            {
                return SearchHelper.findByDateAndExamples(ss, token, query, startDate, endDate, useLike, pages, outlineTypeClass, datePropName, objType, rsp, orderby);
            }

        }
        catch( Exception e ) {
            log.error( "Exception caught: ", e );
            
            rsp.setResult( ErrorCode.FAIL);
        }
        
        return rsp;
    }
                
    protected   ObjectType                              objType;
    protected   Privilege                               readPriv;
    protected   Class<?>                                typeClass;
    protected   Class<?>                                outlineTypeClass;
    protected   Logger                                  log;
    protected   boolean                                 useLocalSecurity;
    protected   boolean                                 hasObjectLevelACL;
    
}
