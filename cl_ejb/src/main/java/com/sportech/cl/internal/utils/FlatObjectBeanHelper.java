package com.sportech.cl.internal.utils;

import javax.ejb.EJB;

import org.apache.log4j.Logger;
import org.hibernate.Session;

import com.sportech.cl.global.utils.config.ConfigLoader;
import com.sportech.cl.internal.validators.Validator;
import com.sportech.cl.model.common.ErrorCode;
import com.sportech.cl.model.database.utils.EntityWithId;
import com.sportech.cl.model.database.utils.PersistentWithId;
import com.sportech.cl.model.response.AddEntityResponse;
import com.sportech.cl.model.response.GenericResponse;
import com.sportech.cl.model.security.ObjectType;
import com.sportech.cl.model.security.Permission;
import com.sportech.cl.model.security.Privilege;
import com.sportech.cl.model.security.SecurityToken;
import com.sportech.cl.services.security.AccessControlInterface;
import com.sportech.cl.services.security.AccessControlRemoteLocator;

public class FlatObjectBeanHelper<EntityType extends PersistentWithId<EntityType>, EntityOutlineType extends EntityWithId>
                 extends ReadOnlyBeanHelper<EntityType, EntityOutlineType>
{
	private static final String AUDIT_AREA = "Data Access";
	
    public
    FlatObjectBeanHelper(Session ss,
                          ObjectType                          _objType,
                          Privilege                           _readPriv,
                          Privilege                           _writePriv,
                          Class<?>                            _typeClass,
                          Class<?>                            _outlineTypeClass,
                          Logger                              _log)
    {
        super(ss, _objType, _readPriv, _typeClass, _outlineTypeClass, _log );
        
        this.writePriv = _writePriv;
    }
    
    public void setValidator(Validator<EntityType> _validator)
    {
        this.validator = _validator;
    }
    
    public AddEntityResponse add(SecurityToken token, EntityType obj) 
    {
        AddEntityResponse  rsp = new AddEntityResponse();
        
        try {

            if( !SecurityHelper.checkAccess(token, writePriv))
            {
                rsp.setResult(ErrorCode.E_ACCESS_DENIED);
                return rsp;
            }
            
            if( (validator != null) && !validator.Validate(obj, true, rsp))
                return rsp;

            massageObjectBeforeAdd( obj );
        
            ss.save( obj );
            
            AuditHelper.writeAuditLog(ss, token, obj, "Add", AUDIT_AREA);
            
            if( hasObjectLevelACL )
            {
                if( acl_worker == null ) {
                    acl_worker = AccessControlRemoteLocator.getRemoteIface( ConfigLoader.getCfg() );
                }
                
                acl_worker.SetCreator(token, objType, obj.getId());
                acl_worker.CreateDefaultEntries(objType, obj.getId());
            }
            
            ss.flush();
            
            rsp.setId(obj.getId());
            return rsp;
        }
        catch( Exception e ) {
            log.error( "Exception caught: ", e );

            rsp.setResult(ErrorCode.FAIL );
            return rsp;
        }
    }

    public GenericResponse update(SecurityToken token, Long id, EntityType new_obj) 
    {
        GenericResponse    rsp = new GenericResponse();
        
        try {

            if( hasObjectLevelACL ? !SecurityHelper.checkAccess(token, writePriv, id, objType, Permission.WRITE_PERM)
                                  : !SecurityHelper.checkAccess(token, writePriv) )
            {
                rsp.setResult(ErrorCode.E_ACCESS_DENIED);
                return rsp;
            }
            
            if( (validator != null) && !validator.Validate(new_obj, false, rsp))
                return rsp;

            @SuppressWarnings("unchecked")
            EntityType old_obj = (EntityType)ss.get( typeClass,id);

            if( old_obj != null ) {
                old_obj.copy4persistence(new_obj);
                ss.flush();
            }
            else {
                rsp.setResult(ErrorCode.E_NOT_FOUND);
            }
            AuditHelper.writeAuditLog(ss, token, new_obj, "Update", AUDIT_AREA);
            return rsp;
        }
        catch( Exception e ) {
            log.error( "Exception caught: ", e );
            
            rsp.setResult(ErrorCode.FAIL );
            return rsp;
        }
    }
    
    public GenericResponse delete(SecurityToken token, Long id) 
    {
        try {

            if( hasObjectLevelACL ? !SecurityHelper.checkAccess(token, writePriv, id, objType, Permission.DELETE_PERM)
                                  : !SecurityHelper.checkAccess(token, writePriv) )
                return new GenericResponse(ErrorCode.E_ACCESS_DENIED);
            
            @SuppressWarnings("unchecked")
            EntityType obj = (EntityType)ss.get( typeClass,id);

            if( obj != null )
            {
                ss.delete(obj);
                ss.flush();
                AuditHelper.writeAuditLog(ss, token, obj, "Delete", AUDIT_AREA);
                return new GenericResponse();
            }
            else
                return new GenericResponse(ErrorCode.E_NOT_FOUND);
        }
        catch( Exception e ) {
            log.error( "Exception caught: ", e );
            
            return new GenericResponse( ErrorCode.FAIL);
        }
    }
    
    protected void  massageObjectBeforeAdd( EntityType obj ) {} 

    protected   Privilege                               writePriv;
    protected   Validator<EntityType>                   validator;
    
    @EJB
    protected   AccessControlInterface                  acl_worker = null;
    
}
