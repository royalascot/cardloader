package com.sportech.cl.internal.validators;

import com.sportech.cl.model.common.ErrorCode;
import com.sportech.cl.model.database.Track;
import com.sportech.cl.model.response.GenericResponse;

public class TrackValidator implements Validator<Track>
{
    public TrackValidator() {}  
    
    public boolean Validate( Track track, boolean create, GenericResponse rsp)
    {
        if( !create && (track.getId() == null) ) 
            rsp.setResult(ErrorCode.E_VAL_FAILED, "Track ID is empty");
        if( track.getCountryCode() == null )
            rsp.setResult(ErrorCode.E_VAL_FAILED, "Track country code is empty");
        if( track.getName() == null )
            rsp.setResult(ErrorCode.E_VAL_FAILED, "Track name is empty");
        if( track.getToteTrackId() == null )
            rsp.setResult(ErrorCode.E_VAL_FAILED, "Track Tote track ID is empty");
        if( track.getActive() == null )
            rsp.setResult(ErrorCode.E_VAL_FAILED, "Active indicator is empty");
        
        return rsp.isOK();
    }
}
