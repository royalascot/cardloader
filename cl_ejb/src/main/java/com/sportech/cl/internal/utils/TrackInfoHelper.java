package com.sportech.cl.internal.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URI;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import com.sportech.cl.global.utils.config.FtpConfig;
import com.sportech.cl.internal.utils.http.FileProcessor;
import com.sportech.cl.internal.utils.http.Processor;
import com.sportech.cl.model.database.FtpImport;

public class TrackInfoHelper {

	static private final Logger log = Logger.getLogger(TrackInfoHelper.class);
	
	private static final int TIME_OUT = 1000000;
	
	private static final String prefix = "http://";
	private String incomingPath;
	private FtpConfig config;

	private Pattern filePattern = Pattern.compile("^<A HREF=\".*\\.xml\"", Pattern.UNICODE_CASE | Pattern.CASE_INSENSITIVE);

	private Session ss;
	
	public TrackInfoHelper(FtpConfig config, Session ss) {
		this.config = config;
		incomingPath = config.getDownloadPath();
		this.ss = ss;
	}

	public void checkNewFiles() {
		try {
			log.info("Downloading new files from track info");
			URI uri = new URI(prefix + config.getFtpServer() + "/" + config.getFtpDirectoryName());
			readHttpContent(uri, new DirectoryProcessor());
		} catch (Exception e) {
			log.error("Error login", e);
		}
	}

	private class DirectoryProcessor implements Processor {

		@SuppressWarnings("unchecked")
		public void process(InputStream instream) throws IOException {

			BufferedReader reader = new BufferedReader(new InputStreamReader(instream));
			String line;
			while ((line = reader.readLine()) != null) {
				log.debug(line);
				Matcher m = filePattern.matcher(line);
				if (m.find()) {
					String fileName = m.group(0);
					fileName = StringUtils.substring(fileName, 9, fileName.length() - 1);
					log.debug("Found file:" + fileName);
					File newFile = new File(incomingPath, fileName);
					if (newFile.exists()) {
						log.info("File has been downloaded:" + fileName);
						continue;
					}

					Criteria crit = ss.createCriteria(FtpImport.class).add(Restrictions.eq("filename", fileName));

					List<FtpImport> imports = (List<FtpImport>) crit.list();

					if (imports == null || imports.size() == 0) {
						log.info("new file:" + fileName);
						saveFile(ss, fileName);
					} else {
						log.info("Skip file with the same name as an existing one: " + fileName);
						continue;
					}
				}
			}
		}
	};

	private void saveFile(Session ss, String s) throws IOException {
		try {
			URI uri = new URI(prefix + config.getFtpServer() + "/" + config.getFtpDirectoryName() + "/" + s);
			readHttpContent(uri, new FileProcessor(ss, incomingPath, s));
		} catch (Exception e) {
			log.error("Error download file: " + s, e);
		}
	}

	private void readHttpContent(URI uri, Processor processor) throws Exception {

		DefaultHttpClient httpclient = new DefaultHttpClient();

		HttpParams params = httpclient.getParams();
		HttpConnectionParams.setConnectionTimeout(params, TIME_OUT);
		HttpConnectionParams.setSoTimeout(params, TIME_OUT);

		httpclient.getCredentialsProvider().setCredentials(new AuthScope(config.getFtpServer(), 80),
		        new UsernamePasswordCredentials(config.getFtpUsername(), config.getFtpPassword()));

		HttpGet httpget = new HttpGet(uri);
		HttpResponse response = httpclient.execute(httpget);
		log.debug("executing request " + httpget.getRequestLine());

		log.info(response.getStatusLine());

		HttpEntity entity = response.getEntity();
		if (entity != null) {
			InputStream instream = entity.getContent();
			try {
				processor.process(instream);
			} catch (IOException ex) {
				log.error("Error io", ex);
			} catch (RuntimeException ex) {
				httpget.abort();
				log.error("Error io", ex);
			} finally {
				try {
					instream.close();
				} catch (Exception ex) {
					log.error("Error io", ex);
				}
			}
		}
	}

}
