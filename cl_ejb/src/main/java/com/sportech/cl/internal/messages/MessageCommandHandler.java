package com.sportech.cl.internal.messages;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

import javax.ejb.AccessTimeout;
import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.ObjectMessage;
import javax.persistence.PersistenceContext;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import com.sportech.cl.global.utils.config.ConfigLoader;
import com.sportech.cl.global.utils.config.FtpConfig;
import com.sportech.cl.internal.utils.AustraliaHelper;
import com.sportech.cl.internal.utils.FtpHelper;
import com.sportech.cl.internal.utils.TrackHelper;
import com.sportech.cl.internal.utils.TrackInfoHelper;
import com.sportech.cl.model.database.HardCard;
import com.sportech.cl.model.database.HardPool;
import com.sportech.cl.model.database.HardRace;
import com.sportech.cl.model.database.Track;
import com.sportech.cl.model.database.utils.DateUtils;
import com.sportech.cl.model.messaging.CommandMessage;
import com.sportech.common.model.PoolType;
import com.sportech.common.util.DateHelper;

@MessageDriven(activationConfig = {

@ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue"),

@ActivationConfigProperty(propertyName = "destination", propertyValue = "queue/cardloaderQueue")

})
public class MessageCommandHandler implements MessageListener {

	static private final Logger log = Logger.getLogger(MessageCommandHandler.class);
	
	@PersistenceContext
    private Session ss;
	
    private SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm");

    @AccessTimeout(value = 10, unit = TimeUnit.MINUTES)
    public void onMessage(Message message) {
        try {
            final ObjectMessage textMessage = (ObjectMessage) message;
            final Object question = textMessage.getObject();
            CommandMessage cm = (CommandMessage) question;
            if (cm != null) {
                if (StringUtils.equals(cm.getCommand(), CommandMessage.ADDCARD)) {
                    addCard(cm);
                } else if (StringUtils.equals(cm.getCommand(), CommandMessage.Equibase)) {
                    FtpConfig config = ConfigLoader.getCfg().getEquibaseConfig();
                    checkFtp(config);
                } else if (StringUtils.equals(cm.getCommand(), CommandMessage.EquibaseSimulcast)) {
                    FtpConfig config = ConfigLoader.getCfg().getEquibaseSimulcastConfig();
                    checkFtp(config);
                } else if (StringUtils.equals(cm.getCommand(), CommandMessage.TrackMaster)) {
                    checkFtp(ConfigLoader.getCfg().getTrackmasterConfig());
                } else if (StringUtils.equals(cm.getCommand(), CommandMessage.TrackInfo)) {
                    TrackInfoHelper helper = new TrackInfoHelper(ConfigLoader.getCfg().getTrackinfoConfig(), ss);
                    helper.checkNewFiles();
                } else if (StringUtils.equals(cm.getCommand(), "Australia")) {
                    AustraliaHelper helper = new AustraliaHelper(ConfigLoader.getCfg().getAustraliaConfig());
                    helper.checkNewFiles(ss);
                }

            }
        } catch (JMSException e) {
            log.error("Error receiving message", e);
        }
    }

    private void checkFtp(FtpConfig config) {
        Date currentTime = new Date();
        FtpHelper helper = new FtpHelper(config);
        log.info("Starting downloading new files at " + currentTime);
        if (helper != null) {
            helper.checkNewFiles(ss);
        }
        log.info("Finished downloading new files");
    }

    private void convertPostTime(HardCard card) {
        Track track = card.getTrack();
        TimeZone tz = track.getTimeZone();
        Date firstRaceTime = DateUtils.addDate(new Date(), 1000);
        for (HardRace hr : card.getRaces()) {
            hr.setPostTime(DateHelper.getLocalTime(tz, hr.getPostTime()));
            if (hr.getPostTime().getTime() < firstRaceTime.getTime()) {
                firstRaceTime = hr.getPostTime();
            }
        }
        card.setFirstRaceTime(firstRaceTime);
    }

    private void addCard(CommandMessage cm) {

        HardCard eventData = null;
        try {
            eventData = cm.getCard();
            if (eventData == null) {
                log.error("Empty event data");
                return;
            }
        } catch (Exception e) {
            log.error("Error parsing event data", e);
            return;
        }

        Track track = eventData.getTrack();
        if (track == null) {
            log.error("Empty track");
            return;
        }

        TrackHelper helper = new TrackHelper(true);
        try {
            helper.saveTrack(ss, eventData);
            convertPostTime(eventData);
            eventData.setDescription(track.getName() + " - " + timeFormat.format(eventData.getFirstRaceTime()));
            eventData.setApproved(false);
            ss.save(eventData);
            savePools(eventData);
        } catch (Exception e) {
            log.error("Error saving card", e);
        }
        log.info("Added virtual racing event card: " + eventData.getId());
    }

    private void savePools(HardCard card) {
        for (HardRace race : card.getRaces()) {
            log.debug("save race" + race.getNumber());

            String poolIdsList = "1,2,4,5,6";
            if (StringUtils.isEmpty(poolIdsList)) {
                log.error("Race does not have pools.");
                continue;
            }

            String[] poolIds = poolIdsList.split(",");

            for (String poolId : poolIds) {
                log.debug("save pool " + poolId);
                PoolType pool = null;
                pool = PoolType.fromId(Integer.parseInt(poolId));
                if (pool == null) {
                    log.error("Can not find pool with pool id:" + poolId);
                    continue;
                }
                HardPool hardPool;
                int legCount = pool.getRaceCount();
                if (legCount == 1) {
                    hardPool = getHardPool(card, pool);
                    if (hardPool == null) {
                        hardPool = new HardPool();
                        hardPool.setParent(card);
                        hardPool.setPoolTypeId(pool.getId());
                        hardPool.setTotePoolType(pool.getToteId());
                        hardPool.setRaceBitmap(0L);
                    }
                    long raceMap = hardPool.getRaceBitmap();
                    raceMap |= (long) (1 << (race.getNumber() - 1));
                    hardPool.setRaceBitmap(raceMap);
                } else {
                    hardPool = new HardPool();
                    hardPool.setParent(card);
                    hardPool.setPoolTypeId(pool.getId());
                    hardPool.setTotePoolType(pool.getToteId());
                    hardPool.setRaceBitmap(0L);
                    long legs = 0L;
                    for (int i = 0; i < legCount; i++) {
                        legs |= (long) (1 << (race.getNumber() + i - 1));
                    }
                    hardPool.setRaceBitmap(legs);
                }
                ss.save(hardPool);
                ss.flush();
            }
        }
    }

    private HardPool getHardPool(HardCard card, PoolType poolType) {
		Criteria crit = ss.createCriteria(HardPool.class).add(Restrictions.eq("parent", card))
		        .add(Restrictions.eq("poolTypeId", poolType.getId()));
		HardPool pool = (HardPool) crit.uniqueResult();
		return pool;
	}
    
}
