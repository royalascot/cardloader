package com.sportech.cl.internal.validators;

import com.sportech.cl.model.common.ErrorCode;
import com.sportech.cl.model.response.GenericResponse;

import com.sportech.cl.model.database.Tote;

public class ToteValidator implements Validator<Tote>
{
    public ToteValidator() {}  
    
    public boolean Validate( Tote tote, boolean create, GenericResponse rsp)
    {
        if( !create && (tote.getId() == null) ) 
            rsp.setResult(ErrorCode.E_VAL_FAILED, "Tote ID is empty");
        if( tote.getDescription() == null )
            rsp.setResult(ErrorCode.E_VAL_FAILED, "Tote description is empty");
        if( tote.getActive() == null )
            rsp.setResult(ErrorCode.E_VAL_FAILED, "Active indicator is empty");
        
        return rsp.isOK();
    }
}
