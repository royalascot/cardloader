package com.sportech.cl.internal.utils.http;

import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CookieStore;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.protocol.ClientContext;
import org.apache.http.cookie.Cookie;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HTTP;
import org.apache.http.protocol.HttpContext;
import org.apache.log4j.Logger;

import com.sportech.cl.global.utils.config.FtpConfig;

public class HttpHelper {

	static private final Logger log = Logger.getLogger(HttpHelper.class);

	private static final int TIME_OUT = 1000000;

	private FtpConfig config;

	private DefaultHttpClient httpclient;

	private List<Cookie> cookies;

	public HttpHelper(FtpConfig config) {
		this.config = config;
		httpclient = new DefaultHttpClient();
		HttpParams params = httpclient.getParams();
		HttpConnectionParams.setConnectionTimeout(params, TIME_OUT);
		HttpConnectionParams.setSoTimeout(params, TIME_OUT);
		CookieStore cookieStore = new BasicCookieStore();
		HttpContext httpContext = new BasicHttpContext();
		httpContext.setAttribute(ClientContext.COOKIE_STORE, cookieStore);
	}

	public void get(URI uri, Processor processor) throws Exception {

		httpclient.getCredentialsProvider().setCredentials(new AuthScope(config.getFtpServer(), 80),
		        new UsernamePasswordCredentials(config.getFtpUsername(), config.getFtpPassword()));

		HttpGet httpget = new HttpGet(uri);
		HttpResponse response = httpclient.execute(httpget);
		log.debug("executing request " + httpget.getRequestLine());

		log.info(response.getStatusLine());

		HttpEntity entity = response.getEntity();
		if (entity != null) {
			InputStream instream = entity.getContent();
			try {
				processor.process(instream);
			} catch (IOException ex) {
				log.error("Error io", ex);
			} catch (RuntimeException ex) {
				httpget.abort();
				log.error("Error io", ex);
			} finally {
				try {
					instream.close();
				} catch (Exception ex) {
					log.error("Error io", ex);
				}
			}
		}
	}

	public void login(URI uri) throws Exception {

		HttpPost httpost = new HttpPost(uri);
		List<NameValuePair> nvps = new ArrayList<NameValuePair>();
		nvps.add(new BasicNameValuePair("user", config.getFtpUsername()));
		nvps.add(new BasicNameValuePair("password", config.getFtpPassword()));
		httpost.addHeader("Referrer", config.getFtpServer() + "/pages/login/8.php?pid=30");
		httpost.setEntity(new UrlEncodedFormEntity(nvps, HTTP.UTF_8));

		HttpResponse response = httpclient.execute(httpost);
		HttpEntity entity = response.getEntity();

		log.info("Login post form received: " + response.getStatusLine());
		if (entity != null) {
			InputStream is = entity.getContent();
			is.close();
		}

		cookies = httpclient.getCookieStore().getCookies();
		if (cookies.isEmpty()) {
			log.warn("No cookies found");
		} else {
			for (int i = 0; i < cookies.size(); i++) {
				log.info("- " + cookies.get(i).toString());
			}
		}

	}

	public void getWithCookies(URI uri, Processor processor) throws Exception {

		HttpGet httpget = new HttpGet(uri);
		HttpResponse response = httpclient.execute(httpget);
		log.info("executing request " + httpget.getRequestLine());
		log.info(response.getStatusLine());

		HttpEntity entity = response.getEntity();
		if (entity != null) {
			InputStream instream = entity.getContent();
			try {
				processor.process(instream);
			} catch (IOException ex) {
				log.error("Error io", ex);
			} catch (RuntimeException ex) {
				httpget.abort();
				log.error("Error io", ex);
			} finally {
				try {
					instream.close();
				} catch (Exception ex) {
					log.error("Error io", ex);
				}
			}
		}
	}

	public void shutdown() {
		try {
			httpclient.getConnectionManager().shutdown();
		} catch (Exception e) {
			log.error("Error shutdown", e);
		}
	}

}
