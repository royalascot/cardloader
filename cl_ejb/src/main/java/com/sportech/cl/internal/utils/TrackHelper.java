package com.sportech.cl.internal.utils;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import com.sportech.cl.model.database.CardConfig;
import com.sportech.cl.model.database.HardCard;
import com.sportech.cl.model.database.Track;
import com.sportech.cl.utils.importer.ImportException;

@SuppressWarnings("unchecked")
public class TrackHelper {

    static private final Logger log = Logger.getLogger(TrackHelper.class);

    private boolean createNewTrack = false;

    public TrackHelper(boolean createNewTrack) {
        this.createNewTrack = createNewTrack;
    }

    public List<Track> getAllTracks(Session ss) {
        Criteria crit = ss.createCriteria(Track.class);
        crit.add(Restrictions.eq("active", true));
        List<Track> tracks = (List<Track>) crit.list();
        return tracks;
    }
        
    public void saveTrack(Session ss, HardCard eventData) throws ImportException {
        Track track = eventData.getTrack();
        track.setActive(true);
        Criteria crit = ss.createCriteria(Track.class);
        crit.add(Restrictions.eq("active", true));
        crit.add(Restrictions.eq("equibaseId", track.getEquibaseId()));
        if (track.getCountryCode() != null) {
            crit.add(Restrictions.eq("countryCode", track.getCountryCode()));
        } else {
            crit.add(Restrictions.isNull("countryCode"));
        }
        if (track.getPerfType() != null) {
            crit.add(Restrictions.eq("perfType", track.getPerfType()));
        }
        List<Track> tracks = (List<Track>) crit.list();
        if (tracks == null || tracks.size() == 0) {
            createTrack(track, ss);
        } else {
            if (tracks.size() > 1) {
                log.warn("Found multiple tracks for code: " + track.getEquibaseId());
            }
            Track existingTrack = null;
            for (Track t : tracks) {
                if (track.getTrackType() == null) {
                    existingTrack = t;
                    break;
                }
                if (t.getTrackType() == null) {
                    t.setTrackType(track.getTrackType());
                    ss.saveOrUpdate(t);
                    existingTrack = t;
                    break;
                }
                if (t.getTrackType().equals(track.getTrackType())) {
                    existingTrack = t;
                    break;
                }
            }
            if (existingTrack != null) {
                eventData.setTrack(existingTrack);
                if (eventData.getCardType() == null) {
                    eventData.setCardType(existingTrack.getTrackType());
                }
            } else {
                createTrack(track, ss);
            }
        }
    }

    private void createTrack(Track track, Session ss) throws ImportException {
        if (createNewTrack) {
            try {
                if (track.getCardConfig() == null) {
                    List<CardConfig> templates = (List<CardConfig>) ss.createQuery("FROM CardConfig WHERE name = :name")
                            .setString("name", "Default").list();
                    if (templates != null && templates.size() > 0) {
                        track.setCardConfig(templates.get(0));
                    }
                }
                ss.saveOrUpdate(track);
            } catch (Exception e) {
                log.error("Error saving new track", e);
                throw new ImportException("Error saving new track data.");
            }
        } else {
            log.error("Can not find track code:" + track.getEquibaseId());
            throw new ImportException("Can not find track with track code: " + track.getEquibaseId());
        }
    }

}
