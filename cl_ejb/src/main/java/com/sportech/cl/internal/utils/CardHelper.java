package com.sportech.cl.internal.utils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import com.sportech.cl.model.database.HardCard;
import com.sportech.cl.model.database.HardRace;
import com.sportech.cl.model.database.Runner;
import com.sportech.cl.model.database.SoftCard;
import com.sportech.cl.model.database.utils.DateUtils;

public class CardHelper {

	static private final Logger log = Logger.getLogger(CardHelper.class);

	public static HardRace findRace(HardCard card, long num) {
		if (card == null || card.getRaces() == null) {
			return null;
		}
		for (HardRace r : card.getRaces()) {
			if (r.getNumber() != null && r.getNumber() == num) {
				return r;
			}
		}
		return null;
	}
	
	public static Runner findRunner(HardRace race, long num) {
		if (race == null || race.getRunners() == null) {
			return null;
		}
		for (Runner r: race.getRunners()) {
			if (r.getPosition() != null && r.getPosition() == num) {
				return r;
			}
		}
		return null;
	}
	
	@SuppressWarnings("unchecked")
    public static List<HardCard> getCardsForDay(Session ss, Date d, Long trackId) {
		log.debug("Getting card for date " + d);

		Criteria crit = ss.createCriteria(HardCard.class);		
		if (trackId != null) {
			crit.add(Restrictions.eq("track.id", trackId));
		}
		
		Date start = DateUtils.addDate(d, -1);
		Date end = DateUtils.addDate(d, 1);
		crit.add(Restrictions.between("cardDate", start, end));
		List<HardCard> result = (List<HardCard>) crit.list();
		List<HardCard> cards = new ArrayList<HardCard>();
		for (HardCard hc : result) {
			Date cardDate = hc.getCardDate();
			if (DateUtils.isSameDay(d, cardDate)) {
				cards.add(hc);
			}
		}
		return cards;
	}

    @SuppressWarnings("unchecked")
    public static void deleteCard(Session ss, Long id) {

        if (id == null) {
            return;
        }

        try {
            log.info("Deleting host card: " + id);

            Criteria soft = ss.createCriteria(SoftCard.class).add(Restrictions.eq("hardCardId", id));
            List<SoftCard> softCards = (List<SoftCard>) soft.list();

            SQLQuery queryCardInUse = ss.createSQLQuery("delete from cards_in_use where soft_card_fk = :cid");
            SQLQuery querySoftRace = ss.createSQLQuery("delete from soft_race where soft_card_fk = :cid");
            SQLQuery querySoftPool = ss.createSQLQuery("delete from soft_pool where soft_card_fk = :cid");
            SQLQuery querySoftCard = ss.createSQLQuery("delete from soft_card where soft_card_pk = :cid");
            SQLQuery queryRunner = ss
                    .createSQLQuery("delete from runner where hard_race_fk in (select hard_race_pk from hard_race where hard_card_fk = :cid)");
            SQLQuery queryHardRace = ss.createSQLQuery("delete from hard_race where hard_card_fk = :cid");
            SQLQuery queryHardPool = ss.createSQLQuery("delete from hard_pool where hard_card_fk = :cid");
            SQLQuery queryHardCard = ss.createSQLQuery("delete from hard_card where hard_card_pk = :cid");
            SQLQuery queryImport = ss.createSQLQuery("delete from import where import_pk = :cid");

            for (SoftCard sc : softCards) {
                queryCardInUse.setLong("cid", sc.getId());
                queryCardInUse.executeUpdate();
                querySoftRace.setLong("cid", sc.getId());
                querySoftRace.executeUpdate();
                querySoftPool.setLong("cid", sc.getId());
                querySoftPool.executeUpdate();
                querySoftCard.setLong("cid", sc.getId());
                querySoftCard.executeUpdate();
            }

            Criteria hard = ss.createCriteria(HardCard.class).add(Restrictions.eq("id", id));
            List<HardCard> hardCards = (List<HardCard>) hard.list();
            for (HardCard sc : hardCards) {
                queryRunner.setLong("cid", sc.getId());
                queryRunner.executeUpdate();
                queryHardRace.setLong("cid", sc.getId());
                queryHardRace.executeUpdate();
                queryHardPool.setLong("cid", sc.getId());
                queryHardPool.executeUpdate();
                queryHardCard.setLong("cid", sc.getId());
                queryHardCard.executeUpdate();
                if (sc.getImportId() != null) {
                    Criteria sameFileCardsQuery = ss.createCriteria(HardCard.class).add(
                            Restrictions.eq("importId", sc.getImportId()));
                    List<HardCard> sameFileCards = (List<HardCard>) sameFileCardsQuery.list();
                    if (sameFileCards != null && sameFileCards.size() == 0) {
                        log.info("Deleting import entry:" + sc.getImportId());
                        queryImport.setLong("cid", sc.getImportId());
                        queryImport.executeUpdate();
                    }
                }
            }

            log.info("Host card " + id + " and its related entities are deleted.");

        } catch (Exception e) {
            log.error("Error deleting card:", e);
        }

    }

}
