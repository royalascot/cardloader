package com.sportech.cl.internal.monitor;

import java.io.File;
import java.io.FileInputStream;
import java.nio.file.Path;
import java.util.Date;

import javax.annotation.PostConstruct;
import javax.ejb.AccessTimeout;
import javax.ejb.ConcurrencyManagement;
import javax.ejb.ConcurrencyManagementType;
import javax.ejb.EJB;
import javax.ejb.Lock;
import javax.ejb.LockType;
import javax.ejb.Schedule;
import javax.ejb.Singleton;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import com.sportech.cl.global.utils.config.ClConfig;
import com.sportech.cl.global.utils.config.ConfigLoader;
import com.sportech.cl.model.database.Import;
import com.sportech.cl.model.importer.ConverterType;
import com.sportech.cl.services.imports.ImportsBean;

@Singleton
@ConcurrencyManagement(ConcurrencyManagementType.CONTAINER)
public class BetfredFileMonitor {

    static private final Logger log = Logger.getLogger(BetfredFileMonitor.class);

    private long TIME_THRESHOLD = 60000L;

    private String processedFilePath = null;
    private String incomingFilePath = null;
    private String errorFilePath = null;

    private boolean enabled = true;

    @EJB
    ImportsBean importer;

    @PostConstruct
    public void init() {
        try {
            ClConfig config = ConfigLoader.getCfg();
            incomingFilePath = config.getCommonConfig().getBetfredFilePath();
            processedFilePath = config.getCommonConfig().getProcessedFilePath();
            errorFilePath = config.getCommonConfig().getErrorFilePath();

            if (StringUtils.isNotEmpty(incomingFilePath)) {
                enabled = true;
                log.info("Start monitoring Betfred files at:" + incomingFilePath);
            } else {
                enabled = false;
                log.info("Error or missing 'betfred-file-path' configuration. no scanning betfred files.");
            }

            if (StringUtils.isNotEmpty(processedFilePath)) {
                log.info("Proccessed files will be moved to: " + processedFilePath);
            } else {
                log.error("Error or missing 'processed-file-path' configuration.");
            }

            if (StringUtils.isNotEmpty(errorFilePath)) {
                log.info("Error files will be moved to: " + errorFilePath);
            } else {
                log.error("Error or missing 'error-file-path' configuration.");
            }

        } catch (Exception e) {
            log.error("Error initializing file monitor", e);
        }
    }

    @Schedule(minute = "*", hour = "*", persistent = false)
    @Lock(LockType.WRITE)
    @AccessTimeout(value = 0)
    public void checkDirectory() {

        if (!enabled) {
            log.debug("Directory monitoring is disabled.");
            return;
        }

        long current = (new Date()).getTime();
        try {
            File folder = new File(incomingFilePath);
            File[] listOfFiles = folder.listFiles();
            if (listOfFiles != null) {
                for (File child : listOfFiles) {
                    if (current - child.lastModified() < TIME_THRESHOLD) {
                        log.info("Skip fresh new file:" + child);
                        continue;
                    }
                    log.info("Found new file:" + child);
                    try {
                        processFile(child.toPath());
                    } catch (Exception e) {
                        log.error("Error processing file", e);
                    }
                }
            }
        } catch (Exception ex) {
            log.error("Generic Error", ex);
        }
    }

    @Lock(LockType.WRITE)
    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    private void moveFile(String path, File file, String surfix) {
        try {
            File newFile = new File(path, file.getName() + surfix);
            file.renameTo(newFile);
        } catch (Exception e) {
            log.error("Error moving file", e);
        }
    }

    private void processFile(Path path) {
        File file = path.toFile();
        String fileName = file.getName();
        byte[] theData = null;

        try {
            FileInputStream inputStream = new FileInputStream(file);
            theData = IOUtils.toByteArray(inputStream);
            inputStream.close();
            if (theData == null || theData.length == 0) {
                moveFile(errorFilePath, file, ".empty");
                log.error("Empty file: " + fileName);
                return;
            }
        } catch (Exception e) {
            log.error("Failed to open file", e);
            return;
        }

        try {

            Import theImport = new Import();
            theImport.setData(theData);
            theImport.setFilename(fileName);
            theImport.setLog("");
            theImport.setStatus("Unprocessed");

            long id = -1;

            try {

                id = importer.addImportSource(theImport, ConverterType.BETFRED_RMS);

            } catch (Exception e) {
                moveFile(errorFilePath, file, ".error");
                importer.saveErrorLog(theImport);
                return;
            }

            if (id < -1) {
                id = -id;
                moveFile(errorFilePath, file, ".dup_" + id);
                return;
            } else if (id < 0) {
                moveFile(errorFilePath, file, ".error");
                importer.saveErrorLog(theImport);
                return;
            }

            try {
                moveFile(processedFilePath, file, "." + id);
            } catch (Exception e) {
                log.error("Error moving file to processed directory", e);
            }
        } catch (Exception ex) {
            try {
                log.error("Generic error", ex);
                moveFile(errorFilePath, file, ".error");
            } catch (Exception e) {
                log.error("Error moving file to processed directory", e);
            }
        }
    }

}
