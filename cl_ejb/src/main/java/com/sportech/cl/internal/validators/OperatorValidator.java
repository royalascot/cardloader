package com.sportech.cl.internal.validators;

import com.sportech.cl.model.common.ErrorCode;
import com.sportech.cl.model.database.Operator;
import com.sportech.cl.model.response.GenericResponse;

public class OperatorValidator implements Validator<Operator>
{
    public OperatorValidator() {}  
    
    public boolean Validate( Operator op, boolean create, GenericResponse rsp)
    {
        if( !create && (op.getId() == null) ) 
            rsp.setResult(ErrorCode.E_VAL_FAILED, "Operator ID is empty");
        if( op.getFirstName() == null )
            rsp.setResult(ErrorCode.E_VAL_FAILED, "Operator first name is empty");
        if( op.getLastName() == null )
            rsp.setResult(ErrorCode.E_VAL_FAILED, "Operator last name is empty");
        if( op.getActive() == null )
        {
            if( create )
                op.setActive(true);
            else
                rsp.setResult(ErrorCode.E_VAL_FAILED, "Active indicator is empty");
        }
        
        return rsp.isOK();
    }


}
