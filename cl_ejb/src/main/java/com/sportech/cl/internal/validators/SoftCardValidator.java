package com.sportech.cl.internal.validators;

import com.sportech.cl.internal.utils.PoolsHelper;
import com.sportech.cl.model.common.ErrorCode;
import com.sportech.cl.model.database.SoftTote;
import com.sportech.cl.model.database.SoftRace;
import com.sportech.cl.model.database.SoftCard;
import com.sportech.cl.model.response.GenericResponse;

public class SoftCardValidator implements Validator<SoftCard> {

	public SoftCardValidator() {
		pools_helper = new PoolsHelper();
	}

	public SoftCardValidator(PoolsHelper _pools_helper) {
		pools_helper = _pools_helper;
	}

	public boolean Validate(SoftCard sc, boolean create, GenericResponse rsp) {
		if (!create && (sc.getId() == null))
			rsp.setResult(ErrorCode.E_VAL_FAILED, "Card ID is empty");
		if (sc.getDescription() == null)
			rsp.setResult(ErrorCode.E_VAL_FAILED, "Card description is empty");

		for (SoftRace race : sc.getRaces()) {
			if (race.getRace() == null)
				rsp.setResult(ErrorCode.E_VAL_FAILED,
				        "Hard race ID is empty" + (race.getNumber() != null ? " for race No. " + race.getNumber() : ""));

			if (race.getNumber() == null)
				rsp.setResult(ErrorCode.E_VAL_FAILED, "Race number is empty"
				        + (race.getNumber() != null ? " for race with hard race ID " + race.getNumber() : ""));

		}

		for (SoftTote tote : sc.getTotes()) {
			if ((tote.getToteId() == null))
				rsp.setResult(ErrorCode.E_VAL_FAILED, "Tote ID is empty");
		}

		pools_helper.validateCompactPools(sc.getPools(), rsp);

		return rsp.isOK();
	}

	private PoolsHelper pools_helper;

}
