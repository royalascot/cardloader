package com.sportech.cl.internal.utils;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.ClassUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Example;
import org.hibernate.criterion.Junction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import com.sportech.cl.model.common.ErrorCode;
import com.sportech.cl.model.common.PageParams;
import com.sportech.cl.model.database.utils.EntityWithId;
import com.sportech.cl.model.database.utils.SortOrder;
import com.sportech.cl.model.response.ObjectListResponse;
import com.sportech.cl.model.security.ObjectType;
import com.sportech.cl.model.security.SecurityToken;

public class SearchHelper {
	public static <T extends EntityWithId> ObjectListResponse<T> findByExamples(Session ss, SecurityToken token, T[] query, Boolean useLike,
	        PageParams pages, Class<?> resultClass, ObjectType objType, ObjectListResponse<T> rsp, SortOrder[] orderby)
	        throws java.lang.reflect.InvocationTargetException, java.lang.IllegalAccessException {
		return doFind(ss, token, query, null, null, useLike, pages, resultClass, null, objType, rsp, orderby);
	}

	public static <T extends EntityWithId> ObjectListResponse<T> findByDateAndExamples(Session ss, SecurityToken token, T[] query,
	        Date startDate, Date endDate, Boolean useLike, PageParams pages, Class<?> resultClass, String datePropName,
	        ObjectType objType, ObjectListResponse<T> rsp, SortOrder[] orderby) throws java.lang.reflect.InvocationTargetException,
	        java.lang.IllegalAccessException {

		return doFind(ss, token, query, startDate, endDate, useLike, pages, resultClass, datePropName, objType, rsp, orderby);
	}

	@SuppressWarnings("unchecked")
	protected static <T> ObjectListResponse<T> doFind(Session ss, SecurityToken token, T[] query, Date startDate, Date endDate,
	        Boolean useLike, PageParams pages, Class<?> resultClass, String datePropName, ObjectType objType,
	        ObjectListResponse<T> rsp, SortOrder[] orderby) throws java.lang.reflect.InvocationTargetException,
	        java.lang.IllegalAccessException {
		if (null == pages) {
			pages = new PageParams(0, Integer.MAX_VALUE);
		}

		Criteria crit = ss.createCriteria(resultClass).setFirstResult(pages.getFirstResult())
		        .setMaxResults(pages.getMaxResult());
		if ((token.getUser() != null) && !SecurityToken.isAdmin(token)) {
			List<Class<?>> ifaces = ClassUtils.getAllInterfaces(resultClass); 
			if (ifaces != null) {
				for (Class<?> c : ifaces) {
					if (StringUtils.contains(c.getName(), "TrackAccessFilterable")) {
						crit.add(Restrictions.in("trackId", token.getUser().getTrackIds()));
						if (log.isDebugEnabled()) {
							log.debug("Add trackId condition:" + StringUtils.join(token.getUser().getTrackIds(), ","));
						}
						break;
					}
				}
			}
		}
		Junction outerAND = Restrictions.conjunction();

		if (query != null) {
			Junction innerOR = Restrictions.disjunction();

			for (T q : query) {

				if (isEmptyObject(q))
					log.debug("Skipping empty query !");
				else {
					Example stmt = Example.create(q);

					if (useLike != null && useLike)
						stmt.enableLike();
					innerOR.add(stmt);
				}
			}

			outerAND.add(innerOR);
		}

		if (startDate != null || endDate != null) {
			Junction dateAND = Restrictions.conjunction();

			if (startDate != null) {
				Calendar tmp = Calendar.getInstance();

				tmp.setTime(startDate);
				tmp.set(Calendar.HOUR_OF_DAY, 0);
				tmp.set(Calendar.MINUTE, 0);
				tmp.set(Calendar.SECOND, 0);
				tmp.set(Calendar.MILLISECOND, 0);

				dateAND.add(Restrictions.ge(datePropName, tmp.getTime()));
			}

			if (endDate != null) {

				Calendar tmp = Calendar.getInstance();

				tmp.setTime(endDate);
				tmp.set(Calendar.HOUR_OF_DAY, 23);
				tmp.set(Calendar.MINUTE, 59);
				tmp.set(Calendar.SECOND, 59);
				tmp.set(Calendar.MILLISECOND, 999);

				dateAND.add(Restrictions.le(datePropName, tmp.getTime()));
			}

			outerAND.add(dateAND);
		}

		crit.add(outerAND);
		if (orderby != null) {
			for (SortOrder o : orderby) {
				if (o.getRank() == SortOrder.OrderRank.ASC) {
					crit.addOrder(Order.asc(o.getFieldName()));
				} else {
					crit.addOrder(Order.desc(o.getFieldName()));
				}
			}
		}
		crit.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);

		List<T> objects = (List<T>) crit.list();

		if ((objects != null) && !objects.isEmpty()) {
			rsp.setResult(ErrorCode.SUCCESS);
			rsp.setObjects(objects);
		} else
			rsp.setResult(ErrorCode.E_NOT_FOUND);

		return rsp;
	}

	private static boolean isEmptyObject(Object o) throws java.lang.reflect.InvocationTargetException,
	        java.lang.IllegalAccessException {
		Class<?> cl = o.getClass();

		while ((cl != null) && (!cl.isPrimitive())) {
			for (Field f : cl.getDeclaredFields()) {
				Method getter = getGetter(f, cl);

				if (getter != null) {
					if (getter.invoke(o) != null)
						return false;
				} else {
					int mod = f.getModifiers();
					if (!Modifier.isStatic(mod) && Modifier.isPublic(mod) && (f.get(o) != null))
						return false;
				}
			}

			cl = cl.getSuperclass();
		}

		return true;
	}

	private static Method getGetter(Field field, Class<?> cl) {
		for (Method method : cl.getMethods()) {
			if ((method.getName().startsWith("get")) && (method.getName().length() == (field.getName().length() + 3))) {
				if (method.getName().toLowerCase().endsWith(field.getName().toLowerCase())) {
					return method;

				}
			}
		}

		return null;
	}

	static private final Logger log = Logger.getLogger(SearchHelper.class);
}
