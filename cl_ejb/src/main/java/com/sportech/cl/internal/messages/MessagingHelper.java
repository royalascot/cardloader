package com.sportech.cl.internal.messages;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.JMSException;
import javax.jms.MessageProducer;
import javax.jms.ObjectMessage;
import javax.jms.Queue;
import javax.jms.Session;
import javax.naming.InitialContext;

import org.apache.log4j.Logger;

import com.sportech.cl.model.messaging.CommandMessage;

public class MessagingHelper {

	static private final Logger log = Logger.getLogger(MessagingHelper.class);

	public static void addCommandMessage(String command) {
		try {
			CommandMessage cm = new CommandMessage();
			cm.setCommand(command);
			queueMessage(cm);
		} catch (Exception e) {
			log.error("Error building json body", e);
		}
	}

	private static void queueMessage(CommandMessage cm) {
		Connection connection = null;
		try {
			InitialContext ic = new InitialContext();
			ConnectionFactory cf = (ConnectionFactory) ic.lookup("/ConnectionFactory");
			Queue orderQueue = (Queue) ic.lookup("/queue/cardloader");
			connection = cf.createConnection();
			Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
			MessageProducer producer = session.createProducer(orderQueue);
			connection.start();
			ObjectMessage message = session.createObjectMessage();
			message.setObject(cm);
			producer.send(message);
			session.close();
		} catch (Exception e) {
			log.error("Error creating queue", e);
		} finally {
			if (connection != null) {
				try {
					connection.close();
				} catch (JMSException e) {
					log.error("Error closing connection", e);
				}
			}
		}
	}

}
