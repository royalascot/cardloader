package com.sportech.cl.internal.utils;

import java.util.Collection;
import java.util.HashSet;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import com.sportech.cl.model.common.ErrorCode;
import com.sportech.cl.model.database.utils.Pool;
import com.sportech.cl.model.response.GenericResponse;
import com.sportech.common.model.PoolType;
import com.sportech.common.util.DecimalHelper;

public class PoolsHelper {
    static private final Logger log = Logger.getLogger(PoolsHelper.class);

    public boolean isMultiRacePool(Integer poolTypeId) {
        PoolType pt = getPoolType(poolTypeId);

        if (pt == null)
            return false;
        else
            return pt.isMultiRace();
    }

    public static int getFirstRace(Long raceBitmask) {
        if (raceBitmask == null)
            return 0;

        long l_mask = raceBitmask.longValue();
        int i_first_race = 0;

        for (long i_bit = 0; i_bit < Long.SIZE; ++i_bit)
            if ((l_mask & (1L << i_bit)) > 0) {
                i_first_race = (int) (i_bit + 1);
                break;
            }

        return i_first_race;
    }

    public static int getLastRace(Long raceBitmask) {
        if (raceBitmask == null)
            return 0;

        long l_mask = raceBitmask.longValue();
        int i_last_race = 0;

        for (long i_bit = 0; i_bit < Long.SIZE; ++i_bit)
            if ((l_mask & (1L << i_bit)) > 0)
                i_last_race = (int) (i_bit + 1);

        return i_last_race;
    }

    public static int getRaceCount(Long raceBitmask) {
        if (raceBitmask == null)
            return 0;

        return Long.bitCount(raceBitmask);
    }

    public boolean poolsInConflict(Long raceBitmask_1, Long raceBitmask_2, Integer poolTypeId) {
        PoolType poolType = getPoolType(poolTypeId);

        if (poolType == null)
            return false;
        else
            return poolsInConflict(raceBitmask_1, raceBitmask_2, poolType);
    }

    public boolean poolsInConflict(Long raceBitmask_1, Long raceBitmask_2, PoolType poolType) {
        if (raceBitmask_1 == null || raceBitmask_2 == null || !poolType.isMultiRace())
            return false;

        if (poolType.isExchange())
            return (raceBitmask_1.longValue() & raceBitmask_2.longValue()) > 0;
        else
            return ((getFirstRace(raceBitmask_1) == getFirstRace(raceBitmask_2)) || (getLastRace(raceBitmask_1) == getLastRace(raceBitmask_2)));
    }

    private <T extends Pool> void removeDuplicatePools(Collection<T> pools) {

        HashSet<T> poolsToRemove = new HashSet<T>();

        for (T pool : pools) {

            if (pool.getPoolTypeId() == null) {
                continue;
            }
            if (poolsToRemove.contains(pool)) {
                continue;
            }
            PoolType poolType = getPoolType(pool.getPoolTypeId());

            for (T anotherPool : pools) {
                if (anotherPool == pool) {
                    continue;
                }
                if (pool.getPoolTypeId().equals(anotherPool.getPoolTypeId())) {
                    if (pool.getRaceBitmap().equals(anotherPool.getRaceBitmap())) {
                        poolsToRemove.add(anotherPool);
                    } else {
                        if (!poolType.isMultiRace()) {
                            if (StringUtils.equalsIgnoreCase(pool.getPoolName(), anotherPool.getPoolName())
                                    && DecimalHelper.areEqual(pool.getGuarantee(), anotherPool.getGuarantee())
                                    && StringUtils.equalsIgnoreCase(pool.getPoolCode(), anotherPool.getPoolCode())) {
                                pool.setRaceBitmap(pool.getRaceBitmap() | anotherPool.getRaceBitmap());
                                poolsToRemove.add(anotherPool);
                            }
                        }
                    }

                }
            }
        }
        pools.removeAll(poolsToRemove);
    }

    private <T extends Pool> void removeEmptyPools(Collection<T> pools) {

        HashSet<T> poolsToRemove = new HashSet<T>();

        for (T pool : pools) {

            if (pool.getPoolTypeId() == null) {
                continue;
            }

            if (pool.getRaceBitmap() == null || pool.getRaceBitmap() == 0) {
                poolsToRemove.add(pool);
                log.info("Removed empty pool with pool type id: " + pool.getPoolTypeId());
                continue;
            }
        }
        pools.removeAll(poolsToRemove);
    }

    public <T extends Pool> boolean validateCompactPools(Collection<T> pools_coll, GenericResponse rsp) {
        removeEmptyPools(pools_coll);
        removeDuplicatePools(pools_coll);
        return validateCompactPoolsInternal(pools_coll, rsp);
    }

    private <T extends Pool> boolean validateCompactPoolsInternal(Collection<T> pools_coll, GenericResponse rsp) {

        for (T pool : pools_coll) {

            if (pool.getPoolTypeId() == null) {
                rsp.setResult(ErrorCode.E_VAL_FAILED, "Pool type ID is empty");
            } else {

                PoolType poolType = getPoolType(pool.getPoolTypeId());
                int raceCount = getRaceCount(pool.getRaceBitmap());
                int poolRaceCount = poolType.getRaceCount();

                if (pool.getRaceBitmap() == null || pool.getRaceBitmap().longValue() == 0) {
                    rsp.setResult(ErrorCode.E_VAL_FAILED, "Races mask for pool with type=" + pool.getPoolTypeId() + " is empty");
                } else if (poolType.isMultiRace() && (!poolType.isVariLeg()) && (raceCount != poolRaceCount)) {
                    rsp.setResult(ErrorCode.E_VAL_FAILED, "Invalid nunber of races for pool with type=" + pool.getPoolTypeId()
                            + " expected : " + poolType.getRaceCount() + " ,actual : " + raceCount);
                } else if (poolType.isMultiRace() && poolType.isVariLeg() && (raceCount < poolRaceCount)) {
                    rsp.setResult(ErrorCode.E_VAL_FAILED, "Invalid nunber of races for pool with type=" + pool.getPoolTypeId()
                            + " expected at least : " + poolType.getRaceCount() + " ,actual : " + raceCount);
                } else {
                    for (T another_pool : pools_coll) {
                        if (another_pool == pool) {
                            continue;
                        }
                        if (pool.getPoolTypeId().equals(another_pool.getPoolTypeId())) {
                            if (!pool.getRaceBitmap().equals(another_pool.getRaceBitmap()))
                                if (poolType.isMultiRace()) {
                                    if (poolsInConflict(pool.getRaceBitmap(), another_pool.getRaceBitmap(), poolType)) {
                                        rsp.setResult(ErrorCode.E_VAL_FAILED, "Pools with type=" + pool.getPoolTypeId()
                                                + " ,race masks " + pool.getRaceBitmap() + " and " + another_pool.getRaceBitmap()
                                                + " are in conflict");
                                    }
                                }

                        }
                    }
                }
            }
        }
        return rsp.isOK();
    }

    private static PoolType getPoolType(Integer poolTypeId) {
        if (poolTypeId == null)
            return null;
        return PoolType.fromId(poolTypeId);
    }

}
