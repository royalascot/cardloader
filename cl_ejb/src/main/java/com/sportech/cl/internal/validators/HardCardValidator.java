package com.sportech.cl.internal.validators;

import com.sportech.cl.model.response.GenericResponse;
import com.sportech.cl.model.common.ErrorCode;
import com.sportech.cl.model.database.HardCard;
import com.sportech.cl.model.database.HardRace;
import com.sportech.cl.model.database.Runner;
import com.sportech.cl.internal.utils.PoolsHelper;

public class HardCardValidator implements Validator<HardCard>
{
    public HardCardValidator()                               { pools_helper = new PoolsHelper(); }  
    public HardCardValidator( PoolsHelper _pools_helper )    { pools_helper = _pools_helper; }
    
    public boolean Validate( HardCard hc, boolean create, GenericResponse rsp)
    {
        if( !create && (hc.getId() == null) ) 
            rsp.setResult(ErrorCode.E_VAL_FAILED, "Card ID is empty");

        for( HardRace race : hc.getRaces() )
        {
            if( race.getNumber() == null )
                rsp.setResult(ErrorCode.E_VAL_FAILED, "Race number is empty");
            
            for( Runner runner : race.getRunners() )
            {
                if( runner.getName() == null )
                    rsp.setResult(ErrorCode.E_VAL_FAILED, "Runner name is empty");
                
                if( runner.getAlsoEligible() != null && runner.getAlsoEligible() )
                {
                    if( runner.getPosition() != null )
                        rsp.setResult(ErrorCode.E_VAL_FAILED, "Runner position must be empty for 'also-eligible' runners");
                }
                else 
                {
                    if( runner.getPosition() == null )
                        rsp.setResult(ErrorCode.E_VAL_FAILED, "Runner position is empty");
                }
                
            }
        }
        
        pools_helper.validateCompactPools(hc.getPools(), rsp);
        
        return rsp.isOK();
    }
    
    private PoolsHelper pools_helper;    
}
