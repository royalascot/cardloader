package com.sportech.cl.internal.utils;

import org.apache.log4j.Logger;
import org.hibernate.Session;

import com.sportech.cl.model.database.utils.EntityWithId;
import com.sportech.cl.model.database.utils.PersistentParent;
import com.sportech.cl.model.security.ObjectType;
import com.sportech.cl.model.security.Privilege;

public class BeanHelper<EntityType extends PersistentParent<EntityType>, EntityOutlineType extends EntityWithId>
                extends FlatObjectBeanHelper<EntityType, EntityOutlineType>
{
    public
    BeanHelper(Session ss,  
            ObjectType                          _objType,
            Privilege                           _readPriv,
            Privilege                           _writePriv,
            Class<?>                            _typeClass,
            Class<?>                            _outlineTypeClass,
            Logger                              _log)
    {
        super(ss, _objType, _readPriv, _writePriv, _typeClass, _outlineTypeClass, _log );
    }

    @Override
    protected void  massageObjectBeforeAdd( EntityType obj ) 
    {
        obj.setChildParents();
    }
    
}
