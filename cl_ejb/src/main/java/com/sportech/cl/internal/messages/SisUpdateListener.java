package com.sportech.cl.internal.messages;

import javax.ejb.ActivationConfigProperty;
import javax.ejb.EJB;
import javax.ejb.MessageDriven;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.ObjectMessage;

import org.apache.log4j.Logger;

import com.sportech.cl.sis.gateway.SisCardHandlerBean;
import com.sportech.common.model.entity.EventCard;

@MessageDriven(activationConfig = {

@ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue"),

@ActivationConfigProperty(propertyName = "maxSession", propertyValue = "1"),

@ActivationConfigProperty(propertyName = "destination", propertyValue = "queue/sisUpdate") })
public class SisUpdateListener implements MessageListener {

	static private final Logger log = Logger.getLogger(SisUpdateListener.class);

	@EJB
	SisCardHandlerBean sisHandler;

	public void onMessage(Message message) {
		try {
			log.info("Got card update message");
			final ObjectMessage textMessage = (ObjectMessage) message;
			if (textMessage != null && textMessage.getObject() != null) {
				EventCard ec = (EventCard) textMessage.getObject();
				if (ec != null) {
					sisHandler.processCard(ec);
				}
			}
			log.info("Processed card update message");
		} catch (Exception e) {
			log.error("Error processing message", e);
		}
	}

}
