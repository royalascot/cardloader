package com.sportech.cl.internal.utils;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.lang.Math;

import javax.interceptor.AroundInvoke;
import javax.interceptor.InvocationContext;

import com.sportech.cl.model.common.ErrorCode;
import com.sportech.cl.model.response.GenericResponse;

/*
 * Check if required parameters of method are not NULL. To be user in @Interceptors chain AFTER 'Rollbacker' one !
 *     - (to give 'Rollbacker' chance to rollback transaction in case of error(s))
 * Parameters are checked IF:
 *     - Method returns response derived from GenericResponse (needed to report error(s))
 *     - Required parameters are annotated with @com.sportech.cl.internal.utils.RequiredParam
 */

public class ParametersChecker
{
    @AroundInvoke
    public Object   checkParams( InvocationContext invctx ) throws Exception
    {
        Method method = invctx.getMethod();
        
        Class<?>    retType = method.getReturnType();
        
        if(GenericResponse.class.isAssignableFrom( retType ))
        {
            /*
             * Check for absent required parameters
             */
            GenericResponse rsp = (GenericResponse)retType.newInstance();
            
            Annotation[][]  paramsAnnotations = method.getParameterAnnotations();
            
            if( paramsAnnotations != null )
            {
                Object[] parameters = invctx.getParameters();
                
                for( int iParam = 0; iParam < Math.min(paramsAnnotations.length, parameters.length); ++iParam )
                {
                    Annotation[] thisParamAnnotations = paramsAnnotations[ iParam ]; 
                    
                    if( thisParamAnnotations != null )
                    {
                        for( Annotation annotation : thisParamAnnotations )
                        {
                            if( (annotation instanceof RequiredParam) && (parameters[iParam] == null ) )
                            {
                                RequiredParam   reqAnnotation = (RequiredParam)annotation;
                                String          parName = reqAnnotation.name().isEmpty() ? "arg" + iParam : reqAnnotation.name();
                                
                                rsp.setResult(ErrorCode.E_BAD_PARAMETER, "Method '" + method.getName() +"', parameter '" + parName + "' is NULL" );
                            }
                        }
                    }
                }
            }
            
            if( !rsp.isOK() )
                return rsp;
        }

        return invctx.proceed();
    }

}
