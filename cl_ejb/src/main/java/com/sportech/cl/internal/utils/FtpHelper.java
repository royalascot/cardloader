package com.sportech.cl.internal.utils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;
import org.apache.commons.net.ftp.FTPReply;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.jboss.util.file.Files;

import com.sportech.cl.global.utils.config.FtpConfig;
import com.sportech.cl.model.database.FtpImport;

public class FtpHelper {

	static private final Logger log = Logger.getLogger(FtpHelper.class);

	private static final int TIMEOUT = 3000000;
	private FTPClient ftp = null;
	private FtpConfig config = null;
	private Pattern filePattern;

	public FtpHelper(FtpConfig config) {
		ftp = new FTPClient();
		ftp.setControlKeepAliveTimeout(3000);
		ftp.setDataTimeout(TIMEOUT);
		ftp.setDefaultTimeout(TIMEOUT);

		this.config = config;
		String fileMasks = config.getFtpFileMasks();
		if (StringUtils.isNotEmpty(fileMasks)) {
			filePattern = Pattern.compile(fileMasks, Pattern.UNICODE_CASE | Pattern.CASE_INSENSITIVE);
		} else {
			filePattern = Pattern.compile(".*");
		}
	}

	@SuppressWarnings("unchecked")
	public void checkNewFiles(Session ss) {
		try {
			int reply;
			ftp.connect(config.getFtpServer());

			try {
				ftp.setSoTimeout(TIMEOUT);
			} catch (Exception e) {
				log.error("Error setting socket timeout", e);
			}

			reply = ftp.getReplyCode();

			if (!FTPReply.isPositiveCompletion(reply)) {
				ftp.disconnect();
				log.error("FTP server refused connection.");
				return;
			}

			ftp.login(config.getFtpUsername(), config.getFtpPassword());
			ftp.setFileType(FTP.BINARY_FILE_TYPE);
			ftp.enterLocalPassiveMode();
			
			FTPFile[] files = ftp.listFiles(config.getFtpDirectoryName());
			for (FTPFile f : files) {
				String s = f.getName();
				log.debug("list file:" + s);
				Matcher m = filePattern.matcher(s);
				if (m.find()) {

					File newFile = new File(config.getDownloadPath(), s);
					if (newFile.exists()) {
						log.info("file has been downloaded:" + s);
						continue;
					}

					Criteria crit = ss.createCriteria(FtpImport.class).add(Restrictions.eq("filename", s));

					List<FtpImport> imports = (List<FtpImport>) crit.list();

					if (imports == null || imports.size() == 0) {
						saveFile(ss, s, f);
					} else {
						Calendar c = f.getTimestamp();
						boolean skip = false;
						for (FtpImport i : imports) {
							if (i.getFileDate() != null && i.getFileDate().getTime() == c.getTimeInMillis()) {
								log.info("Skip file with the same name and timestamp as an existing one: " + s);
								skip = true;
								break;
							}
						}
						if (skip) {
							continue;
						}
						log.info("save file with exisiting name but different timestamp: " + s);
						saveFile(ss, s, f);
					}
				} else {
					log.info("Skip unmatched file:" + s);
				}
			}

			ftp.logout();

		} catch (IOException e) {
			log.error("Error getting file", e);
		} catch (Exception ex) {
			log.error("Error getting file", ex);
		} finally {
			if (ftp.isConnected()) {
				try {
					ftp.disconnect();
				} catch (IOException ioe) {
					// do nothing
				}
			}
		}
	}

	private void saveFile(Session ss, String s, FTPFile f) throws IOException {
		try {

			long fileSize = f.getSize();

			File newFile = new File(config.getDownloadPath(), s);
			FileOutputStream fos = new FileOutputStream(newFile);
			String ftpFile = FilenameUtils.concat(config.getFtpDirectoryName(), s);
			ftpFile = FilenameUtils.separatorsToUnix(ftpFile);
			ftp.retrieveFile(ftpFile, fos);
			fos.close();
			if (fileSize > 0) {
				long fsize = newFile.length();
				if (fsize < fileSize) {
					log.info("File size doesn't match. delete temp file: " + newFile.getName());
					Files.delete(newFile);
					return;
				}
			}
			log.info("save new file: " + ftpFile);

			FtpImport i = new FtpImport();
			i.setChecksum(0L);
			i.setFileDate(f.getTimestamp().getTime());
			i.setFilename(s);
			i.setTimestamp(new Date());
			ss.save(i);
			ss.flush();
		} catch (IOException e) {
			log.error("Error downloading file", e);
			throw e;
		}
	}

}
