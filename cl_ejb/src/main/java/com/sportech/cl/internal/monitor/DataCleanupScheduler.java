package com.sportech.cl.internal.monitor;

import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.annotation.PostConstruct;
import javax.ejb.AccessTimeout;
import javax.ejb.ConcurrencyManagement;
import javax.ejb.ConcurrencyManagementType;
import javax.ejb.Lock;
import javax.ejb.LockType;
import javax.ejb.Schedule;
import javax.ejb.Singleton;
import javax.persistence.PersistenceContext;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import com.sportech.cl.global.utils.config.ClConfig;
import com.sportech.cl.global.utils.config.ConfigLoader;
import com.sportech.cl.model.database.HardCard;
import com.sportech.cl.model.database.SoftCard;

@Singleton
@ConcurrencyManagement(ConcurrencyManagementType.CONTAINER)
public class DataCleanupScheduler {

	static private final Logger log = Logger.getLogger(DataCleanupScheduler.class);

	private int retentionDays = 7;
	
	@PersistenceContext
    private Session ss;
	
	@PostConstruct
	public void init() {
		try {
			ClConfig config = ConfigLoader.getCfg();
			String days = config.getCommonConfig().getDataRetentionDays();

			if (StringUtils.isNotEmpty(days)) {
				retentionDays = Integer.parseInt(days);
			}
			log.info("Clean up data older than " + retentionDays + " days.");
		} catch (Exception e) {
			log.error("Error initializing data clean up task", e);
		}
	}
	
	@Schedule(hour = "1", persistent = false)
	@Lock(LockType.WRITE)
	@AccessTimeout(value = 100, unit = TimeUnit.MINUTES)
	public void cleanupData() {
		clearCards();
	}

	
	@Schedule(hour = "2", persistent = false)
	@Lock(LockType.WRITE)
	@AccessTimeout(value = 100, unit = TimeUnit.MINUTES)
	public void cleanupImportFiles() {
		Date oldDate = DateUtils.addDays(new Date(), -(retentionDays * 2));
		log.info("Clean up import file data older than: " + oldDate);
		try {
			SQLQuery queryFiles = ss.createSQLQuery("delete from ftp_import where timestamp <= :cid");
			SQLQuery queryImportFiles = ss.createSQLQuery("delete from import where timestamp <= :cid");

			queryImportFiles.setDate("cid", oldDate);
			queryImportFiles.executeUpdate();
			queryFiles.setDate("cid", oldDate);
			queryFiles.executeUpdate();
		} catch (Exception e) {
			log.error("Error cleaning up import file data:", e);
		}
	}

	@SuppressWarnings("unchecked")
	private void clearCards() {
		try {
			Date oldDate = DateUtils.addDays(new Date(), -retentionDays);
			log.info("Clean up card data older than: " + oldDate);

			Criteria soft = ss.createCriteria(SoftCard.class).add(Restrictions.le("cardDate", oldDate));
			List<SoftCard> softCards = (List<SoftCard>) soft.list();

			SQLQuery queryCardInUse = ss.createSQLQuery("delete from cards_in_use where soft_card_fk = :cid");
			SQLQuery querySoftRace = ss.createSQLQuery("delete from soft_race where soft_card_fk = :cid");
			SQLQuery querySoftPool = ss.createSQLQuery("delete from soft_pool where soft_card_fk = :cid");
			SQLQuery querySoftCard = ss.createSQLQuery("delete from soft_card where soft_card_pk = :cid");
			SQLQuery queryRunner = ss
			        .createSQLQuery("delete from runner where hard_race_fk in (select hard_race_pk from hard_race where hard_card_fk = :cid)");
			SQLQuery queryHardRace = ss.createSQLQuery("delete from hard_race where hard_card_fk = :cid");
			SQLQuery queryHardPool = ss.createSQLQuery("delete from hard_pool where hard_card_fk = :cid");
			SQLQuery queryHardCard = ss.createSQLQuery("delete from hard_card where hard_card_pk = :cid");
			SQLQuery queryImport = ss.createSQLQuery("delete from import where import_pk = :cid");

			for (SoftCard sc : softCards) {
				queryCardInUse.setLong("cid", sc.getId());
				queryCardInUse.executeUpdate();
				querySoftRace.setLong("cid", sc.getId());
				querySoftRace.executeUpdate();
				querySoftPool.setLong("cid", sc.getId());
				querySoftPool.executeUpdate();
				querySoftCard.setLong("cid", sc.getId());
				querySoftCard.executeUpdate();
			}

			Criteria hard = ss.createCriteria(HardCard.class).add(Restrictions.le("cardDate", oldDate));
			List<HardCard> hardCards = (List<HardCard>) hard.list();
			for (HardCard sc : hardCards) {
				queryRunner.setLong("cid", sc.getId());
				queryRunner.executeUpdate();
				queryHardRace.setLong("cid", sc.getId());
				queryHardRace.executeUpdate();
				queryHardPool.setLong("cid", sc.getId());
				queryHardPool.executeUpdate();
				queryHardCard.setLong("cid", sc.getId());
				queryHardCard.executeUpdate();
                if (sc.getImportId() != null) {
                    Criteria sameFileCardsQuery = ss.createCriteria(HardCard.class).add(
                            Restrictions.eq("importId", sc.getImportId()));
                    List<HardCard> sameFileCards = (List<HardCard>) sameFileCardsQuery.list();
                    if (sameFileCards != null && sameFileCards.size() == 0) {
                        queryImport.setLong("cid", sc.getImportId());
                        queryImport.executeUpdate();
                    }
                }
			}

		} catch (Exception e) {
			log.error("Error cleaning up data:", e);
		}
	}

}
