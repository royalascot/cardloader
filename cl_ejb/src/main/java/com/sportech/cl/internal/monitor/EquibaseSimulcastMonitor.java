package com.sportech.cl.internal.monitor;

import java.util.Date;

import javax.annotation.PostConstruct;
import javax.ejb.Schedule;
import javax.ejb.Singleton;
import javax.persistence.PersistenceContext;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.hibernate.Session;

import com.sportech.cl.global.utils.config.ConfigLoader;
import com.sportech.cl.global.utils.config.FtpConfig;
import com.sportech.cl.internal.messages.MessagingHelper;
import com.sportech.cl.internal.utils.FtpHelper;
import com.sportech.cl.model.database.utils.DateUtils;
import com.sportech.cl.model.messaging.CommandMessage;

@Singleton
public class EquibaseSimulcastMonitor {

	static private final Logger log = Logger.getLogger(EquibaseSimulcastMonitor.class);

	private String checkTime;

	private boolean checkSite = false;

	private FtpHelper helper = null;
	
	@PersistenceContext
    private Session ss;
	
	@PostConstruct
	public void init() {
		FtpConfig config = ConfigLoader.getCfg().getEquibaseSimulcastConfig();
		if (config == null) {
			log.info("No Equibase simulcast configuration. Downloading SIMD* from Equibase is disabled.");
			checkSite = false;
			return;
		}
		checkTime = config.getFtpCheckTime();
		if (StringUtils.isNotEmpty(checkTime)) {
			checkSite = true;
			log.info("Check Equibase FTP site at:" + checkTime);
			helper = new FtpHelper(config);
		} else {
			checkSite = false;
			log.info("Error or missing 'check-site-time' configuration. Downloading from Equibase is disabled.");
		}
		
	}

	@Schedule(minute = "*", hour = "*", persistent = false)
	public void checkFiles() {
		if (checkSite) {
			if (DateUtils.isLaunchTime(checkTime)) {
				MessagingHelper.addCommandMessage(CommandMessage.EquibaseSimulcast);
			}
		}
	}

	@javax.ejb.Lock(javax.ejb.LockType.WRITE)
	public void checkNewFiles() {
		Date currentTime = new Date();
		log.info("Starting downloading new files at " + currentTime);
		if (helper != null) {
			helper.checkNewFiles(ss);
		}
		log.info("Finished downloading new files");
	}

}