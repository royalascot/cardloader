package com.sportech.cl.internal.monitor;

import javax.annotation.PostConstruct;
import javax.ejb.Schedule;
import javax.ejb.Singleton;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import com.sportech.cl.global.utils.config.ConfigLoader;
import com.sportech.cl.global.utils.config.FtpConfig;
import com.sportech.cl.internal.messages.MessagingHelper;
import com.sportech.cl.model.database.utils.DateUtils;
import com.sportech.cl.model.messaging.CommandMessage;

@Singleton
public class TrackInfoMonitor {

	static private final Logger log = Logger.getLogger(TrackInfoMonitor.class);

	private String checkTime;
	private FtpConfig config;

	private boolean checkSite = false;

	@PostConstruct
	public void init() {
		config = ConfigLoader.getCfg().getTrackinfoConfig();
		if (config == null) {
			checkSite = false;
			log.info("Missing track info configuration. Downloading from Track Info is disabled.");
			return;
		}
		checkTime = config.getFtpCheckTime();
		if (StringUtils.isNotEmpty(checkTime)) {
			checkSite = true;
			log.info("Check Track Info site at:" + checkTime);
		} else {
			checkSite = false;
			log.info("Error or missing 'track-info-check-time' configuration. Downloading from Track Info is disabled.");
		}
	}

	@Schedule(minute = "*", hour = "*", persistent = false)
	public void checkFiles() {
		if (checkSite) {
			if (DateUtils.isLaunchTime(checkTime)) {
				MessagingHelper.addCommandMessage(CommandMessage.TrackInfo);
			}
		}
	}

}
