package com.sportech.cl.internal.utils;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.Session;

import com.sportech.cl.model.database.AuditLog;
import com.sportech.cl.model.database.utils.EntityWithId;
import com.sportech.cl.model.security.SecurityToken;

public class AuditHelper {
	public static void writeAuditLog(Session ss, SecurityToken token, Object type, String action, String area) {
		AuditLog log = new AuditLog();
		log.setContent(action);
		log.setCategory(type.getClass().getSimpleName());
		String name = "System";
		if (token != null && !StringUtils.isEmpty(token.getLoginName())) {
			name = token.getLoginName();
		}
		log.setArea(area);
		log.setUsername(name);
		if (type instanceof EntityWithId) {
			EntityWithId entity = (EntityWithId) type;
			if (entity != null) {
				Long id = entity.getId();
				if (id != null) {
					log.setData("Id=" + id);
				}
			}
		}
		ss.save(log);
		ss.flush();
	}

	public static void writeAuditLog(Session ss, SecurityToken token, Object type, String action) {
		writeAuditLog(ss, token, type, action, "User");
	}

}
