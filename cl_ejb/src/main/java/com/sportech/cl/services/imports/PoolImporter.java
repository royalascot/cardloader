package com.sportech.cl.services.imports;

import java.util.Arrays;
import java.util.TreeMap;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import com.sportech.cl.model.database.CardConfig;
import com.sportech.cl.model.database.HardCard;
import com.sportech.cl.model.database.HardPool;
import com.sportech.cl.model.database.HardRace;
import com.sportech.cl.model.database.Track;
import com.sportech.cl.model.database.utils.JSonHelper;
import com.sportech.cl.model.importer.PoolLeg;
import com.sportech.cl.model.importer.PoolList;
import com.sportech.common.model.PoolType;

public class PoolImporter {

    static private final Logger log = Logger.getLogger(PoolImporter.class);
    
    public void savePools(Session ss, HardCard hc) {
        saveHardPools(ss, hc);
    }
    
    private void saveHardPools(Session ss, HardCard card) {
        TreeMap<Long, PoolList> poolMap = new TreeMap<Long, PoolList>();
        long totalRaces = 0;
        for (HardRace race : card.getRaces()) {
            totalRaces++;
            if (StringUtils.isEmpty(race.getPoolIdList())) {
                continue;
            }
            PoolList poolList = JSonHelper.fromJSon(PoolList.class, race.getPoolIdList());
            if (poolList == null) {
                continue;
            }
            poolMap.put(race.getNumber(), poolList);
        }

        for (HardRace race : card.getRaces()) {
            if (StringUtils.isEmpty(race.getPoolIdList())) {
                continue;
            }
            PoolList poolList = poolMap.get(race.getNumber());
            if (poolList.getLegs() == null || poolList.getLegs().size() < 1) {
                log.error("Race does not have pools.");
                continue;
            }

            Integer[] poolIds = poolList.getLegs().get(0).getPoolIds();

            for (Integer poolId : poolIds) {
                log.debug("save pool " + poolId);
                PoolType pool = null;
                if (poolId == -1) {
                    long poolLegs = 1L + (long) (card.getRaces().size()) - race.getNumber();
                    pool = getPickAllType(poolLegs);
                } else {
                    pool = PoolType.fromId(poolId);
                }
                if (pool == null) {
                    log.error("Can not find pool with pool id:" + poolId);
                    continue;
                }
                HardPool hardPool;
                int legCount = pool.getRaceCount();
                if (legCount == 1) {
                    hardPool = getHardPool(ss, card, pool);
                    if (hardPool == null) {
                        hardPool = new HardPool();
                        hardPool.setParent(card);
                        hardPool.setPoolTypeId(pool.getId());
                        hardPool.setTotePoolType(pool.getToteId());
                        hardPool.setRaceBitmap(0L);
                    }
                    long raceMap = hardPool.getRaceBitmap();
                    raceMap |= (long) (1 << (race.getNumber() - 1));
                    hardPool.setRaceBitmap(raceMap);
                } else {
                    hardPool = new HardPool();
                    hardPool.setParent(card);
                    hardPool.setPoolTypeId(pool.getId());
                    hardPool.setTotePoolType(pool.getToteId());
                    hardPool.setRaceBitmap(0L);
                    if (pool == PoolType.GSL){
                    	hardPool.setPoolCode(pool.getCode());
                    	hardPool.setPoolName(pool.getName());
                    }
                    long legs = (long) (1 << (race.getNumber() - 1));
                    int foundCount = 1;
                    boolean foundAnotherFirstLeg = false;
                    for (int lc = 2; lc <= legCount && !foundAnotherFirstLeg; lc++) {
                        boolean found = false;
                        for (long nextLeg = race.getNumber() + 1; nextLeg <= totalRaces && !foundAnotherFirstLeg; nextLeg++) {
                            PoolList pl = poolMap.get(nextLeg);
                            if (pl != null && pl.getLegs() != null && pl.getLegs().size() > 0) {
                                if (Arrays.asList(pl.getLegs().get(0).getPoolIds()).contains(pool.getId())) {
                                    log.info("Found another first leg from race " + nextLeg + ", stop searching legs for "  + pool.getId());
                                    foundAnotherFirstLeg = true;
                                    break;
                                }
                                for (PoolLeg followingLeg : pl.getLegs()) {
                                    if (followingLeg.getLegNumber().intValue() == lc) {
                                        if (Arrays.asList(followingLeg.getPoolIds()).contains(pool.getId())) {
                                            log.info("Found defined secondary leg race: " + nextLeg + " for leg " + lc);
                                            legs |= 1L << (nextLeg - 1);
                                            foundCount++;
                                            found = true;
                                            break;
                                        }
                                    }
                                }
                            }
                            if (found) {
                                break;
                            }
                        }
                    }
                    if (foundCount != legCount) {
                        for (int i = 0; i < legCount; i++) {
                            legs |= (long) (1 << (race.getNumber() + i - 1));
                        }
                    }
                    hardPool.setRaceBitmap(legs);
                }
                ss.save(hardPool);
                ss.flush();
            }
        }
    }

    public static void copyCardConfig(Session ss, HardCard hc) {
        Track track = hc.getTrack();
        CardConfig template = track.getCardConfig();
        if (template != null) {
            hc.setConfig(template);
            ss.saveOrUpdate(hc);
            ss.flush();
        }
    }

    private PoolType getPickAllType(long legCount) {
        String name = "Pick " + legCount;
        return PoolType.fromName(name);
    }

    private HardPool getHardPool(Session ss, HardCard card, PoolType poolType) {
        Criteria crit = ss.createCriteria(HardPool.class).add(Restrictions.eq("parent", card))
                .add(Restrictions.eq("poolTypeId", poolType.getId()));
        HardPool pool = (HardPool) crit.uniqueResult();
        return pool;
    }

}
