package com.sportech.cl.services.dictionaries;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.LocalBean;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.interceptor.Interceptors;
import javax.persistence.PersistenceContext;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import com.sportech.cl.internal.utils.CardHelper;
import com.sportech.cl.internal.utils.FlatObjectBeanHelper;
import com.sportech.cl.internal.utils.RequiredParam;
import com.sportech.cl.internal.validators.TrackValidator;
import com.sportech.cl.model.common.ErrorCode;
import com.sportech.cl.model.common.PageParams;
import com.sportech.cl.model.database.HardCard;
import com.sportech.cl.model.database.Pattern;
import com.sportech.cl.model.database.Track;
import com.sportech.cl.model.database.utils.SortOrder;
import com.sportech.cl.model.response.AddEntityResponse;
import com.sportech.cl.model.response.GenericResponse;
import com.sportech.cl.model.response.TrackListResponse;
import com.sportech.cl.model.security.ObjectType;
import com.sportech.cl.model.security.Privilege;
import com.sportech.cl.model.security.SecurityToken;

@Stateless
@Interceptors({ com.sportech.cl.internal.utils.Rollbacker.class, com.sportech.cl.internal.utils.ParametersChecker.class })
@LocalBean
@Remote(TracksRemote.class)
public class TracksBean implements TracksRemote {

    static private final Logger log = Logger.getLogger(TracksBean.class);

    @PersistenceContext
    private Session ss;
    
    protected FlatObjectBeanHelper<Track, Track> helper;

    public TracksBean() {
    }

    @PostConstruct
    protected void init() {
        helper = new FlatObjectBeanHelper<Track, Track>(ss, ObjectType.DICT_TRACK, Privilege.READ_DICTIONARIES,
                Privilege.MANAGE_DICTIONARIES, Track.class, Track.class, log);
        helper.hasObjectLevelACL(false);
        helper.setValidator(new TrackValidator());
    }

    @Override
    public List<Pattern> getTrackPatterns(Track track) {
        if (track.getId() == null) {
            return new ArrayList<Pattern>();
        }
        if (track.getActive() != null && !track.getActive()) {
            return new ArrayList<Pattern>();
        }
        @SuppressWarnings("unchecked")
        List<Pattern> patterns = (List<Pattern>) ss.createQuery("from Pattern where trackId = :id").setLong("id", track.getId()).list();
        return patterns;
    }

    @Override
    public Track get(Long id) {
        Track t = (Track) ss.get(Track.class, id);
        if (t != null && t.getActive() != null && !t.getActive()) {
            return null;
        }
        return t;
    }

    @Override
    public TrackListResponse find(SecurityToken token, Track[] query, Boolean useLike, PageParams pages) {
        if (query != null && query.length > 0) {
            query[0].setActive(true);
        }
        return (TrackListResponse) helper.find(token, query, useLike, pages, new TrackListResponse(),
                new SortOrder[] { new SortOrder("equibaseId") });
    }

    @Override
    public AddEntityResponse add(SecurityToken token, @RequiredParam(name = "track") Track track) {
        // New Track must be ACTIVE
        track.setActive(true);
        AddEntityResponse rsp = new AddEntityResponse();
        save(track, rsp);
        rsp.setId(track.getId());
        return rsp;
    }

    @Override
    public GenericResponse update(SecurityToken token, @RequiredParam(name = "id") Long id,
            @RequiredParam(name = "new_track") Track track) {
        GenericResponse rsp = new GenericResponse();
        return save(track, rsp);
    }

    @Override
    public GenericResponse enable(SecurityToken token, @RequiredParam(name = "id") Long id,
            @RequiredParam(name = "enabled") Boolean enabled) {

        GenericResponse rsp = new GenericResponse();

        try {
            Track track = (Track) ss.get(Track.class, id);
            if (track != null) {
                track.setActive(enabled);
                ss.flush();
            } else {
                rsp.setResult(ErrorCode.E_NOT_FOUND);
            }
            return rsp;
        } catch (Exception e) {
            log.error("Exception caught: ", e);
            rsp.setResult(ErrorCode.FAIL);
            return rsp;
        }
    }

    @Override
    public GenericResponse delete(SecurityToken token, @RequiredParam(name = "id") Long id) {
        GenericResponse response = new GenericResponse();
        try {
            Track track = (Track) ss.get(Track.class, id);
            deleteTrackLinks(ss, id);
            ss.delete(track);
        } catch (Exception e) {
            log.error("Error deleting track", e);
            response.setResult(ErrorCode.ERROR, "Can not delete track in use.");
        }
        return response;
    }

    @SuppressWarnings("unchecked")
    private void deleteTrackLinks(Session ss, Long id) {

        if (id == null) {
            return;
        }

        try {
            log.info("Deleting track related entities: " + id);

            Criteria soft = ss.createCriteria(HardCard.class).add(Restrictions.eq("track.id", id));
            List<HardCard> cards = (List<HardCard>) soft.list();
            for (HardCard hc : cards) {
                CardHelper.deleteCard(ss, hc.getId());
            }
            
            List<Pattern> patterns = (List<Pattern>) ss.createQuery("from Pattern where trackId = :id").setLong("id", id).list();
            for (Pattern p : patterns) {
                ss.delete(p);
            }
            ss.flush();
            
            SQLQuery queryCardInUse = ss.createSQLQuery("delete from group_track where track_id = :cid");
            SQLQuery querySoftRace = ss.createSQLQuery("delete from track_tote where track_id = :cid");
            SQLQuery queryPattern = ss.createSQLQuery("delete from pattern where dict_track_fk = :cid");
            queryCardInUse.setLong("cid", id);
            queryCardInUse.executeUpdate();
            querySoftRace.setLong("cid", id);
            querySoftRace.executeUpdate();
            queryPattern.setLong("cid", id);
            queryPattern.executeUpdate();

            log.info("Deleted " + id + " and its related entities.");

        } catch (Exception e) {
            log.error("Error deleting track sub-entities:", e);
        }

    }

    private GenericResponse save(Track track, GenericResponse rsp) {
        try {
            String s = saveTrack(ss, track);
            if (s != null) {
                rsp.setResult(ErrorCode.E_VAL_FAILED, s);
            } else {
                rsp.setResult(ErrorCode.SUCCESS);
            }
            return rsp;
        } catch (Exception e) {
            log.error("Exception caught: ", e);
            rsp.setResult(ErrorCode.FAIL);
            return rsp;
        }
    }
    
    @SuppressWarnings("unchecked")
    private String saveTrack(Session ss, Track track) {
        Criteria crit = ss.createCriteria(Track.class);
        crit.add(Restrictions.eq("active", true));
        crit.add(Restrictions.eq("equibaseId", track.getEquibaseId()));
        if (track.getCountryCode() != null) {
            crit.add(Restrictions.eq("countryCode", track.getCountryCode()));
        } 
        else {
            return "Empty country code";
        }
        if (track.getPerfType() != null) {
            crit.add(Restrictions.eq("perfType", track.getPerfType()));
        }
        else {
            return "Empty perf type";
        }
        if (track.getTrackType() != null) {
            crit.add(Restrictions.eq("trackType", track.getTrackType()));
        }
        else {
            return "Empty track type";
        }
        if (track.getId() != null) {
            crit.add(Restrictions.ne("id",  track.getId()));
        }
        List<Track> tracks = (List<Track>) crit.list();
        if (tracks != null && tracks.size() > 0) {
            return "Track already exisits.";
        } 
        ss.saveOrUpdate(track);
        return null;
    }

}
