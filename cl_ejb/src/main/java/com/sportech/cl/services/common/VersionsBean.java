package com.sportech.cl.services.common;

import java.io.IOException;
import java.net.URL;
import java.util.Enumeration;
import java.util.jar.Attributes;
import java.util.jar.Manifest;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Remote;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.interceptor.Interceptors;
import javax.persistence.PersistenceContext;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.hibernate.SQLQuery;
import org.hibernate.Session;

import com.sportech.cl.model.common.CardLoaderVersion;
import com.sportech.cl.model.common.ErrorCode;
import com.sportech.cl.model.common.Version;
import com.sportech.cl.model.response.VersionResponse;
import com.sportech.cl.services.imports.ImportsBean;

@Stateless
@Interceptors(com.sportech.cl.internal.utils.Rollbacker.class)
@LocalBean
@Remote(VersionsRemote.class)
public class VersionsBean implements VersionsInterface {
	
	@Resource
	SessionContext ctx;
	
	@PersistenceContext
    private Session ss;
	
	@PostConstruct
	public void ejbCreated() {
	}

	private void getBuildVersion(CardLoaderVersion vers) {
		try {
			Enumeration<URL> resources = getClass().getClassLoader().getResources("META-INF/MANIFEST.MF");
			while (resources.hasMoreElements()) {
				try {
					Manifest manifest = new Manifest(resources.nextElement().openStream());
					Attributes attrs = manifest.getMainAttributes(); // "Application-Version");
					if (attrs != null) {
						String version = attrs.getValue("Application-Version");
						String buildNumber = attrs.getValue("Build-Number");
						if (!StringUtils.isEmpty(version)) {
							if (!StringUtils.isEmpty(buildNumber)) {
								version += " build " + buildNumber;
							}
							vers.addComponent(new Version("Application Version", version));
							return;
						}
						
					}

				} catch (IOException e) {
					log.error("IOException caught: ", e);
				}
			}
		} catch (Exception ex) {
			log.error("Exception caught: ", ex);
		}
	}

	public VersionResponse getVersions() {
		VersionResponse rsp = new VersionResponse();

		try {
			CardLoaderVersion vers = new CardLoaderVersion();

			getBuildVersion(vers);

			SQLQuery query = ss.createSQLQuery("select schema_version()");
			String dbVers = (String) query.uniqueResult();

			if (dbVers != null) {
				vers.addComponent(new Version("Database Schema", dbVers));
			}

			rsp.setVersions(vers);
		}

		catch (Exception e) {

			log.error("Exception caught: ", e);

			rsp.setResult(ErrorCode.FAIL);
		}

		return rsp;
	}

	@EJB
	ImportsBean imports_worker;

	static private final Logger log = Logger.getLogger(VersionsBean.class);
}
