package com.sportech.cl.services.security;

import java.util.Iterator;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.ejb.LocalBean;
import javax.ejb.Remote;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.interceptor.Interceptors;
import javax.persistence.PersistenceContext;
import javax.transaction.TransactionSynchronizationRegistry;

import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;

import com.sportech.cl.internal.utils.RequiredParam;
import com.sportech.cl.internal.utils.SecurityHelper;
import com.sportech.cl.model.common.ErrorCode;
import com.sportech.cl.model.database.Ace;
import com.sportech.cl.model.response.AceListResponse;
import com.sportech.cl.model.response.GenericResponse;
import com.sportech.cl.model.security.ObjectType;
import com.sportech.cl.model.security.Permission;
import com.sportech.cl.model.security.Privilege;
import com.sportech.cl.model.security.SecurityToken;

@Stateless
@Interceptors({com.sportech.cl.internal.utils.Rollbacker.class, com.sportech.cl.internal.utils.ParametersChecker.class})
@LocalBean
@Remote(AccessControlRemote.class)
@TransactionAttribute(javax.ejb.TransactionAttributeType.REQUIRES_NEW)
public class AccessControlBean implements AccessControlRemote {
    @Resource(mappedName = "java:comp/TransactionSynchronizationRegistry")
    TransactionSynchronizationRegistry TSR;
    
    @Resource SessionContext ctx;
    
    @PersistenceContext
    private Session ss;
    
    public AccessControlBean() {}
    
    @PostConstruct
    protected void Init() 
    {
    }
    
    /**
     * Create and save a new Access Control entry indicating ownership of a database entity
     */
    @Override
    public void SetCreator(SecurityToken token, ObjectType objType, Long objId) {
        try {
            Ace entry = new Ace();
            /*
             * TODO: finally check field size (int or long)
             */
            entry.setPermissions( (int)(Permission.READ_PERM | Permission.WRITE_PERM | Permission.DELETE_PERM | Permission.OWNER_PERM) );
            entry.setActorTypeId( token.getActorTypeId() );
            entry.setActorId( token.getActorId() );
            entry.setObjectTypeId( objType.getId() );
            entry.setObjectId( objId.intValue() );
            
            ss.save( entry );
            ss.flush();
            
        }
        catch( Exception e) {
            log.error( "Exception caught: ", e );
            ctx.setRollbackOnly();
        }            
    }
    
    /**
     * Wrapper for database function `has_privilege`
     */
    @Override
    public boolean HasPrivilege(SecurityToken token, ObjectType objType, Long objId, Long accessMask) {
        try {
           
            // If this isn't a managed object type, just return true
            if (null == objType)
                return true;
            
            Query query = ss.createSQLQuery("select has_privilege(?,?,?,?,?)").
                setInteger(0, objType.getId()).
                setInteger(1, objId.intValue()).
                setInteger(2, token.getActorTypeId()).
                setInteger(3, token.getActorId()).
                setInteger(4, accessMask.intValue());
            
            Object resultObj = query.uniqueResult();
            if (null != resultObj)
                return ((Boolean)resultObj).booleanValue();
        }
        catch( Exception e) {
            log.error( "Exception caught: ", e );
            ctx.setRollbackOnly();
        }   
        
        return false;
    }
    
    /**
     * Creates "default" entries from those ACL entries with the default bit set, and the object_id set to NULL
     */
    @SuppressWarnings("unchecked")
    @Override
    public void CreateDefaultEntries(ObjectType objType, Long objId) {
        
        try {
                       
            Query query = ss.createSQLQuery("select ac_pk, (permissions_bitmap ^ 256) as permissions_bitmap, actor_type_fk, actor_id, object_type_fk, object_id from ac_entry where	(permissions_bitmap & 256) > 0 and object_type_fk = (?)");
            query.setInteger(0, objType.getId());
            ((SQLQuery)query).addEntity(Ace.class);
                    
            List<Ace> entries = (List<Ace>)query.list();
                        
            Iterator<Ace> itor = entries.listIterator();
            while (itor.hasNext()) {
                Ace defAce = itor.next();
                
                Ace entry = new Ace();
                entry.setPermissions( defAce.getPermissions() );
                entry.setActorTypeId( defAce.getActorTypeId() );
                entry.setActorId( defAce.getActorId() );
                entry.setObjectTypeId( objType.getId() );
                entry.setObjectId( objId.intValue() );
                
                ss.save( entry );
            }
        }
        catch( Exception e) {
            log.error( "Exception caught: ", e );
            ctx.setRollbackOnly();
        }   
    }
    
    @Override
    public AceListResponse      getObjectACL(SecurityToken token, @RequiredParam(name="objTypeId") Integer objTypeId, 
                                                                  @RequiredParam(name="id")        Long    objId)
    {
        try {
            
            if( !checkObjectTypeId( objTypeId ) )
                return new AceListResponse(ErrorCode.E_VAL_FAILED, "Empty or incorrect object type specified ");
            
            if( !SecurityHelper.checkAccess(token, objId, ObjectType.getById(objTypeId), Permission.OWNER_PERM) ) 
                return new AceListResponse(ErrorCode.E_ACCESS_DENIED);
         
            @SuppressWarnings("unchecked")
            List<Ace> acl = (List<Ace>)ss.createSQLQuery("SELECT * " + OBJECT_ACL_CLAUSE) 
                                                        .addEntity(Ace.class)
                                                        .setInteger(0, objTypeId)
                                                        .setLong(1, objId)
                                                        .setLong(2, Permission.DEFAULT_PERM)
                                                        .list();
                    
            AceListResponse rsp = new AceListResponse();
            
            rsp.setEntries(acl);
            return rsp;
        }
        catch( Exception e ) {
            log.error( "Exception caught: ", e );
            
            return new AceListResponse( ErrorCode.FAIL);
        }
    }
    
    @Override
    public GenericResponse      setObjectACL(SecurityToken token, @RequiredParam(name="objTypeId") Integer   objTypeId, 
                                                                  @RequiredParam(name="id")        Long      objId, 
                                                                                                   List<Ace> acl )
    {
        try {
            
            if( !checkObjectTypeId( objTypeId ) )
                return new AceListResponse(ErrorCode.E_VAL_FAILED, "Empty or incorrect object type specified ");

            if( !SecurityHelper.checkAccess(token, objId, ObjectType.getById(objTypeId), Permission.OWNER_PERM) ) 
                return new GenericResponse(ErrorCode.E_ACCESS_DENIED);

            /*
             * First of all - cleanup existing ACL
             */
            Query query = ss.createSQLQuery( "DELETE " + OBJECT_ACL_CLAUSE)
                                           .setInteger(0, objTypeId)
                                           .setLong(1, objId)
                                           .setLong(2, Permission.DEFAULT_PERM);
            query.executeUpdate();

            if(acl != null )
            {
                for( Ace ace : acl )
                {
                    ace.setId(null);
                    ace.setObjectTypeId(objTypeId);
                    ace.setObjectId(objId.intValue());
                    
                    ss.save(ace);
                }
            }
            
            ss.flush();
            
            return new GenericResponse();
        }
        catch( Exception e ) {
            log.error( "Exception caught: ", e );
            
            return new GenericResponse( ErrorCode.FAIL);
        }
    }

    @Override
    public AceListResponse      getObjectTypeACL(SecurityToken token, @RequiredParam(name="objTypeId") Integer objTypeId)
    {
        try {
           
            if( !checkObjectTypeId( objTypeId ) )
                return new AceListResponse(ErrorCode.E_VAL_FAILED, "Empty or incorrect object type specified ");
            
            if( !SecurityHelper.checkAccess(token, Privilege.SUPERUSER) ) 
                return new AceListResponse(ErrorCode.E_ACCESS_DENIED);

            @SuppressWarnings("unchecked")
            List<Ace> acl = (List<Ace>)ss.createSQLQuery("SELECT * " + OBJECT_TYPE_ACL_CLAUSE) 
                                                        .addEntity(Ace.class)
                                                        .setInteger(0, objTypeId)
                                                        .setLong(1, Permission.DEFAULT_PERM)
                                                        .list();
                    
            AceListResponse rsp = new AceListResponse();
            
            rsp.setEntries(acl);
            return rsp;
        }
        catch( Exception e ) {
            log.error( "Exception caught: ", e );
            
            return new AceListResponse( ErrorCode.FAIL);
        }
    }
    
    @Override
    public GenericResponse      setObjectTypeACL(SecurityToken token, @RequiredParam(name="objTypeId") Integer   objTypeId, 
                                                                                                       List<Ace> acl )
    {
        try {
            
            if( !checkObjectTypeId( objTypeId ) )
                return new AceListResponse(ErrorCode.E_VAL_FAILED, "Empty or incorrect object type specified ");

            if( !SecurityHelper.checkAccess(token, Privilege.SUPERUSER) ) 
                return new GenericResponse(ErrorCode.E_ACCESS_DENIED);
            /*
             * First of all - cleanup existing ACL
             */
            Query query = ss.createSQLQuery( "DELETE " + OBJECT_TYPE_ACL_CLAUSE)
                                           .setInteger(0, objTypeId)
                                           .setLong(1, Permission.DEFAULT_PERM);
            query.executeUpdate();

            if(acl != null )
            {
                for( Ace ace : acl )
                {
                    ace.setId(null);
                    ace.setObjectTypeId(objTypeId);
                    ace.setObjectId(null);
                    
                    ss.save(ace);
                }
            }
            
            ss.flush();
            
            return new GenericResponse();
        }
        catch( Exception e ) {
            log.error( "Exception caught: ", e );
            
            return new GenericResponse( ErrorCode.FAIL);
        }
    }

    @Override
    public AceListResponse      getDefaultACL(SecurityToken token, @RequiredParam(name="objTypeId") Integer objTypeId)
    {
        try {
            
            if( !checkObjectTypeId( objTypeId ) )
                return new AceListResponse(ErrorCode.E_VAL_FAILED, "Empty or incorrect object type specified ");
            
            if( !SecurityHelper.checkAccess(token, Privilege.SUPERUSER) ) 
                return new AceListResponse(ErrorCode.E_ACCESS_DENIED);

            @SuppressWarnings("unchecked")
            List<Ace> acl = (List<Ace>)ss.createSQLQuery("SELECT * " + DEFAULT_ACL_CLAUSE) 
                                                        .addEntity(Ace.class)
                                                        .setInteger(0, objTypeId)
                                                        .setLong(1, Permission.DEFAULT_PERM)
                                                        .list();
            
            if( acl != null )
            {
                for( Ace ace : acl )
                    ace.setPermissions(ace.getPermissions() ^ (int)Permission.DEFAULT_PERM);
            }
                    
            AceListResponse rsp = new AceListResponse();
            
            rsp.setEntries(acl);
            return rsp;
        }
        catch( Exception e ) {
            log.error( "Exception caught: ", e );
            
            return new AceListResponse( ErrorCode.FAIL);
        }
    }
    
    @Override
    public GenericResponse      setDefaultACL(SecurityToken token, @RequiredParam(name="objTypeId") Integer   objTypeId, 
                                                                                                    List<Ace> acl )
    {
        try {
            
            if( !checkObjectTypeId( objTypeId ) )
                return new AceListResponse(ErrorCode.E_VAL_FAILED, "Empty or incorrect object type specified ");

            if( !SecurityHelper.checkAccess(token, Privilege.SUPERUSER) ) 
                return new GenericResponse(ErrorCode.E_ACCESS_DENIED);
            /*
             * First of all - cleanup existing ACL
             */
            Query query = ss.createSQLQuery( "DELETE " + DEFAULT_ACL_CLAUSE)
                                           .setInteger(0, objTypeId)
                                           .setLong(1, Permission.DEFAULT_PERM);
            query.executeUpdate();

            if(acl != null )
            {
                for( Ace ace : acl )
                {
                    ace.setId(null);
                    ace.setObjectTypeId(objTypeId);
                    ace.setObjectId(null);
                    ace.setPermissions(ace.getPermissions() | (int)Permission.DEFAULT_PERM);
                    
                    ss.save(ace);
                }
            }
            
            ss.flush();
            
            return new GenericResponse();
        }
        catch( Exception e ) {
            log.error( "Exception caught: ", e );
            
            return new GenericResponse( ErrorCode.FAIL);
        }
    }
    
    protected boolean checkObjectTypeId( Integer objTypeId )
    {
        ObjectType     supportedTypes[] = { ObjectType.PATTERN, ObjectType.HARD_CARD, ObjectType.SOFT_CARD };
        
        if( objTypeId == null )
            return false;
        
        for( ObjectType objType : supportedTypes )
        {
            if( objType.getId().equals(objTypeId))
                return true;
        }
        
        return false;
        
        
    }

    static private final String OBJECT_ACL_CLAUSE = " from ac_entry where object_type_fk = ?"
                                                   +" and object_id = ?"
                                                   +" and ((permissions_bitmap & ? ) = 0)";
    static private final String OBJECT_TYPE_ACL_CLAUSE = " from ac_entry where object_type_fk = ?"
                                                        +" and object_id is null"
                                                        +" and ((permissions_bitmap & ? ) = 0)";
    static private final String DEFAULT_ACL_CLAUSE = " from ac_entry where object_type_fk = ?"
                                                    +" and object_id is null"
                                                    +" and ((permissions_bitmap & ? ) > 0)";
    
    static private final Logger log = Logger.getLogger(AccessControlBean.class);
}
