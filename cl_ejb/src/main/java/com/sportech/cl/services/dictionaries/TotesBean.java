package com.sportech.cl.services.dictionaries;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.interceptor.Interceptors;
import javax.persistence.PersistenceContext;

import org.apache.log4j.Logger;
import org.hibernate.Session;

import com.sportech.cl.internal.utils.FlatObjectBeanHelper;
import com.sportech.cl.internal.utils.RequiredParam;
import com.sportech.cl.internal.utils.SecurityHelper;
import com.sportech.cl.internal.validators.ToteValidator;
import com.sportech.cl.model.common.ErrorCode;
import com.sportech.cl.model.common.PageParams;
import com.sportech.cl.model.database.Tote;
import com.sportech.cl.model.database.ToteOutline;
import com.sportech.cl.model.response.AddEntityResponse;
import com.sportech.cl.model.response.GenericResponse;
import com.sportech.cl.model.response.ToteListResponse;
import com.sportech.cl.model.response.ToteResponse;
import com.sportech.cl.model.security.ActorType;
import com.sportech.cl.model.security.Privilege;
import com.sportech.cl.model.security.SecurityToken;
import com.sportech.cl.services.security.UsersBean;

@Stateless
@Interceptors({ com.sportech.cl.internal.utils.Rollbacker.class, com.sportech.cl.internal.utils.ParametersChecker.class })
@LocalBean
@Remote(TotesRemote.class)
public class TotesBean implements TotesRemote {

	@PersistenceContext
    private Session ss;
	
	public TotesBean() {
	}

	@PostConstruct
	protected void init() {
		helper = new FlatObjectBeanHelper<Tote, ToteOutline>(ss, null, Privilege.READ_DICTIONARIES, Privilege.MANAGE_TOTES,
		        Tote.class, ToteOutline.class, log);
		helper.setValidator(new ToteValidator());
		helper.hasObjectLevelACL(false);
	}

	@Override
	public ToteResponse get(SecurityToken token, @RequiredParam(name = "id") Long id) {
		ToteResponse r = new ToteResponse();
		Tote op = (Tote) ss.get(Tote.class, id);
		if (op != null) {
			int s = op.getTracks().size();
			r.setStatus(s);
			r.setTote(op);
		}
		r.setStatus(0);
		return r;
	}

	@Override
	public ToteListResponse find(SecurityToken token, ToteOutline[] query, Boolean useLike, PageParams pages) {
		ToteListResponse response = (ToteListResponse) helper.find(token, query, useLike, pages, new ToteListResponse(), null);
		if (token.getUser() != null && !SecurityToken.isAdmin(token)) {
			if (response.getStatus() == 0) {
				List<ToteOutline> filtered = new ArrayList<ToteOutline>();
				Set<Long> totes = token.getUser().getTotes();
				for (ToteOutline s : response.getTotes()) {
					if (totes != null && totes.contains(s.getId())) {
						filtered.add(s);
					}
				}
				response.setTotes(filtered);
				if (filtered.size() == 0) {
					response.setResult(ErrorCode.E_NOT_FOUND.getId(), "No totes available.");
				}
			}
		}
		return response;
	}

	@Override
	public AddEntityResponse add(SecurityToken token, @RequiredParam(name = "Tote") Tote tote,
	        @RequiredParam(name = "username") String username, @RequiredParam(name = "password") String password) {
		try {
			// New Tote must be ACTIVE
			tote.setActive(true);

			AddEntityResponse rsp = helper.add(token, tote);

			if (!rsp.isOK())
				return rsp;

			AddEntityResponse userRsp = users_worker.addToteUser(token, tote, username, password);

			if (!userRsp.isOK())
				return userRsp;

			return rsp;
		} catch (Exception e) {
			log.error("Exception caught: ", e);

			return new AddEntityResponse(ErrorCode.FAIL);
		}

	}

	@Override
	public GenericResponse update(SecurityToken token, @RequiredParam(name = "id") Long id,
	        @RequiredParam(name = "new_tote") Tote new_tote) {
		return helper.update(token, id, new_tote);
	}

	public GenericResponse enable(SecurityToken token, @RequiredParam(name = "id") Long id,
	        @RequiredParam(name = "enabled") Boolean enabled) {
		GenericResponse rsp = new GenericResponse();

		try {

			if (!SecurityHelper.checkAccess(token, Privilege.MANAGE_TOTES)) {
				rsp.setResult(ErrorCode.E_ACCESS_DENIED);
				return rsp;
			}

			Tote op = (Tote) ss.get(Tote.class, id);

			if (op != null) {
				GenericResponse userRsp = users_worker.enableUsersForActor(token, ActorType.TOTE, id, enabled);

				if (!userRsp.isOK())
					return userRsp;

				op.setActive(enabled);

				ss.flush();
			} else {
				rsp.setResult(ErrorCode.E_NOT_FOUND);
			}

			return rsp;
		} catch (Exception e) {
			log.error("Exception caught: ", e);

			rsp.setResult(ErrorCode.FAIL);
			return rsp;
		}
	}

	@Override
	public GenericResponse delete(SecurityToken token, @RequiredParam(name = "id") Long id) {
		try {
			GenericResponse userRsp = users_worker.delUsersForActor(token, ActorType.TOTE, id);

			if (!userRsp.isOK())
				return userRsp;

			return helper.delete(token, id);
		} catch (Exception e) {
			log.error("Exception caught: ", e);

			return new GenericResponse(ErrorCode.FAIL);
		}
	}

	@Override
	public GenericResponse saveTote(Tote tote) {
		GenericResponse response = new GenericResponse();
		try {
			ss.saveOrUpdate(tote);
		} catch (Exception e) {
			log.error("Exception caught: ", e);
			response.setStatus(ErrorCode.FAIL.getId());
		}
		return response;
	}

	protected FlatObjectBeanHelper<Tote, ToteOutline> helper;

	@EJB
	protected UsersBean users_worker;

	static private final Logger log = Logger.getLogger(TotesBean.class);
}
