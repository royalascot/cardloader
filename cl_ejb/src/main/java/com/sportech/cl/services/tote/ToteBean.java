package com.sportech.cl.services.tote;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.TimeZone;

import javax.annotation.PostConstruct;
import javax.ejb.LocalBean;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.persistence.PersistenceContext;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.apache.log4j.Logger;
import org.hibernate.Session;

import com.sportech.cl.internal.utils.AuditHelper;
import com.sportech.cl.internal.utils.ReadOnlyBeanHelper;
import com.sportech.cl.internal.utils.RequiredParam;
import com.sportech.cl.model.common.ErrorCode;
import com.sportech.cl.model.database.HardCard;
import com.sportech.cl.model.database.PassThroughBag;
import com.sportech.cl.model.database.PoolMinimum;
import com.sportech.cl.model.database.PoolParameter;
import com.sportech.cl.model.database.SoftCard;
import com.sportech.cl.model.database.SoftCardBase;
import com.sportech.cl.model.database.SoftCardInUse;
import com.sportech.cl.model.database.SoftCardOutline;
import com.sportech.cl.model.database.SoftPool;
import com.sportech.cl.model.database.SoftRace;
import com.sportech.cl.model.database.Tote;
import com.sportech.cl.model.database.ToteCard;
import com.sportech.cl.model.database.TotePool;
import com.sportech.cl.model.database.ToteRace;
import com.sportech.cl.model.database.Track;
import com.sportech.cl.model.database.utils.JSonHelper;
import com.sportech.cl.model.database.utils.PoolHelper;
import com.sportech.cl.model.response.GenericResponse;
import com.sportech.cl.model.response.ToteCardListResponse;
import com.sportech.cl.model.response.ToteCardResponse;
import com.sportech.cl.model.security.ObjectType;
import com.sportech.cl.model.security.Privilege;
import com.sportech.cl.model.security.SecurityToken;
import com.sportech.common.model.CardType;
import com.sportech.common.util.DecimalHelper;

/**
 * Session Bean implementation class HardCardsBean
 */
@Stateless
@LocalBean
@Remote(ToteRemote.class)
public class ToteBean implements ToteRemote, Comparator<SoftCardBase> {

    private static final String AUDIT_AREA = "Tote Web Service";

    static private final Logger log = Logger.getLogger(ToteBean.class);

    private static final long GSL = 40;
    private static final long PK4 = 9;

    protected ReadOnlyBeanHelper<ToteCard, SoftCardOutline> helper;

    @PersistenceContext
    Session ss;
    
    public ToteBean() {
    }

    @PostConstruct
    protected void init() {
        helper = new ReadOnlyBeanHelper<ToteCard, SoftCardOutline>(ss, ObjectType.SOFT_CARD, Privilege.READ_CARDS,
                ToteCard.class, SoftCardOutline.class, log);
    }

    @Override
    public ToteCardResponse getCard(SecurityToken token, @RequiredParam(name = "id") Long id) {
        log.info("Tote " + token.getLoginName() + " getCard(" + id + ")");
        ToteCardResponse rsp = new ToteCardResponse();
        
        
        SoftCard softCard = (SoftCard) ss.get(SoftCard.class, id);
        if (softCard != null) {
        	
            ToteCard card = new ToteCard();
            rsp.setCard(card);
            try {
            	PropertyUtils.copyProperties(card, softCard);
            	card.setSource(softCard.getSource());
            	card.setRaces(new HashSet<ToteRace>());
            	card.setPools(new HashSet<TotePool>());
            	for (SoftRace softRace : softCard.getRaces()) {
            		ToteRace tr = new ToteRace();
            		PropertyUtils.copyProperties(tr, softRace);
            		if (softRace.getHardRace() == null) {
            			SoftRace sourceRace = softRace.getParentRace();
            			if (sourceRace != null) {
            				SoftCard sc = sourceRace.getParent();
            				tr.setMirrorFrom(sc.getId() + ":" + sourceRace.getNumber());
            			}
            		}
            		card.getRaces().add(tr);
            	}
            	for (SoftPool softPool : softCard.getPools()) {
            		TotePool tp = new TotePool();
            		PropertyUtils.copyProperties(tp, softPool);
            		card.getPools().add(tp);
            	}            	
            } catch (Exception e) {
            	log.error("Error copying properties", e);
            }
            if (!card.getApproved() || card.getPools().size() == 0 || card.getRaces().size() == 0) {
            	rsp.setResult(ErrorCode.E_NOT_FOUND, "This is not CURRENT or valid card");
                rsp.setCard(null);
                log.error("Tote getCard failed:" + rsp.getStatus());
                return rsp;
            }
            
            Track t = card.getTrack();
            if (t != null) {
                TimeZone trackTimeZone = TimeZone.getDefault();
                TimeZone toteTimeZone = TimeZone.getDefault();
                if (t.getTimeZone() != null) {
                    trackTimeZone = t.getTimeZone();
                }
                Tote tote = (Tote) ss.get(Tote.class, token.getUserId().longValue());
                if (tote != null && tote.getTimeZone() != null) {
                    toteTimeZone = tote.getTimeZone();
                }
                for (ToteRace tr : card.getRaces()) {
                    tr.setTimeZone(toteTimeZone);
                    tr.setTrackTimeZone(trackTimeZone);
                    tr.setCardDate(card.getCardDate());
                }
            }
            // Populating pass through fields
            if (card.getHardCardId() != null) {
                HardCard hc = (HardCard) ss.get(HardCard.class, card.getHardCardId());
                if (hc != null) {
                    String p = hc.getPassThroughs();
                    if (!StringUtils.isEmpty(p)) {
                        PassThroughBag pb = JSonHelper.fromJSon(PassThroughBag.class, p);
                        if (pb != null) {
                            card.setHostMeetNumber(pb.getHostMeetNumber());
                            card.setTrackIdentifier(pb.getTrackId());
                            card.setMeetingCodeUK(pb.getMeetingCodeUK());
                        }
                    }
                    processCdms(card);
                    card.setEventCode(hc.getEventCode());
                }
            }

            changeCardType(card);
            AuditHelper.writeAuditLog(ss, token, card, "Tote getCard", AUDIT_AREA);
        }
        else {
            rsp.setResult(ErrorCode.E_NOT_FOUND, "Can not find card.");
        }
        log.info("Tote " + token.getLoginName() + " getCard() result:" + rsp.getStatus());
        return rsp;
    }

    @Override
    public ToteCardListResponse findCurrentCards(SecurityToken token) {

        log.info("Tote findCurrentCards:" + token.getLoginName());

        /*
         * Criteria for 'current' cards for particular Tote: - Card date >=
         * TODAY - Card is approved - This particular Tote is in the list of
         * 'allowed Totes' for card
         */
        SoftCardOutline[] query = new SoftCardOutline[1];

        query[0] = new SoftCardOutline();

        query[0].setApproved(true);

        Calendar today = Calendar.getInstance();

        today.set(Calendar.HOUR_OF_DAY, 0);
        today.set(Calendar.MINUTE, 0);
        today.set(Calendar.SECOND, 0);
        today.set(Calendar.MILLISECOND, 0);

        ToteCardListResponse response = (ToteCardListResponse) helper.findByDate(token, query, today.getTime(), null, false, null,
                SoftCardOutline.DATE_PROP_NAME, new ToteCardListResponse(), null);
        if (response.getStatus() == 0) {
            Tote t = (Tote) ss.get(Tote.class, token.getUserId().longValue());
            boolean checkSingleApproval = false;
            if (t.getFlag() != null && t.getFlag()) {
                checkSingleApproval = true;
                log.info("Tote '" + t.getToteCode() + "' needs card approval.");
            }
            List<SoftCardOutline> filtered = new ArrayList<SoftCardOutline>();

            for (SoftCardOutline s : response.getCards()) {
                if (StringUtils.contains(s.getToteName(), token.getLoginName())) {
                    if (checkSingleApproval) {
                        if (!StringUtils.contains(s.getToteApprovalList(), token.getLoginName())) {
                            continue;
                        }
                    }
                    if (t.getTracks() != null) {
                        for (Track track : t.getTracks()) {
                            if (track.getId().longValue() == s.getTrackId().longValue()) {
                                changeCardType(s);
                                ss.evict(s);
                                filtered.add(s);
                                log.info("findCurrentCards: add card '" + s.getId() + "' to tote '" + t.getToteCode() + "'.");
                                AuditHelper.writeAuditLog(ss, token, s, "Tote findCard() Found One", AUDIT_AREA);
                                break;
                            }
                        }
                    }
                }
            }
            Collections.sort(filtered, this);
            response.setCards(filtered);
            if (filtered.size() == 0) {
                response.setResult(ErrorCode.E_NOT_FOUND.getId(), "No cards available.");
            }
        }
        log.info("findCurrentCards: returned status:" + response.getStatus());
        if (response.isOK()) {
            log.info("findCurrentCards() found " + response.getCards().size() + " cards.");
        }
        return response;
    }

    @Override
    public GenericResponse setCardInUse(SecurityToken token, @RequiredParam(name = "id") Long id,
            @RequiredParam(name = "isInUse") Boolean isInUse) {
        log.info("setCardInUse(" + id + ")");
        try {
            SoftCardOutline sc = (SoftCardOutline) ss.get(SoftCardOutline.class, id);

            if (sc == null)
                return new GenericResponse(ErrorCode.E_NOT_FOUND);

            if (isInUse) {
                SoftCardInUse su = new SoftCardInUse();

                su.setSoftCardId(id);
                su.setToteId(token.getActorId().longValue());

                ss.saveOrUpdate(su);
                AuditHelper.writeAuditLog(ss, token, sc, "Tote Set Card Approved", AUDIT_AREA);

            } else {
                SoftCardInUse su = (SoftCardInUse) ss
                        .createQuery("from SoftCardInUse as su where su.softCardId = :cid and su.toteId=:tid").setLong("cid", id)
                        .setLong("tid", token.getActorId().longValue()).uniqueResult();

                if (su != null)
                    ss.delete(su);
            }

            ss.flush();

            return new GenericResponse();
        } catch (Exception e) {
            log.error("Exception caught: ", e);

            return new GenericResponse(ErrorCode.FAIL);
        }
    }

    // Tote does not support VirtualHarness type. This is a temporary fix until
    // tote code gets updated.
    private void changeCardType(SoftCardBase card) {
        if (card.getCardType() == CardType.VirtualHarness) {
            card.setCardType(CardType.VirtualHorse);
        }
    }

    public int compare(SoftCardBase c1, SoftCardBase c2) {

        if (c1 == null || c2 == null) {
            return -1;
        }
        if (c1.getHardCardId() == null || c2.getHardCardId() == null) {
            return -1;
        }
        HardCard hc1 = (HardCard) ss.get(HardCard.class, c1.getHardCardId());
        HardCard hc2 = (HardCard) ss.get(HardCard.class, c2.getHardCardId());
        if (hc1 == null || hc2 == null) {
            return -1;
        }

        TimeZone trackTimeZone1 = TimeZone.getDefault();
        TimeZone trackTimeZone2 = TimeZone.getDefault();
        Track t1 = c1.getTrack();
        Track t2 = c2.getTrack();
        if (t1 != null && t1.getTimeZone() != null) {
            trackTimeZone1 = t1.getTimeZone();
        }
        if (t2 != null && t2.getTimeZone() != null) {
            trackTimeZone2 = t2.getTimeZone();
        }

        Date p1 = hc1.getFirstRaceTime();
        Date p2 = hc2.getFirstRaceTime();
        if (p1 == null || p2 == null) {
            return -1;
        }
        long l1 = DateUtils.getFragmentInMilliseconds(p1, Calendar.DAY_OF_YEAR) - trackTimeZone1.getRawOffset();
        long l2 = DateUtils.getFragmentInMilliseconds(p2, Calendar.DAY_OF_YEAR) - trackTimeZone2.getRawOffset();

        return (int) (l1 - l2);

    }

    private void processCdms(ToteCard card) {
        if (card.getConfig() == null) {
            log.error("Card does not have configuration.");
            return;
        }
        
        PoolMinimum p = card.getConfig().getPoolMinimum();
        if (p == null) {
            log.error("Card config does not have CDMs.");
            return;            
        }
        
        PoolMinimum pm = new PoolMinimum();
        pm.setDefaultMinimums(PoolHelper.clonePoolParameters(p.getDefaultMinimums(), false));
        pm.setOverrideMinimums(PoolHelper.clonePoolParameters(p.getOverrideMinimums(), false));

        for (TotePool tp : card.getPools()) {
            if (tp.getPoolTypeId() != null && tp.getPoolTypeId() == GSL) {
                Long rid = tp.getRaceBitmap();
                if (rid != null) {
                    for (long i = 1; i <= card.getRaces().size(); i++) {
                        if (PoolHelper.isRaceInPool(i, rid)) {
                            PoolParameter gsl = null;
                            for (PoolParameter pp : p.getOverrideMinimums()) {
                                if (pp.getRaces().contains(i)) {
                                    gsl = pp;
                                    break;
                                }
                            }
                            if (gsl == null) {
                                for (PoolParameter pp : p.getDefaultMinimums()) {
                                    if (pp.getId() == GSL) {
                                        gsl = pp;
                                        break;
                                    }
                                }
                            }
                            if (gsl != null) {
                                PoolParameter pk4 = new PoolParameter();
                                pk4.setIncrementAmount(DecimalHelper.toCents(gsl.getIncrementAmount()));
                                pk4.setMinAmount(DecimalHelper.toCents(gsl.getMinAmount()));
                                pk4.getRaces().add(i);
                                pk4.setId(PK4); 
                                pk4.setParameterType(gsl.getParameterType());
                                pm.getOverrideMinimums().add(pk4);
                            }
                            break;
                        }
                    }
                }
            }
        }
        card.setPoolMinimum(pm);
    }

}
