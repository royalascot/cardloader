package com.sportech.cl.services.dictionaries;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.interceptor.Interceptors;
import javax.persistence.PersistenceContext;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;

import com.sportech.cl.model.database.CardConfig;

@Stateless
@Interceptors(com.sportech.cl.internal.utils.Rollbacker.class)
@LocalBean
@Remote(PoolTypesRemote.class)
@SuppressWarnings("unchecked")
public class PoolTypesBean implements PoolTypesRemote {
    
    static private final Logger log = Logger.getLogger(PoolTypesBean.class);

    @PersistenceContext
    private Session ss;
    
    public List<CardConfig> find(CardConfig filter) {    
        String ql = "FROM CardConfig";
        String name = null;
        if (filter != null) {
            if (StringUtils.isNotBlank(filter.getName())) {
                ql += " WHERE name = :name";
                name = filter.getName();
            }
        }
        Query query = ss.createQuery(ql);
        if (name != null) {
            query.setString("name", name);
        }
        List<CardConfig> templates = (List<CardConfig>) query.list();
        return templates;
    }

    public CardConfig get(Long id) {
        CardConfig t = null;
        t = (CardConfig) ss.get(CardConfig.class, id);
        return t;
    }

    public CardConfig save(CardConfig item) {
        ss.saveOrUpdate(item);
        return item;
    }

    public boolean delete(Long id) {
        CardConfig t = get(id);
        if (t != null) {
            ss.delete(t);
            log.info("Card template deleted:" + id);
        }
        return true;
    }

}
