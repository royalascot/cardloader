package com.sportech.cl.services.cards;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import java.util.TimeZone;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.interceptor.Interceptors;
import javax.persistence.PersistenceContext;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.hibernate.Session;

import com.sportech.cl.global.utils.config.ConfigLoader;
import com.sportech.cl.internal.businesslogic.SoftcardGenerator;
import com.sportech.cl.internal.utils.AuditHelper;
import com.sportech.cl.internal.utils.BeanHelper;
import com.sportech.cl.internal.utils.PoolsHelper;
import com.sportech.cl.internal.utils.RequiredParam;
import com.sportech.cl.internal.utils.SecurityHelper;
import com.sportech.cl.internal.validators.SoftCardValidator;
import com.sportech.cl.model.common.ErrorCode;
import com.sportech.cl.model.common.PageParams;
import com.sportech.cl.model.common.PoolMergeType;
import com.sportech.cl.model.database.HardCard;
import com.sportech.cl.model.database.HardRace;
import com.sportech.cl.model.database.Pattern;
import com.sportech.cl.model.database.SoftCard;
import com.sportech.cl.model.database.SoftCardOutline;
import com.sportech.cl.model.database.SoftRace;
import com.sportech.cl.model.database.SoftTote;
import com.sportech.cl.model.database.Tote;
import com.sportech.cl.model.database.Track;
import com.sportech.cl.model.database.utils.SortOrder;
import com.sportech.cl.model.response.AddEntityResponse;
import com.sportech.cl.model.response.GenericResponse;
import com.sportech.cl.model.response.SoftCardListResponse;
import com.sportech.cl.model.response.SoftCardResponse;
import com.sportech.cl.model.security.ObjectType;
import com.sportech.cl.model.security.Permission;
import com.sportech.cl.model.security.Privilege;
import com.sportech.cl.model.security.SecurityToken;
import com.sportech.cl.services.security.AccessControlBean;

/**
 * Session Bean implementation class SoftCardsBean
 */
@Stateless
@Interceptors({ com.sportech.cl.internal.utils.Rollbacker.class, com.sportech.cl.internal.utils.ParametersChecker.class })
@LocalBean
@Remote(SoftCardsRemote.class)
public class SoftCardsBean implements SoftCardsRemote {
   
    @PersistenceContext
    private Session ss;
    
    public SoftCardsBean() {
    }

    @PostConstruct
    protected void init() {
        pools_helper = new PoolsHelper();
        helper = new BeanHelper<SoftCard, SoftCardOutline>(ss, ObjectType.SOFT_CARD, Privilege.READ_CARDS,
                Privilege.MANAGE_CARDS, SoftCard.class, SoftCardOutline.class, log);
        helper.setValidator(new SoftCardValidator(pools_helper));
    }

    @Override
    public SoftCardResponse get(SecurityToken token, @RequiredParam(name = "id") Long id) {
        SoftCardResponse response = (SoftCardResponse) helper.get(token, id, new SoftCardResponse());
        if (response.isOK()) {
            SoftCard sc = response.getCard();
            TimeZone trackTimeZone = TimeZone.getDefault();
            TimeZone timeZone = token.getTimeZone();
            Track t = sc.getTrack();
            if (t != null && t.getTimeZone() != null) {
                trackTimeZone = t.getTimeZone();
            }
            for (SoftRace sr : sc.getRaces()) {
                HardRace hr = sr.getRace();
                hr.setTimeZone(timeZone);
                hr.setTrackTimeZone(trackTimeZone);
            }
        }
        return response;
    }

    @Override
    public SoftCardListResponse find(SecurityToken token, SoftCardOutline[] query, Date startDate, Date endDate, Boolean useLike,
            PageParams pages) {
        return (SoftCardListResponse) helper.findByDate(token, query, startDate, endDate, useLike, pages,
                SoftCardOutline.DATE_PROP_NAME, new SoftCardListResponse(), new SortOrder[] { new SortOrder("description") });
    }

    @Override
    public AddEntityResponse add(SecurityToken token, @RequiredParam(name = "card") SoftCard card) {
        // New SoftCard is always created in 'not-approved' state
        card.setApproved(false);
        card.setLoadCode(StringUtils.upperCase(card.getLoadCode()));
        return helper.add(token, card);
    }

    @Override
    public GenericResponse update(SecurityToken token, @RequiredParam(name = "id") Long id,
            @RequiredParam(name = "new_card") SoftCard new_card) {
        new_card.setLoadCode(StringUtils.upperCase(new_card.getLoadCode()));
        GenericResponse response = new GenericResponse();
        SoftCardValidator validator = new SoftCardValidator();
        try {
            if (validator.Validate(new_card, false, response)) {
                populateTotes(new_card);
                ss.saveOrUpdate(new_card);
                ss.flush();
            }
        } catch (Exception e) {
            log.error("Error updating load card.", e);
            response.setResult(ErrorCode.FAIL, "Error updating load card");
        }
        return response;
    }

    @Override
    public GenericResponse delete(SecurityToken token, @RequiredParam(name = "id") Long id) {
        return helper.delete(token, id);
    }

    @Override
    public GenericResponse approve(SecurityToken token, @RequiredParam(name = "id") Long id,
            @RequiredParam(name = "approved") Boolean approved) {
        GenericResponse rsp = new GenericResponse();

        try {
            if (!SecurityHelper.checkAccess(token, Privilege.MANAGE_CARDS, id, ObjectType.SOFT_CARD, Permission.WRITE_PERM)) {
                rsp.setResult(ErrorCode.E_ACCESS_DENIED);
                return rsp;
            }

            SoftCard card = (SoftCard) ss.get(SoftCard.class, id);

            if (card != null) {
                card.setApproved(approved);

                ss.flush();
                AuditHelper.writeAuditLog(ss, token, card, "Approve");
            } else {
                rsp.setResult(ErrorCode.E_NOT_FOUND);
            }

            return rsp;
        } catch (Exception e) {
            log.error("Exception caught: ", e);

            rsp.setResult(ErrorCode.FAIL);
            return rsp;
        }
    }

    public AddEntityResponse createFromHardCard(SecurityToken token, @RequiredParam(name = "hardCardId") Long hardCardId,
            Long patternId, Integer mergeTypeId, Boolean onlyPatternRaces, boolean withPublish) {
        try {
            if ((!SecurityHelper.checkAccess(token, Privilege.MANAGE_CARDS))
                    || (!SecurityHelper.checkAccess(token, Privilege.READ_CARDS, hardCardId, ObjectType.HARD_CARD,
                            Permission.READ_PERM))
                    || ((patternId != null) && (!SecurityHelper.checkAccess(token, Privilege.READ_PATTERNS, patternId,
                            ObjectType.PATTERN, Permission.READ_PERM)))) {
                return new AddEntityResponse(ErrorCode.E_ACCESS_DENIED);
            } else {
                PoolMergeType mergeType = ConfigLoader.getDefaultMergeType(mergeTypeId);

                HardCard hc = (HardCard) ss.get(HardCard.class, hardCardId);

                if (hc == null)
                    return new AddEntityResponse(ErrorCode.E_NOT_FOUND, "Hard card not found");

                ss.evict(hc);

                Pattern pt = null;

                if (patternId != null) {
                    pt = (Pattern) ss.get(Pattern.class, patternId);

                    if (pt == null)
                        return new AddEntityResponse(ErrorCode.E_NOT_FOUND, "Pattern specified, but not found");

                    ss.evict(pt);
                }

                if ((mergeType != PoolMergeType.HARD_CARD_ONLY) && (pt == null))
                    return new AddEntityResponse(ErrorCode.E_NOT_FOUND,
                            "Pattern expected according to merge type, but not specified");

                if (onlyPatternRaces == null)
                    onlyPatternRaces = false;

                if (onlyPatternRaces && (pt == null))
                    return new AddEntityResponse(ErrorCode.E_NOT_FOUND, "Pattern expected to get race count, but not specified");

                SoftCard sc = SoftcardGenerator.generateSoftCard(hc, pt, mergeType, onlyPatternRaces);
                populateTotes(sc);
                if (withPublish) {
                    sc.setApproved(true);
                }
                ss.save(sc);
                AuditHelper.writeAuditLog(ss, token, sc, "Generated");

                acl_worker.SetCreator(token, ObjectType.SOFT_CARD, sc.getId());
                acl_worker.CreateDefaultEntries(ObjectType.SOFT_CARD, sc.getId());

                ss.flush();

                return new AddEntityResponse(sc.getId());
            }
        } catch (Exception e) {
            log.error("Exception caught: ", e);

            return new AddEntityResponse(ErrorCode.FAIL);
        }
    }

    private void populateTotes(SoftCard sc) {
        Set<String> toteCodes = new HashSet<String>();
        for (SoftTote st : sc.getTotes()) {
            Integer toteId = st.getToteId();
            Tote tote = (Tote) ss.get(Tote.class, toteId.longValue());
            if (tote != null) {
                String name = tote.getToteCode();
                if (!StringUtils.isEmpty(name)) {
                    toteCodes.add(name);
                }
            }
        }
        sc.setToteName(StringUtils.join(toteCodes, ","));
    }

    protected BeanHelper<SoftCard, SoftCardOutline> helper;
    protected PoolsHelper pools_helper;
    @EJB
    protected AccessControlBean acl_worker;

    static private final Logger log = Logger.getLogger(SoftCardsBean.class);
}
