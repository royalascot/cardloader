package com.sportech.cl.services.security;

import java.util.HashSet;
import java.util.Set;
import java.util.TimeZone;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.ejb.LocalBean;
import javax.ejb.Remote;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.interceptor.Interceptors;
import javax.persistence.PersistenceContext;

import org.apache.log4j.Logger;
import org.hibernate.Session;

import com.sportech.cl.internal.utils.SecurityHelper;
import com.sportech.cl.model.common.ErrorCode;
import com.sportech.cl.model.database.Group;
import com.sportech.cl.model.database.Role;
import com.sportech.cl.model.database.Tote;
import com.sportech.cl.model.database.Track;
import com.sportech.cl.model.database.User;
import com.sportech.cl.model.response.LoginResponce;
import com.sportech.cl.model.security.SecurityToken;

/**
 * Session Bean implementation class AuthenticateEJB
 */
@Stateless
@Interceptors(com.sportech.cl.internal.utils.Rollbacker.class)
@LocalBean
@Remote(AuthenticateRemote.class)
@TransactionAttribute(javax.ejb.TransactionAttributeType.REQUIRES_NEW)
public class AuthenticateBean implements AuthenticateRemote {
	
	@Resource
	SessionContext ctx;
	
	@PersistenceContext
	private Session ss;

	@PostConstruct
	public void ejbCreated() {
	}

	@Override
	public LoginResponce Logon(String username, String password) {
		try {
	
			User usr = (User) ss.createQuery("from User as usr where usr.identifier = :name and usr.active=1").setString("name", username.toLowerCase()).uniqueResult();
			if (usr != null) {

				String hashedPwd = SecurityHelper.getHashedPassword(password);

				if (hashedPwd.equals(usr.getPasswd())) {
					Set<Role> roles = new HashSet<Role>();
					Set<Track> tracks = new HashSet<Track>();
					Set<Long> trackIds = new HashSet<Long>();
					Set<Long> totes = new HashSet<Long>();
					trackIds.add(-1L);
					for (Group g : usr.getGroups()) {
						roles.addAll(g.getRoles());
						tracks.addAll(g.getTracks());
						for (Track t : g.getTracks()) {
							trackIds.add(t.getId());
						}
						for (Tote tote : g.getTotes()) {
							totes.add(tote.getId());
						}
					}
					usr.setTracks(tracks);
					usr.setRoles(roles);
					usr.setTrackIds(trackIds);
					usr.setTotes(totes);
					return new LoginResponce(new SecurityToken(usr));
				}
			}

			return new LoginResponce(ErrorCode.E_AUTH_INV_USER_PWD);
		} catch (Exception e) {
			log.error("Exception caught: ", e);

			return new LoginResponce(ErrorCode.FAIL);
		}
	}

	@Override
	public LoginResponce loginTote(String username, String password) {
		try {
	
			Tote usr = (Tote) ss.createQuery("from Tote as usr where usr.loginName = :name and usr.active=1").setString("name", username.toLowerCase()).uniqueResult();
			if (usr != null) {

				String hashedPwd = SecurityHelper.getHashedPassword(password);

				if (hashedPwd.equals(usr.getPassword())) {
					SecurityToken token = new SecurityToken(usr.getToteCode(), usr.getId().intValue(), 2, 1, 4);
					token.setTimeZone(TimeZone.getDefault());
					if (usr.getTimeZone() != null) {
						token.setTimeZone(usr.getTimeZone());
					}
					return new LoginResponce(token);
				}
			}

			return new LoginResponce(ErrorCode.E_AUTH_INV_USER_PWD);
		} catch (Exception e) {
			log.error("Exception caught: ", e);

			return new LoginResponce(ErrorCode.FAIL);
		}
	}

	/*
	 * Local variables
	 */
	// Standard JB logger
	static private final Logger log = Logger.getLogger(AuthenticateBean.class);
}
