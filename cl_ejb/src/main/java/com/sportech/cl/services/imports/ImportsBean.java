package com.sportech.cl.services.imports;

import java.io.ByteArrayInputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import javax.annotation.PostConstruct;
import javax.ejb.LocalBean;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.interceptor.Interceptors;
import javax.persistence.PersistenceContext;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import com.sportech.cl.global.utils.config.ClConfig;
import com.sportech.cl.global.utils.config.ConfigLoader;
import com.sportech.cl.internal.businesslogic.SoftcardGenerator;
import com.sportech.cl.internal.utils.AuditHelper;
import com.sportech.cl.internal.utils.CardHelper;
import com.sportech.cl.internal.utils.FlatObjectBeanHelper;
import com.sportech.cl.internal.utils.RequiredParam;
import com.sportech.cl.internal.utils.SecurityHelper;
import com.sportech.cl.internal.utils.TrackHelper;
import com.sportech.cl.model.common.ErrorCode;
import com.sportech.cl.model.common.PageParams;
import com.sportech.cl.model.common.PoolMergeType;
import com.sportech.cl.model.database.HardCard;
import com.sportech.cl.model.database.HardRace;
import com.sportech.cl.model.database.Import;
import com.sportech.cl.model.database.ImportOutline;
import com.sportech.cl.model.database.Pattern;
import com.sportech.cl.model.database.SoftCard;
import com.sportech.cl.model.database.Track;
import com.sportech.cl.model.database.utils.DateUtils;
import com.sportech.cl.model.importer.ConverterType;
import com.sportech.cl.model.response.ImportListResponse;
import com.sportech.cl.model.response.ImportResponse;
import com.sportech.cl.model.response.StringResponse;
import com.sportech.cl.model.security.Privilege;
import com.sportech.cl.model.security.SecurityToken;
import com.sportech.cl.utils.importer.CardImporter;
import com.sportech.cl.utils.importer.CardImporterFactory;
import com.sportech.cl.utils.importer.ImportException;
import com.sportech.cl.utils.sis.SisParserHelper;
import com.sportech.common.model.CardType;

/**
 * Session Bean implementation class ImportsBean
 */
@Stateless
@Interceptors({ com.sportech.cl.internal.utils.Rollbacker.class, com.sportech.cl.internal.utils.ParametersChecker.class })
@LocalBean
@Remote(ImportsRemote.class)
public class ImportsBean implements ImportsRemote {

    static private final Logger log = Logger.getLogger(ImportsBean.class);

    @PersistenceContext
    private Session ss;
    
    private boolean createNewTrack = false;

    private TrackHelper trackHelper = null;

    private PoolImporter poolImporter = null;

    private enum ImportStatus {
        SUCCESS, MERGED, DUPLICATE
    }

    @PostConstruct
    protected void init() {
        imp_worker = new FlatObjectBeanHelper<ImportOutline, ImportOutline>(ss, null, Privilege.READ_IMPORTS,
                Privilege.MANAGE_IMPORTS, ImportOutline.class, ImportOutline.class, log);
        imp_worker.hasObjectLevelACL(false);
        ClConfig config = ConfigLoader.getCfg();
        Boolean cflag = config.getCommonConfig().getCreateNewTrack();
        if (cflag != null) {
            createNewTrack = cflag;
        }
        trackHelper = new TrackHelper(createNewTrack);
        poolImporter = new PoolImporter();
    }

    private byte[] processZipFile(Import importData) {
        byte[] theData = importData.getData();
        try {
            String fileName = importData.getFilename();
            String extension = FilenameUtils.getExtension(fileName);
            if (StringUtils.equalsIgnoreCase(extension, "zip")) {
                ByteArrayInputStream fis = new ByteArrayInputStream(importData.getData());
                ZipInputStream zis = new ZipInputStream(fis);
                ZipEntry entry;
                if ((entry = zis.getNextEntry()) != null) {
                    log.info("Extracting: " + entry.getName());
                    theData = IOUtils.toByteArray(zis);
                }
                zis.close();
            }
        } catch (Exception e) {
            log.error("Error extracting file", e);
        }
        return theData;
    }

    public ImportStatus saveCard(Session ss, HardCard eventData, Long importId) throws ImportException {

        ImportStatus status = ImportStatus.SUCCESS;

        try {

            trackHelper.saveTrack(ss, eventData);

            if (checkExistingCards(eventData)) {
                if (importId != null) {
                    Import importingData = (Import) ss.load(Import.class, importId);
                    if (importingData != null) {
                        if (eventData.getId() == null) {
                            log.info("Card has been imported already.");
                            importingData.setStatus("Duplicate Card");
                            importingData.setLog("Card has been imported already.");
                            status = ImportStatus.DUPLICATE;
                        } else {
                            log.info("Merged card with existing card" + eventData.getId());
                            importingData.setStatus("Merged Card");
                            importingData.setLog("Card is merged into an existing card" + eventData.getId());
                            status = ImportStatus.MERGED;
                        }
                        importingData.setTrackId(eventData.getTrack().getId());
                        ss.saveOrUpdate(importingData);
                    }
                    return status;
                }
            }

            updateCardRaces(ss, eventData);

            if (importId != null) {
                Import i = (Import) ss.load(Import.class, importId);
                if (i != null) {
                    i.setStatus("Success");
                    i.setTrackId(eventData.getTrack().getId());
                    ss.saveOrUpdate(i);
                }
            }
            ss.flush();

        } catch (ImportException ie) {
            throw ie;
        } catch (Exception e) {
            log.error("Error generating objects from parsed result", e);
            throw new ImportException("Error saving parsed results.");
        }
        return status;
    }

    private long doImport(Import importData, ConverterType conv, long importId) throws ImportException {
        log.debug("importing from data");
        byte[] data = null;
        
        try {
            data = processZipFile(importData);
        } catch (Exception e) {
            log.error("Error loading or unzipping data", e);
            throw new ImportException("Error reading or unzipping data.");
        }

        List<HardCard> cards = null;
        try {
            List<Track> allTracks = trackHelper.getAllTracks(ss);
            CardImporter importer = CardImporterFactory.create(conv, allTracks);
            cards = importer.parse(data);
            if (cards == null) {
                log.error("Empty data.");
                return importId;
            }
        } catch (ImportException ie) {
            throw ie;
        } catch (Exception e) {
            log.error("Error parsing data", e);
            throw new ImportException("Failed to parse imported data.");
        }

        try {
            
            List<HardCard> clones = new ArrayList<HardCard>();
            boolean hasDuplicate = false;
            ImportStatus status = ImportStatus.SUCCESS;
            for (HardCard hc : cards) {

                hc.setApproved(false);
                hc.setImportId(importId);

                if (StringUtils.equals(hc.getProvider(), "Clone")) {
                    clones.add(hc);
                    continue;
                }

                status = saveCard(ss, hc, importId);
                if (status == ImportStatus.DUPLICATE) {
                    hasDuplicate = true;
                    log.info("Skipped duplicate card " + hc.getDescription());
                }
                else if (status == ImportStatus.SUCCESS) {
                    generateDefaultCard(ss, hc, false);
                }
            }

            for (HardCard clone : clones) {
                for (HardRace race : clone.getRaces()) {
                    String source = race.getCloneSource();
                    Long rn = race.getNumber();
                    if (StringUtils.isNotBlank(source)) {
                        String[] parts = source.split(":");
                        long rnumber = Long.parseLong(parts[1]);
                        boolean found = false;
                        for (HardCard sourceCard : cards) {
                            Track sourceTrack = sourceCard.getTrack();
                            String toteCode = SisParserHelper.getToteCode(sourceTrack.getCountryCode(),
                                    sourceTrack.getEquibaseId(), sourceTrack.getName());
                            if (StringUtils.equalsIgnoreCase(toteCode, parts[0])) {
                                for (HardRace sourceRace : sourceCard.getRaces()) {
                                    if (sourceRace.getNumber() == rnumber) {
                                        race.copy4persistence(sourceRace);
                                        race.setCloneSource(source + "|" + sourceCard.getId() + "|" + sourceRace.getNumber());
                                        race.setNumber(rn);
                                        found = true;
                                        log.info("Found source race " + sourceRace.getId() + " for clone race " + source);
                                        break;
                                    }
                                }
                                if (found) {
                                    break;
                                }
                            }
                        }
                    }
                }
                status = saveCard(ss, clone, importId);
                if (status == ImportStatus.DUPLICATE) {
                    hasDuplicate = true;
                    log.info("Skipped duplicate card " + clone.getDescription());
                } else if (status == ImportStatus.SUCCESS) {
                    log.info("Saved clone card " + clone.getId());
                    generateDefaultCard(ss, clone, true);
                }
            }
            if (hasDuplicate) {
                Import importingData = (Import) ss.load(Import.class, importId);
                if (importingData != null) {
                    importingData.setStatus("Duplicate Card");
                    importingData.setLog("Found duplicate cards.");
                    ss.save(importingData);
                }
                importId = -importId;
            }
        } catch (ImportException ie) {
            throw ie;
        } catch (Exception e) {
            log.error("Error generating objects from parsed result", e);
            throw new ImportException("Error saving parsed results.");
        }
        log.debug("imported data:" + importId);
        return importId;
    }

    @SuppressWarnings("unchecked")
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public long addImportSource(Import importingData, ConverterType convertType) {
        Criteria crit = ss.createCriteria(Import.class);
        crit.add(Restrictions.eq("checksum", importingData.getChecksum()));
        crit.add(Restrictions.eq("status", ErrorCode.SUCCESS.getCode()));
        List<Import> imports = (List<Import>) crit.list();

        if (imports != null && imports.size() > 0) {
            for (Import i : imports) {
                log.debug("Found same checksum file: " + i.getFilename());
                if (StringUtils.equals(i.getFilename(), importingData.getFilename())) {
                    importingData.setStatus("Duplicate File");
                    importingData.setLog("Same file has already been imported with Id:" + i.getId());
                    ss.save(importingData);
                    return -i.getId();
                }
            }
        }

        ss.save(importingData);

        long importId = importingData.getId();

        try {

            importId = doImport(importingData, convertType, importId);

        } catch (ImportException ie) {
            log.error("Error importing file", ie);
            logError(importingData, ie.getMessage());
            return -1;
        } catch (Exception e) {
            log.error("Error importing file", e);
            logError(importingData, null);
            return -1;
        }
        return importId;
    }

    private void logError(Import importingData, String message) {
        importingData.setStatus("Failure");
        String logMessage = "Failed to import file";
        if (!StringUtils.isEmpty(message)) {
            logMessage = message;
        } else {
            logMessage += ".";
        }
        importingData.setLog(logMessage);
    }

    @SuppressWarnings("rawtypes")
    public void saveErrorLog(Import importingData) {
        ss.saveOrUpdate(importingData);
        Criteria critHardCard = ss.createCriteria(HardCard.class)
                .add(Restrictions.eq("importId", importingData.getId()));
        List hardCards = critHardCard.list();
        for (Object o : hardCards) {
            HardCard hardCard = (HardCard) o;
            if (hardCard != null) {
                ss.delete(hardCard);
            }
        }
        ss.flush();
    }

    @Override
    public StringResponse getImportLog(SecurityToken token, @RequiredParam(name = "id") Long id) {
        try {
            if (!SecurityHelper.checkAccess(token, Privilege.READ_IMPORTS))
                return new StringResponse(ErrorCode.E_ACCESS_DENIED);

            Criteria crit = ss.createCriteria(Import.class).add(Restrictions.idEq(id))
                    .setProjection(Projections.property("log"));

            String status = (String) crit.uniqueResult();

            if (status != null)
                return new StringResponse(status);
            else
                return new StringResponse(ErrorCode.E_NOT_FOUND);
        } catch (Exception e) {
            log.error("Exception caught: ", e);

            return new StringResponse(ErrorCode.FAIL);
        }
    }

    public ImportResponse get(SecurityToken token, @RequiredParam(name = "id") Long id) {
        return (ImportResponse) imp_worker.get(token, id, new ImportResponse());
    }

    public ImportListResponse find(SecurityToken token, ImportOutline[] query, Date startDate, Date endDate, Boolean useLike,
            PageParams pages) {
        return (ImportListResponse) imp_worker.findByDate(token, query, startDate, endDate, useLike, pages,
                ImportOutline.DATE_PROP_NAME, new ImportListResponse(), null);
    }

    protected FlatObjectBeanHelper<ImportOutline, ImportOutline> imp_worker;

    private boolean checkExistingCards(HardCard data) {

        log.debug("Check duplicat card:" + data.getCardDate().getTime() + "TrackID" + data.getTrack().getId());
        List<HardCard> cards = CardHelper.getCardsForDay(ss, data.getCardDate(), data.getTrack().getId());
        Date firstRacePostTime = DateUtils.addDate(new Date(), 1000);
        long firstRaceNumber = 1000L;
        Set<HardRace> races = data.getRaces();
        Set<Long> raceNumberSet = new HashSet<Long>();
        Set<String> postTimeSet = new HashSet<String>();

        for (HardRace r : races) {
            raceNumberSet.add(r.getNumber());
            if (r.getPostTime() != null) {
                postTimeSet.add(DateUtils.toPostTimeStr(r.getPostTime()));
                if (r.getNumber() != null && r.getNumber() < firstRaceNumber) {
                    firstRaceNumber = r.getNumber();
                    if (r.getPostTime() != null) {
                        firstRacePostTime = r.getPostTime();
                    }
                }
            }
        }
        if (data.getFirstRaceTime() != null) {
            firstRacePostTime = data.getFirstRaceTime();
        } else {
            data.setFirstRaceTime(firstRacePostTime);
        }
        String firstRaceTime = DateUtils.toPostTimeStr(firstRacePostTime);
        log.info("Found card first post time: " + firstRaceTime);
        for (HardCard hc : cards) {
            log.info("Found existing card with same card date:" + data.getCardDate() + "TrackID" + data.getTrack().getId());
            // Check race post time and race numbers
            boolean overlappingNumber = false;
            boolean overlappingPostTime = false;
            for (HardRace r : hc.getRaces()) {
                if (raceNumberSet.contains(r.getNumber())) {
                    overlappingNumber = true;
                }
                if (r.getPostTime() != null) {
                    String ptime = DateUtils.toPostTimeStr(r.getPostTime());
                    if (StringUtils.equals(firstRaceTime, ptime)) {
                        log.info("Found duplicated card with same first race post time: " + firstRaceTime);
                        return true;
                    }
                    if (postTimeSet.contains(ptime)) {
                        overlappingPostTime = true;
                    }
                }
            }
            // No overlapping, merge the two cards together.
            // Note: If there are two performances within the same day,
            // usually the race numbers will be the same (overlapping).
            // Therefore they won't be merged here.
            if (!overlappingNumber && !overlappingPostTime) {
                HardCard resultCard = (HardCard) ss.get(HardCard.class, hc.getId());
                for (HardRace hr : data.getRaces()) {
                    hr.setParent(resultCard);
                    resultCard.getRaces().add(hr);
                }
                ss.saveOrUpdate(resultCard);
                ss.flush();
                data.setRaces(new HashSet<HardRace>());
                data.setId(-resultCard.getId());
                updateCardRaces(ss, resultCard);
                log.info("Merging into existing card" + hc.getId());
                return true;
            }
        }
        return false;
    }

    public void appendRaces(Session ss, HardCard data) {
        List<HardCard> cards = CardHelper.getCardsForDay(ss, data.getCardDate(), data.getTrack().getId());
        Date firstRacePostTime = DateUtils.addDate(new Date(), 1000);
        long firstRaceNumber = 1000L;
        Set<HardRace> races = data.getRaces();
        Set<Long> raceNumberSet = new HashSet<Long>();
        Set<String> postTimeSet = new HashSet<String>();

        for (HardRace r : races) {
            raceNumberSet.add(r.getNumber());
            if (r.getPostTime() != null) {
                postTimeSet.add(DateUtils.toPostTimeStr(r.getPostTime()));
                if (r.getNumber() != null && r.getNumber() < firstRaceNumber) {
                    firstRaceNumber = r.getNumber();
                    if (r.getPostTime() != null) {
                        firstRacePostTime = r.getPostTime();
                    }
                }
            }
        }
        if (data.getFirstRaceTime() != null) {
            firstRacePostTime = data.getFirstRaceTime();
        } else {
            data.setFirstRaceTime(firstRacePostTime);
        }
        String firstRaceTime = DateUtils.toPostTimeStr(firstRacePostTime);
        log.info("Found card first post time: " + firstRaceTime);
        for (HardCard card : cards) {
            log.info("Found existing card with same card date:" + data.getCardDate() + "TrackID" + data.getTrack().getId());
            poolImporter.savePools(ss, card);
        }
    }

    public void updateCardRaces(Session ss, HardCard eventData) {
        eventData.setDescription(DateUtils.buildName(eventData.getTrack().getName(), eventData.getFirstRaceTime()));
        if (eventData.getCardType() != CardType.GreyHound) {
            if (eventData.getTrack().getPerfType() != null) {
                eventData.setPerfType(eventData.getTrack().getPerfType());
                log.info("Set card '" + eventData.getDescription() + "' perf type to: " + eventData.getTrack().getPerfType());
            }
        }

        String meetingCode = eventData.getTrack().getTrackCode();
        if (StringUtils.isEmpty(meetingCode)) {
            meetingCode = eventData.getTrack().getEquibaseId();
        }
        eventData.setMeetingCode(meetingCode);
        ss.save(eventData);
        ss.flush();

        poolImporter.savePools(ss, eventData);

        PoolImporter.copyCardConfig(ss, eventData);

    }

    private void generateDefaultCard(Session ss, HardCard hc, boolean cloned) {
        Track track = hc.getTrack();
        if (track != null && track.getAutoGenerate() != null && track.getAutoGenerate()) {
            Pattern pattern = track.getDefaultPattern();
            if (pattern != null) {
                hc.setApproved(true);
                Integer mergeTypeId = pattern.getMergeType();
                SoftCard sc = null;
                if (!cloned) {
                    sc = SoftcardGenerator.generateSoftCard(hc, pattern, PoolMergeType.getById(mergeTypeId), true);
                } else {
                    sc = SoftcardGenerator.generateMirrorCard(ss, hc, pattern);
                    if (sc == null) {
                        AuditHelper.writeAuditLog(ss, null, hc, "Failed to generate mirror card from ");
                        log.error("Error generating mirror card from card " + hc.getDescription());
                    }
                }
                if (sc != null) {
                    sc.setApproved(false);
                    ss.save(sc);
                    AuditHelper.writeAuditLog(ss, null, sc, "Generated automatically from pattern " + pattern.getLoadCode());
                    log.info("Automatically generated load card " + sc.getLoadCode() + " for track " + track.getName());
                }
            }
        }
    }

}
