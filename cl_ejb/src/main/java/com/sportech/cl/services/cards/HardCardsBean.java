package com.sportech.cl.services.cards;

import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.interceptor.Interceptors;
import javax.persistence.PersistenceContext;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import com.sportech.cl.global.utils.config.ConfigLoader;
import com.sportech.cl.internal.businesslogic.SoftcardGenerator;
import com.sportech.cl.internal.utils.AuditHelper;
import com.sportech.cl.internal.utils.BeanHelper;
import com.sportech.cl.internal.utils.CardHelper;
import com.sportech.cl.internal.utils.RequiredParam;
import com.sportech.cl.internal.utils.SecurityHelper;
import com.sportech.cl.internal.validators.HardCardValidator;
import com.sportech.cl.model.common.ErrorCode;
import com.sportech.cl.model.common.PageParams;
import com.sportech.cl.model.common.PoolMergeType;
import com.sportech.cl.model.database.HardCard;
import com.sportech.cl.model.database.HardCardOutline;
import com.sportech.cl.model.database.HardRace;
import com.sportech.cl.model.database.Pattern;
import com.sportech.cl.model.database.SoftCard;
import com.sportech.cl.model.database.Track;
import com.sportech.cl.model.database.utils.SortOrder;
import com.sportech.cl.model.response.AddEntityResponse;
import com.sportech.cl.model.response.GenericResponse;
import com.sportech.cl.model.response.HardCardListResponse;
import com.sportech.cl.model.response.HardCardResponse;
import com.sportech.cl.model.security.ObjectType;
import com.sportech.cl.model.security.Permission;
import com.sportech.cl.model.security.Privilege;
import com.sportech.cl.model.security.SecurityToken;
import com.sportech.cl.utils.exporter.RifExporter;

/**
 * Session Bean implementation class HardCardsBean
 */
@Stateless
@Interceptors({ com.sportech.cl.internal.utils.Rollbacker.class, com.sportech.cl.internal.utils.ParametersChecker.class })
@LocalBean
@Remote(HardCardsRemote.class)
public class HardCardsBean implements HardCardsRemote {

    @EJB
    SoftCardsBean softCardBean;

    @PersistenceContext
    private Session ss;
    
    public HardCardsBean() {
    }

    @PostConstruct
    protected void init() {
        helper = new BeanHelper<HardCard, HardCardOutline>(ss, ObjectType.HARD_CARD, Privilege.READ_CARDS,
                Privilege.MANAGE_CARDS, HardCard.class, HardCardOutline.class, log);
        helper.setValidator(new HardCardValidator());
    }

    @Override
    public String generateRif(Date date) {
        String result = null;
        List<HardCard> cards = CardHelper.getCardsForDay(ss, date, null);
        result = new RifExporter().build(date, cards);
        return result;
    }

    @Override
    public HardCardResponse get(SecurityToken token, @RequiredParam(name = "id") Long id) {
        HardCardResponse r = (HardCardResponse) helper.get(token, id, new HardCardResponse());
        if (r.isOK()) {
            HardCard c = r.getCard();
            TimeZone trackTimeZone = TimeZone.getDefault();
            TimeZone timeZone = token.getTimeZone();
            Track t = c.getTrack();
            if (t != null && t.getTimeZone() != null) {
                trackTimeZone = t.getTimeZone();
            }
            for (HardRace hr : c.getRaces()) {
                hr.setTimeZone(timeZone);
                hr.setTrackTimeZone(trackTimeZone);
            }
        }
        return r;
    }

    @Override
    public HardCardListResponse find(SecurityToken token, HardCardOutline[] query, Date startDate, Date endDate, Boolean useLike,
            PageParams pages) {
        return (HardCardListResponse) helper.findByDate(token, query, startDate, endDate, useLike, pages,
                HardCardOutline.DATE_PROP_NAME, new HardCardListResponse(), new SortOrder[] { new SortOrder("description") });
    }

    @Override
    public AddEntityResponse add(SecurityToken token, @RequiredParam(name = "card") HardCard card) {
        return helper.add(token, card);
    }

    @Override
    public GenericResponse update(SecurityToken token, @RequiredParam(name = "id") Long id,
            @RequiredParam(name = "new_card") HardCard new_card) {
        GenericResponse response = new GenericResponse();
        HardCardValidator validator = new HardCardValidator();

        try {
            if (validator.Validate(new_card, false, response)) {
                ss.saveOrUpdate(new_card);
            }
        } catch (Exception e) {
            log.error("Error updating host card.", e);
            response.setResult(ErrorCode.FAIL, "Error updating host card");
        }
        return response;
    }

    @Override
    public GenericResponse delete(SecurityToken token, @RequiredParam(name = "id") Long id) {
        try {
            CardHelper.deleteCard(ss, id);
            return new GenericResponse();
        } catch (Exception e) {
            log.error("Exception caught: ", e);
            return new GenericResponse(ErrorCode.FAIL);
        }
    }

    @Override
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public GenericResponse approve(SecurityToken token, @RequiredParam(name = "id") Long id,
            @RequiredParam(name = "approved") Boolean approved) {
        GenericResponse rsp = new GenericResponse();

        try {
            if (!SecurityHelper.checkAccess(token, Privilege.MANAGE_CARDS, id, ObjectType.SOFT_CARD, Permission.WRITE_PERM)) {
                rsp.setResult(ErrorCode.E_ACCESS_DENIED);
                return rsp;
            }

            HardCard card = (HardCard) ss.get(HardCard.class, id);

            if (card != null) {
                if (approved != null && approved) {
                    if (card.getConfig() == null) {
                        rsp.setResult(ErrorCode.FAIL, "Missing configurations.");
                        return rsp;
                    }
                }
                card.setApproved(approved);
                ss.flush();
                AuditHelper.writeAuditLog(ss, token, card, "Toggle approval to:" + approved);
            } else {
                rsp.setResult(ErrorCode.E_NOT_FOUND);
            }

            return rsp;
        } catch (Exception e) {
            log.error("Exception caught: ", e);
            rsp.setResult(ErrorCode.FAIL);
            return rsp;
        }
    }

    @Override
    @SuppressWarnings("rawtypes")
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public int generateSoftCards(Long id) {

        int count = 0;

        HardCard hardCard = (HardCard) ss.get(HardCard.class, id);

        Criteria crit = ss.createCriteria(Pattern.class).add(Restrictions.eq("track.id", hardCard.getTrack().getId()));
        List patterns = crit.list();
        if (patterns != null) {
            for (Object o : patterns) {
                HardCard card = (HardCard) ss.get(HardCard.class, id);
                ss.evict(card);
                Pattern p = (Pattern) o;
                Integer mergeTypeId = p.getMergeType();
                PoolMergeType mergeType = ConfigLoader.getDefaultMergeType(mergeTypeId);
                ss.evict(p);
                SoftCard sc = SoftcardGenerator.generateSoftCard(card, p, mergeType, true);
                sc.setApproved(false);
                ss.save(sc);
                AuditHelper.writeAuditLog(ss, null, sc, "Generated");
                count++;
            }
            ss.flush();
        }
        return count;
    }

    protected BeanHelper<HardCard, HardCardOutline> helper;

    static private final Logger log = Logger.getLogger(HardCardsBean.class);
}
