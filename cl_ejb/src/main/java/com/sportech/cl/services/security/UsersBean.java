package com.sportech.cl.services.security;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.LocalBean;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.interceptor.Interceptors;
import javax.persistence.PersistenceContext;

import org.apache.log4j.Logger;
import org.hibernate.Session;

import com.sportech.cl.internal.utils.FlatObjectBeanHelper;
import com.sportech.cl.internal.utils.ReadOnlyBeanHelper;
import com.sportech.cl.internal.utils.RequiredParam;
import com.sportech.cl.internal.utils.SecurityHelper;
import com.sportech.cl.model.common.ErrorCode;
import com.sportech.cl.model.common.PageParams;
import com.sportech.cl.model.database.Group;
import com.sportech.cl.model.database.Role;
import com.sportech.cl.model.database.Tote;
import com.sportech.cl.model.database.User;
import com.sportech.cl.model.response.AddEntityResponse;
import com.sportech.cl.model.response.GenericResponse;
import com.sportech.cl.model.response.ObjectListResponse;
import com.sportech.cl.model.response.ObjectResponse;
import com.sportech.cl.model.security.ActorType;
import com.sportech.cl.model.security.Privilege;
import com.sportech.cl.model.security.SecurityToken;

@Stateless
@Interceptors(com.sportech.cl.internal.utils.Rollbacker.class)
@LocalBean
@Remote(UsersRemote.class)
public class UsersBean implements UsersRemote {

	static private final Logger log = Logger.getLogger(UsersBean.class);

	@PersistenceContext
	private Session ss;

	protected FlatObjectBeanHelper<User, User> helper;

	protected ReadOnlyBeanHelper<Group, Group> groupHelper;

	protected ReadOnlyBeanHelper<Role, Role> roleHelper;

	public UsersBean() {
	}

	@PostConstruct
	protected void init() {
		helper = new FlatObjectBeanHelper<User, User>(ss, null, Privilege.MANAGE_OPERATORS, Privilege.MANAGE_OPERATORS, User.class, User.class, log);
		helper.hasObjectLevelACL(false);
		groupHelper = new ReadOnlyBeanHelper<Group, Group>(ss, null, Privilege.MANAGE_OPERATORS, User.class, Group.class, log);
		groupHelper.hasObjectLevelACL(false);
		roleHelper = new ReadOnlyBeanHelper<Role, Role>(ss, null, Privilege.MANAGE_OPERATORS, Role.class, Role.class, log);
		roleHelper.hasObjectLevelACL(false);
	}

	@Interceptors(com.sportech.cl.internal.utils.ParametersChecker.class)
	@Override
	public GenericResponse setUserPassword(SecurityToken token, @RequiredParam(name = "username") String username, @RequiredParam(name = "password") String password) {
		try {
	
			User usr = (User) ss.createQuery("from User as usr where usr.identifier = :name").setString("name", username.toLowerCase()).uniqueResult();

			if (usr == null) {
				return new GenericResponse(ErrorCode.E_NOT_FOUND);
			}

			ActorType actorType = ActorType.getById(usr.getActorTypeId());
			Privilege priv = null;

			if (actorType.equals(ActorType.TOTE))
				priv = Privilege.MANAGE_TOTES;
			else if (actorType.equals(ActorType.OPERATOR))
				priv = Privilege.MANAGE_OPERATORS;
			else if (actorType.equals(ActorType.CUSTOMER))
				priv = Privilege.MANAGE_CUSTOMERS;

			if (priv == null) {
				log.error("Can't determine required privilige for actor type id = " + actorType.getId());
				return new GenericResponse(ErrorCode.FAIL);
			}

			if (!SecurityHelper.checkAccess(token, priv)) {
				return new GenericResponse(ErrorCode.E_ACCESS_DENIED);
			}

			String hashedPwd = SecurityHelper.getHashedPassword(password);

			usr.setPasswd(hashedPwd);

			ss.flush();

			return new GenericResponse();
		} catch (Exception e) {
			log.error("Exception caught: ", e);

			return new GenericResponse(ErrorCode.FAIL);
		}
	}

	public GenericResponse delUsersForActor(SecurityToken token, ActorType actorType, Long actorId) {
		Privilege priv = null;

		if (actorType.equals(ActorType.TOTE))
			priv = Privilege.MANAGE_TOTES;
		else if (actorType.equals(ActorType.OPERATOR))
			priv = Privilege.MANAGE_OPERATORS;
		else if (actorType.equals(ActorType.CUSTOMER))
			priv = Privilege.MANAGE_CUSTOMERS;

		if (priv == null) {
			log.error("Can't determine required privilige for actor type id = " + actorType.getId());
			return new GenericResponse(ErrorCode.FAIL);
		}

		if (!SecurityHelper.checkAccess(token, priv)) {
			return new GenericResponse(ErrorCode.E_ACCESS_DENIED);
		}

		@SuppressWarnings("unchecked")
		List<User> users = (List<User>) ss.createQuery("from User as usr where usr.actorId = :aid and usr.actorTypeId=:atid").setLong("aid", actorId).setInteger("atid", actorType.getId()).list();

		for (User user : users) {
			ss.delete(user);
		}

		ss.flush();

		return new GenericResponse();
	}

	public GenericResponse enableUsersForActor(SecurityToken token, ActorType actorType, Long actorId, Boolean enabled) {
		Privilege priv = null;

		if (actorType.equals(ActorType.TOTE))
			priv = Privilege.MANAGE_TOTES;
		else if (actorType.equals(ActorType.OPERATOR))
			priv = Privilege.MANAGE_OPERATORS;
		else if (actorType.equals(ActorType.CUSTOMER))
			priv = Privilege.MANAGE_CUSTOMERS;

		if (priv == null) {
			log.error("Can't determine required privilige for actor type id = " + actorType.getId());
			return new GenericResponse(ErrorCode.FAIL);
		}

		if (!SecurityHelper.checkAccess(token, priv)) {
			return new GenericResponse(ErrorCode.E_ACCESS_DENIED);
		}

		@SuppressWarnings("unchecked")
		List<User> users = (List<User>) ss.createQuery("from User as usr where usr.actorId = ? and usr.actorTypeId=?").setLong(0, actorId).setInteger(1, actorType.getId()).list();

		for (User user : users) {
			user.setActive(enabled);
		}

		ss.flush();

		return new GenericResponse();
	}

	public AddEntityResponse addToteUser(SecurityToken token, Tote tote, String username, String password) {
		return addUser(token, Privilege.MANAGE_TOTES, tote.getId().intValue(), ActorType.TOTE, username, password, 4);
	}

	protected AddEntityResponse addUser(SecurityToken token, Privilege priv, Integer actorId, ActorType actorType, String username, String password, Integer role) {
		AddEntityResponse rsp = new AddEntityResponse();

		try {
			if (!SecurityHelper.checkAccess(token, priv)) {
				rsp.setResult(ErrorCode.E_ACCESS_DENIED);
				return rsp;
			}

			if (actorId == null)
				rsp.setResult(ErrorCode.E_VAL_FAILED, "Actor ID is null ");
			if (username == null || username.isEmpty())
				rsp.setResult(ErrorCode.E_VAL_FAILED, "Username is empty ");
			else
				username = username.trim();
			if (password == null || password.isEmpty())
				rsp.setResult(ErrorCode.E_VAL_FAILED, "Password is empty ");

			if (rsp.isOK()) {
				User usr = new User();

				usr.setIdentifier(username);
				usr.setPasswd(SecurityHelper.getHashedPassword(password));
				usr.setSalt("For " + actorType.getDescription() + " with ID " + actorId);
				usr.setActorTypeId(actorType.getId());
				usr.setActorId(actorId.intValue());
				usr.setRoleId(role);
				usr.setActive(true);

				ss.save(usr);

				ss.flush();

				rsp.setId(usr.getUserId());
			}
			return rsp;
		} catch (Exception e) {
			log.error("Exception caught: ", e);

			rsp.setResult(ErrorCode.FAIL);
			return rsp;
		}
	}

	@Override
	public List<User> find(SecurityToken token, User[] query, Boolean useLike, PageParams pages) {
		ObjectListResponse<User> rsp = new ObjectListResponse<User>();
		ObjectListResponse<User> response = helper.find(token, query, useLike, pages, rsp, null);
		return response.getObjects();
	}

	@Override
	public ObjectResponse<User> get(SecurityToken token, @RequiredParam(name = "id") Long id) {
		ObjectResponse<User> user = helper.get(token, id, new ObjectResponse<User>());
		return user;
	}

	@Override
	public User getUser(Long id) {
		User g = (User) ss.get(User.class, id);
		if (g != null) {
			int s = g.getGroups().size();
			if (s > 0) {
				s = 0;
			}
		}
		return g;
	}

	@Override
	public GenericResponse update(SecurityToken token, @RequiredParam(name = "id") Long id, @RequiredParam(name = "new_op") User new_op) {
		return helper.update(token, id, new_op);
	}

	@Override
	public AddEntityResponse add(SecurityToken token, User user) {
		try {
			user.setPasswd("EMPTY");
			AddEntityResponse userRsp = helper.add(token, user);
			return userRsp;
		} catch (Exception e) {
			log.error("Exception caught: ", e);
			return new AddEntityResponse(ErrorCode.FAIL);
		}

	}

	@Override
	public GenericResponse saveGroup(Group group) {
		GenericResponse response = new GenericResponse();
		try {
			ss.saveOrUpdate(group);
		} catch (Exception e) {
			log.error("Exception caught: ", e);
			response.setStatus(ErrorCode.FAIL.getId());
		}
		return response;
	}

	@Override
	public GenericResponse deleteGroup(Long id) {
		GenericResponse response = new GenericResponse();
		try {
			Group g = (Group) ss.get(Group.class, id);
			ss.delete(g);
		} catch (Exception e) {
			log.error("Exception caught: ", e);
			response.setStatus(ErrorCode.FAIL.getId());
		}
		return response;
	}

	@Override
	public GenericResponse delete(SecurityToken token, @RequiredParam(name = "id") Long id) {
		try {
			return helper.delete(token, id);
		} catch (Exception e) {
			log.error("Exception caught: ", e);
			return new GenericResponse(ErrorCode.FAIL);
		}
	}

	@Override
	public List<Group> find(SecurityToken token, Group[] query, Boolean useLike, PageParams pages) {
		ObjectListResponse<Group> rsp = new ObjectListResponse<Group>();
		ObjectListResponse<Group> response = groupHelper.find(token, query, useLike, pages, rsp, null);
		return response.getObjects();
	}

	@Override
	public List<Role> find(SecurityToken token, Role[] query, Boolean useLike, PageParams pages) {
		ObjectListResponse<Role> rsp = new ObjectListResponse<Role>();
		ObjectListResponse<Role> response = roleHelper.find(token, query, useLike, pages, rsp, null);
		return response.getObjects();
	}

	@Override
	public Group getGroup(Long id) {
		Group g = (Group) ss.get(Group.class, id);
		if (g != null) {
			int s = g.getRoles().size();
			s = g.getTracks().size();
			s = g.getUsers().size();
			s = g.getTotes().size();
			if (s > 0) {
				s = 0;
			}
		}
		return g;
	}

	@Override
	public List<Tote> getAllTotes() {

		@SuppressWarnings("unchecked")
		List<Tote> totes = (List<Tote>) ss.createQuery("from Tote").list();

		return totes;
	}

}
