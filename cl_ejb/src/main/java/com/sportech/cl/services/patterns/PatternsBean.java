package com.sportech.cl.services.patterns;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.annotation.PostConstruct;
import javax.ejb.LocalBean;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.interceptor.Interceptors;
import javax.persistence.PersistenceContext;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.hibernate.type.LongType;

import com.sportech.cl.internal.utils.AuditHelper;
import com.sportech.cl.internal.utils.BeanHelper;
import com.sportech.cl.internal.utils.RequiredParam;
import com.sportech.cl.internal.utils.SecurityHelper;
import com.sportech.cl.internal.validators.PatternValidator;
import com.sportech.cl.model.common.ErrorCode;
import com.sportech.cl.model.common.PageParams;
import com.sportech.cl.model.database.Pattern;
import com.sportech.cl.model.database.PatternOutline;
import com.sportech.cl.model.database.PatternTote;
import com.sportech.cl.model.database.PatternUsed;
import com.sportech.cl.model.database.Tote;
import com.sportech.cl.model.database.utils.SortOrder;
import com.sportech.cl.model.response.AddEntityResponse;
import com.sportech.cl.model.response.GenericResponse;
import com.sportech.cl.model.response.PatternListResponse;
import com.sportech.cl.model.response.PatternResponse;
import com.sportech.cl.model.response.PatternUsedListResponse;
import com.sportech.cl.model.security.ObjectType;
import com.sportech.cl.model.security.Privilege;
import com.sportech.cl.model.security.SecurityToken;

/**
 * Session Bean implementation class PatternsBean
 */
@Stateless
@Interceptors({ com.sportech.cl.internal.utils.Rollbacker.class,
		com.sportech.cl.internal.utils.ParametersChecker.class })
@LocalBean
@Remote(PatternsRemote.class)
public class PatternsBean implements PatternsRemote {

	@PersistenceContext
    private Session ss;
	
	public PatternsBean() {
	}

	@PostConstruct
	protected void init() {
		helper = new BeanHelper<Pattern, PatternOutline>(ss, ObjectType.PATTERN, Privilege.READ_PATTERNS,
				Privilege.MANAGE_PATTERNS, Pattern.class, PatternOutline.class,
				log);
		helper.setValidator(new PatternValidator());
	}

	@Override
	public PatternResponse getPattern(SecurityToken token,
			@RequiredParam(name = "id") Long id) {
		return (PatternResponse) helper.get(token, id, new PatternResponse());
	}

	public PatternListResponse find(SecurityToken token,
			PatternOutline[] criteria, Boolean useLike, PageParams pages) {
		return (PatternListResponse) helper.find(token, criteria, useLike,
				pages, new PatternListResponse(),
				new SortOrder[] { new SortOrder("trackEquibaseId"), new SortOrder("description")});
	}

	public AddEntityResponse addPattern(SecurityToken token,
			@RequiredParam(name = "pattern") Pattern pattern) {
		pattern.setLoadCode(StringUtils.upperCase(pattern.getLoadCode()));
		if (checkUnique(null, pattern)) {
			AddEntityResponse res = helper.add(token, pattern);
			if (res.getStatus() == 0) {
				populateTotes(pattern);				
			}
			AuditHelper.writeAuditLog(ss, token, pattern, "Add Pattern");
			return res;		
		}
		else {
			AddEntityResponse response = new AddEntityResponse();
			response.setResult(ErrorCode.ERROR, "Pattern with the same description or load code exists.");
			return response;
		}
	}

	public GenericResponse updatePattern(SecurityToken token,
			@RequiredParam(name = "id") Long id,
			@RequiredParam(name = "pattern") Pattern pattern) {
		pattern.setLoadCode(StringUtils.upperCase(pattern.getLoadCode()));
		if (checkUnique(id, pattern)) {
			GenericResponse res = helper.update(token, id, pattern);
			if (res.getStatus() == 0) {
				populateTotes(pattern);				
			}
			AuditHelper.writeAuditLog(ss, token, pattern, "Change Pattern");
			return res;
		}
		else {
			GenericResponse response = new GenericResponse();
			response.setResult(ErrorCode.ERROR, "Pattern with the same description or load code exists.");
			return response;
		}

	}

	public GenericResponse deletePattern(SecurityToken token,
			@RequiredParam(name = "id") Long id) {

		@SuppressWarnings("unchecked")
		List<Long> vlist = (List<Long>) ss
				.createSQLQuery(
						"select pattern_fk from soft_card where pattern_fk = ?")
				.addScalar("pattern_fk", LongType.INSTANCE)
				.setLong(0, id).list();

		if (vlist != null && vlist.size() > 0) {
			return new PatternUsedListResponse(ErrorCode.E_OBJECT_IN_USE,
					"Can not delete this pattern: it has already been used.");
		}
		GenericResponse resp = helper.delete(token, id);
		if (resp.getStatus() == 0) {
		    SQLQuery queryPattern = ss.createSQLQuery("update dict_track set pattern_id = null, auto_generate = 0 where pattern_id = :cid");
		    queryPattern.setLong("cid", id);
		    queryPattern.executeUpdate();
		}
		return resp;
	}

	public PatternUsedListResponse find4HardCard(SecurityToken token,
			@RequiredParam(name = "hardCardId") Long hardCardId) {

		try {

			if (!SecurityHelper.checkAccess(token, Privilege.READ_PATTERNS)) {
				return new PatternUsedListResponse(ErrorCode.E_ACCESS_DENIED);
			}

			@SuppressWarnings("unchecked")
			List<Long> vlist = (List<Long>) ss
					.createSQLQuery(
							"select dict_track_fk from hard_card where hard_card_pk = ?")
					.addScalar("dict_track_fk", LongType.INSTANCE)
					.setLong(0, hardCardId).list();

			if (vlist == null || vlist.size() == 0)
				return new PatternUsedListResponse(ErrorCode.E_NOT_FOUND,
						"Hard Card not found");

			Long trackId = vlist.get(0);

			if (trackId == null)
				return new PatternUsedListResponse(ErrorCode.E_NOT_FOUND,
						"Hard Card has no data to match with Pattern");

			@SuppressWarnings("unchecked")
			List<PatternUsed> patterns = (List<PatternUsed>) ss
					.createSQLQuery("call p_get_appropriate_patterns( ?,? )")
					.addEntity(PatternUsed.class).setLong(0, hardCardId)
					.setLong(1, trackId).list();

			PatternUsedListResponse rsp = new PatternUsedListResponse();

			if ((patterns != null) && !patterns.isEmpty()) {
				rsp.setResult(ErrorCode.SUCCESS);
				rsp.setObjects(patterns);
			} else
				rsp.setResult(ErrorCode.E_NOT_FOUND);

			return rsp;
		} catch (Exception e) {
			log.error("Exception caught: ", e);

			return new PatternUsedListResponse(ErrorCode.FAIL);
		}
	}
	
	private boolean checkUnique(Long id, Pattern p) {
		if (checkUniqueDescription(id, p) && checkUniqueLoadCode(id, p)) {
			return true;
		}
		return false;
	}
	
	@SuppressWarnings("rawtypes")
	private boolean checkUniqueDescription(Long id, Pattern p) {

		Criteria crit = ss.createCriteria(Pattern.class)
			    .add(Restrictions.and(
			    		Restrictions.eq("trackId", p.getTrackId()),
						Restrictions.eq("description", p.getDescription())));
		if (id != null) {
			crit.add(Restrictions.ne("id", id));
		}
		List result = crit.list();
		if (result != null && result.size() > 0) {
			return false;
		}
		return true;
	}

	@SuppressWarnings("rawtypes")
	private boolean checkUniqueLoadCode(Long id, Pattern p) {

		Criteria crit = ss.createCriteria(Pattern.class)
			    .add(Restrictions.and(
			    		Restrictions.eq("trackId", p.getTrackId()),
						Restrictions.eq("loadCode", p.getLoadCode())));
		if (id != null) {
			crit.add(Restrictions.ne("id", id));
		}
		List result = crit.list();
		if (result != null && result.size() > 0) {
			return false;
		}
		return true;
	}

	private void populateTotes(Pattern p) {
		Pattern sc = (Pattern) ss.get(Pattern.class,  p.getId());
		Set<String> toteCodes = new HashSet<String>();
		for (PatternTote st : sc.getTotes()) {
			Integer toteId = st.getToteId();			
			Tote tote = (Tote) ss.get(Tote.class, toteId.longValue());
			if (tote != null) {
				String name = tote.getToteCode();
				if (!StringUtils.isEmpty(name)) {					
					toteCodes.add(name);
				}
			}
		}
		sc.setToteList(StringUtils.join(toteCodes, ","));
		ss.update(sc);
		ss.flush();
	}

	protected BeanHelper<Pattern, PatternOutline> helper;

	static private final Logger log = Logger.getLogger(PatternsBean.class);
	
}
