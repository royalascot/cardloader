package com.sportech.cl.sis.gateway;

import java.util.Date;
import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.apache.log4j.Logger;
import org.hibernate.Session;

import com.sportech.cl.internal.utils.TrackHelper;
import com.sportech.cl.model.database.HardCard;
import com.sportech.cl.services.imports.PoolImporter;
import com.sportech.common.model.entity.EventCard;
import com.sportech.common.util.DateHelper;

@Stateless
@LocalBean
public class SisCardHandlerBean {

	static private final Logger log = Logger.getLogger(SisCardHandlerBean.class);

	private SisCardImporter importer = new SisCardImporter();

	@PersistenceContext
	EntityManager em;

	@PersistenceContext
	Session ss;

	public void processCard(EventCard card) {
		Date d = DateHelper.stringToDate(card.getCardDate());
		String jql = "FROM HardCard WHERE cardDate = :date AND sourceId = :sisId";
		TypedQuery<HardCard> query = em.createQuery(jql, HardCard.class);
		query.setParameter("date", d);
		query.setParameter("sisId", card.getSourceId());
		List<HardCard> cards = query.getResultList();
		try {
			if (cards != null && cards.size() > 0) {
				HardCard oldCard = cards.get(0);
				boolean changed = importer.update(oldCard, card);
				if (changed) {
					new PoolImporter().savePools(ss, oldCard);
				}
				em.persist(oldCard);
				log.info("updated existing SIS card: " + oldCard.getDescription());
			} else {
				HardCard c = importer.create(card);
				c.setApproved(false);
				c.setSourceId(card.getSourceId());
				TrackHelper trackHelper = new TrackHelper(true);
				trackHelper.saveTrack(ss, c);
				ss.saveOrUpdate(c);
				new PoolImporter().savePools(ss, c);
				PoolImporter.copyCardConfig(ss, c);
				log.info("created new SIS card: " + c.getDescription());
			}
		} catch (Exception e) {
			log.error("Error processing card", e);
		}
	}

}
