package com.sportech.cl.sis.gateway;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import com.sportech.cl.internal.utils.CardHelper;
import com.sportech.cl.model.database.HardCard;
import com.sportech.cl.model.database.HardPool;
import com.sportech.cl.model.database.HardRace;
import com.sportech.cl.model.database.PassThroughBag;
import com.sportech.cl.model.database.Runner;
import com.sportech.cl.model.database.Track;
import com.sportech.cl.model.database.utils.DateUtils;
import com.sportech.cl.model.database.utils.JSonHelper;
import com.sportech.cl.model.database.utils.PoolHelper;
import com.sportech.cl.utils.importer.ImportException;
import com.sportech.cl.utils.sis.SisParserHelper;
import com.sportech.common.model.PerformanceType;
import com.sportech.common.model.PoolType;
import com.sportech.common.model.entity.BetInterest;
import com.sportech.common.model.entity.EventCard;
import com.sportech.common.model.entity.EventPool;
import com.sportech.common.model.entity.EventRace;
import com.sportech.common.util.DateHelper;

public class SisCardImporter {

	static private final Logger log = Logger.getLogger(SisCardImporter.class);

	private static final String WEIGHT_UNIT = "lb";

	public boolean update(HardCard hc, EventCard card) {
		boolean changed = false;
		try {
			if (!hc.getApproved()) {
				importCard(hc, card);
				changed = true;
			} else {
				log.info("Skipped updating approved card " + hc.getDescription());
				HardCard dummy = new HardCard();
				importCard(dummy, card);
				changed = applyChanges(dummy, hc);
			}
		} catch (Exception e) {
			log.error("Error updating card", e);
		}
		return changed;
	}

	public HardCard create(EventCard card) {
		HardCard hc = new HardCard();
		try {
			importCard(hc, card);
		} catch (Exception e) {
			log.error("Error creating card", e);
		}
		return hc;
	}

	private void findFirstPostTime(HardCard card) {
		long firstRaceNumber = 1000;
		Date firstRacePostTime = null;
		for (HardRace r : card.getRaces()) {
			if (r.getPostTime() != null) {
				if (r.getNumber() != null && r.getNumber() < firstRaceNumber) {
					firstRaceNumber = r.getNumber();
					if (r.getPostTime() != null) {
						firstRacePostTime = r.getPostTime();
					}
				}
			}
		}
		card.setFirstRaceTime(firstRacePostTime);
	}

	private void importRunner(HardRace r, BetInterest starter) throws ImportException {
		try {

			Runner runner = CardHelper.findRunner(r, starter.getPosition());
			if (runner == null) {
				runner = new Runner();
				runner.setParent(r);
				runner.setPosition(starter.getPosition());
				r.getRunners().add(runner);
			}
			runner.setJockeyName(starter.getJockeyName());
			runner.setName(starter.getName());
			runner.setShortName(starter.getShortName());
			runner.setWeightCarried(starter.getWeightCarried());
			runner.setWeightUnits(WEIGHT_UNIT);
			runner.setScratched(starter.getScratched());
			runner.setProgramNumber(starter.getProgramNumber());
			runner.setPosition(starter.getPosition());
			runner.setCoupleIndicator(starter.getCoupleIndicator());
			if (StringUtils.isNotEmpty(starter.getFract())) {
				runner.setMornlineOdds(starter.getFract());
			}

		} catch (Exception e) {
			log.error("Error importing runners", e);
			throw new ImportException("Error importing runners.");
		}
	}

	private void importRace(HardCard card, EventRace race) throws ImportException {
		HardRace hr = CardHelper.findRace(card, race.getNumber());
		if (hr == null) {
			hr = new HardRace();
			hr.setNumber(race.getNumber());
			hr.setParent(card);
			card.getRaces().add(hr);
		}

		try {

			hr.setNumber(race.getNumber());
			hr.setDistance(race.getDistance());
			hr.setDistanceText(race.getDistance());

			hr.setRaceType(race.getRaceType());
			hr.setCourse(race.getCourse());
			hr.setPostTime(race.getPostTime());
			hr.setHandicap(race.getHandicap());
			hr.setNumberPositions(race.getPlacesExpected());
			hr.setRaceNameLong(race.getRaceNameLong());
			hr.setRaceNameShort(race.getRaceNameShort());
			hr.setPoolIdList(race.getPoolIdList());
			hr.setRaceCode(race.getRaceCode());
			if (hr.getRunners() == null) {
				hr.setRunners(new ArrayList<Runner>());
			}
			for (BetInterest b : race.getRunners()) {
				importRunner(hr, b);
			}

		} catch (Exception e) {
			log.error("Error importing race", e);
			throw new ImportException("Error importing race.");
		}
	}

	private HardCard importCard(HardCard card, EventCard raceCard) throws ImportException {

		try {
			if (card.getTrack() == null) {
				Track track = new Track();
				track.setTrackCode(raceCard.getTrackCode());
				track.setEquibaseId(raceCard.getTrackCode());
				track.setName(raceCard.getTrackName());
				track.setCountryCode(raceCard.getCountryCode());
				track.setTrackType(raceCard.getCardType());
				track.setPerfType(raceCard.getPerfType());
				card.setTrack(track);
			}
			card.setProvider("SIS");
			card.setPerfType(PerformanceType.Matinee);
			card.setCardType(raceCard.getCardType());
			card.setCardDate(DateHelper.stringToDate(raceCard.getCardDate()));
			card.setFirstRaceTime(DateUtils.fromPostTimeStr(raceCard.getStartTime()));

			PassThroughBag p = new PassThroughBag();
			p.setMeetingCodeUK(SisParserHelper.getToteCode(raceCard.getCountryCode(), raceCard.getTrackCode(), raceCard.getTrackName()));
			card.setPassThroughs(JSonHelper.toJSon(p));

			if (card.getRaces() == null) {
				card.setRaces(new HashSet<HardRace>());
			}
			if (raceCard.getRaces() != null) {
				for (EventRace r : raceCard.getRaces()) {
					importRace(card, r);
				}
			}
			if (raceCard.getPools() != null) {
				if (card.getPools() == null) {
					card.setPools(new HashSet<HardPool>());
				}
				for (EventPool r : raceCard.getPools()) {
					HardPool cp = new HardPool();
					PoolType pt = PoolType.fromCode(r.getCode());
					if (pt == null) {
						log.error("Unknown pool type:" + r.getCode());
						continue;
					}
					cp.setPoolTypeId(pt.getId());
					PoolHelper.populatePool(card, cp, r.getRaces());
					card.getPools().add(cp);
				}
			}
			findFirstPostTime(card);
			card.setDescription(DateUtils.buildName(raceCard.getName(), card.getFirstRaceTime()));
			return card;

		} catch (Exception e) {
			log.error("Error importing card", e);
			throw new ImportException("Error importing card.");
		}
	}

	private boolean applyChanges(HardCard oldCard, HardCard newCard) {
		boolean changed = false;
		for (HardRace race : oldCard.getRaces()) {
			for (Runner runner : race.getRunners()) {
				Runner newRunner = findMatchingRunner(newCard, race, runner);
				if (newRunner != null) {
					if (newRunner.getMornlineOdds() != null) {
						String newOdds = newRunner.getMornlineOdds();
						String oldOdds = runner.getMornlineOdds();
						if (StringUtils.isEmpty(oldOdds) || !StringUtils.equalsIgnoreCase(newOdds, oldOdds)) {
							runner.setMornlineOdds(newRunner.getMornlineOdds());
							changed = true;
							log.info("Update runner " + runner.getPosition() + " price from" + oldOdds + " to " + newOdds);
						}
					}
				}
			}
		}
		return changed;
	}

	private Runner findMatchingRunner(HardCard hc, HardRace hr, Runner runner) {
		for (HardRace race : hc.getRaces()) {
			if (race.getNumber().longValue() == hr.getNumber().longValue()) {
				for (Runner r : race.getRunners()) {
					if (r.getPosition() != null && r.getPosition().longValue() == runner.getPosition().longValue()) {
						return r;
					}
				}
			}
		}
		return null;
	}

}
