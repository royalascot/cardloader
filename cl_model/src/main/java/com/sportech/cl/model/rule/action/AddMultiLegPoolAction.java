package com.sportech.cl.model.rule.action;

import java.util.ArrayList;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import com.sportech.cl.model.database.HardRace;
import com.sportech.cl.model.database.utils.PoolContainer;
import com.sportech.cl.model.database.utils.PoolHelper;
import com.sportech.cl.model.rule.RuleAction;
import com.sportech.cl.model.rule.RuleContext;
import com.sportech.common.model.PoolType;

public class AddMultiLegPoolAction implements RuleAction {

	static private final Logger log = Logger.getLogger(AddMultiLegPoolAction.class);

	private static final String name = "Add Multi-leg Pool (P1) to Race (P2)";

	public String getName() {
		return name;
	}

	public boolean execute(RuleContext context) {
		PoolContainer container = context.getTarget();
		HardRace race = context.getRace();
		if (container == null) {
			return false;
		}
		if (race == null) {
			return false;
		}
		String pool = (String) context.getValue("param1");
		String rn = (String) context.getValue("param2");
		if (pool != null && race != null) {
			if (StringUtils.isNumeric(rn)) {
				long raceNumber = Long.parseLong(rn);
				if (raceNumber != race.getNumber()) {
					return true;
				}
				PoolType pt = PoolType.fromCode(pool);
				if (pt != null) {
					ArrayList<Long> races = new ArrayList<Long>();
					for (long r = raceNumber; r < raceNumber + pt.getRaceCount(); r++) {
						races.add(r);
					}
					PoolHelper.refreshMultiPool(container, pt.getId(), raceNumber, StringUtils.join(races, ","));
					log.info("Rule engine added new multi leg pool " + pt.getCode() + " to race " + race.getNumber());
				}
			}
		}
		return true;
	}
}
