package com.sportech.cl.model.response;

import com.sportech.cl.model.database.SoftCard;

public class SoftCardResponse  extends ObjectResponse<SoftCard>
{
    public SoftCardResponse() {}
    public SoftCardResponse(GenericResponse rsp)            { super(rsp); }

    public SoftCard getCard()           { return getObject(); }
    public void setCard(SoftCard card)  { setObject( card ); }

    private static final long serialVersionUID = -1L;
}
