package com.sportech.cl.model.security;

import com.sportech.cl.model.common.ExceptionStringWriter;

public enum ActorType
{
	 TOTE					(1,  "Tote")
	,OPERATOR				(2,  "Operator")
	,CUSTOMER				(3,  "Customer")
    ,GROUP                  (4,  "Group")
	;

	   private final Integer 	id;
	   private final String 	description;
	 
	 ActorType(Integer id, String description) {
		   	this.id = id;
		   	this.description = description;
		   }
	 

	   public static ActorType getById(Integer passedInt) 
	   {
		if(passedInt != null) {
		   for(ActorType e : ActorType.values()) {
		       if(e.getId().equals(passedInt)) {
		           return e;
		       }
		   }
		}
	
		return null;
	   }

	   public boolean equals(ActorType e) {
		   	return id.equals(e.getId());
		   }
	   
	   public String toString()  {
	   	StringBuffer str = new StringBuffer();
	   	try {
				str.append("ActorType(" + id + ",<" + description + ">)");
			} catch (RuntimeException e) {
				ExceptionStringWriter.exception2str(e, str);
			}
	   	return str.toString();
	   }
	   
	   public Integer getId() {
	       return id;
	   }
	   
	   public String getDescription() {
	       return description;
	   }

	   static final long serialVersionUID = -1L;	
}
