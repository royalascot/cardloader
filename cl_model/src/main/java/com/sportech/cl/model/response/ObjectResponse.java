package com.sportech.cl.model.response;

import javax.xml.bind.annotation.XmlTransient;

import com.sportech.cl.model.common.ErrorCode;

public class ObjectResponse<ObjType> extends GenericResponse 
{
	public ObjectResponse() {}
	public ObjectResponse(GenericResponse rsp)			       { super(rsp); } 
	public ObjectResponse(ErrorCode	code, String... _errors)   { super( code, _errors ); }
	public ObjectResponse(int _status, String... _errors)      { super(_status, _errors); }
	public ObjectResponse(ObjType	obj)				       { this.object = obj; }

	@XmlTransient
	public ObjType getObject() 							{ return object; }
	public void setObject(ObjType	obj)  				{ this.object = obj; }

	protected ObjType	object;
	
	private static final long serialVersionUID = -1L;
}
