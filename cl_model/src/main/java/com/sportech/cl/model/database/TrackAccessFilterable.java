package com.sportech.cl.model.database;

public interface TrackAccessFilterable {
	public Long getTrackId();
}
