package com.sportech.cl.model.database.utils;


public interface PersistentWithParent<T, PT> extends Persistent<T>
{
	public boolean equals4parent(T obj);
	
	public void	setParentCascade(PT pt);
}
