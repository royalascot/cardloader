package com.sportech.cl.model.rule;

public enum RelationOperatorType {
    
    LESS,
    
    CONTAINS,
    
    GREATER
    
}
