package com.sportech.cl.model.database;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;

import org.apache.commons.lang3.StringUtils;
import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonProperty;

import com.sportech.common.model.PoolType;
import com.sportech.common.util.ListHelper;

public class PoolParameter implements Serializable, Comparable<PoolParameter> {

    private static final long serialVersionUID = 1L;

    @JsonProperty("id")
    private Long id;

    @JsonProperty("type")
    private PoolParameterType parameterType;

    @JsonProperty("min")
    private BigDecimal minAmount;

    @JsonProperty("inc")
    private BigDecimal incrementAmount;

    @JsonProperty("race")
    @XmlTransient
    private Set<Long> races;

    @JsonIgnore
    private PoolType poolType;

    @JsonCreator
    public PoolParameter() {
    }

    @XmlElement(name = "pool_type_id")
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @XmlTransient
    public PoolParameterType getParameterType() {
        return parameterType;
    }

    public void setParameterType(PoolParameterType parameterType) {
        this.parameterType = parameterType;
    }

    @XmlElement(name = "minimum_amount")
    public BigDecimal getMinAmount() {
        return minAmount;
    }

    public void setMinAmount(BigDecimal minAmount) {
        this.minAmount = minAmount;
    }

    @XmlElement(name = "increment_amount")
    public BigDecimal getIncrementAmount() {
        return incrementAmount;
    }

    public void setIncrementAmount(BigDecimal incrementAmount) {
        this.incrementAmount = incrementAmount;
    }

    @XmlElement(name = "race_number")
    @JsonIgnore
    public String getRaceNumber() {
        String text = ListHelper.build(getRaces());
        if (StringUtils.isBlank(text)) {
            return null;
        }
        return text;
    }

    public void setRaceNumber(String v) {
        Set<String> l = ListHelper.parse(v);
        if (l != null) {
            try {
                getRaces().clear();
                for (String i : l) {
                    if (StringUtils.isNumeric(i)) {
                        getRaces().add(Long.parseLong(i));
                    }
                }
            } catch (Exception e) {

            }
        }
    }

    @XmlTransient
    public PoolType getPoolType() {
        return poolType;
    }

    public void setPoolType(PoolType poolType) {
        this.poolType = poolType;
        if (poolType != null) {
            this.id = (long) poolType.getId();
        }
    }

    @XmlTransient
    public Set<Long> getRaces() {
        if (races == null) {
            races = new HashSet<Long>();
        }
        return races;
    }

    public void setRaces(Set<Long> races) {
        this.races = races;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        if (StringUtils.isNotBlank(getRaceNumber())) {
            result = prime * result + getRaceNumber().hashCode();
        }
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        PoolParameter other = (PoolParameter) obj;
        return compareTo(other) == 0;
    }

    @Override
    public int compareTo(PoolParameter other) {
        if (this == other) {
            return 0;
        }
        if (id == null) {
            return -1;
        }
        if (other.id == null) {
           return 1;
        } 
        if (!id.equals(other.id)) {
            return id.compareTo(other.id);
        }
        if (getRaces() == null && other.getRaces() == null) {
            return 0;
        }
        if (getRaces().containsAll(other.getRaces()) && other.getRaces().containsAll(getRaces())) {
            return 0;            
        }
        return getRaceNumber().compareTo(other.getRaceNumber());
    }
    
}
