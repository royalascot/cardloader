package com.sportech.cl.model.response;

import com.sportech.cl.model.database.Pattern;

public class PatternResponse  extends ObjectResponse<Pattern>
{
    public PatternResponse() {}
    public PatternResponse(GenericResponse rsp)            { super(rsp); }

    public Pattern getPattern()           { return getObject(); }
    public void setPattern(Pattern card)  { setObject( card ); }

    private static final long serialVersionUID = -1L;
}
