package com.sportech.cl.model.database;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlTransient;

import com.sportech.common.model.CardType;
import com.sportech.common.model.PerformanceType;

@MappedSuperclass
@XmlAccessorType(XmlAccessType.PROPERTY)
public class SoftCardBase implements Serializable {

	public static final String DATE_PROP_NAME = "cardDate";

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "soft_card_pk")
	private Long id;
	@Column(name = "description")
	private String description;
	@Column(name = "sparse_races")
	private Boolean sparseRaces;
	@Column(name = "sparse_pools")
	private Boolean sparsePools;
	@Column(name = "load_code")
	private String loadCode;
	@Column(name = "pattern_fk")
	private Long patternId;
	@Column(name = "approved")
	private Boolean approved;
	@Column(name = "hard_card_fk")
	private Long hardCardId;
	@Column(name = "card_date")
	private java.util.Date cardDate;
	@Column(name = "download_list")
	private String downloadedTotes;
	@Column(name = "access_list")
	private String toteName;

	@Column(name = "source")
	private String source;

	@JoinColumn(name = "dict_track_fk")
	@ManyToOne(fetch = FetchType.EAGER)
	private Track track;

	@JoinColumn(name = "config_id")
	@ManyToOne(fetch = FetchType.EAGER)
	private CardConfig config;

	@Column(name = "dict_track_fk", insertable = false, updatable = false)
	private Long trackId;

	@Column(name = "tote_approval_list")
	private String toteApprovalList;

	@Column(name = "card_type")
	private CardType cardType;

	@Column(name = "perf_type")
	private PerformanceType perfType;

	@Column(name = "feature_race")
	private Integer featureRace;

	public SoftCardBase() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public PerformanceType getPerfType() {
		return perfType;
	}

	public void setPerfType(PerformanceType perfType) {
		this.perfType = perfType;
	}

	public CardType getCardType() {
		return cardType;
	}

	public void setCardType(CardType cardType) {
		this.cardType = cardType;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Boolean getSparseRaces() {
		return sparseRaces;
	}

	public void setSparseRaces(Boolean sparseRaces) {
		this.sparseRaces = sparseRaces;
	}

	public Boolean getSparsePools() {
		return sparsePools;
	}

	public void setSparsePools(Boolean sparsePools) {
		this.sparsePools = sparsePools;
	}

	public String getLoadCode() {
		return loadCode;
	}

	public void setLoadCode(String loadCode) {
		this.loadCode = loadCode;
	}

	public Long getPatternId() {
		return patternId;
	}

	public void setPatternId(Long patternId) {
		this.patternId = patternId;
	}

	public Boolean getApproved() {
		return approved;
	}

	public void setApproved(Boolean approved) {
		this.approved = approved;
	}

	public Long getHardCardId() {
		return hardCardId;
	}

	public void setHardCardId(Long hardCardId) {
		this.hardCardId = hardCardId;
	}

	public Integer getFeatureRace() {
		return featureRace;
	}

	public void setFeatureRace(Integer featureRace) {
		this.featureRace = featureRace;
	}

	@XmlElement
	@XmlSchemaType(name = "date")
	public java.util.Date getCardDate() {
		return cardDate;
	}

	public void setCardDate(java.util.Date cardDate) {
		this.cardDate = cardDate;
	}

	public String getToteName() {
		return toteName;
	}

	public void setToteName(String toteName) {
		this.toteName = toteName;
	}

	public String getDownloadedTotes() {
		return downloadedTotes;
	}

	public void setDownloadedTotes(String downloadedTotes) {
		this.downloadedTotes = downloadedTotes;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	@XmlTransient
	public CardConfig getConfig() {
		return config;
	}

	public void setConfig(CardConfig config) {
		this.config = config;
	}

	@XmlTransient
	public Track getTrack() {
		return track;
	}

	public void setTrack(Track track) {
		this.track = track;
	}

	@XmlTransient
	public String getToteApprovalList() {
		return toteApprovalList;
	}

	public void setToteApprovalList(String toteApprovalList) {
		this.toteApprovalList = toteApprovalList;
	}

	@Transient
	public Integer getDownloadCount() {
		if (downloadedTotes != null) {
			String[] totes = downloadedTotes.split(",");
			return totes.length;
		}
		return 0;
	}

	@XmlElement
	public Long getTrackId() {
		return trackId;
	}

	public void setTrackId(Long trackId) {
		this.trackId = trackId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SoftCardBase other = (SoftCardBase) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	private static final long serialVersionUID = -1L;

}
