package com.sportech.cl.model.rule.action;

import org.apache.log4j.Logger;

import com.sportech.cl.model.database.HardRace;
import com.sportech.cl.model.database.utils.PoolContainer;
import com.sportech.cl.model.database.utils.PoolHelper;
import com.sportech.cl.model.rule.RuleAction;
import com.sportech.cl.model.rule.RuleContext;
import com.sportech.common.model.PoolType;

public class RemoveSingleLegPool implements RuleAction {
    
    static private final Logger log = Logger.getLogger(RemoveSingleLegPool.class);

    private static final String name = "Remove Pool (P1) from Race";

    public String getName() {
        return name;
    }

    public boolean execute(RuleContext context) {
        PoolContainer container = context.getTarget();
        HardRace race = context.getRace();
        if (container == null) {
            return false;
        }
        if (race == null) {
            return false;
        }
        String pool = (String) context.getValue("param1");
        if (pool != null) {
            PoolType pt = PoolType.fromCode(pool);
            if (pt != null) {
                PoolHelper.refreshSinglePool(container, pt.getId(), race.getNumber(), false); 
                log.info("Rule engine removed pool " + pt.getCode() + " from race " + race.getNumber());
            }
        }
        return true;
    }
    
}

