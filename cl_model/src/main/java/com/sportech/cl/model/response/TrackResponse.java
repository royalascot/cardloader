package com.sportech.cl.model.response;

import com.sportech.cl.model.database.Track;

public class TrackResponse  extends ObjectResponse<Track>
{
    public TrackResponse() {}
    public TrackResponse(GenericResponse rsp)   { super(rsp); }

    public Track getTrack()                     { return getObject(); }
    public void setTrack(Track track)           { setObject( track ); }

    private static final long serialVersionUID = -1L;
}
