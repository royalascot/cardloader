package com.sportech.cl.model.database;

import java.io.Serializable;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.sportech.cl.model.database.utils.PersistentWithId;

@Entity
@Table(name = "totes")
public class Tote extends ToteBase implements Serializable, PersistentWithId<Tote>, Comparable<Tote> {
	
	private String password;
	
	private Set<UserOutline> users = new HashSet<UserOutline>();

	private List<Track> tracks;

	private List<Group> groups;
	
	@Column(name="password")
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@ManyToMany(mappedBy = "totes")
	public List<Group> getGroups() {
		return groups;
	}

	public void setGroups(List<Group> groups) {
		this.groups = groups;
	}

	@ManyToMany()
	@JoinTable(name = "track_tote", joinColumns = { @JoinColumn(name = "tote_id", referencedColumnName = "tote_pk") }, inverseJoinColumns = { @JoinColumn(name = "track_id", referencedColumnName = "dict_track_pk") })
	public List<Track> getTracks() {
		return tracks;
	}

	public void setTracks(List<Track> tracks) {
		this.tracks = tracks;
	}

	@Transient
	public Set<UserOutline> getUsers() {
		return users;
	}

	public void setUsers(Set<UserOutline> users) {
		this.users = users;
	}

	@Override
	public void copy4persistence(Tote from) {
		setDescription(from.getDescription());
		setRegion(from.getRegion());
		setToteCode(from.getToteCode());
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((getToteCode() == null) ? 0 : getToteCode().hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Tote other = (Tote) obj;
		if (getToteCode() == null) {
			if (other.getToteCode() != null)
				return false;
		} else if (!getToteCode().equals(other.getToteCode()))
			return false;
		return true;
	}

	@Override
	public int compareTo(Tote t) {
		if (t == null || getName() == null || t.getName() == null) {
			return -1;
		}	
		return getName().compareTo(t.getName());
	}

	private static final long serialVersionUID = -1L;
}
