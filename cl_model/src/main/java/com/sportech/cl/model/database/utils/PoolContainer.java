package com.sportech.cl.model.database.utils;

import java.util.Collection;

import com.sportech.cl.model.database.HardRace;

public interface PoolContainer {
    
    public Pool addPool(int poolTypeId, long bitmap);

    public void removePool(Pool p);

    public Collection<Pool> getPoolCollection();

    public Integer getRaceCount();

    public void setRaceCount(Integer count);

    public boolean isReadonly();

    public boolean canDisableRace();

    public boolean hasRunners();

    public HardRace getPoolRace(long raceNumber);
    
}
