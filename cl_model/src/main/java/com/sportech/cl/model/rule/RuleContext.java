package com.sportech.cl.model.rule;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

import com.sportech.cl.model.database.HardCard;
import com.sportech.cl.model.database.HardRace;
import com.sportech.cl.model.database.SoftCard;

public class RuleContext {

    private HardCard card;
    private HardRace race;
    private SoftCard target;
    private Date firstPostTime = null;
    
    private Map<String, Object> parameters = new HashMap<String, Object>();

    public HardCard getCard() {
        return card;
    }

    public void setCard(HardCard card) {
        this.card = card;
        calculateFirstPostTime(card);
    }

    public HardRace getRace() {
        return race;
    }

    public void setRace(HardRace race) {
        this.race = race;
    }

    public SoftCard getTarget() {
        return target;
    }

    public void setTarget(SoftCard target) {
        this.target = target;
    }

    public Date getFirstPostTime() {
        return firstPostTime;
    }

    public void setFirstPostTime(Date firstPostTime) {
        this.firstPostTime = firstPostTime;
    }

    public Map<String, Object> getParameters() {
        return parameters;
    }

    public void setParameters(Map<String, Object> parameters) {
        this.parameters = parameters;
    }

    public Object getValue(String key) {
        return parameters.get(key);
    }
    
    public void setValue(String key, Object value) {
        parameters.put(key, value);
    }
    
    private void calculateFirstPostTime(HardCard card) {
        TreeMap<Long, Date> races = new TreeMap<Long, Date>();
        for (HardRace r : card.getRaces()) {
            if (r != null) {
                races.put(r.getNumber(), r.getPostTime());
            }
        }
        for (Long l : races.keySet()) {
            firstPostTime = races.get(l);
            if (firstPostTime != null) {
                break;
            }
        }
    }

}
