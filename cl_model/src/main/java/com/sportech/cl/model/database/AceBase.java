package com.sportech.cl.model.database;

import java.io.Serializable;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.MappedSuperclass;
import javax.persistence.Id;
import javax.persistence.Column;
import javax.xml.bind.annotation.XmlTransient;

@MappedSuperclass
public class AceBase implements Serializable {

	@Id
	@Column(name = "ac_pk")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	protected Long id;

	@Column(name = "object_type_fk")
	protected Integer objectTypeId;
	@Column(name = "actor_id")
	protected Integer actorId;
	@Column(name = "actor_type_fk")
	protected Integer actorTypeId;
	@Column(name = "permissions_bitmap")
	protected Integer permissions;

	public AceBase() {
	}

	@XmlTransient
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@XmlTransient
	public Integer getObjectTypeId() {
		return objectTypeId;
	}

	public void setObjectTypeId(Integer objectTypeId) {
		this.objectTypeId = objectTypeId;
	}

	@XmlTransient
	public Integer getActorId() {
		return actorId;
	}

	public void setActorId(Integer actorId) {
		this.actorId = actorId;
	}

	@XmlTransient
	public Integer getActorTypeId() {
		return actorTypeId;
	}

	public void setActorTypeId(Integer actorTypeId) {
		this.actorTypeId = actorTypeId;
	}

	@XmlTransient
	public Integer getPermissions() {
		return permissions;
	}

	public void setPermissions(Integer permissions) {
		this.permissions = permissions;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());

		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AceBase other = (AceBase) obj;

		if (id != null && other.id != null)
			return id.equals(other.id);

		if (actorId == null) {
			if (other.actorId != null)
				return false;
		} else if (!actorId.equals(other.actorId))
			return false;
		if (actorTypeId == null) {
			if (other.actorTypeId != null)
				return false;
		} else if (!actorTypeId.equals(other.actorTypeId))
			return false;
		if (objectTypeId == null) {
			if (other.objectTypeId != null)
				return false;
		} else if (!objectTypeId.equals(other.objectTypeId))
			return false;
		return true;
	}

	protected void copy(AceBase from) {
		this.setObjectTypeId(from.getObjectTypeId());
		this.setActorId(from.getActorId());
		this.setActorTypeId(from.getActorTypeId());
		this.setPermissions(from.getPermissions());
	}

	private static final long serialVersionUID = -1L;
}
