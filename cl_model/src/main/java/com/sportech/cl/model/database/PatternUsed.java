package com.sportech.cl.model.database;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name="p_get_appropriate_patterns")
public class PatternUsed extends PatternOutlineBase
{
    @Column(name="already_used")    private Boolean alreadyUsed;
    
    public Boolean getAlreadyUsed()                     { return alreadyUsed; }
    public void setAlreadyUsed(Boolean alreadyUsed)     { this.alreadyUsed = alreadyUsed; }

    private static final long serialVersionUID = -1L;
}
