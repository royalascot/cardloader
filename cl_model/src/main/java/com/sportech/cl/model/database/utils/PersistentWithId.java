package com.sportech.cl.model.database.utils;

public interface PersistentWithId<T> extends EntityWithId, Persistent<T>
{

}
