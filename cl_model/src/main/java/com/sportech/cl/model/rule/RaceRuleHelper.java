package com.sportech.cl.model.rule;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.TreeMap;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.codehaus.jackson.map.ObjectMapper;

import com.sportech.cl.model.database.HardRace;
import com.sportech.cl.model.database.SoftCard;
import com.sportech.cl.model.database.SoftRace;

public class RaceRuleHelper {

    static private final Logger log = Logger.getLogger(RaceRuleHelper.class);

    private Date firstPostTime = null;

    public RaceRuleHelper(SoftCard card) {
        TreeMap<Long, Date> races = new TreeMap<Long, Date>();
        for (SoftRace r : card.getRaces()) {
            if (r.getHardRace() != null) {
                races.put(r.getHardRace().getNumber(), r.getHardRace().getPostTime());
            }
        }
        for (Long l : races.keySet()) {
            firstPostTime = races.get(l);
            if (firstPostTime != null) {
                break;
            }
        }
    }

    public static String toText(Collection<RuleCondition> rules) {
        ObjectMapper mapper = new ObjectMapper();
        List<String> ruleList = new ArrayList<String>();
        try {
            for (RuleCondition r : rules) {
                String value = mapper.writeValueAsString(r);
                ruleList.add(value);
            }
        } catch (Exception e) {
            log.error("Error generating race rule text.", e);
        }
        return StringUtils.join(ruleList, "|");
    }

    public static List<RuleCondition> fromText(String text) {
        List<RuleCondition> rules = new ArrayList<RuleCondition>();
        ObjectMapper mapper = new ObjectMapper();
        String[] ruleList = StringUtils.split(text, "|");
        if (ruleList != null) {
            for (String r : ruleList) {
                RuleCondition rr = null;
                try {
                    rr = mapper.readValue(r, RuleCondition.class);
                } catch (Exception e) {
                    log.error("Error parsing rule:", e);
                }
                if (rr != null) {
                    rules.add(rr);
                }
            }
        }
        return rules;
    }

    public static boolean equals(RuleCondition r1, RuleCondition r2) {
        if (r1 != null && r2 != null) {
            if (r1.getRuleType() != null && r2.getRuleType() != null && r1.getRuleType() != r2.getRuleType()) {
                return false;
            }
            return StringUtils.equals(r1.getField().getFieldName(), r2.getField().getFieldName())
                    && (r1.getRelationOperator() == r2.getRelationOperator()) && StringUtils.equals(r1.getValue(), r2.getValue());
        }
        return false;
    }

    public boolean applyRaceRule(HardRace race, RuleCondition rule) {
        try {
            RuleContext context = new RuleContext();
            context.setRace(race);
            return rule.eval(context);
        } catch (Exception e) {
            log.error("Error applying race rule", e);
        }
        return false;
    }

}
