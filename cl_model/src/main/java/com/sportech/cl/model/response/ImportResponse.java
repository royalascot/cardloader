package com.sportech.cl.model.response;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.sportech.cl.model.database.ImportOutline;

@XmlRootElement
public class ImportResponse  extends ObjectResponse<ImportOutline>
{
    public ImportResponse() {}
    public ImportResponse(GenericResponse rsp)            { super(rsp); }

    @XmlElement(name="import")
    public ImportOutline getImport()           { return getObject(); }

    private static final long serialVersionUID = -1L;
}
