package com.sportech.cl.model.database.utils;

public class SortOrder {
	public enum OrderRank {
		ASC,
		DESC
	};
	
	private String fieldName;
	private OrderRank rank;
	
	public SortOrder(String name) {
		this(name, OrderRank.ASC); 
	}
	
	public SortOrder(String name, OrderRank rank) {
		this.fieldName = name;
		this.rank = rank;
	}
	
	public String getFieldName() {
		return fieldName;
	}
	
	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}
	
	public OrderRank getRank() {
		return rank;
	}

	public void setRank(OrderRank rank) {
		this.rank = rank;
	}
	
}
