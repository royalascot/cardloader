package com.sportech.cl.model.database;

import java.io.Serializable;
import java.util.Date;
import java.util.zip.CRC32;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.xml.bind.annotation.XmlElement;

@MappedSuperclass
public class ImportBase implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "import_pk")
	private Long id;
	@Column(name = "timestamp")
	private Date timestamp;

	@Column(name = "status")
	private String status;
	@Column(name = "original_filename")
	private String filename;
	@Column(name = "checksum")
	private Long checksum;
	@Column(name = "file_date")
	private Date fileDate;
	@Column(name = "dict_track_fk")
	private Long trackId;

	public ImportBase() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}

	public static long getCrc(byte[] data) {
		CRC32 x = new CRC32();
		x.update(data);
		return x.getValue();
	}

	@XmlElement
	public Long getChecksum() {
		return checksum;
	}

	public void setChecksum(Long checksum) {
		this.checksum = checksum;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getFilename() {
		return filename;
	}

	public void setFilename(String filename) {
		this.filename = filename;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	public Long getTrackId() {
		return trackId;
	}

	public void setTrackId(Long trackId) {
		this.trackId = trackId;
	}

	public Date getFileDate() {
		return fileDate;
	}

	public void setFileDate(Date fileDate) {
		this.fileDate = fileDate;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ImportBase other = (ImportBase) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	protected void copy(ImportBase from) {
		setTimestamp(from.getTimestamp());
		setStatus(from.getStatus());
		setFilename(from.getFilename());
		setChecksum(from.getChecksum());
	}

	private static final long serialVersionUID = -1L;
}
