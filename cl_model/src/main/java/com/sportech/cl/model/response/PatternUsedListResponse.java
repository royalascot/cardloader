package com.sportech.cl.model.response;

import java.util.Collection;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;

import com.sportech.cl.model.common.ErrorCode;
import com.sportech.cl.model.database.PatternUsed;

public class PatternUsedListResponse extends ObjectListResponse<PatternUsed> 
{
    public PatternUsedListResponse() {}
    public PatternUsedListResponse(GenericResponse rsp)                { super(rsp); } 
    public PatternUsedListResponse(ErrorCode code, String... _errors)   { super( code, _errors ); }

    @XmlElement(name="pattern")
    public List<PatternUsed> getCards()                     { return getObjects(); }
    public void setCards(Collection<PatternUsed> patterns)  { setObjects(patterns); }
    
    private static final long serialVersionUID = -1L;
}
