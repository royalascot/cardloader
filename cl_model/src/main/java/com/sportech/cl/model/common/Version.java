package com.sportech.cl.model.common;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAttribute;

public class Version implements Serializable
{
    protected String            name;
    protected String            version;
    protected java.util.Date    buildDate;
    protected String            buildSystem;
    
    public Version()                                        {}
    public Version( String _name, String _version )         {
                                                                this.name = _name;
                                                                this.version = _version; 
                                                            }
    
    @XmlAttribute(name="name", required=true)
    public String getName()                                 { return name; }
    public void setName(String name)                        { this.name = name; }
    
    @XmlAttribute(name="version", required=true)
    public String getVersion()                              { return version; }
    public void setVersion(String version)                  { this.version = version; }

    @XmlAttribute(name="build-date", required=false)
    public java.util.Date getBuildDate()                    { return buildDate; }
    public void setBuildDate(java.util.Date buildDate)      { this.buildDate = buildDate; }

    @XmlAttribute(name="build-system", required=false)
    public String getBuildSystem()                          { return buildSystem; }
    public void setBuildSystem(String buildSystem)          { this.buildSystem = buildSystem; }

    private static final long serialVersionUID = -1L;
}
