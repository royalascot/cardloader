package com.sportech.cl.model.database.utils;

import java.util.Collection;
import java.util.Vector;
import java.util.Iterator;

public class PersistenceHelper 
{

	public static <PT,T extends PersistentWithParent<T,PT>> void mergeCollections( Collection<T> old_coll, Collection<T> new_coll, PT parent)
	{
		if( new_coll.isEmpty() ) {
			old_coll.clear();
		}
		else {
			Vector<T>	toBeDeleted = new Vector<T>(old_coll);
			Vector<T>	toBeAdded = new Vector<T>();
			
			Iterator<T> itr = new_coll.iterator();
			
			while( itr.hasNext()) 
			{
				T	new_elem = itr.next();
				T	old_elem = find( old_coll, new_elem );
				
				
				if( old_elem != null ) {
					toBeDeleted.remove(old_elem);
					
					old_elem.copy4persistence(new_elem);
					
				}
				else {
					new_elem.setParentCascade(parent);
					toBeAdded.add(new_elem);
				}
			}
			
			old_coll.removeAll(toBeDeleted);
			old_coll.addAll(toBeAdded);
		}
	}

	public static <PT,T extends PersistentWithParent<T,PT>> void setParentCascade( Collection<T> coll, PT parent)
	{
		if( coll != null && !coll.isEmpty() )
		{
			Iterator<T> itr = coll.iterator();
			
			while( itr.hasNext()) 
			{
				T	elem = itr.next();
				
				elem.setParentCascade(parent);
			}
		}
		
	}

	public static <PT,T extends PersistentWithParent<T,PT>> T find( Collection<T> coll, T what )
	{
		Iterator<T>	itr = coll.iterator();
		
		while( itr.hasNext()) 
		{
			T	e = itr.next();
			
			if( e.equals4parent(what))
				return e;
		}
		
		return null;
	}
	
	
}
