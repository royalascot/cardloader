package com.sportech.cl.model.database;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.sportech.cl.model.database.utils.PersistentWithId;

@Entity
@Table(name = "import")
public class Import extends ImportBase implements Serializable, PersistentWithId<Import> {
	
	@Transient
	private byte[] data;
	
	@Lob
	@Column(name = "log")
	private String log;

	public Import() {
	}

	public byte[] getData() {
		return data;
	}

	private void setData(byte[] data, boolean calcChecksum) {
		this.data = data;
		if (calcChecksum) {
			setChecksum(getCrc(data));
		}
	}

	public void setData(byte[] data) {
		setData(data, true);
	}

	public String getLog() {
		return log;
	}

	public void setLog(String log) {
		this.log = log;
	}

	@Override
	public void copy4persistence(Import from) {
		super.copy(from);
		setData(from.getData(), false);
		setLog(from.getLog());
	}

	private static final long serialVersionUID = -1L;

}
