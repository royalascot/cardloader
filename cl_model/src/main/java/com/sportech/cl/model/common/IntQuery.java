package com.sportech.cl.model.common;

public class IntQuery extends QueryBase {
    private Integer _value;

    public IntQuery() {
        super( IntegerPredicate.EQ.getCode() );
        _value = 0;
    }
    
    public IntQuery(String predicate, Integer value) {
        super(predicate);
        this._value = value;
    }
    
    public Integer getValue()                   { return _value; }
    public void setValue(Integer newValue)      { _value = newValue; }
    
    public IntegerPredicate getIntPredicate()   { return IntegerPredicate.ivalueOf(_query); }
}
