package com.sportech.cl.model.common;

public class ReQuery extends QueryBase {
    protected boolean _negate;
    
    public ReQuery() { super(); }
    
    public ReQuery(boolean negate) {
        super();
        _negate = negate;
    }
    
    public ReQuery(String query, boolean negate) {
        super(query);
        _negate = negate;
    }
    
    public boolean getNegate()                { return _negate; }
    public void setNegate( boolean newValue ) { _negate = newValue; }
}