package com.sportech.cl.model.database;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlTransient;

import com.sportech.cl.model.database.utils.PersistentWithParent;

@Entity
@Table(name = "hard_pool")
public class HardPool extends PoolBase implements PersistentWithParent<HardPool, HardCard> {
	
	private HardCard parent;

	public HardPool() {
	}

	public HardPool(PoolBase src) {
		super(src);
	}

	@ManyToOne
	@JoinColumn(name = "hard_card_fk")
	@XmlTransient
	public HardCard getParent() {
		return parent;
	}

	public void setParent(HardCard parent) {
		this.parent = parent;
	}

	@Override
	public boolean equals4parent(HardPool obj) {
		return equals(obj);
	}

	@Override
	public void copy4persistence(HardPool from) {
		copy(from);
	}

	@Override
	public void setParentCascade(HardCard pt) {
		setParent(pt);
	}

	private static final long serialVersionUID = -1L;
	
}
