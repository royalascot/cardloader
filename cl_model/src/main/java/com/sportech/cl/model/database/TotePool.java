package com.sportech.cl.model.database;

import javax.persistence.Column;
import javax.persistence.Entity; 
import javax.persistence.Id;
import javax.persistence.Table; 
import javax.xml.bind.annotation.XmlTransient;

@Entity
@Table(name="soft_pool")
public class TotePool  extends PoolBase
{
    public TotePool()                              {}
    
    @Id
    @Column(name="pool_pk")
    @XmlTransient
    public Long getId()                     { return id; }
    public void setId(Long id)              { this.id = id; }
    
    private static final long serialVersionUID = -1L;
}
