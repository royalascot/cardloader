package com.sportech.cl.model.common;

public class StringQuery extends QueryBase {
    protected boolean _like;
    
    public StringQuery() { super(); }
    
    public StringQuery(boolean like) {
        super();
        _like = like;
    }
    
    public StringQuery(String query, boolean like) {
        super(query);
        _like = like;
    }
    
    public boolean getLike()                { return _like; }
    public void setLike( boolean newValue ) { _like = newValue; }
}

