package com.sportech.cl.model.response;

import com.sportech.cl.model.common.ErrorCode;

public class AddEntityResponse extends GenericResponse {
	public AddEntityResponse() {}
	public AddEntityResponse(GenericResponse rsp)                   { super(rsp); } 
	public AddEntityResponse(ErrorCode	code, String... _errors)    { super( code, _errors ); }
	public AddEntityResponse(int _status, String... _errors)        { super(_status, _errors); }

	public AddEntityResponse( Long _id)
	{
		super( ErrorCode.SUCCESS );
		
		setId( _id );
	}
	
	public Long getId() 		{ return id; }
	public void setId(Long id) 	{ this.id = id; }
	
	protected String innerXmlString() {
	    StringBuilder sb = new StringBuilder();
        
        sb.append( super.innerXmlString() );
        sb.append( "<id>" );
        sb.append( id );
        sb.append( "</id>" );
        
        return sb.toString();
	}
	
	public String toXmlString() {
        StringBuilder sb = new StringBuilder();
        sb.append( "</AddEntityResponse>" );
        sb.append( innerXmlString() );
        sb.append( "</AddEntityResponse>" );
        
        return sb.toString();
    }

	protected Long	id;
	
	private static final long serialVersionUID = -1L;
}
