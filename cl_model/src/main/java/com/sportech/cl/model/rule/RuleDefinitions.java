package com.sportech.cl.model.rule;

import java.util.HashMap;
import java.util.Map;

import com.sportech.cl.model.rule.action.AddMultiLegPoolAction;
import com.sportech.cl.model.rule.action.AddSingleLegPoolAction;
import com.sportech.cl.model.rule.action.ExcludeRaceAction;
import com.sportech.cl.model.rule.action.RemoveSingleLegPool;
import com.sportech.cl.model.rule.field.PlaceRunnerCountField;
import com.sportech.cl.model.rule.field.RaceCountField;
import com.sportech.cl.model.rule.field.RaceNumberField;

@SuppressWarnings("serial")
public class RuleDefinitions {

    public static RuleField descriptionField = new RuleField("Description", "raceNameLong", RuleDataType.String, ContainerType.Race);
    public static RuleField postTimeField = new RuleField("Post Time", "postTime", RuleDataType.Time, ContainerType.Race);
    public static RuleField breedTypeField = new RuleField("Breed Type", "breed", RuleDataType.String, ContainerType.Race);
    
    public static RuleField[] raceFields = new RuleField[] { 
        
        descriptionField, 
        
        postTimeField, 
        
        breedTypeField,
        
        new PlaceRunnerCountField(),
        
        new RaceCountField(),
        
        new RaceNumberField()
        
    };

    private static Map<String, RuleAction> actions = new HashMap<String, RuleAction>() {
        {
            put("Exclude Race", new ExcludeRaceAction());
            put("Add Pool (P1) to Race", new AddSingleLegPoolAction());
            put("Remove Pool (P1) from Race", new RemoveSingleLegPool());
            put("Add Multi-leg Pool (P1) to Race (P2)", new AddMultiLegPoolAction());
        }
    };
         
    private static Map<RuleDataType, RelationOperatorType[]> operatorMap = new HashMap<RuleDataType, RelationOperatorType[]>() {
        {
            put(RuleDataType.Time, new RelationOperatorType[] { RelationOperatorType.GREATER, RelationOperatorType.LESS });
            put(RuleDataType.String, new RelationOperatorType[] { RelationOperatorType.CONTAINS });
            put(RuleDataType.Integer, new RelationOperatorType[] { RelationOperatorType.GREATER, RelationOperatorType.LESS });
        }
    };

    public static RuleField[] getFields() {
        return raceFields;
    }

    public static RelationOperatorType[] getOperators(RuleField field) {
        return operatorMap.get(field.getDataType());
    }

    public static String[] getActionNames() {
        return actions.keySet().toArray(new String[0]);
    }
    
    public static RuleAction getActionByName(String name) {
        return actions.get(name);
    }
    
    public static void registerAction(String name, RuleAction action) {
    	actions.put(name, action);
    }
    
}
