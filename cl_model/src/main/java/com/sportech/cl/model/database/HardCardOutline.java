package com.sportech.cl.model.database;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import com.sportech.cl.model.database.utils.EntityWithId;
import com.sportech.common.model.CardType;

@Entity
@Table(name = "v_hard_card_outline")
public class HardCardOutline implements Serializable, EntityWithId, TrackAccessFilterable {
	public static final String DATE_PROP_NAME = "cardDate";

	private Long id;
	private Long trackId;
	private String trackName;
	private String trackEquibaseId;
	private String trackCountryCode;
	private Long importId;
	private String eventCode;
	private String provider;
	private java.util.Date cardDate;
	private Boolean enabled;
	private Boolean approved;
	private Long raceCount;
	private Long patternCount;
	private CardType cardType;
	private String description;
	
	private Set<SoftCard> softCards = new HashSet<SoftCard>();

	public HardCardOutline() {
	}

	@Id
	@Column(name = "hard_card_pk")
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name = "dict_track_fk")
	public Long getTrackId() {
		return trackId;
	}

	public void setTrackId(Long trackId) {
		this.trackId = trackId;
	}

	@Column(name = "track_name")
	public String getTrackName() {
		return trackName;
	}

	public void setTrackName(String trackName) {
		this.trackName = trackName;
	}

	@Column(name = "track_equibase_id")
	public String getTrackEquibaseId() {
		return trackEquibaseId;
	}

	public void setTrackEquibaseId(String trackEquibaseId) {
		this.trackEquibaseId = trackEquibaseId;
	}

	@Column(name = "track_country_code")
	public String getTrackCountryCode() {
		return trackCountryCode;
	}

	public void setTrackCountryCode(String trackCountryCode) {
		this.trackCountryCode = trackCountryCode;
	}

	@Column(name = "import_fk")
	public Long getImportId() {
		return importId;
	}

	public void setImportId(Long importId) {
		this.importId = importId;
	}

	@Column(name = "event_code")
	public String getEventCode() {
		return eventCode;
	}

	public void setEventCode(String eventCode) {
		this.eventCode = eventCode;
	}

	@Column(name = "provider")
	public String getProvider() {
		return provider;
	}

	public void setProvider(String provider) {
		this.provider = provider;
	}

	@XmlElement
	@XmlSchemaType(name = "date")
	@Column(name = "card_date")
	public java.util.Date getCardDate() {
		return cardDate;
	}

	public void setCardDate(java.util.Date cardDate) {
		this.cardDate = cardDate;
	}

	@Column(name = "enabled")
	public Boolean getEnabled() {
		return enabled;
	}

	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}

	@Column(name = "approved")
	public Boolean getApproved() {
		return approved;
	}

	public void setApproved(Boolean approved) {
		this.approved = approved;
	}

	@Column(name = "race_count")
	public Long getRaceCount() {
		return raceCount;
	}

	public void setRaceCount(Long raceCount) {
		this.raceCount = raceCount;
	}

	@Column(name = "card_type")
	public CardType getCardType() {
		return cardType;
	}

	public void setCardType(CardType cardType) {
		this.cardType = cardType;
	}

	@Column(name = "description")
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@OneToMany(fetch = FetchType.EAGER)
	@JoinColumn(name = "hard_card_fk")
	@Fetch(FetchMode.SELECT)
	public Set<SoftCard> getSoftCards() {
		return softCards;
	}

	public void setSoftCards(Set<SoftCard> softCards) {
		this.softCards = softCards;
	}

	@XmlElement(name = "softCardsCount")
	@Transient
	public int getSoftCardsCount() {
		if (getSoftCards() == null) {
			return 0;
		}
		return getSoftCards().size();
	}

	public void setSoftCardsCount(int value) {
	}

	@Column(name = "pattern_count")
	public Long getPatternCount() {
		return patternCount;
	}

	public void setPatternCount(Long patternCount) {
		this.patternCount = patternCount;
	}

	@Transient
	public String getPatternCountText() {
		if (patternCount == null || patternCount == 0L) {
			return "None";
		}
		return "" + patternCount;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		HardCardOutline other = (HardCardOutline) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	private static final long serialVersionUID = -1L;

}
