package com.sportech.cl.model.database;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.sportech.cl.model.database.utils.EntityWithId;

@Entity
@Table(name="v_tote_outline")
public class ToteOutline extends ToteBase implements Serializable, EntityWithId, TrackAccessFilterable, Comparable<ToteOutline> 
{
    public ToteOutline() {}
    
    private Long trackId;
    
    @Column(name="track_id")
    public Long getTrackId() {
		return trackId;
	}

	public void setTrackId(Long trackId) {
		this.trackId = trackId;
	}

	@Override
	public int compareTo(ToteOutline t) {
		if (t == null || getName() == null || t.getName() == null) {
			return -1;
		}	
		return getName().compareTo(t.getName());
	}

	private static final long serialVersionUID = -1L;
}

