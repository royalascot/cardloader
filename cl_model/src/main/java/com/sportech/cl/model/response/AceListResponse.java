package com.sportech.cl.model.response;

import java.util.Collection;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;

import com.sportech.cl.model.common.ErrorCode;
import com.sportech.cl.model.database.Ace;

public class AceListResponse extends ObjectListResponse<Ace> 
{
    public AceListResponse() {}
    public AceListResponse(GenericResponse rsp)                 { super(rsp); } 
    public AceListResponse(ErrorCode code, String... _errors)   { super( code, _errors ); }

    @XmlElement(name="ace")
    public List<Ace> getEntries()                    { return getObjects(); }
    public void setEntries(Collection<Ace> entries)  { setObjects(entries); }
    
    private static final long serialVersionUID = -1L;
}
