package com.sportech.cl.model.response;

import com.sportech.cl.model.database.Tote;

public class ToteResponse  extends ObjectResponse<Tote>
{
    public ToteResponse() {}
    public ToteResponse(GenericResponse rsp)   { super(rsp); }

    public Tote getTote()                      { return getObject(); }
    public void setTote(Tote tote)             { setObject( tote ); }

    private static final long serialVersionUID = -1L;
}
