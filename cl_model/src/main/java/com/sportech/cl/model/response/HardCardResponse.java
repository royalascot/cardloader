package com.sportech.cl.model.response;

import com.sportech.cl.model.database.HardCard;

public class HardCardResponse  extends ObjectResponse<HardCard>
{
	public HardCardResponse() {}
	public HardCardResponse(GenericResponse rsp)			{ super(rsp); }

	public HardCard getCard() 			{ return getObject(); }
	public void setCard(HardCard card)  { setObject( card ); }

	private static final long serialVersionUID = -1L;
}
