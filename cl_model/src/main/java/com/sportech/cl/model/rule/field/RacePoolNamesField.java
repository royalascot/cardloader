package com.sportech.cl.model.rule.field;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import com.sportech.cl.model.database.HardCard;
import com.sportech.cl.model.database.HardRace;
import com.sportech.cl.model.database.utils.Pool;
import com.sportech.cl.model.database.utils.PoolContainer;
import com.sportech.cl.model.database.utils.PoolHelper;
import com.sportech.cl.model.rule.ContainerType;
import com.sportech.cl.model.rule.RuleContext;
import com.sportech.cl.model.rule.RuleDataType;
import com.sportech.cl.model.rule.RuleField;
import com.sportech.common.model.PoolType;

public class RacePoolNamesField extends RuleField {
    
    static private final Logger log = Logger.getLogger(RacePoolNamesField.class);
    
    public RacePoolNamesField() {
        super("Race Pools", "Race Pools", RuleDataType.String, ContainerType.Race);
    }
    
    @Override
    public Object getValue(RuleContext context) {
        if (context == null) {
            return null;
        }
        HardCard card = context.getCard();
        if (card == null) {
            return null;
        }
        HardRace race = context.getRace();
        if (race == null) {
            return null;
        }
        return getPoolsForRace(card, race.getNumber());
    }
    
    private String getPoolsForRace(PoolContainer container, Long raceNumber) {
        if (container == null || container.getPoolCollection() == null || raceNumber == null) {
            return "";
        }
        long mask = (1 << (raceNumber - 1));
        List<String> poolNames = new ArrayList<String>();
        for (Pool pp : container.getPoolCollection()) {
            PoolType poolType = PoolType.fromId(pp.getPoolTypeId());
            if (poolType.isMultiRace()) {
                if ((pp.getRaceBitmap() & mask) > 0L) {
                    poolNames.add(poolType.getCode());
                    log.info("Added pool " + poolType.getCode() + "for race " + raceNumber);
                }
            } else {
                if (PoolHelper.isRaceInPool(raceNumber, pp.getRaceBitmap())) {
                    poolNames.add(poolType.getCode());
                }
            }
        }
        return StringUtils.join(poolNames, ",");
    }

}
