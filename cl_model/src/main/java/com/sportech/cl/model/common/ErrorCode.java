package com.sportech.cl.model.common;

import java.io.Serializable;

public enum ErrorCode implements Serializable {
	// Generic errors
	 SUCCESS					(0,   "SUCCESS",				"Operation completed successfully")
	,FAIL						(-1,  "FAIL",					"Generic failure.")
	,ERROR					    (-10000, "ERROR", "")
	// Authentication errors
	,E_AUTH_NO_HEADERS			(-10, "E_AUTH_NO_HEADERS",		"Web Service requires authentication but no HTTP headers found in request.")
	,E_AUTH_UNABLE_TO_DECODE	(-11, "E_AUTH_UNABLE_TO_DECODE","It was unable to decode authentication information.")
	,E_AUTH_INV_USER_PWD		(-12, "E_AUTH_INV_USER_PWD",	"Authentication failed. Username or password is invalid")
	,E_ACCESS_DENIED			(-13, "E_ACCESS_DENIED",		"Access denied")
	// Database errors
	,E_NOT_FOUND				(-22, "E_NOT_FOUND", "Object not found in database")
	,E_BAD_DUPLICATE            (-23, "E_BAD_DUPLICATE", "Object already exists in database (and shouldn't)")
    ,E_VAL_FAILED               (-24, "E_VAL_FAILED", "Object validation failed")
    ,E_BAD_PARAMETER            (-25, "E_BAD_PARAMETER", "Incorrect parameter passed to method call")
    ,E_OBJECT_IN_USE            (-26, "E_OBJECT_IN_USE", "The object can not be deleted: It is used by other data.")
	// Import errors
	,E_BAD_CONVERTER            (-40, "E_BAD_CONVERTER", "Unknown converter identifier")
    ,E_BAD_FILE_NAME            (-41, "E_BAD_FILE_NAME", "Bad import file name specified")
    ,E_BAD_FILE_CONTENT         (-42, "E_BAD_FILE_CONTENT", "Bad import file content")
	// Type conversion errors
	,E_BAD_INT_PREDICATE        (-50, "E_BAD_INT_PREDICATE", "Bad predicate. Predicate should be one of [eq, ne, lt, le, ge, gt] ")
	;
	 
	 
   private final Integer 	id;
   private final String 	code;
   private final String 	description;

   ErrorCode(Integer id, String code, String description) {
   	this.id = id;
   	this.code = code;
   	this.description = description;
   }

   public static Boolean	isOK( int iCode )
   {
	   return SUCCESS.getId().equals( iCode );
   }

   public Boolean	isOK()
   {
	   return SUCCESS.equals( this );
   }
   
   public static ErrorCode getErrorCodeById(int passedInt) {
       for(ErrorCode e : ErrorCode.values()) {
           if(e.getId().equals(passedInt)) {
               return e;
           }
       }
       
       return null;
   }

   public static ErrorCode getErrorCodeById(Integer passedInt) {
   	if(passedInt != null) {
   		return getErrorCodeById(passedInt.intValue());
   	}
   	else {
   		return null;
   	}
   }

   public boolean equals(ErrorCode e) {
	   	return id.equals(e.getId());
	   }
   
   public String toString()  {
   	StringBuffer str = new StringBuffer();
   	try {
			str.append("ErrorCode(" + id + ",<" + description + ">)");
		} catch (RuntimeException e) {
			ExceptionStringWriter.exception2str(e, str);
		}
   	return str.toString();
   }
   
   public Integer getId() {
       return id;
   }
   
   public String getDescription() {
       return description;
   }

	public String getCode() {
		return code;
	}
   
   static final long serialVersionUID = -1L;	

}
