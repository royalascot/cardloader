package com.sportech.cl.model.comparator;

import java.util.Comparator;

import com.sportech.cl.model.database.HardRace;

public class HardRaceComparator implements Comparator<HardRace> {

	public int compare(HardRace p1, HardRace p2) {
		Long r1 = p1.getNumber();
		Long r2 = p2.getNumber();
		if (r1 != null) {
			return r1.compareTo(r2);
		}
		return -1;
	}

}
