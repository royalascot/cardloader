package com.sportech.cl.model.response;

import javax.xml.bind.annotation.XmlRootElement;

import com.sportech.cl.model.common.ErrorCode;

@XmlRootElement
public class AddImportResponse extends AddEntityResponse {
    public AddImportResponse() {}
    public AddImportResponse(GenericResponse rsp)                  { super(rsp); } 
    public AddImportResponse(AddEntityResponse rsp)                { super(rsp); } 
    public AddImportResponse(ErrorCode code, String... _errors)    { super(code, _errors); }
    public AddImportResponse(int status, String... _errors)        { super(status, _errors); }
    public AddImportResponse(Long id)                              { super(id); }

    public AddImportResponse(Long id, String importStatus)
    {
        super( ErrorCode.SUCCESS );
        
        setId(id);
        setImportStatus( importStatus );
    }
    
    public AddImportResponse(Long id, ErrorCode code)
    {
        super( code );
        setId(id);
    }
    
    public String getImportStatus()                 { return _importStatus; }
    public void setImportStatus(String newStatus)   { this._importStatus = newStatus; }
    
    protected String innerXmlString() {
        StringBuilder sb = new StringBuilder();
        
        sb.append( super.innerXmlString() );
        sb.append( "<ImportStatus>" );
        sb.append( _importStatus );
        sb.append( "</ImportStatus>" );
        
        return sb.toString();
    }
    
    public String toXmlString() {
        StringBuilder sb = new StringBuilder();
        sb.append( "<ImportResponse>" );
        sb.append( innerXmlString() );
        sb.append( "</ImportResponse>" );
        
        return sb.toString();
    }

    protected String _importStatus;
    
    private static final long serialVersionUID = -1L;
}
