package com.sportech.cl.model.security;

public interface Permission
{
    public static final long   READ_PERM   = 0x0001;
    public static final long   WRITE_PERM  = 0x0002;
    //public static final int   DELETE_PERM = 0x0004;
    
    
    public static final long   CREATE_PERM = 0x0004;
    public static final long   DELETE_PERM = 0x0008;
    public static final long   OWNER_PERM = 0x0010; 
    
    
    public static final long   DEFAULT_PERM = 0x0100; // When an access control entry has this bit set, it is used in conjunction with the other fields to be a "template" for new entries
    

    public static final long   DENY_PERM   = 0x1000;
}
