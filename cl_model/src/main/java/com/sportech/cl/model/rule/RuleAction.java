package com.sportech.cl.model.rule;

public interface RuleAction {

    public String getName();

    public boolean execute(RuleContext context);
    
}
