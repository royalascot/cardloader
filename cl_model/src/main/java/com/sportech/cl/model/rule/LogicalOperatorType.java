package com.sportech.cl.model.rule;

public enum LogicalOperatorType {
    
    OR,
    
    AND
    
}
