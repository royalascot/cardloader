package com.sportech.cl.model.rule.action;

import org.apache.log4j.Logger;

import com.sportech.cl.model.database.HardRace;
import com.sportech.cl.model.database.utils.PoolContainer;
import com.sportech.cl.model.database.utils.PoolHelper;
import com.sportech.cl.model.rule.RuleAction;
import com.sportech.cl.model.rule.RuleContext;

public class ExcludeRaceAction implements RuleAction {

    static private final Logger log = Logger.getLogger(ExcludeRaceAction.class);

    private static final String name = "Exclude Race";

    public String getName() {
        return name;
    }

    public boolean execute(RuleContext context) {
        PoolContainer container = context.getTarget();
        HardRace race = context.getRace();
        if (container == null) {
            return false;
        }
        if (race == null) {
            return false;
        }

        Long raceNumber = race.getNumber();
        PoolHelper helper = new PoolHelper(container);
        helper.disablePoolsforRace(raceNumber);
        log.info("Rule action: excluded " + raceNumber + " from card.");
        return true;
    }

}
