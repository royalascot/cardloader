package com.sportech.cl.model.entity;

import java.math.BigDecimal;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

@XmlAccessorType(XmlAccessType.PROPERTY)
public class Pool {

    private Long id;
    private String name;
    private String code;
    private Integer cardLoaderPoolTypeId;
    private Integer totePoolTypeId;
    private Integer numberPositions;
    private String specialPoolCode;
    private String specialPoolName;
    private BigDecimal guarantee;
    private BigDecimal bonusGuarantee;
    private BigDecimal broughtForward;
    private BigDecimal bonusBroughtForward;

    private String raceNumbers;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Integer getCardLoaderPoolTypeId() {
        return cardLoaderPoolTypeId;
    }

    public void setCardLoaderPoolTypeId(Integer cardLoaderPoolTypeId) {
        this.cardLoaderPoolTypeId = cardLoaderPoolTypeId;
    }

    public Integer getTotePoolTypeId() {
        return totePoolTypeId;
    }

    public void setTotePoolTypeId(Integer totePoolTypeId) {
        this.totePoolTypeId = totePoolTypeId;
    }

    public Integer getNumberPositions() {
        return numberPositions;
    }

    public void setNumberPositions(Integer numberPositions) {
        this.numberPositions = numberPositions;
    }

    public String getSpecialPoolCode() {
        return specialPoolCode;
    }

    public void setSpecialPoolCode(String specialPoolCode) {
        this.specialPoolCode = specialPoolCode;
    }

    public String getSpecialPoolName() {
        return specialPoolName;
    }

    public void setSpecialPoolName(String specialPoolName) {
        this.specialPoolName = specialPoolName;
    }

    public String getRaceNumbers() {
        return raceNumbers;
    }

    public void setRaceNumbers(String raceNumbers) {
        this.raceNumbers = raceNumbers;
    }

	public BigDecimal getGuarantee() {
		return guarantee;
	}

	public void setGuarantee(BigDecimal guarantee) {
		this.guarantee = guarantee;
	}

    public BigDecimal getBonusGuarantee() {
        return bonusGuarantee;
    }

    public void setBonusGuarantee(BigDecimal bonusGuarantee) {
        this.bonusGuarantee = bonusGuarantee;
    }

    public BigDecimal getBroughtForward() {
        return broughtForward;
    }

    public void setBroughtForward(BigDecimal broughtForward) {
        this.broughtForward = broughtForward;
    }

    public BigDecimal getBonusBroughtForward() {
        return bonusBroughtForward;
    }

    public void setBonusBroughtForward(BigDecimal bonusBroughtForward) {
        this.bonusBroughtForward = bonusBroughtForward;
    }
    
}
