package com.sportech.cl.model.database;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.sportech.common.model.PoolType;

import com.sportech.cl.model.database.utils.PoolHelper;

public class PoolTableRow {

    private long raceNumber;

    private long raceCount;

    private String runners;
    
    private String postTime;
    
    private Integer placePositions;

    private Map<Integer, PoolTableCell> poolCells = new HashMap<Integer, PoolTableCell>();

    private List<Integer> poolTypeIds = null;

    public long getRaceNumber() {
        return raceNumber;
    }

    public void setRaceNumber(long raceNumber) {
        this.raceNumber = raceNumber;
    }

    public String getRunners() {
        return runners;
    }

    public void setRunners(String runners) {
        this.runners = runners;
    }

    public List<Integer> getPoolTypeIds() {
        return poolTypeIds;
    }

    public void setPoolTypeIds(List<Integer> poolTypeIds) {
        this.poolTypeIds = poolTypeIds;
    }

    public long getRaceCount() {
        return raceCount;
    }

    public void setRaceCount(long raceCount) {
        this.raceCount = raceCount;
    }

    public void addCell(Integer id, PoolTableCell cell) {
        poolCells.put(id, cell);
    }

    public PoolTableCell getCell(Integer i) {
        if (poolCells == null) {
            return null;
        }
        PoolTableCell cell = poolCells.get(i);
        return cell;
    }

    public String getPostTime() {
        return postTime;
    }

    public void setPostTime(String postTime) {
        this.postTime = postTime;
    }

    public Integer getPlacePositions() {
        return placePositions;
    }

    public void setPlacePositions(Integer placePositions) {
        this.placePositions = placePositions;
    }

    public List<PoolTableCell> getPoolCells() {
        List<PoolTableCell> cells = new ArrayList<PoolTableCell>();
        PoolTableCell cell = new PoolTableCell(raceCount, raceNumber);
        cells.add(cell);
        if (poolTypeIds != null) {
            for (Integer i : poolTypeIds) {
                cell = poolCells.get(i);
                if (cell != null) {
                    cells.add(cell);
                } else {
                    PoolType pt = PoolType.fromId(i);
                    if (pt != null) {
                        boolean found = false;
                        if (pt.isMultiRace()) {
                            PoolTableCell c = new PoolTableCell(raceCount, raceNumber, i, " ", pt.getRaceCount());
                            c.setSelectedLegs(PoolHelper.getDefaultLegs(pt, raceNumber, raceCount));
                            found = true;
                            cells.add(c);
                        } else {
                            cells.add(new PoolTableCell(raceCount, raceNumber, i, false));
                            found = true;
                        }
                        if (!found) {
                            cells.add(new PoolTableCell(raceCount, raceNumber, i, " ", pt.getRaceCount()));
                        }
                    }
                }
            }
        }

        return cells;
    }

}
