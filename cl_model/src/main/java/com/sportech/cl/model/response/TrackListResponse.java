package com.sportech.cl.model.response;

import java.util.Collection;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;

import com.sportech.cl.model.database.Track;

public class TrackListResponse extends ObjectListResponse<Track> 
{
    public TrackListResponse() {}
    public TrackListResponse(GenericResponse rsp)      { super(rsp); } 

    @XmlElement(name="track")
    public List<Track> getTracks()                     { return getObjects(); }
    public void setTracks(Collection<Track> tracks)    { setObjects(tracks); }
    
    private static final long serialVersionUID = -1L;
}
