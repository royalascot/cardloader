package com.sportech.cl.model.messaging;

import java.io.Serializable;

import com.sportech.cl.model.database.HardCard;

public class CommandMessage implements Serializable {
	
	private static final long serialVersionUID = -1L;
	
	public static final String ADDCARD = "AddCard";
	public static final String Equibase = "Equibase";
	public static final String EquibaseSimulcast = "EquibaseSimulcast";
	public static final String TrackInfo = "TrackInfo";
	public static final String TrackMaster = "TrackMaster";
	public static final String SisCard = "SisCard";
	
	private String command;
	private String body;
	private HardCard card;

	public String getCommand() {
		return command;
	}

	public void setCommand(String command) {
		this.command = command;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}

	public HardCard getCard() {
		return card;
	}

	public void setCard(HardCard card) {
		this.card = card;
	}
	
}
