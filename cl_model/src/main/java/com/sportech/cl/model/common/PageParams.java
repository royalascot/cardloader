package com.sportech.cl.model.common;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlElement;

public class PageParams implements Serializable {

    protected Integer _firstResult;
    protected Integer _maxResult;

    public PageParams() {
        _firstResult = 0;
        _maxResult = 100;
    }
    
    public PageParams(Integer firstResult, Integer maxResult) {
        this._firstResult = firstResult;
        this._maxResult = maxResult;
    }

    public PageParams(PageParams src ) 
    {
        if( src != null )
        {
            this._firstResult = src.getFirstResult();
            this._maxResult = src.getMaxResult();
        }
    }
    
    @XmlElement(name="firstResult")
    public Integer getFirstResult()                 { return _firstResult; }
    public void setFirstResult( Integer newValue )  { _firstResult = newValue; }
    
    @XmlElement(name="maxResult")
    public Integer getMaxResult()                   { return _maxResult; }  
    public void setMaxResult( Integer newValue )    { _maxResult = newValue; }
    
    private static final long serialVersionUID = 1L;
}

