package com.sportech.cl.model.comparator;

import java.util.Comparator;

import com.sportech.cl.model.database.utils.Pool;
import com.sportech.common.model.PoolType;

public class PoolComparator implements Comparator<Pool> {

	public int compare(Pool p1, Pool p2) {
		PoolType pt1 = PoolType.fromId(p1.getPoolTypeId());
		PoolType pt2 = PoolType.fromId(p2.getPoolTypeId());
		if (pt1.isMultiRace() && (!pt2.isMultiRace())) {
			return 1;
		}
		if (!pt1.isMultiRace() && (pt2.isMultiRace())) {
			return -1;
		}
		return p1.getPoolTypeId().compareTo(p2.getPoolTypeId());
	}
	
}
