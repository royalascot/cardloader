package com.sportech.cl.model.rule;

import java.util.Collection;

import org.apache.commons.lang3.StringUtils;

public class RuleExecutor {

	public static void executeRules(RuleContext context, Collection<RuleCondition> rules) {
		if (rules != null) {
			for (RuleCondition r : rules) {
				executeRule(context, r);
			}
		}
	}

	public static void executeRule(RuleContext context, RuleCondition rule) {
		if (rule.eval(context)) {
			String actionName = rule.getActionName();
			if (StringUtils.isNotBlank(actionName)) {
				RuleAction action = RuleDefinitions.getActionByName(actionName);
				if (action != null) {
					if (!StringUtils.isEmpty(rule.getParams())) {
						String[] params = StringUtils.split(rule.getParams(), ",");
						if (params != null) {
							for (int i = 1; i <= params.length; i++) {
								context.setValue("param" + i, params[i - 1]);
							}
						}
					}
					action.execute(context);
				}
			}
		}
	}

}
