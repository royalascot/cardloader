package com.sportech.cl.model.database;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlElement;

import org.apache.commons.lang3.StringUtils;

import com.sportech.cl.model.database.utils.EntityWithId;

@Entity
@Table(name = "v_soft_card_outline")
public class SoftCardOutline extends SoftCardBase implements Serializable, EntityWithId, TrackAccessFilterable {
	
	@Column(name = "track_name")
	private String trackName;
	@Column(name = "track_equibase_id")
	private String trackEquibaseId;
	@Column(name = "track_country_code")
	private String trackCountryCode;
	@Column(name = "pattern_description")
	private String patternDescription;
	@Column(name = "race_count")
	private Long raceCount;
	@Column(name = "in_use_count")
	private Long inUseCount;

	@Transient
	private Boolean toteApproved;
	
	public SoftCardOutline() {
	}

	public String getTrackName() {
		return trackName;
	}

	public void setTrackName(String trackName) {
		this.trackName = trackName;
	}

	public String getTrackEquibaseId() {
		return trackEquibaseId;
	}

	public void setTrackEquibaseId(String trackEquibaseId) {
		this.trackEquibaseId = trackEquibaseId;
	}

	public String getTrackCountryCode() {
		return trackCountryCode;
	}

	public void setTrackCountryCode(String trackCountryCode) {
		this.trackCountryCode = trackCountryCode;
	}

	public String getPatternDescription() {
		return patternDescription;
	}

	public void setPatternDescription(String patternDescription) {
		this.patternDescription = patternDescription;
	}

	public Long getRaceCount() {
		return raceCount;
	}

	public void setRaceCount(Long raceCount) {
		this.raceCount = raceCount;
	}

	public Long getInUseCount() {
		return inUseCount;
	}

	public void setInUseCount(Long inUseCount) {
		this.inUseCount = inUseCount;
	}
	
	public Boolean getToteApproved() {
		return toteApproved;
	}

	public void setToteApproved(Boolean toteApproved) {
		this.toteApproved = toteApproved;
	}

	@Transient
	@XmlElement
	public Boolean getMirrorCard() {
	    return StringUtils.isNotEmpty(getSource());
	}
	
	private static final long serialVersionUID = -1L;

}
