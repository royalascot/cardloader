package com.sportech.cl.model.database;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;

import com.sportech.cl.model.database.utils.DateUtils;

public class ToteRace extends SoftRace {

	private TimeZone timeZone;

	private TimeZone trackTimeZone;

	private Date cardDate;
	
	private String mirrorFrom;

	@XmlSchemaType(name = "time")
	public java.util.Date getPostTime() {
		Date postTime = getRace().getPostTime();
		if (cardDate != null && postTime != null) {
			postTime = DateUtils.combine(cardDate, postTime);
		}
		Date utc = DateUtils.getUTCTime(trackTimeZone, postTime);
		return DateUtils.getLocalTime(timeZone, utc);
	}

	@XmlElement
	public String getDistance() {
		return getRace().getDistance();
	}

	@XmlElement
	public String getDistanceUnit() {
		return getRace().getDistanceUnit();
	}

	@XmlElement
	public Boolean getEvening() {
		return getRace().getEvening();
	}

	@XmlElement
	public String getBreed() {
		return getRace().getBreed();
	}

	@XmlElement
	public String getCourse() {
		return getRace().getCourse();
	}

	@XmlElement
	public String getRaceType() {
		return getRace().getRaceType();
	}

	@XmlElement
	public String getRaceNameShort() {
		return getRace().getRaceNameShort();
	}

	@XmlElement
	public String getRaceNameLong() {
		return getRace().getRaceNameLong();
	}

	@XmlElement
	public BigDecimal getPurse() {
		return getRace().getPurse();
	}

	@XmlElement
	public Boolean getEnabled() {
		return getRace().getEnabled();
	}

	@XmlElement(name = "runner")
	public List<Runner> getRunners() {
		return getRace().getRunners();
	}

	@XmlElement(name = "eventId")
	public Long getEventId() {
		return getRace().getEventId();
	}

	@XmlElement(name = "distance_text")
	public String getDistanceText() {
	    return getRace().getDistanceText();
	}
	
	@XmlElement(name = "sis_race_type")
	public String getRaceCode() {
	    return getRace().getRaceCode();
	}
	
	@XmlElement(name = "handicap")
	public Boolean getHandicap() {
	    return getRace().getHandicap();
	}
	
	public TimeZone getTimeZone() {
		return timeZone;
	}

	public void setTimeZone(TimeZone timeZone) {
		this.timeZone = timeZone;
	}

	public TimeZone getTrackTimeZone() {
		return trackTimeZone;
	}

	public void setTrackTimeZone(TimeZone trackTimeZone) {
		this.trackTimeZone = trackTimeZone;
	}

	public Date getCardDate() {
		return cardDate;
	}

	public void setCardDate(Date cardDate) {
		this.cardDate = cardDate;
	}

	public String getMirrorFrom() {
		return mirrorFrom;
	}

	public void setMirrorFrom(String mirrorFrom) {
		this.mirrorFrom = mirrorFrom;
	}

	private static final long serialVersionUID = -1L;

}
