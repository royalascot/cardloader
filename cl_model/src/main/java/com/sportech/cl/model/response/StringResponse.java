package com.sportech.cl.model.response;

import javax.xml.bind.annotation.XmlRootElement;

import com.sportech.cl.model.common.ErrorCode;

@XmlRootElement
public class StringResponse extends GenericResponse {
    public StringResponse() {}
    public StringResponse(GenericResponse rsp)                  { super( rsp ); } 
    public StringResponse(ErrorCode code, String... _errors)    { super( code, _errors ); }
    public StringResponse(int _status, String... _errors)       { super( _status, _errors ); }

    public StringResponse( String data ) {
        super( ErrorCode.SUCCESS );
        setString( data );
    }
    
    public String getString()           { return _data; }
    public void setString(String data)  { this._data = data; }
    
    protected String _data;
    
    private static final long serialVersionUID = -1L;
}
