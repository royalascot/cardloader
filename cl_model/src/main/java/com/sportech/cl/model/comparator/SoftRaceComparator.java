package com.sportech.cl.model.comparator;

import java.util.Comparator;

import com.sportech.cl.model.database.SoftRace;

public class SoftRaceComparator implements Comparator<SoftRace> {

	@Override
	public int compare(SoftRace p1, SoftRace p2) {
		if (p1 == null || p2 == null) {
			return -1;
		}
		Long r1 = p1.getNumber();
		Long r2 = p2.getNumber();
		if (r1 != null) {
			return r1.compareTo(r2);
		}
		return -1;
	}

}
