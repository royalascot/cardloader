package com.sportech.cl.model.common;

import java.io.Serializable;
import java.util.ArrayList;

import javax.xml.bind.annotation.XmlElement;

public class CardLoaderVersion implements Serializable
{
    ArrayList<Version>  components = new ArrayList<Version>();
    
    @XmlElement(name="component")
    public ArrayList<Version> getComponents()                   { return components; }
    public void setComponents(ArrayList<Version> components)    { this.components = components; }

    public  void    addComponent( Version component )           { components.add(component); }
    
    private static final long serialVersionUID = -1L;
}
