package com.sportech.cl.model.database;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;

import com.sportech.cl.model.database.utils.Pool;

@MappedSuperclass
public class PoolBase implements Serializable, Pool {

	protected Long id;
	protected Integer poolTypeId;
	protected Integer totePoolType;
	protected Long raceBitmap;
	protected String poolCode;
	protected String poolName;

	protected String status;

	protected BigDecimal guarantee;
	protected BigDecimal broughtForward;
	protected BigDecimal bonusGuarantee;
	protected BigDecimal bonusBroughtForward;
	protected Boolean itspAllowed;
	protected Boolean isNorminated;

	protected Boolean forceFullCollation;
	protected Boolean fractionalBetting;
	protected Boolean forceWillPay;

	public PoolBase() {
	}

	public PoolBase(PoolBase src) {
		copy(src);
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "pool_pk")
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@XmlTransient
	@Column(name = "dict_pool_fk")
	public Integer getPoolTypeId() {
		return poolTypeId;
	}

	public void setPoolTypeId(Integer poolTypeId) {
		this.poolTypeId = poolTypeId;
	}

	@Column(name = "race_bitmap")
	public Long getRaceBitmap() {
		return raceBitmap;
	}

	public void setRaceBitmap(Long raceBitmap) {
		this.raceBitmap = raceBitmap;
	}

	@XmlElement(name = "poolAbbr")
	@Column(name = "pool_code")
	public String getPoolCode() {
		return poolCode;
	}

	public void setPoolCode(String poolCode) {
		this.poolCode = poolCode;
	}

	@XmlElement(name = "poolName")
	@Column(name = "pool_name")
	public String getPoolName() {
		return poolName;
	}

	public void setPoolName(String poolName) {
		this.poolName = poolName;
	}

	@Column(name = "guarantee")
	public BigDecimal getGuarantee() {
		return guarantee;
	}

	public void setGuarantee(BigDecimal guarantee) {
		this.guarantee = guarantee;
	}

	@Column(name = "status")
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Column(name = "brought_forward")
	public BigDecimal getBroughtForward() {
		return broughtForward;
	}

	public void setBroughtForward(BigDecimal broughtForward) {
		this.broughtForward = broughtForward;
	}

	@Column(name = "bonus_guarantee")
	public BigDecimal getBonusGuarantee() {
		return bonusGuarantee;
	}

	public void setBonusGuarantee(BigDecimal bonusGuarantee) {
		this.bonusGuarantee = bonusGuarantee;
	}

	@Column(name = "bonus_brought_forward")
	public BigDecimal getBonusBroughtForward() {
		return bonusBroughtForward;
	}

	public void setBonusBroughtForward(BigDecimal bonusBroughtForward) {
		this.bonusBroughtForward = bonusBroughtForward;
	}

	@Column(name = "itsp_allowed")
	public Boolean getItspAllowed() {
		return itspAllowed;
	}

	public void setItspAllowed(Boolean itspAllowed) {
		this.itspAllowed = itspAllowed;
	}

	@Column(name = "norminated")
	public Boolean getIsNorminated() {
		return isNorminated;
	}

	public void setIsNorminated(Boolean isNorminated) {
		this.isNorminated = isNorminated;
	}

	@Column(name = "force_full_collation")
	public Boolean getForceFullCollation() {
		return forceFullCollation;
	}

	public void setForceFullCollation(Boolean forceFullCollation) {
		this.forceFullCollation = forceFullCollation;
	}

	@Column(name = "fractional_betting")
	public Boolean getFractionalBetting() {
		return fractionalBetting;
	}

	public void setFractionalBetting(Boolean fractionalBetting) {
		this.fractionalBetting = fractionalBetting;
	}

	@Column(name = "force_willpay")
	public Boolean getForceWillPay() {
		return forceWillPay;
	}

	public void setForceWillPay(Boolean forceWillPay) {
		this.forceWillPay = forceWillPay;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((poolTypeId == null) ? 0 : poolTypeId.hashCode());
		result = prime * result + ((raceBitmap == null) ? 0 : raceBitmap.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PoolBase other = (PoolBase) obj;

		if (poolTypeId == null) {
			if (other.poolTypeId != null)
				return false;
		} else if (!poolTypeId.equals(other.poolTypeId))
			return false;
		if (raceBitmap == null) {
			if (other.raceBitmap != null)
				return false;
		} else if (!raceBitmap.equals(other.raceBitmap))
			return false;

		return true;
	}

	@XmlElement(name = "poolTypeId")
	@Column(name = "tote_pool_type")
	public Integer getTotePoolType() {
		return totePoolType;
	}

	public void setTotePoolType(Integer totePoolType) {
		this.totePoolType = totePoolType;
	}

	protected void copy(PoolBase from) {
		this.setTotePoolType(from.getTotePoolType());
		this.setPoolTypeId(from.getPoolTypeId());
		this.setRaceBitmap(from.getRaceBitmap());
		this.setPoolCode(from.getPoolCode());
		this.setPoolName(from.getPoolName());
		this.setBonusBroughtForward(from.getBonusBroughtForward());
		this.setBonusGuarantee(from.getBonusGuarantee());
		this.setBroughtForward(from.getBroughtForward());
		this.setGuarantee(from.getGuarantee());
		this.setStatus(from.getStatus());
		this.setIsNorminated(from.getIsNorminated());
		this.setItspAllowed(from.getItspAllowed());
		this.setForceFullCollation(from.getForceFullCollation());
		this.setForceWillPay(from.getForceWillPay());
		this.setFractionalBetting(from.getFractionalBetting());
	}

	private static final long serialVersionUID = -1L;

}
