package com.sportech.cl.model.common;

import java.io.PrintWriter;
import java.io.StringWriter;

public class ExceptionStringWriter {
	
	public static StringBuffer exception2str(Exception e, StringBuffer buf) {
		StringWriter swrt = new StringWriter();
	    PrintWriter  pwrt = new PrintWriter(swrt);
	    e.printStackTrace(pwrt);
	    
	    buf.append( "\n\n Exception caught: " + swrt.toString());
		
		return buf;
	}
}
