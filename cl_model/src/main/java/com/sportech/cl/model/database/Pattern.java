package com.sportech.cl.model.database;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlElement;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import com.sportech.cl.model.database.utils.PersistenceHelper;
import com.sportech.cl.model.database.utils.PersistentParent;
import com.sportech.cl.model.database.utils.Pool;
import com.sportech.cl.model.database.utils.PoolContainer;
import com.sportech.cl.model.rule.RaceRuleHelper;
import com.sportech.cl.model.rule.RuleCondition;

@Entity
@Table(name = "pattern")
public class Pattern implements Serializable, PersistentParent<Pattern>, PoolContainer {
	private Long id;
	private String description;
	private Integer raceCount;
	private String loadCode;
	private Integer mergeType;
	private Boolean forceRaceCount;
	private Long trackId;
	private Track track;
	private String toteList;
	private String raceRuleText;

	private Set<PatternPool> pools = new HashSet<PatternPool>();
	private Set<PatternTote> totes = new HashSet<PatternTote>();

	public Pattern() {
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "pattern_pk")
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name = "description")
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Column(name = "race_count")
	public Integer getRaceCount() {
		return raceCount;
	}

	public void setRaceCount(Integer raceCount) {
		this.raceCount = raceCount;
	}

	@Column(name = "load_code")
	public String getLoadCode() {
		return loadCode;
	}

	public void setLoadCode(String loadCode) {
		this.loadCode = loadCode;
	}

	@Column(name = "merge_type")
	public Integer getMergeType() {
		return mergeType;
	}

	public void setMergeType(Integer merge_type) {
		this.mergeType = merge_type;
	}

	@Column(name = "force_race_count")
	public Boolean getForceRaceCount() {
		return forceRaceCount;
	}

	public void setForceRaceCount(Boolean force_race_count) {
		this.forceRaceCount = force_race_count;
	}

	@Column(name = "dict_track_fk")
	public Long getTrackId() {
		return trackId;
	}

	public void setTrackId(Long trackId) {
		this.trackId = trackId;
	}

	@XmlElement(name = "track")
	@OneToOne(cascade = CascadeType.REFRESH, fetch = FetchType.EAGER)
	@JoinColumn(name = "dict_track_fk", insertable = false, updatable = false)
	public Track getTrack() {
		return track;
	}

	public void setTrack(Track track) {
		this.track = track;
	}

	@XmlElement(name = "pool")
	@OneToMany(mappedBy = "parent", cascade = CascadeType.ALL, fetch = FetchType.EAGER, orphanRemoval = true)
	@Fetch(FetchMode.SELECT)
	public Set<PatternPool> getPools() {
		return pools;
	}

	public void setPools(Set<PatternPool> pools) {
		this.pools = pools;
	}

	@XmlElement(name = "allowedTote")
	@OneToMany(mappedBy = "parent", cascade = CascadeType.ALL, fetch = FetchType.EAGER, orphanRemoval = true)
	@Fetch(FetchMode.SELECT)
	@org.hibernate.annotations.Where(clause = "object_type_fk = 3 and actor_type_fk = 1")
	public Set<PatternTote> getTotes() {
		return totes;
	}

	public void setTotes(Set<PatternTote> totes) {
		this.totes = totes;
	}

	@Column(name = "tote_list")
	public String getToteList() {
		return toteList;
	}

	public void setToteList(String toteList) {
		this.toteList = toteList;
	}

	@Column(name = "race_rules")
	public String getRaceRuleText() {
		return raceRuleText;
	}

	public void setRaceRuleText(String raceRuleText) {
		this.raceRuleText = raceRuleText;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Pattern other = (Pattern) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	public void copy4persistence(Pattern from) {
		setDescription(from.getDescription());
		setLoadCode(from.getLoadCode());
		setRaceCount(from.getRaceCount());
		setMergeType(from.getMergeType());
		setForceRaceCount(from.getForceRaceCount());
		setTrackId(from.getTrackId());
		setRaceRuleText(from.getRaceRuleText());

		PersistenceHelper.mergeCollections(getPools(), from.getPools(), this);
		PersistenceHelper.mergeCollections(getTotes(), from.getTotes(), this);
	}

	@Override
	public void setChildParents() {
		PersistenceHelper.setParentCascade(getPools(), this);
		PersistenceHelper.setParentCascade(getTotes(), this);
	}

	private static final long serialVersionUID = -1L;

	@Override
	public Pool addPool(int poolTypeId, long bitmap) {
		PatternPool p = new PatternPool();
		p.setPoolTypeId(poolTypeId);
		p.setRaceBitmap(bitmap);
		p.setParent(this);
		getPools().add(p);
		return p;
	}

	@Override
	public void removePool(Pool p) {
		getPools().remove((PatternPool) p);
	}

	@Override
	@Transient
	public Collection<Pool> getPoolCollection() {
		List<Pool> pools = new ArrayList<Pool>();
		pools.addAll(getPools());
		return pools;
	}

	@Override
	@Transient
	public HardRace getPoolRace(long i) {
		return null;
	}

	@Override
	@Transient
	public boolean isReadonly() {
		return false;
	}

	@Transient
	public List<RuleCondition> getRaceRules() {
		return RaceRuleHelper.fromText(getRaceRuleText());
	}

	@Transient
	public void setRaceRules(List<RuleCondition> rules) {
		String text = RaceRuleHelper.toText(rules);
		setRaceRuleText(text);
	}

	@Override
	@Transient
	public boolean canDisableRace() {
		return false;
	}

	@Override
	@Transient
	public boolean hasRunners() {
		return false;
	}

}
