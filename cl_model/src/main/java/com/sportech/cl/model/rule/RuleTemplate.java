package com.sportech.cl.model.rule;

public class RuleTemplate {

    private String category;
    private String name;
    private RuleCondition rule;
    private RuleAction action;
    private String actionName;

    public RuleTemplate(String category, String name, RuleCondition rule, String actionName, RuleAction action) {
        this.category = category;
        this.name = name;
        this.rule = rule;
        this.action = action;
        this.actionName = actionName;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public RuleCondition getRule() {
        return rule;
    }

    public void setRule(RuleCondition rule) {
        this.rule = rule;
    }

    public RuleAction getAction() {
        return action;
    }

    public void setAction(RuleAction action) {
        this.action = action;
    }

    public String getActionName() {
        return actionName;
    }

    public void setActionName(String actionName) {
        this.actionName = actionName;
    }

}
