package com.sportech.cl.model.security;

public enum Privilege {
	NONE(-1, "No privileges")
   ,SUPERUSER( 0, "All operations allowed")
   ,MANAGE_OPERATORS( 1, "Add/Remove/Modify operators")
   ,MANAGE_TOTES(2, "Add/Remove/Modify Totes")
   ,MANAGE_CUSTOMERS(3, "Add/Remove/Modify Customers")
   ,MANAGE_PATTERNS(4, "Add/Remove/Modify Patterns")
   ,READ_PATTERNS(5, "Read Patterns")
   ,MANAGE_IMPORTS(6, "Import external data and manage import resources")
   ,READ_IMPORTS(7, "Read import results and logs")
   ,MANAGE_CARDS(8, "Add/Remove/Modify hard and soft cards")
   ,READ_CARDS(9, "Read hard and soft cards")
   ,READ_DICTIONARIES(10, "Read dictionary objects")
   ,MANAGE_DICTIONARIES(11, "Manage dictionary objects")
   ;

	private final Integer		id;
	private final String		description;
	
	Privilege( Integer id, String description )
	{
		this.id = id;
		this.description = description; 
	}

	public static Privilege getById(Integer passedInt) 
	{
		if(passedInt != null) {
			for(Privilege e : Privilege.values()) {
				if(e.getId().equals(passedInt)) {
					return e;
				}
			}
		}

		return null;
	}

	public boolean equals(Privilege e) {
		return id.equals(e.getId());
	}

    public boolean equals(Integer privId ) {
        return id.equals(privId);
    }
	
	public Integer getId() {
		return id;
	}

	public String getDescription() {
		return description;
	}
	
}
