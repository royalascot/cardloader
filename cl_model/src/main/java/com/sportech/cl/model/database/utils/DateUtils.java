package com.sportech.cl.model.database.utils;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.TimeZone;

import javax.xml.datatype.XMLGregorianCalendar;

import org.apache.commons.lang3.StringUtils;

public class DateUtils {

    static private final long A_MINUTE = 60000;
    static private final SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
    static private final SimpleDateFormat postTimeFormat = new SimpleDateFormat("HH:mm");

    public static Date combine(Date date, Date time) {
        if (date == null || time == null) {
            return time;
        }
        Calendar cal1 = Calendar.getInstance();
        cal1.setTime(date);
        Calendar cal2 = Calendar.getInstance();
        cal2.setTime(time);
        Calendar result = Calendar.getInstance();
        result.set(Calendar.YEAR, cal1.get(Calendar.YEAR));
        result.set(Calendar.MONTH, cal1.get(Calendar.MONTH));
        result.set(Calendar.DAY_OF_MONTH, cal1.get(Calendar.DAY_OF_MONTH));
        result.set(Calendar.HOUR_OF_DAY, cal2.get(Calendar.HOUR_OF_DAY));
        result.set(Calendar.MINUTE, cal2.get(Calendar.MINUTE));
        result.set(Calendar.SECOND, cal2.get(Calendar.SECOND));
        return result.getTime();
    }

    public static Date addDate(Date startDate, int offset) {
        return addOffset(startDate, Calendar.DATE, offset);
    }

    public static Date addMinute(Date startDate, int offset) {
        return addOffset(startDate, Calendar.MINUTE, offset);
    }

    public static Date fixYear(Date d) {
        return addOffset(d, Calendar.YEAR, 2000);
    }

    public static Date addOffset(Date startDate, int unit, int offset) {
        if (startDate == null) {
            return null;
        }
        Calendar cal = Calendar.getInstance();
        cal.setTime(startDate);
        cal.add(unit, offset);
        Date endDate = cal.getTime();
        return endDate;
    }

    public static boolean isSameDay(Date d1, Date d2) {
        Calendar cal1 = Calendar.getInstance();
        cal1.setTime(d1);
        Calendar cal2 = Calendar.getInstance();
        cal2.setTime(d2);
        if (cal1.get(Calendar.YEAR) != cal2.get(Calendar.YEAR)) {
            return false;
        }
        if (cal1.get(Calendar.MONTH) != cal2.get(Calendar.MONTH)) {
            return false;
        }
        if (cal1.get(Calendar.DAY_OF_MONTH) != cal2.get(Calendar.DAY_OF_MONTH)) {
            return false;
        }
        return true;
    }

    public static boolean isSameTime(Date d1, Date d2) {
        if (d1 == null || d2 == null) {
            return false;
        }
        Calendar cal1 = Calendar.getInstance();
        cal1.setTime(d1);
        Calendar cal2 = Calendar.getInstance();
        cal2.setTime(d2);
        if (cal1.get(Calendar.HOUR_OF_DAY) != cal2.get(Calendar.HOUR_OF_DAY)) {
            return false;
        }
        if (cal1.get(Calendar.MINUTE) != cal2.get(Calendar.MINUTE)) {
            return false;
        }
        if (cal1.get(Calendar.SECOND) != cal2.get(Calendar.SECOND)) {
            return false;
        }
        return true;
    }

    public static Date getLocalTime(TimeZone timeZone, Date utc) {
        if (utc == null) {
            return null;
        }
        int offset = timeZone.getOffset(new Date().getTime());
        return new Date(utc.getTime() + offset);
    }

    public static Date getUTCTime(TimeZone timeZone, Date local) {
        if (local == null) {
            return null;
        }
        int offset = timeZone.getOffset(new Date().getTime());
        return new Date(local.getTime() - offset);
    }

    public static boolean isLaunchTime(String checkTime) {
        if (StringUtils.isEmpty(checkTime)) {
            return false;
        }
        String[] hourMinuteTexts = checkTime.split(",");
        if (hourMinuteTexts != null) {
            for (String hourMinuteText : hourMinuteTexts) {
                hourMinuteText = StringUtils.trim(hourMinuteText);
                String[] hourMinute = hourMinuteText.split(":");
                if (hourMinute == null || hourMinute.length != 2) {
                    return false;
                }
                Calendar c = Calendar.getInstance();
                Date currentTime = c.getTime();
                GregorianCalendar g = new GregorianCalendar();
                g.setTime(currentTime);
                g.set(Calendar.HOUR_OF_DAY, Integer.parseInt(hourMinute[0]));
                g.set(Calendar.MINUTE, Integer.parseInt(hourMinute[1]));
                long difference = Math.abs(g.getTimeInMillis() - currentTime.getTime());
                if (difference < A_MINUTE) {
                    return true;
                }
            }
        }
        return false;
    }

    public static Date getCardDate(XMLGregorianCalendar c) {
        if (c == null) {
            return null;
        }
        GregorianCalendar g = new GregorianCalendar();
        g.set(c.getYear(), c.getMonth() - 1, c.getDay(), 0, 0);
        return g.getTime();
    }

    public static Date getPostTime(XMLGregorianCalendar c) {
        if (c == null) {
            return null;
        }
        GregorianCalendar g = new GregorianCalendar();
        g.set(0, 0, 0, c.getHour(), c.getMinute(), 0);
        return g.getTime();
    }

    public static String toPostTimeStr(Date e) {
        if (e == null) {
            return "";
        }
        return postTimeFormat.format(e);
    }

    public static Date fromPostTimeStr(String d) {
        if (StringUtils.isNotBlank(d)) {
            try {
                return postTimeFormat.parse(d);
            } catch (Exception e) {

            }
        }
        return null;
    }

    public static String getDateStr(Date d) {
        return sdf.format(d);
    }

    public static String buildName(String name, Date postTime) {
        String description = name;
        if (postTime != null) {
            description += " - " + DateUtils.toPostTimeStr(postTime);
        }
        return description;
    }

    public static Date combineDateAndTime(Date startDate, Date time) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(startDate);
        Calendar tcal = Calendar.getInstance();
        Calendar result = Calendar.getInstance();
        tcal.setTime(time);
        result.set(cal.get(Calendar.YEAR),  cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH), tcal.get(Calendar.HOUR_OF_DAY), tcal.get(Calendar.MINUTE), 0);     
        Date endDate = result.getTime();
        return endDate;
    }

}
