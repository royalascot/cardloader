package com.sportech.cl.model.response;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;

import com.sportech.cl.model.database.ImportOutline;

public class ImportListResponse extends ObjectListResponse<ImportOutline> 
{
    public ImportListResponse() {}
    public ImportListResponse(GenericResponse rsp)      { super(rsp); } 

    @XmlElement(name="import")
    public List<ImportOutline> getImports()                    { return getObjects(); }
    
    private static final long serialVersionUID = -1L;
}
