package com.sportech.cl.model.response;

import java.util.Collection;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;

import com.sportech.cl.model.database.PatternOutline;

public class PatternListResponse extends ObjectListResponse<PatternOutline> 
{
    public PatternListResponse() {}
    public PatternListResponse(GenericResponse rsp)                { super(rsp); } 

    @XmlElement(name="pattern")
    public List<PatternOutline> getCards()                     { return getObjects(); }
    public List<PatternOutline> getPatterns()                  { return getObjects(); }
    public void setCards(Collection<PatternOutline> patterns)  { setObjects(patterns); }
    
    private static final long serialVersionUID = -1L;
}
