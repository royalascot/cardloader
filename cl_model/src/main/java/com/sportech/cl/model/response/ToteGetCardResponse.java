package com.sportech.cl.model.response;

import com.sportech.cl.model.entity.Card;

public class ToteGetCardResponse extends GenericResponse {
    
    private static final long serialVersionUID = 2817090429001405528L;
    
    private Card card;

    public Card getCard() {
        return card;
    }

    public void setCard(Card card) {
        this.card = card;
    }    

}
