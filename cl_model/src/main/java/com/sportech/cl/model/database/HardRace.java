package com.sportech.cl.model.database;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.TimeZone;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlTransient;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import com.sportech.cl.model.database.utils.PersistenceHelper;
import com.sportech.cl.model.database.utils.PersistentWithParent;
import com.sportech.common.model.PerformanceType;
import com.sportech.common.model.RunnerContainer;

@Entity
@Table(name = "hard_race")
public class HardRace implements Serializable, PersistentWithParent<HardRace, HardCard>, RunnerContainer {

	private Long id;
	private Long number;
	private java.util.Date postTime;
	private String distance;
	private String distanceUnit;
	private Boolean evening;
	private PerformanceType perfType;
	private String breed;
	private String course;
	private String raceType;
	private String raceNameShort;
	private String raceNameLong;
	private String wagerText;
	private String poolIdList;

	private BigDecimal purse;

	private Boolean enabled;

	private List<Runner> runners = new ArrayList<Runner>();

	private HardCard parent;

	private TimeZone timeZone;
	private TimeZone trackTimeZone;

	private Long eventId;

	private String distanceText;
	private String raceCode;
	private Boolean handicap;

	private String cloneSource;

	private Integer numberPositions;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "hard_race_pk")
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@ManyToOne
	@JoinColumn(name = "hard_card_fk")
	@XmlTransient
	public HardCard getParent() {
		return parent;
	}

	public void setParent(HardCard parent) {
		this.parent = parent;
	}

	@Column(name = "clone_source")
	public String getCloneSource() {
		return cloneSource;
	}

	public void setCloneSource(String cloneSource) {
		this.cloneSource = cloneSource;
	}

	@Column(name = "number")
	public Long getNumber() {
		return number;
	}

	public void setNumber(Long number) {
		this.number = number;
	}

	@XmlSchemaType(name = "time")
	@Column(name = "post_time")
	public java.util.Date getPostTime() {
		return postTime;
	}

	public void setPostTime(java.util.Date postTime) {
		this.postTime = postTime;
	}

	@Column(name = "distance")
	public String getDistance() {
		return distance;
	}

	public void setDistance(String distance) {
		this.distance = distance;
	}

	@Column(name = "distance_unit")
	public String getDistanceUnit() {
		return distanceUnit;
	}

	public void setDistanceUnit(String distanceUnit) {
		this.distanceUnit = distanceUnit;
	}

	@Column(name = "evening")
	public Boolean getEvening() {
		return evening;
	}

	public void setEvening(Boolean evening) {
		this.evening = evening;
	}

	@Column(name = "breed")
	public String getBreed() {
		return breed;
	}

	public void setBreed(String breed) {
		this.breed = breed;
	}

	@Column(name = "course")
	public String getCourse() {
		return course;
	}

	public void setCourse(String course) {
		this.course = course;
	}

	@Column(name = "race_type")
	public String getRaceType() {
		return raceType;
	}

	public void setRaceType(String raceType) {
		this.raceType = raceType;
	}

	@Column(name = "race_name_short")
	public String getRaceNameShort() {
		return raceNameShort;
	}

	public void setRaceNameShort(String raceNameShort) {
		this.raceNameShort = raceNameShort;
	}

	@Column(name = "race_name_long")
	public String getRaceNameLong() {
		return raceNameLong;
	}

	public void setRaceNameLong(String raceNameLong) {
		this.raceNameLong = raceNameLong;
	}

	@Column(name = "purse")
	public BigDecimal getPurse() {
		return purse;
	}

	public void setPurse(BigDecimal purse) {
		this.purse = purse;
	}

	@Column(name = "enabled")
	public Boolean getEnabled() {
		return enabled;
	}

	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}

	@XmlElement(name = "runner")
	@OneToMany(mappedBy = "parent", cascade = CascadeType.ALL, fetch = FetchType.EAGER, orphanRemoval = true)
	@Fetch(FetchMode.SELECT)
	public List<Runner> getRunners() {
		return runners;
	}

	public void setRunners(List<Runner> runners) {
		this.runners = runners;
	}

	@Column(name = "wager_text")
	public String getWagerText() {
		return wagerText;
	}

	public void setWagerText(String wagerText) {
		this.wagerText = wagerText;
	}

	@Column(name = "pool_id_list")
	public String getPoolIdList() {
		return poolIdList;
	}

	public void setPoolIdList(String poolIdList) {
		this.poolIdList = poolIdList;
	}

	@Column(name = "event_id")
	public Long getEventId() {
		return eventId;
	}

	public void setEventId(Long eventId) {
		this.eventId = eventId;
	}

	@Column(name = "perf_type")
	public PerformanceType getPerfType() {
		return perfType;
	}

	public void setPerfType(PerformanceType perfType) {
		this.perfType = perfType;
	}

	@Column(name = "distance_text")
	public String getDistanceText() {
		return distanceText;
	}

	public void setDistanceText(String distanceText) {
		this.distanceText = distanceText;
	}

	@Column(name = "race_code")
	public String getRaceCode() {
		return raceCode;
	}

	public void setRaceCode(String raceCode) {
		this.raceCode = raceCode;
	}

	@Column(name = "handicap")
	public Boolean getHandicap() {
		return handicap;
	}

	public void setHandicap(Boolean handicap) {
		this.handicap = handicap;
	}

	@Column(name = "number_positions")
	public Integer getNumberPositions() {
		return numberPositions;
	}

	public void setNumberPositions(Integer numberPositions) {
		this.numberPositions = numberPositions;
	}

	@Override
	public boolean equals4parent(HardRace obj) {
		return equals(obj);
	}

	@Override
	public void copy4persistence(HardRace from) {
		this.setNumber(from.getNumber());
		this.setPostTime(from.getPostTime());
		this.setDistance(from.getDistance());
		this.setDistanceUnit(from.getDistanceUnit());
		this.setEvening(from.getEvening());
		this.setBreed(from.getBreed());
		this.setCourse(from.getCourse());
		this.setRaceType(from.getRaceType());
		this.setRaceNameShort(from.getRaceNameShort());
		this.setRaceNameLong(from.getRaceNameLong());
		this.setPurse(from.getPurse());
		this.setEnabled(from.getEnabled());
		this.setPoolIdList(from.getPoolIdList());
		this.setHandicap(from.getHandicap());
		this.setDistanceText(from.getDistanceText());
		this.setRaceCode(from.getRaceCode());
		this.setCloneSource(from.getCloneSource());
		this.setNumberPositions(from.getNumberPositions());
		this.setRunners(new ArrayList<Runner>());
		for (Runner r : from.getRunners()) {
			Runner cloned = new Runner();
			cloned.copy4persistence(r);
			cloned.setId(null);
			cloned.setParent(this);
			this.getRunners().add(cloned);
		}
	}

	@Override
	public void setParentCascade(HardCard pt) {
		setParent(pt);
		PersistenceHelper.setParentCascade(getRunners(), this);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((parent == null) ? 0 : parent.hashCode());
		result = prime * result + ((number == null) ? 0 : number.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;

		HardRace other = (HardRace) obj;

		if (number == null) {
			if (other.number != null)
				return false;
		} else if (!number.equals(other.number))
			return false;

		if (parent == null) {
			if (other.parent != null)
				return false;
		} else if (!parent.equals(other.parent))
			return false;
		return true;
	}

	@Transient
	public TimeZone getTimeZone() {
		return timeZone;
	}

	public void setTimeZone(TimeZone timeZone) {
		this.timeZone = timeZone;
	}

	@Transient
	public TimeZone getTrackTimeZone() {
		return trackTimeZone;
	}

	public void setTrackTimeZone(TimeZone trackTimeZone) {
		this.trackTimeZone = trackTimeZone;
	}

	private static final long serialVersionUID = -1L;

}
