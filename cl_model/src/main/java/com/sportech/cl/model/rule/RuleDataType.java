package com.sportech.cl.model.rule;

public enum RuleDataType {
	String,
	Time,
	DateTime,
	Integer
}
