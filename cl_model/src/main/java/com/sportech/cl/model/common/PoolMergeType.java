package com.sportech.cl.model.common;

import java.io.Serializable;

public enum PoolMergeType implements Serializable
{
    HARD_CARD_ONLY          (1, "Use Host Card Pools only")
   ,PATTERN_ONLY            (2, "Use Pattern Pools only")
   ,HARD_CARD_PRIORITY      (3, "Merge pools - Host Card priority")
   ,PATTERN_PRIORITY        (4, "Merge pools - Pattern priority")
   ,ONLY_IN_PATTERN         (5, "Only pools which are in Pattern")
   ,NOT_IN_PATTERN          (6, "Pools which are NOT in pattern")
   ;
    
    private final Integer       id;
    private final String        description;
    
    PoolMergeType( Integer id, String description )
    {
        this.id = id;
        this.description = description; 
    }

    public static PoolMergeType getById(Integer passedInt) 
    {
        if(passedInt != null) {
            for(PoolMergeType e : PoolMergeType.values()) {
                if(e.getId().equals(passedInt)) {
                    return e;
                }
            }
        }

        return null;
    }

    public boolean equals(PoolMergeType e) {
        return id.equals(e.getId());
    }

    public Integer getId() {
        return id;
    }

    public String getDescription() {
        return description;
    }

    private static final long serialVersionUID = -1L;
}
