package com.sportech.cl.model.database;


import javax.persistence.Entity; 
import javax.persistence.Table; 
import javax.persistence.Column; 
import javax.xml.bind.annotation.XmlElement;

import com.sportech.cl.model.database.utils.PersistentParent;


@Entity
@Table(name="ac_entry")
public class Ace extends AceBase implements PersistentParent<Ace>
{
    @Column(name="object_id") private Integer  objectId;
	
	public Ace() {}
		
	@XmlElement
	public Integer getObjectId() 						 { return objectId; }
	public void setObjectId(Integer objectId) 			 { this.objectId = objectId; }
	
	@XmlElement
    public Long getId()                                  { return super.getId(); }
    public void setId(Long Id)                           { super.setId(Id); }

    @XmlElement
    public Integer getActorId()                          { return super.getActorId(); }
    public void setActorId(Integer actorId)              { super.setActorId(actorId); }

    @XmlElement
    public Integer getActorTypeId()                      { return super.getActorTypeId(); }
    public void setActorTypeId(Integer actorTypeId)      { super.setActorTypeId(actorTypeId); }
    
    @XmlElement
    public Integer getPermissions()                      { return super.getPermissions(); }
    public void setPermissions(Integer permissions)      { super.setPermissions(permissions); }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        Ace other = (Ace) obj;
        if (objectId == null)
        {
            if (other.objectId != null)
                return false;
        } else if (!objectId.equals(other.objectId))
            return false;
        
        return super.equals(obj);
    }

    private static final long serialVersionUID = -1L;

    @Override
    public void copy4persistence(Ace from) {
        super.copy( (Ace)from );
        setObjectId( from.getObjectId() );
    }

    @Override
    public void setChildParents() {}
}
