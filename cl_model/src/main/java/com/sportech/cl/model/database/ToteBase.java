package com.sportech.cl.model.database;

import java.io.Serializable;
import java.util.TimeZone;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.Transient;

@MappedSuperclass
public class ToteBase implements Serializable {

	private Long id;

	private String description;

	private String region;

	private Boolean active;

	private String toteCode;

	private Boolean flag;

	private String loginName;

	private TimeZone timeZone;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "tote_pk")
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name = "description")
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Column(name = "region")
	public String getRegion() {
		return region;
	}

	public void setRegion(String region) {
		this.region = region;
	}

	@Column(name = "active")
	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	@Transient
	public String getName() {
		return description;
	}

	@Column(name = "tote_code")
	public String getToteCode() {
		return toteCode;
	}

	public void setToteCode(String toteCode) {
		this.toteCode = toteCode;
	}

	@Column(name = "autoenabled")
	public Boolean getFlag() {
		return flag;
	}

	public void setFlag(Boolean flag) {
		this.flag = flag;
	}

	@Column(name = "login_name")
	public String getLoginName() {
		return loginName;
	}

	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}

	@Column(name = "time_zone")
	public TimeZone getTimeZone() {
		return timeZone;
	}

	public void setTimeZone(TimeZone timeZone) {
		this.timeZone = timeZone;
	}

	private static final long serialVersionUID = -1L;
}
