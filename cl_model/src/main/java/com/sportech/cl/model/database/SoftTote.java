package com.sportech.cl.model.database;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlElement;

import com.sportech.cl.model.database.utils.PersistentWithParent;
import com.sportech.cl.model.security.ObjectType;
import com.sportech.cl.model.security.ActorType;
import com.sportech.cl.model.security.Permission;

@Entity
@Table(name="ac_entry")
@SuppressWarnings("deprecation")
@org.hibernate.annotations.Entity(dynamicUpdate = true)
public class SoftTote extends AceBase implements PersistentWithParent<SoftTote, SoftCard>
{
    private static final Integer  OBJECT_TYPE = ObjectType.SOFT_CARD.getId();
    private static final Integer  ACTOR_TYPE  = ActorType.TOTE.getId();
    /*
     * TODO: finally check field size (int or long)
     */
    private static final int      PERM_MASK   = (int)Permission.READ_PERM; 
    
    
    @ManyToOne
    @JoinColumn(name="object_id",nullable=false)
    protected SoftCard    parent;
    
    public SoftTote()
    {
        setObjectTypeId(OBJECT_TYPE);
        setActorTypeId(ACTOR_TYPE);
        setPermissions(PERM_MASK);
    }
    
    @XmlTransient
    public SoftCard getParent()             { return parent; }
    public void setParent(SoftCard parent)  { this.parent = parent; }
    
    @XmlElement( name="toteId")
    public Integer getToteId()                       { return super.getActorId(); }
    public void setToteId(Integer toteId)            { super.setActorId(toteId); }
    
    //
    //  PersistentWithParent methods
    //

    @Override
    public boolean equals4parent(SoftTote obj)
    {
        return equals( obj );
    }
    
    @Override
    public void copy4persistence(SoftTote from) 
    {
        this.setObjectTypeId(OBJECT_TYPE);
        this.setToteId(from.getToteId());
        this.setActorTypeId(ACTOR_TYPE);
        this.setPermissions(PERM_MASK);
    }

    @Override
    public void setParentCascade(SoftCard pt)
    {
        setParent( pt );
    }
    
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        if (parent != null && parent.getId() != null) {
            result = (int) (prime * result + parent.getId());
        }
        if (getToteId() != null) {
            result = (int) (prime * result + getToteId());
        }
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        SoftTote other = (SoftTote) obj;
        if (this.getToteId() != null && other.getToteId() != null) {
            if (this.getToteId().intValue() != other.getToteId().intValue()) {
                return false;
            }
        }
        if (this.getParent() != null && other.getParent() != null) {
            if (this.getParent().getId().longValue() != other.getParent().getId().longValue()) {
                return false;
            }
        }
        return true;
    }
    
    private static final long serialVersionUID = -1L;
}
