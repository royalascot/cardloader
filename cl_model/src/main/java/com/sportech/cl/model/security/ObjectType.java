package com.sportech.cl.model.security;

public enum ObjectType {
	HARD_CARD	(1, "Hard Card")
   ,HARD_RACE	(2, "Hard Race")
   ,PATTERN		(3, "Pattern")
   ,SOFT_CARD	(4, "Soft Card")
   ,SOFT_RACE	(5, "Soft Race")
   ,DICT_TRACK	(6, "Dict Track")
   ,DICT_POOL	(7, "Dict Pool")
   ,SOFT_POOL	(8, "Soft Pool")
   ,HARD_POOL   (9, "Hard Pool")
   ;
	
	
	private final Integer		id;
	private final String		description;
	
	ObjectType( Integer id, String description )
	{
		this.id = id;
		this.description = description; 
	}

	public static ObjectType getById(Integer passedInt) 
	{
		if(passedInt != null) {
			for(ObjectType e : ObjectType.values()) {
				if(e.getId().equals(passedInt)) {
					return e;
				}
			}
		}

		return null;
	}

	public boolean equals(ObjectType e) {
		return id.equals(e.getId());
	}

	public Integer getId() {
		return id;
	}

	public String getDescription() {
		return description;
	}
	
	public static ObjectType getParentType(ObjectType e) 
    {
	    ObjectType result;
        switch (e) {
            case SOFT_POOL:
                result = ObjectType.DICT_POOL;
                break;
            
            case HARD_RACE:
            case HARD_POOL:
                result = ObjectType.HARD_CARD;
                break;
            
            case SOFT_CARD:
                result = ObjectType.PATTERN;
                break;
                
            case SOFT_RACE:
                result = ObjectType.SOFT_CARD;
                break;
            
            default:
                result = null;
                break;
        }
	    
	    return result;
    }

}
