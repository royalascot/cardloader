package com.sportech.cl.model.comparator;

import java.util.Comparator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.sportech.cl.model.database.Runner;

public class RunnerComparator implements Comparator<Runner> {

	private static Pattern numberPattern = Pattern.compile("\\d+");

	public int compare(Runner p1, Runner p2) {
		String r1 = p1.getProgramNumber();
		String r2 = p2.getProgramNumber();
		String n1 = null;
		String n2 = null;
		Matcher m1 = numberPattern.matcher(r1);
		if (m1.find()) {
			n1 = m1.group();
		}
		Matcher m2 = numberPattern.matcher(r2);
		if (m2.find()) {
			n2 = m2.group();
		}
		if (n1 != null && n2 != null) {
			int x1 = Integer.parseInt(n1);
			int x2 = Integer.parseInt(n2);
			int result = x1 - x2;
			if (result == 0) {
				return r1.compareTo(r2);
			}
			return result;
		}
		if (r1 == null) {
			r1 = "";
		}
		return r1.compareTo(r2);
	}

}
