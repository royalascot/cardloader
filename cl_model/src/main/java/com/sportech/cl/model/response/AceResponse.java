package com.sportech.cl.model.response;

import com.sportech.cl.model.database.Ace;

public class AceResponse extends ObjectResponse<Ace>
{
    public AceResponse() {}
    public AceResponse(GenericResponse rsp)            { super(rsp); }

    public Ace getAce()            { return getObject(); }
    public void setAce(Ace entry)  { setObject( entry ); }

    private static final long serialVersionUID = -1L;
}
