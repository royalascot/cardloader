package com.sportech.cl.model.rule;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.log4j.Logger;
import org.codehaus.jackson.annotate.JsonTypeInfo;
import org.codehaus.jackson.annotate.JsonTypeInfo.As;
import org.codehaus.jackson.annotate.JsonTypeInfo.Id;

@JsonTypeInfo(use=Id.CLASS, include=As.PROPERTY, property="class")
public class RuleField {

    static private final Logger log = Logger.getLogger(RuleField.class);

    private String displayName;
    private String fieldName;
    private RuleDataType dataType;
    private ContainerType container;

    public RuleField() {
    }

    public RuleField(String d, String f, RuleDataType t, ContainerType c) {
        displayName = d;
        fieldName = f;
        dataType = t;
        container = c;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getFieldName() {
        return fieldName;
    }

    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }

    public RuleDataType getDataType() {
        return dataType;
    }

    public void setDataType(RuleDataType dataType) {
        this.dataType = dataType;
    }

    public ContainerType getContainer() {
        return container;
    }

    public void setContainer(ContainerType container) {
        this.container = container;
    }

    public Object getValue(RuleContext context) {
        if (context == null) {
            return false;
        }
        
        Object bean = null;
        
        switch (container) {

        case Race:
            bean = context.getRace();
            break;
        case Card:
            bean = context.getCard();
            break;
        }

        if (bean != null) {
            try {
                Object value = PropertyUtils.getSimpleProperty(bean, fieldName);
                return value;
            } catch (Exception e) {
                log.error("Error getting field value", e);
            }
            return null;
        }
        return null;
    }
    
}
