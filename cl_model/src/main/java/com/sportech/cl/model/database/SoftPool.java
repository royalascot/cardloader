package com.sportech.cl.model.database;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlTransient;

import com.sportech.cl.model.database.utils.PersistentWithParent;

@Entity
@Table(name = "soft_pool")
public class SoftPool extends PoolBase implements PersistentWithParent<SoftPool, SoftCard> {
	private SoftCard parent;

	public SoftPool() {
	}

	public SoftPool(PoolBase src) {
		super(src);
	}

	@ManyToOne
	@JoinColumn(name = "soft_card_fk")
	@XmlTransient
	public SoftCard getParent() {
		return parent;
	}

	public void setParent(SoftCard parent) {
		this.parent = parent;
	}

	@Override
	public boolean equals4parent(SoftPool obj) {
		return equals(obj);
	}

	@Override
	public void copy4persistence(SoftPool from) {
		copy(from);
	}

	@Override
	public void setParentCascade(SoftCard pt) {
		setParent(pt);
	}

	private static final long serialVersionUID = -1L;
}
