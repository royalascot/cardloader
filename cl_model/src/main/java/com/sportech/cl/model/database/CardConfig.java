package com.sportech.cl.model.database;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.sportech.cl.model.database.utils.CdmHelper;
import com.sportech.cl.model.database.utils.JSonHelper;

@SuppressWarnings("serial")
@Entity
@Table(name = "card_template")
public class CardConfig implements Serializable {

	private Long id;

	private String name;

	private String description;

	private String settings;

	private PoolMinimum poolMinimum;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name = "name")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "description")
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Column(name = "settings")
	public String getSettings() {
		return settings;
	}

	public void setSettings(String settings) {
		this.settings = settings;
	}

	@Transient
	public PoolMinimum getPoolMinimum() {
		if (poolMinimum == null) {
			poolMinimum = CdmHelper.sortEntries(JSonHelper.fromJSon(PoolMinimum.class, getSettings()));
		}
		return poolMinimum;
	}

	public void setPoolMinimum(PoolMinimum poolMinimum) {
		this.poolMinimum = poolMinimum;
		refreshSettings();
	}

	public void refreshSettings() {
		String settings = JSonHelper.toJSon(poolMinimum);
		setSettings(settings);
	}

}
