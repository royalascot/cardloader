package com.sportech.cl.model.response;

import java.util.Collection;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;

import com.sportech.cl.model.database.ToteOutline;

public class ToteListResponse extends ObjectListResponse<ToteOutline> 
{
    public ToteListResponse() {}
    public ToteListResponse(GenericResponse rsp)      { super(rsp); } 

    @XmlElement(name="tote")
    public List<ToteOutline> getTotes()                    { return getObjects(); }
    public void setTotes(Collection<ToteOutline> totes)    { setObjects(totes); }
    
    private static final long serialVersionUID = -1L;
}
