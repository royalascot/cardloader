package com.sportech.cl.model.entity;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Set;
import java.util.TimeZone;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlTransient;

import com.sportech.cl.model.database.Runner;

@XmlAccessorType(XmlAccessType.PROPERTY)
public class Race {

    private Long id;
    private String raceName;
    private String description;
    private Long raceNumber;
    private Date postTime;

    private String distance;
    private String distanceUnit;
    private String distanceText;
    private String breed;
    private String course;
    private String surface;
    private String raceCode;

    private Boolean handicap;    
    
    private String raceType;
    private String wagerText;
    private String poolIdList;

    private BigDecimal purse;

    private Boolean enabled;

    private Set<Runner> runners;
    private Set<Pool> pools;
    
    private TimeZone timeZone;
    private TimeZone trackTimeZone;
    
    private Long eventIdentifier;

    private String mirrorFrom;
    
    private Date cardDate;
    
    private Integer numberPositions;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getRaceName() {
        return raceName;
    }

    public void setRaceName(String raceName) {
        this.raceName = raceName;
    }

    public Long getRaceNumber() {
        return raceNumber;
    }

    public void setRaceNumber(Long raceNumber) {
        this.raceNumber = raceNumber;
    }

    @XmlSchemaType(name = "time")
    public Date getPostTime() {
        return postTime;
    }

    public void setPostTime(Date postTime) {
        this.postTime = postTime;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public String getDistanceUnit() {
        return distanceUnit;
    }

    public void setDistanceUnit(String distanceUnit) {
        this.distanceUnit = distanceUnit;
    }

    public String getDistanceText() {
        return distanceText;
    }

    public void setDistanceText(String distanceText) {
        this.distanceText = distanceText;
    }

    public String getBreed() {
        return breed;
    }

    public void setBreed(String breed) {
        this.breed = breed;
    }

    public String getCourse() {
        return course;
    }

    public void setCourse(String course) {
        this.course = course;
    }

    public String getSurface() {
        return surface;
    }

    public void setSurface(String surface) {
        this.surface = surface;
    }

    public String getRaceCode() {
        return raceCode;
    }

    public void setRaceCode(String raceCode) {
        this.raceCode = raceCode;
    }

    public Boolean getHandicap() {
        return handicap;
    }

    public void setHandicap(Boolean handicap) {
        this.handicap = handicap;
    }

    public String getRaceType() {
        return raceType;
    }

    public void setRaceType(String raceType) {
        this.raceType = raceType;
    }

    @XmlTransient
    public String getWagerText() {
        return wagerText;
    }

    public void setWagerText(String wagerText) {
        this.wagerText = wagerText;
    }

    @XmlTransient
    public String getPoolIdList() {
        return poolIdList;
    }

    public void setPoolIdList(String poolIdList) {
        this.poolIdList = poolIdList;
    }

    public BigDecimal getPurse() {
        return purse;
    }

    public void setPurse(BigDecimal purse) {
        this.purse = purse;
    }

    public Boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    @XmlElement(name = "runner")
    public Set<Runner> getRunners() {
        return runners;
    }

    public void setRunners(Set<Runner> runners) {
        this.runners = runners;
    }

    @XmlTransient
    public TimeZone getTimeZone() {
        return timeZone;
    }

    public void setTimeZone(TimeZone timeZone) {
        this.timeZone = timeZone;
    }

    @XmlTransient
    public TimeZone getTrackTimeZone() {
        return trackTimeZone;
    }

    public void setTrackTimeZone(TimeZone trackTimeZone) {
        this.trackTimeZone = trackTimeZone;
    }

    @XmlElement(name = "pool")
    public Set<Pool> getPools() {
        return pools;
    }

    public void setPools(Set<Pool> pools) {
        this.pools = pools;
    }

    public Long getEventIdentifier() {
        return eventIdentifier;
    }

    public void setEventIdentifier(Long eventIdentifier) {
        this.eventIdentifier = eventIdentifier;
    }

    public String getMirrorFrom() {
        return mirrorFrom;
    }

    public void setMirrorFrom(String mirrorFrom) {
        this.mirrorFrom = mirrorFrom;
    }

    public Date getCardDate() {
        return cardDate;
    }

    public void setCardDate(Date cardDate) {
        this.cardDate = cardDate;
    }

    public Integer getNumberPositions() {
        return numberPositions;
    }

    public void setNumberPositions(Integer numberPositions) {
        this.numberPositions = numberPositions;
    }
     
}