package com.sportech.cl.model.database;

import java.io.Serializable;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonProperty;

public class PassThroughBag implements Serializable {

	private static final long serialVersionUID = 1L;

	@JsonCreator
	public PassThroughBag() {
	}

	@JsonProperty("trackId")
	private String trackId;

	@JsonProperty("hostMeetNumber")
	private String hostMeetNumber;

	@JsonProperty("meetingCodeUK")
	private String meetingCodeUK;
	
	@JsonProperty("meetingName")
	private String meetingName;
	
	public String getTrackId() {
		return trackId;
	}

	public void setTrackId(String trackId) {
		this.trackId = trackId;
	}

	public String getHostMeetNumber() {
		return hostMeetNumber;
	}

	public void setHostMeetNumber(String hostMeetNumber) {
		this.hostMeetNumber = hostMeetNumber;
	}

	public String getMeetingCodeUK() {
		return meetingCodeUK;
	}

	public void setMeetingCodeUK(String meetingCodeUK) {
		this.meetingCodeUK = meetingCodeUK;
	}

    public String getMeetingName() {
        return meetingName;
    }

    public void setMeetingName(String meetingName) {
        this.meetingName = meetingName;
    }

}
