package com.sportech.cl.model.database;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlElement;

import com.sportech.cl.model.database.utils.PersistentWithParent;
import com.sportech.cl.model.security.ObjectType;
import com.sportech.cl.model.security.ActorType;
import com.sportech.cl.model.security.Permission;

@Entity
@Table(name="ac_entry")
@SuppressWarnings("deprecation")
@org.hibernate.annotations.Entity(dynamicUpdate = true)
public class PatternTote extends AceBase implements PersistentWithParent<PatternTote, Pattern>
{
    private static final Integer  OBJECT_TYPE = ObjectType.PATTERN.getId();
    private static final Integer  ACTOR_TYPE  = ActorType.TOTE.getId();
    /*
     * TODO: finally check field size (int or long)
     */
    private static final int      PERM_MASK   = (int)Permission.READ_PERM; 
    
    
    @ManyToOne
    @JoinColumn(name="object_id",nullable=false)
    protected Pattern    parent;
    
    public PatternTote()
    {
        setObjectTypeId(OBJECT_TYPE);
        setActorTypeId(ACTOR_TYPE);
        setPermissions(PERM_MASK);
    }
    
    @XmlTransient
    public Pattern getParent()             { return parent; }
    public void setParent(Pattern parent)  { this.parent = parent; }
    
    @XmlElement( name="toteId")
    public Integer getToteId()                       { return super.getActorId(); }
    public void setToteId(Integer toteId)            { super.setActorId(toteId); }
    
    //
    //  PersistentWithParent methods
    //

    @Override
    public boolean equals4parent(PatternTote obj)
    {
        return equals( obj );
    }
    
    @Override
    public void copy4persistence(PatternTote from) 
    {
        this.setObjectTypeId(OBJECT_TYPE);
        this.setToteId(from.getToteId());
        this.setActorTypeId(ACTOR_TYPE);
        this.setPermissions(PERM_MASK);
    }

    @Override
    public void setParentCascade(Pattern pt)
    {
        setParent( pt );
    }
    
    private static final long serialVersionUID = -1L;
}
