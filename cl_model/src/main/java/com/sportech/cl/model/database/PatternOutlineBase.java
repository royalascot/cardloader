package com.sportech.cl.model.database;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

import com.sportech.cl.model.database.utils.EntityWithId;

@MappedSuperclass
public class PatternOutlineBase implements Serializable, EntityWithId, TrackAccessFilterable {
    @Id
    @Column(name = "pattern_pk")
    private Long id;
    @Column(name = "description")
    private String description;
    @Column(name = "race_count")
    private Integer raceCount;
    @Column(name = "load_code")
    private String loadCode;
    @Column(name = "tote_list")
    private String toteList;
    @Column(name = "merge_type")
    private Integer mergeType;
    @Column(name = "force_race_count")
    private Boolean forceRaceCount;
    @Column(name = "dict_track_fk")
    private Long trackId;
    @Column(name = "track_name")
    private String trackName;
    @Column(name = "track_equibase_id")
    private String trackEquibaseId;
    @Column(name = "track_country_code")
    private String trackCountryCode;

    public PatternOutlineBase() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getRaceCount() {
        return raceCount;
    }

    public void setRaceCount(Integer raceCount) {
        this.raceCount = raceCount;
    }

    public String getLoadCode() {
        return loadCode;
    }

    public void setLoadCode(String loadCode) {
        this.loadCode = loadCode;
    }

    public Integer getMergeType() {
        return mergeType;
    }

    public void setMergeType(Integer merge_type) {
        this.mergeType = merge_type;
    }

    public Boolean getForceRaceCount() {
        return forceRaceCount;
    }

    public void setForceRaceCount(Boolean force_race_count) {
        this.forceRaceCount = force_race_count;
    }

    public Long getTrackId() {
        return trackId;
    }

    public void setTrackId(Long trackId) {
        this.trackId = trackId;
    }

    public String getTrackName() {
        return trackName;
    }

    public void setTrackName(String trackName) {
        this.trackName = trackName;
    }

    public String getTrackEquibaseId() {
        return trackEquibaseId;
    }

    public void setTrackEquibaseId(String trackEquibaseId) {
        this.trackEquibaseId = trackEquibaseId;
    }

    public String getTrackCountryCode() {
        return trackCountryCode;
    }

    public void setTrackCountryCode(String trackCountryCode) {
        this.trackCountryCode = trackCountryCode;
    }

    public String getToteList() {
        return toteList;
    }

    public void setToteList(String toteList) {
        this.toteList = toteList;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        PatternOutlineBase other = (PatternOutlineBase) obj;
        if (id == null) {
            if (other.id != null)
                return false;
        } else if (!id.equals(other.id))
            return false;
        return true;
    }

    private static final long serialVersionUID = -1L;
}
