package com.sportech.cl.model.response;

import com.sportech.cl.model.database.ToteCard;

public class ToteCardResponse  extends ObjectResponse<ToteCard>
{
    public ToteCardResponse() {}
    public ToteCardResponse(GenericResponse rsp)            { super(rsp); }

    public ToteCard getCard()           { return getObject(); }
    public void setCard(ToteCard card)  { setObject( card ); }

    private static final long serialVersionUID = -1L;
}
