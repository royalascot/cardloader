package com.sportech.cl.model.entity;

import java.util.Set;

import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

import org.apache.commons.lang3.StringUtils;

import com.sportech.cl.model.database.PoolMinimum;
import com.sportech.common.model.CardType;
import com.sportech.common.model.PerformanceType;

@XmlAccessorType(XmlAccessType.PROPERTY)
public class Card {
    
    private Long id;

    private String name;
    
    private String description;

    private String loadCode;
    
    private String trackCode;
    
    private String trackName;
    
    private String countryCode;
    
    private String cardDate;
      
    private CardType cardType;
    
    private PerformanceType perfType;
    
    private Integer featureRace;
    
    private String itspEventCode;
    
    private String provider;
    
    private PoolMinimum poolMinimum;
        
    private String trackIdentifier;
    
    private String hostMeetNumber;
    
    private String trackCondition;
    
    private String meetingCodeUK;
    
    private String source;
    
    private Set<Race> races;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLoadCode() {
        return loadCode;
    }

    public void setLoadCode(String loadCode) {
        this.loadCode = loadCode;
    }

    public CardType getCardType() {
        return cardType;
    }

    public void setCardType(CardType cardType) {
        this.cardType = cardType;
    }

    public PerformanceType getPerfType() {
        return perfType;
    }

    public void setPerfType(PerformanceType perfType) {
        this.perfType = perfType;
    }

    public Integer getFeatureRace() {
        return featureRace;
    }

    public void setFeatureRace(Integer featureRace) {
        this.featureRace = featureRace;
    }

    public String getProvider() {
        return provider;
    }

    public void setProvider(String provider) {
        this.provider = provider;
    }

    public PoolMinimum getPoolMinimum() {
        return poolMinimum;
    }

    public void setPoolMinimum(PoolMinimum poolMinimum) {
        this.poolMinimum = poolMinimum;
    }

    public String getTrackIdentifier() {
        return trackIdentifier;
    }

    public void setTrackIdentifier(String trackIdentifier) {
        this.trackIdentifier = trackIdentifier;
    }

    public String getHostMeetNumber() {
        return hostMeetNumber;
    }

    public void setHostMeetNumber(String hostMeetNumber) {
        this.hostMeetNumber = hostMeetNumber;
    }

    @XmlElement(name = "race")
    public Set<Race> getRaces() {
        return races;
    }

    public void setRaces(Set<Race> races) {
        this.races = races;
    }

    public String getTrackCode() {
        return trackCode;
    }

    public void setTrackCode(String trackCode) {
        this.trackCode = trackCode;
    }

    public String getTrackName() {
        return trackName;
    }

    public void setTrackName(String trackName) {
        this.trackName = trackName;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getItspEventCode() {
        return itspEventCode;
    }

    public void setItspEventCode(String itspEventCode) {
        this.itspEventCode = itspEventCode;
    }

    public String getCardDate() {
        return cardDate;
    }

    public void setCardDate(String cardDate) {
        this.cardDate = cardDate;
    }

    public String getMeetingCodeUK() {
        return meetingCodeUK;
    }

    public void setMeetingCodeUK(String meetingCodeUK) {
        this.meetingCodeUK = meetingCodeUK;
    }

    public String getTrackCondition() {
        return trackCondition;
    }

    public void setTrackCondition(String trackCondition) {
        this.trackCondition = trackCondition;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    @Transient
    @XmlElement
    public Boolean getMirrorCard() {
        return StringUtils.isNotEmpty(getSource());
    }

}
