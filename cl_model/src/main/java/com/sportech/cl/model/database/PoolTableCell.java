package com.sportech.cl.model.database;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

public class PoolTableCell {

	public static final String RACE = "race";
	public static final String SINGLE = "single";
	public static final String MULTIPLE = "multiple";
	private static final String NAME_READONLY = "readonly";
	private static final String NAME_EDITABLE = "editable";

	private String cellType;
	private Long raceNumber;
	private Boolean checked;
	private String raceList;
	private Integer poolTypeId;

	private String poolCode;
	private String poolName;
	private BigDecimal poolGuarantee;
	private Long raceCount;
	private Integer legCount;

	private List<Long> selectedLegs;

	public PoolTableCell() {

	}

	public PoolTableCell(Long rc, Long r) {
		cellType = RACE;
		raceNumber = r;
		raceCount = rc;
		legCount = 0;
	}

	public PoolTableCell(Long rc, Long r, Integer id, Boolean c) {
		poolTypeId = id;
		cellType = SINGLE;
		checked = c;
		raceNumber = r;
		raceCount = rc;
		legCount = 1;
	}

	public PoolTableCell(Long rc, Long r, Integer id, String l, Integer lc) {
		poolTypeId = id;
		cellType = MULTIPLE;
		raceList = l;
		raceNumber = r;
		raceCount = rc;
		legCount = lc;
	}

	public Integer getLegCount() {
		return legCount;
	}

	public void setLegCount(Integer legCount) {
		this.legCount = legCount;
	}

	public String getCellType() {
		return cellType;
	}

	public void setCellType(String cellType) {
		this.cellType = cellType;
	}

	public Long getRaceNumber() {
		return raceNumber;
	}

	public BigDecimal getPoolGuarantee() {
		return poolGuarantee;
	}

	public void setPoolGuarantee(BigDecimal poolGuaranteee) {
		this.poolGuarantee = poolGuaranteee;
	}

	public void setRaceNumber(Long raceNumber) {
		this.raceNumber = raceNumber;
	}

	public Boolean getChecked() {
		return checked;
	}

	public void setChecked(Boolean checked) {
		this.checked = checked;
	}

	public String getRaceList() {
		return raceList;
	}

	public void setRaceList(String raceList) {
		this.raceList = raceList;
	}

	public Integer getPoolTypeId() {
		return poolTypeId;
	}

	public void setPoolTypeId(Integer poolTypeId) {
		this.poolTypeId = poolTypeId;
	}

	public String getPoolCode() {
		return poolCode;
	}

	public void setPoolCode(String poolCode) {
		this.poolCode = poolCode;
	}

	public String getPoolName() {
		return poolName;
	}

	public void setPoolName(String poolName) {
		this.poolName = poolName;
	}

	public String getPoolNameTemplateType() {
		if (RACE.equals(cellType)) {
			return RACE;
		}
		if (!StringUtils.isBlank(raceList)) {
			return NAME_EDITABLE;
		} else if (checked != null && checked == true) {
			return NAME_EDITABLE;
		}
		return NAME_READONLY;
	}

	public Long getRaceCount() {
		return raceCount;
	}

	public void setRaceCount(Long raceCount) {
		this.raceCount = raceCount;
	}

	public List<Long> getAvailableLegs() {
		List<Long> legs = new ArrayList<Long>();
		if (raceCount != null && raceNumber != null) {
			if (getLegCount() + raceNumber - 1 <= raceCount) {
				for (long l = raceNumber + 1; l <= raceCount; l++) {
					legs.add(l);
				}
			}
		}
		return legs;
	}

	public List<Long> getSelectedLegs() {
		return selectedLegs;
	}

	public void setSelectedLegs(List<Long> selectedLegs) {
		this.selectedLegs = selectedLegs;
	}

}
