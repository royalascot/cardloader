package com.sportech.cl.model.rule.field;

import com.sportech.cl.model.database.HardCard;
import com.sportech.cl.model.rule.ContainerType;
import com.sportech.cl.model.rule.RuleContext;
import com.sportech.cl.model.rule.RuleDataType;
import com.sportech.cl.model.rule.RuleField;

public class RaceCountField extends RuleField {
    
    public RaceCountField() {
        super("Race Count", "Race Count", RuleDataType.Integer, ContainerType.Card);
    }
    
    @Override
    public Object getValue(RuleContext context) {
        if (context == null) {
            return null;
        }
        HardCard card = context.getCard();
        if (card == null) {
            return null;
        }
        if (card.getRaces() == null) {
            return 0;
        }
        return card.getRaces().size();
    }
    
}
