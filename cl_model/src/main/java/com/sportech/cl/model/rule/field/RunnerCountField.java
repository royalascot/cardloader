package com.sportech.cl.model.rule.field;

import com.sportech.cl.model.database.HardRace;
import com.sportech.cl.model.rule.ContainerType;
import com.sportech.cl.model.rule.RuleContext;
import com.sportech.cl.model.rule.RuleDataType;
import com.sportech.cl.model.rule.RuleField;

public class RunnerCountField extends RuleField {
    
    public RunnerCountField() {
        super("Runner Count", "Runner Count", RuleDataType.Integer, ContainerType.Race);
    }
    
    @Override
    public Object getValue(RuleContext context) {
        if (context == null) {
            return null;
        }
        HardRace race = context.getRace();
        if (race == null) {
            return null;
        }
        if (race.getRunners() == null) {
            return 0;
        }
        return race.getRunners().size();
    }
    
}
