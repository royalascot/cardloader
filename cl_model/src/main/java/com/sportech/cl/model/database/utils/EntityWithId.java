package com.sportech.cl.model.database.utils;

public interface EntityWithId 
{
	public Long getId();
}
