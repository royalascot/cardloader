package com.sportech.cl.model.response;

import com.sportech.cl.model.security.SecurityToken;
import com.sportech.cl.model.common.ErrorCode;

public class LoginResponce extends GenericResponse 
{
    public LoginResponce(GenericResponse rsp)
    {
        super( rsp  );
    }

	public LoginResponce(ErrorCode	code, String... _errors)
	{
		super( code, _errors );
	}
	
	public LoginResponce( SecurityToken _token )
	{
		super( ErrorCode.SUCCESS);
		
		setToken( _token );
	}

	public String toString() {
		String str = super.toString();
		
		if( isOK() ) {
			if( token != null )
				str += " Token:" + token.toString();
			else
				str += " Token:null";
		}
		return str;
	}
	
	public SecurityToken getToken() {
		return token;
	}
	public void setToken(SecurityToken token) {
		this.token = token;
	}

	private SecurityToken	token = null;
	
	private static final long serialVersionUID = -1L;
}
