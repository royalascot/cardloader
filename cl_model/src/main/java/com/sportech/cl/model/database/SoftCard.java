package com.sportech.cl.model.database;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlElement;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import com.sportech.cl.model.database.utils.PersistenceHelper;
import com.sportech.cl.model.database.utils.PersistentParent;
import com.sportech.cl.model.database.utils.Pool;
import com.sportech.cl.model.database.utils.PoolContainer;

@Entity
@Table(name = "soft_card")
public class SoftCard extends SoftCardBase implements Serializable, PersistentParent<SoftCard>, PoolContainer {
    
	@OneToMany(mappedBy = "parent", cascade = CascadeType.ALL, fetch = FetchType.EAGER, orphanRemoval = true)
    @OrderBy("poolTypeId")
    @Fetch(FetchMode.SELECT)
    private Set<SoftPool> pools = new HashSet<SoftPool>();

    @OneToMany(mappedBy = "parent", cascade = CascadeType.ALL, fetch = FetchType.EAGER, orphanRemoval = true)
    @OrderBy("number")
    @Fetch(FetchMode.SELECT)
    private Set<SoftRace> races = new HashSet<SoftRace>();

    @OneToMany(mappedBy = "parent", cascade = CascadeType.ALL, fetch = FetchType.EAGER, orphanRemoval = true)
    @Fetch(FetchMode.SELECT)
    @org.hibernate.annotations.Where(clause = "object_type_fk = 4 and actor_type_fk = 1")
    private Set<SoftTote> totes = new HashSet<SoftTote>();

    @OneToMany(cascade = CascadeType.REFRESH, fetch = FetchType.EAGER)
    @JoinColumn(name = "soft_card_fk", insertable = false, updatable = false)
    protected Set<SoftCardInUse> usedByTotes = new HashSet<SoftCardInUse>();

    @OneToOne(cascade = CascadeType.REFRESH, fetch = FetchType.EAGER)
    @JoinColumn(name = "pattern_fk", insertable = false, updatable = false)
    private Pattern4SoftCard pattern;

    public SoftCard() {
    }

    @XmlElement(name = "pool")
    public Set<SoftPool> getPools() {
        return pools;
    }

    public void setPools(Set<SoftPool> pools) {
        this.pools = pools;
    }

    @XmlElement(name = "race")
    public Set<SoftRace> getRaces() {
        return races;
    }

    public void setRaces(Set<SoftRace> races) {
        this.races = races;
    }

    @XmlElement(name = "allowedTote")
    public Set<SoftTote> getTotes() {
        return totes;
    }

    public void setTotes(Set<SoftTote> totes) {
        this.totes = totes;
    }

    @XmlElement(name = "usedByTote")
    public Set<SoftCardInUse> getUsedByTotes() {
        return usedByTotes;
    }

    public void setUsedByTotes(Set<SoftCardInUse> usedByTotes) {
        this.usedByTotes = usedByTotes;
    }

    @XmlElement(name = "patternDescription")
    public String getPatternDescription() {
        return pattern != null ? pattern.getDescription() : null;
    }

    public void setPatternDescription(String description) {
    }

    @Override
    public void copy4persistence(SoftCard from) {
        setDescription(from.getDescription());
        setSparseRaces(from.getSparseRaces());
        setSparsePools(from.getSparsePools());
        setLoadCode(from.getLoadCode());
        setPatternId(from.getPatternId());
        setHardCardId(from.getHardCardId());
        setCardDate(from.getCardDate());
        setToteName(from.getToteName());
        setToteApprovalList(from.getToteApprovalList());
        setDownloadedTotes(from.getDownloadedTotes());
        setCardType(from.getCardType());
        setPerfType(from.getPerfType());
        setFeatureRace(from.getFeatureRace());
        setSource(from.getSource());
        setConfig(from.getConfig());
        PersistenceHelper.mergeCollections(getPools(), from.getPools(), this);
        PersistenceHelper.mergeCollections(getRaces(), from.getRaces(), this);
        PersistenceHelper.mergeCollections(getTotes(), from.getTotes(), this);
    }

    @Override
    public void setChildParents() {
        PersistenceHelper.setParentCascade(getPools(), this);
        PersistenceHelper.setParentCascade(getRaces(), this);
        PersistenceHelper.setParentCascade(getTotes(), this);
    }

    @SuppressWarnings("unused")
    @Entity
    @Table(name = "pattern")
    private static class Pattern4SoftCard implements Serializable {
        @Id
        @Column(name = "pattern_pk")
        private Long id;
        @Column(name = "description")
        private String description;

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        private static final long serialVersionUID = -1L;
    }

    private static final long serialVersionUID = -1L;

    @Override
    public Pool addPool(int poolTypeId, long bitmap) {
        SoftPool p = new SoftPool();
        p.setPoolTypeId(poolTypeId);
        p.setRaceBitmap(bitmap);
        p.setParent(this);
        getPools().add(p);
        return p;
    }

    @Override
    public void removePool(Pool p) {
        getPools().remove((SoftPool) p);
    }

    @Override
    @Transient
    public Collection<Pool> getPoolCollection() {
        List<Pool> pools = new ArrayList<Pool>();
        pools.addAll(getPools());
        return pools;
    }

    @Override
    @Transient
    public Integer getRaceCount() {
        return getRaces().size();
    }

    @Override
    @Transient
    public void setRaceCount(Integer count) {

    }

    @Override
    @Transient
    public HardRace getPoolRace(long raceNumber) {
        for (SoftRace r : getRaces()) {
        	if (r.getNumber() == raceNumber) {
        		return r.getRace();
        	}
        }
        return null;
    }

    @Override
    @Transient
    public boolean hasRunners() {
        return true;
    }

    @Override
    @Transient
    public boolean isReadonly() {
        return false;
    }

    @Override
    @Transient
    public boolean canDisableRace() {
        return true;
    }

}
