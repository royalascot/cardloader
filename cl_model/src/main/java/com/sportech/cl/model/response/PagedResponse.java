package com.sportech.cl.model.response;

import javax.xml.bind.annotation.XmlElement;

import com.sportech.cl.model.common.PageParams;

public class PagedResponse<R> extends PageParams {
    
    protected R _response = null;

    public PagedResponse() {}
    
    public PagedResponse(R response) {
        _response = response;
    }
    
    public PagedResponse(R response, PageParams paging) {
        super(paging);
        _response = response;
    }
    
    public PagedResponse(R response, Integer firstResult, Integer maxResult) {
        super(firstResult, maxResult);
        _response = response;
    }
    
    @XmlElement(name="response")
    public R getResponse() { return _response; }
    public void setResponse( R newValue ) { _response = newValue; }
    
    private static final long serialVersionUID = 1L;

}
