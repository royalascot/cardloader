package com.sportech.cl.model.database.utils;

import java.math.BigDecimal;

public interface Pool
{
    public Integer getPoolTypeId();
    public Integer getTotePoolType();
    public Long getRaceBitmap();
    public void setRaceBitmap(Long raceBitmap);
    public void setPoolTypeId(Integer poolTypeId);
    public void setTotePoolType(Integer poolType);
    public String getPoolCode();
    public String getPoolName();
    public BigDecimal getGuarantee();
    public void setPoolCode(String poolCode);
    public void setPoolName(String poolName);
    public void setGuarantee(BigDecimal guarantee);
    
}
