package com.sportech.cl.model.database;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.xml.bind.annotation.XmlElement;

import com.sportech.cl.model.database.utils.EntityWithId;

public class ToteCard extends SoftCardBase implements Serializable, EntityWithId {

	private String trackIdentifier;
	
	private String hostMeetNumber;
	
	private String meetingCodeUK;
	
	private String eventCode;

	private PoolMinimum poolMinimum;
	
	protected Set<TotePool> pools = new HashSet<TotePool>();

	protected Set<ToteRace> races = new HashSet<ToteRace>();

	public ToteCard() {	
	}
	
	@XmlElement(name = "pool")
	public Set<TotePool> getPools() {
		return pools;
	}

	public void setPools(Set<TotePool> pools) {
		this.pools = pools;
	}

	@XmlElement(name = "race")
	public Set<ToteRace> getRaces() {
		return races;
	}

	public void setRaces(Set<ToteRace> races) {
		this.races = races;
	}

	@XmlElement(name = "track-identifier")
	public String getTrackIdentifier() {
		return trackIdentifier;
	}

	public void setTrackIdentifier(String trackIdentifier) {
		this.trackIdentifier = trackIdentifier;
	}

	@XmlElement(name = "host-meet-num")
	public String getHostMeetNumber() {
		return hostMeetNumber;
	}

	public void setHostMeetNumber(String hostMeetNumber) {
		this.hostMeetNumber = hostMeetNumber;
	}

	@XmlElement(name = "itsp-event-code")
	public String getEventCode() {
		return eventCode;
	}

	public void setEventCode(String eventCode) {
		this.eventCode = eventCode;
	}

	@XmlElement(name = "pool_minimums")
	public PoolMinimum getPoolMinimum() {
		return poolMinimum;
	}

	public void setPoolMinimum(PoolMinimum poolMinimum) {
		this.poolMinimum = poolMinimum;
	}

	@XmlElement(name = "meetingCodeUK")
	public String getMeetingCodeUK() {
		return meetingCodeUK;
	}

	public void setMeetingCodeUK(String meetingCodeUK) {
		this.meetingCodeUK = meetingCodeUK;
	}

	private static final long serialVersionUID = -1L;

}
