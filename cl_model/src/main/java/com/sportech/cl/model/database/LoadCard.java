/**
 * 
 */

/**
 * @author jyu
 *
 */
package com.sportech.cl.model.database;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "load_card")
public class LoadCard implements Serializable {

	@ManyToOne(cascade = CascadeType.REFRESH, fetch = FetchType.EAGER)
	@JoinColumn(name = "soft_card_fk", insertable = false, updatable = false)
	protected SoftCard softCard;

	@ManyToOne(cascade = CascadeType.REFRESH, fetch = FetchType.EAGER)
	@JoinColumn(name = "hard_card_id", insertable = false, updatable = false)
	private HardCard hardCard;

	@ManyToOne(cascade = CascadeType.REFRESH, fetch = FetchType.EAGER)
	@JoinColumn(name = "pattern_fk", insertable = false, updatable = false)
	private Pattern pattern;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "load_card_pk")
	private Long id;
	@Column(name = "description")
	private String description;
	@Column(name = "load_code")
	private String loadCode;
	@Column(name = "pattern_fk")
	private Long patternId;
	@Column(name = "approved")
	private Boolean approved;
	@Column(name = "card_date")
	private java.util.Date cardDate;
	@Column(name = "races")
	private String racesList;

	public LoadCard() {
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		LoadCard other = (LoadCard) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	public SoftCard getSoftCard() {
		return softCard;
	}

	public void setSoftCard(SoftCard softCard) {
		this.softCard = softCard;
	}

	public HardCard getHardCard() {
		return hardCard;
	}

	public void setHardCard(HardCard hardCard) {
		this.hardCard = hardCard;
	}

	public Pattern getPattern() {
		return pattern;
	}

	public void setPattern(Pattern pattern) {
		this.pattern = pattern;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getLoadCode() {
		return loadCode;
	}

	public void setLoadCode(String loadCode) {
		this.loadCode = loadCode;
	}

	public Long getPatternId() {
		return patternId;
	}

	public void setPatternId(Long patternId) {
		this.patternId = patternId;
	}

	public Boolean getApproved() {
		return approved;
	}

	public void setApproved(Boolean approved) {
		this.approved = approved;
	}

	public java.util.Date getCardDate() {
		return cardDate;
	}

	public void setCardDate(java.util.Date cardDate) {
		this.cardDate = cardDate;
	}

	public String getRacesList() {
		return racesList;
	}

	public void setRacesList(String racesList) {
		this.racesList = racesList;
	}

	private static final long serialVersionUID = -1L;

}
