package com.sportech.cl.model.response;

import java.util.List;
import java.util.Collection;

import javax.xml.bind.annotation.XmlElement;

import com.sportech.cl.model.database.Pattern;

public class PatternsResponse extends ObjectListResponse<Pattern> 
{
	public PatternsResponse() {}
	public PatternsResponse(GenericResponse rsp)			{ super(rsp); } 
	
	@XmlElement(name="pattern")
	public List<Pattern> getPatterns() 						{ return getObjects(); }
	public void setPatterns(Collection<Pattern> patterns) 	{ setObjects(patterns); }
	
	private static final long serialVersionUID = -1L;

}
