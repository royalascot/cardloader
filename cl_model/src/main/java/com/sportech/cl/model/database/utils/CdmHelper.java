package com.sportech.cl.model.database.utils;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import com.sportech.cl.model.database.PoolMinimum;
import com.sportech.cl.model.database.PoolParameter;

public class CdmHelper {

    public static PoolMinimum sortEntries(PoolMinimum p) {
        PoolMinimum sorted = new PoolMinimum();
        if (p != null) {
            if (p.getDefaultMinimums() != null) {
                sorted.getDefaultMinimums().addAll(p.getDefaultMinimums());
            }
            if (p.getOverrideMinimums() != null) {
                sorted.getOverrideMinimums().addAll(p.getOverrideMinimums());
            }
        }
        return sorted;
    }

    public static String validate(Set<PoolParameter> values) {
        if (values == null) {
            return null;
        }
        for (PoolParameter p : values) {
            if (p.getIncrementAmount() == null || p.getMinAmount() == null) {
                return "Amounts can not be empty.";
            }
            if (BigDecimal.ZERO.compareTo(p.getIncrementAmount()) > 0 || BigDecimal.ZERO.compareTo(p.getMinAmount()) > 0) {
                return "Amounts can not be negative.";
            }
            if (p.getIncrementAmount().compareTo(p.getMinAmount()) > 0) {
                return "Increment amount cannot be greater than minimum amount.";
            }
        }
        return null;
    }

    public static String validateRaces(Set<PoolParameter> defaults, Set<PoolParameter> values) {
        if (values == null) {
            return null;
        }
        Set<PoolParameter> removed = new TreeSet<PoolParameter>();
        Map<Long, PoolParameter> pools = new HashMap<Long, PoolParameter>();
        for (PoolParameter p : defaults) {
            pools.put(p.getId(), p);
        }

        String message = null;
        for (PoolParameter p : values) {
            PoolParameter d = pools.get(p.getId());
            if (d != null) {
                if (isSameAmount(d.getMinAmount(), p.getMinAmount())
                        && isSameAmount(d.getIncrementAmount(), p.getIncrementAmount())) {
                    if (message == null) {
                        message = "Removed duplicate override entries.";
                    }
                    continue;
                }
            }
            removed.add(p);
        }
        values.clear();
        values.addAll(removed);
        return message;
    }

    private static boolean isSameAmount(BigDecimal v1, BigDecimal v2) {
        if (v1 == null || v2 == null) {
            return false;
        }
        return v1.compareTo(v2) == 0;
    }

}
