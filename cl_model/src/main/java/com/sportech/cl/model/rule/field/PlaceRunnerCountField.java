package com.sportech.cl.model.rule.field;

import org.apache.commons.lang3.StringUtils;

import com.sportech.cl.model.database.HardRace;
import com.sportech.cl.model.rule.ContainerType;
import com.sportech.cl.model.rule.RuleContext;
import com.sportech.cl.model.rule.RuleDataType;
import com.sportech.cl.model.rule.RuleField;

public class PlaceRunnerCountField extends RuleField {

    public PlaceRunnerCountField() {
        super("w/ PLC & Runner Count", "w/ PLC & Runner Count", RuleDataType.Integer, ContainerType.Race);
    }

    @Override
    public Object getValue(RuleContext context) {
        if (context == null) {
            return null;
        }
        HardRace race = context.getRace();
        if (race == null) {
            return null;
        }
        if (race.getRunners() == null) {
            return 0;
        }
        RacePoolNamesField f = new RacePoolNamesField();
        String pools = (String) f.getValue(context);
        if (StringUtils.containsIgnoreCase(pools, "PLC")) {
            return race.getRunners().size();
        }
        return 0;
    }

}
