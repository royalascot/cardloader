package com.sportech.cl.model.database;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.sportech.cl.model.database.utils.EntityWithId;

@Entity
@Table(name="users")
public class UserOutline implements Serializable, EntityWithId 
{
    private Long    id;
    private String  identifier;
    private String  salt;
    private Boolean active;
    private String firstName;
    private String lastName;
    
    @Column(name="first_name")
    public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	@Column(name="last_name")
	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public UserOutline()   {}
    
    @Id
    @Column(name="user_pk")
    public Long getId()                             { return id; }
    public void setId(Long id)                      { this.id = id; }

    @Column(name="identifier")
    public String getIdentifier()                   { return identifier; }
    public void setIdentifier(String identifier)    { this.identifier = identifier; }

    @Column(name="salt")
    public String getSalt()                         { return salt; }
    public void setSalt(String salt)                { this.salt = salt; }

    @Column(name="active")
    public Boolean getActive()                      { return active; }
    public void setActive(Boolean active)           { this.active = active; }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        UserOutline other = (UserOutline) obj;
        if (id == null) {
            if (other.id != null)
                return false;
        } else if (!id.equals(other.id))
            return false;
        return true;
    }

    private static final long serialVersionUID = -1L;
    
}
