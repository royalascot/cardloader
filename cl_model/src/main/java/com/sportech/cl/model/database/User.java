package com.sportech.cl.model.database;

import java.io.Serializable;
import java.util.List;
import java.util.Set;
import java.util.TimeZone;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.sportech.cl.model.database.utils.EntityWithId;
import com.sportech.cl.model.database.utils.Persistent;
import com.sportech.cl.model.database.utils.PersistentWithId;

@Entity
@Table(name = "users")
public class User implements Serializable, EntityWithId, PersistentWithId<User>, Persistent<User>, Comparable<User> {

	private Long userId;
	private String identifier;
	private String passwd;
	private String salt;
	private Integer actorTypeId;
	private Integer actorId;
	private Integer roleId;
	private Boolean active;
	private String firstName;
	private String lastName;
	private String officePhone;
	private String officeLocation;
	private TimeZone timeZone;

	private List<Group> groups;

	private Set<Role> roles;

	private Set<Long> totes;

	private Set<Track> tracks;

	private Set<Long> trackIds;

	public User() {
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "user_pk")
	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	@ManyToMany(mappedBy = "users")
	public List<Group> getGroups() {
		return groups;
	}

	public void setGroups(List<Group> groups) {
		this.groups = groups;
	}

	@Column(name = "identifier")
	public String getIdentifier() {
		return identifier;
	}

	public void setIdentifier(String identifier) {
		this.identifier = identifier;
	}

	@Column(name = "password")
	public String getPasswd() {
		return passwd;
	}

	public void setPasswd(String passwd) {
		this.passwd = passwd;
	}

	@Column(name = "salt")
	public String getSalt() {
		return salt;
	}

	public void setSalt(String salt) {
		this.salt = salt;
	}

	@Column(name = "actor_type_fk")
	public Integer getActorTypeId() {
		return actorTypeId;
	}

	public void setActorTypeId(Integer actorTypeId) {
		this.actorTypeId = actorTypeId;
	}

	@Column(name = "actor_id")
	public Integer getActorId() {
		return actorId;
	}

	public void setActorId(Integer actorId) {
		this.actorId = actorId;
	}

	@Column(name = "role_fk")
	public Integer getRoleId() {
		return roleId;
	}

	public void setRoleId(Integer roleId) {
		this.roleId = roleId;
	}

	@Column(name = "active")
	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	@Column(name = "first_name")
	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	@Column(name = "last_name")
	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	@Column(name = "office_phone")
	public String getOfficePhone() {
		return officePhone;
	}

	public void setOfficePhone(String officePhone) {
		this.officePhone = officePhone;
	}

	@Column(name = "office_location")
	public String getOfficeLocation() {
		return officeLocation;
	}

	public void setOfficeLocation(String officeLocation) {
		this.officeLocation = officeLocation;
	}

	@Column(name = "time_zone")
	public TimeZone getTimeZone() {
		return timeZone;
	}

	public void setTimeZone(TimeZone timeZone) {
		this.timeZone = timeZone;
	}

	@Transient
	public String getName() {
		return this.identifier;
	}

	@Override
	public void copy4persistence(User from) {
		setUserId(from.getUserId());
		setIdentifier(from.getIdentifier());
		setPasswd(from.getPasswd());
		setSalt(from.getSalt());
		setActorTypeId(from.getActorTypeId());
		setActorId(from.getActorId());
		setRoleId(from.getRoleId());
		setActive(from.getActive());
		setFirstName(from.getFirstName());
		setLastName(from.getLastName());
		setOfficePhone(from.getOfficePhone());
		setOfficeLocation(from.getOfficeLocation());
		setTimeZone(from.getTimeZone());
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((userId == null) ? 0 : userId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		User other = (User) obj;
		if (userId == null) {
			if (other.userId != null)
				return false;
		} else if (!userId.equals(other.userId))
			return false;
		return true;
	}

	private static final long serialVersionUID = -1L;

	@Override
	@Transient
	public Long getId() {
		return getUserId();
	}

	@Transient
	public Set<Role> getRoles() {
		return roles;
	}

	public void setRoles(Set<Role> roles) {
		this.roles = roles;
	}

	@Transient
	public Set<Track> getTracks() {
		return tracks;
	}

	public void setTracks(Set<Track> tracks) {
		this.tracks = tracks;
	}

	@Transient
	public Set<Long> getTrackIds() {
		return trackIds;
	}

	public void setTrackIds(Set<Long> trackIds) {
		this.trackIds = trackIds;
	}

	@Transient
	public Set<Long> getTotes() {
		return totes;
	}

	public void setTotes(Set<Long> totes) {
		this.totes = totes;
	}

	@Override
	public int compareTo(User t) {
		if (t == null || getName() == null || t.getName() == null) {
			return -1;
		}
		return getName().compareTo(t.getName());
	}

}
