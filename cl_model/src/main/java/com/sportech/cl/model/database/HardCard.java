package com.sportech.cl.model.database;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlTransient;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import com.sportech.cl.model.database.utils.PersistentParent;
import com.sportech.cl.model.database.utils.Pool;
import com.sportech.cl.model.database.utils.PoolContainer;
import com.sportech.common.model.CardType;
import com.sportech.common.model.PerformanceType;

@Entity
@Table(name = "hard_card")
public class HardCard implements Serializable, PersistentParent<HardCard>, PoolContainer {

	private static final long serialVersionUID = -1L;

	private Long id;
	private Track track;
	private Long importId;
	private String eventCode;
	private String provider;
	private Date cardDate;
	private Boolean enabled;
	private Boolean approved;
	private CardType cardType;
	private PerformanceType perfType;
	private Date firstRaceTime;
	private String description;
	private String meetingCode;
	private String passThroughs;
	private String settings;
	private CardConfig config;
	private Integer featureRace;
	private String sourceId;
	
	private Set<HardPool> pools = new HashSet<HardPool>();
	private Set<HardRace> races = new HashSet<HardRace>();

	public HardCard() {
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "hard_card_pk")
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name = "card_type")
	public CardType getCardType() {
		return cardType;
	}

	public void setCardType(CardType cardType) {
		this.cardType = cardType;
	}

	@Column(name = "perf_type")
	public PerformanceType getPerfType() {
		return perfType;
	}

	public void setPerfType(PerformanceType perfType) {
		this.perfType = perfType;
	}

	@XmlTransient
	@JoinColumn(name = "config_id")
	@ManyToOne
	public CardConfig getConfig() {
		return config;
	}

	public void setConfig(CardConfig config) {
		this.config = config;
	}

	@XmlElement(name = "track")
	@JoinColumn(name = "dict_track_fk")
	@ManyToOne
	public Track getTrack() {
		return track;
	}

	public void setTrack(Track track) {
		this.track = track;
	}

	@Column(name = "import_fk")
	public Long getImportId() {
		return importId;
	}

	public void setImportId(Long importId) {
		this.importId = importId;
	}

	@Column(name = "event_code")
	public String getEventCode() {
		return eventCode;
	}

	public void setEventCode(String eventCode) {
		this.eventCode = eventCode;
	}

	@Column(name = "provider")
	public String getProvider() {
		return provider;
	}

	public void setProvider(String provider) {
		this.provider = provider;
	}

	@Column(name = "first_race_time")
	public Date getFirstRaceTime() {
		return firstRaceTime;
	}

	public void setFirstRaceTime(Date firstRaceTime) {
		this.firstRaceTime = firstRaceTime;
	}

	@Column(name = "description")
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@XmlElement
	@XmlSchemaType(name = "date")
	@Column(name = "card_date")
	public java.util.Date getCardDate() {
		return cardDate;
	}

	public void setCardDate(java.util.Date cardDate) {
		this.cardDate = cardDate;
	}

	@XmlElement(required = true)
	@Column(name = "enabled")
	public Boolean getEnabled() {
		return enabled;
	}

	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}

	@XmlElement
	@Column(name = "approved")
	public Boolean getApproved() {
		return approved;
	}

	public void setApproved(Boolean approved) {
		this.approved = approved;
	}

	@Column(name = "meeting_code")
	public String getMeetingCode() {
		return meetingCode;
	}

	public void setMeetingCode(String meetingCode) {
		this.meetingCode = meetingCode;
	}

	@Column(name = "pass_throughs")
	public String getPassThroughs() {
		return passThroughs;
	}

	public void setPassThroughs(String passThroughs) {
		this.passThroughs = passThroughs;
	}

	@XmlElement(name = "pool")
	@OneToMany(mappedBy = "parent", cascade = CascadeType.ALL, fetch = FetchType.EAGER, orphanRemoval = true)
	@OrderBy("poolTypeId")
	@Fetch(FetchMode.SELECT)
	public Set<HardPool> getPools() {
		return pools;
	}

	public void setPools(Set<HardPool> pools) {
		this.pools = pools;
	}

	@XmlElement(name = "race")
	@OneToMany(mappedBy = "parent", cascade = CascadeType.ALL, fetch = FetchType.EAGER, orphanRemoval = true)
	@OrderBy("number")
	@Fetch(FetchMode.SELECT)
	public Set<HardRace> getRaces() {
		return races;
	}

	public void setRaces(Set<HardRace> races) {
		this.races = races;
	}

	@Override
	public void copy4persistence(HardCard from) {
	}

	@Override
	public void setChildParents() {
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cardDate == null) ? 0 : cardDate.hashCode());
		result = prime * result + ((description == null) ? 0 : description.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		HardCard other = (HardCard) obj;
		if (cardDate == null) {
			if (other.cardDate != null)
				return false;
		} else if (!cardDate.equals(other.cardDate))
			return false;
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description))
			return false;
		return true;
	}

	@Override
	public Pool addPool(int poolTypeId, long bitmap) {
		HardPool p = new HardPool();
		p.setPoolTypeId(poolTypeId);
		p.setRaceBitmap(bitmap);
		p.setParent(this);
		getPools().add(p);
		return p;
	}

	@Override
	public void removePool(Pool p) {
		getPools().remove((HardPool) p);
	}

	@Override
	@Transient
	public Collection<Pool> getPoolCollection() {
		List<Pool> pools = new ArrayList<Pool>();
		pools.addAll(getPools());
		return pools;
	}

	@Override
	@Transient
	public Integer getRaceCount() {
		return getRaces().size();
	}

	@Override
	@Transient
	public void setRaceCount(Integer count) {

	}

	@Override
	@Transient
	public HardRace getPoolRace(long raceNumber) {
		for (HardRace r : getRaces()) {
			if (r.getNumber() != null && r.getNumber() == raceNumber) {
				return r;
			}
		}
		return null;
	}

	@Override
	@Transient
	public boolean isReadonly() {
		if (getApproved() != null && getApproved()) {
			return true;
		}
		return false;
	}

	@Override
	@Transient
	public boolean canDisableRace() {
		return false;
	}

	@Override
	@Transient
	public boolean hasRunners() {
		return true;
	}

	@Column(name = "settings", length=1000)
	public String getSettings() {
		return settings;
	}

	public void setSettings(String settings) {
		this.settings = settings;
	}

	@Column(name = "feature_race")
	public Integer getFeatureRace() {
		return featureRace;
	}

	public void setFeatureRace(Integer featureRace) {
		this.featureRace = featureRace;
	}

	@Column(name = "source_id", length=50)
	public String getSourceId() {
		return sourceId;
	}

	public void setSourceId(String sourceId) {
		this.sourceId = sourceId;
	}
	
}
