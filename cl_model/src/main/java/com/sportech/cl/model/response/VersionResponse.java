package com.sportech.cl.model.response;

import javax.xml.bind.annotation.XmlElement;

import com.sportech.cl.model.common.CardLoaderVersion;
import com.sportech.cl.model.common.ErrorCode;

public class VersionResponse extends ObjectResponse<CardLoaderVersion>
{
    public VersionResponse() {}
    public VersionResponse(GenericResponse rsp)                 { super(rsp); }
    public VersionResponse(ErrorCode code, String... _errors)   { super( code, _errors ); }

    @XmlElement(name="components")
    public CardLoaderVersion getVersion()                  { return getObject(); }
    public void setVersions(CardLoaderVersion vers)        { setObject( vers ); }
    
    private static final long serialVersionUID = -1L;
}
