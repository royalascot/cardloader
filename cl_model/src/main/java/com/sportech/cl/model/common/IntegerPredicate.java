package com.sportech.cl.model.common;

public enum IntegerPredicate {
 
   EQ  (0,  "eq",   "Equals"),
   NE  (1,  "ne",   "Not Equal"),
   LT  (2,  "lt",   "Less Than"),
   LE  (3,  "le",   "Less Than or Equal"),
   GE  (4,  "ge",   "Greater Than or Equal"),
   GT  (5,  "gt",   "Greater Than");
    
   private final Integer    _id;
   private final String     _code;
   private final String     _description;

   IntegerPredicate(Integer id, String code, String description) {
       this._id = id;
       this._code = code;
       this._description = description;
   }
  
   public Integer getId()            {return _id;}
   public String getDescription()    {return _description;}
   public String getCode()           {return _code;}
  
   public boolean equals(IntegerPredicate e) {
       return _id.equals(e.getId());
   }

   public static IntegerPredicate ivalueOf(String predicate) {
       return IntegerPredicate.valueOf(predicate.toUpperCase());
   }
   
}
