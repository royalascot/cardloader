package com.sportech.cl.model.database;

import java.io.Serializable;
import java.util.Set;
import java.util.TreeSet;

import javax.xml.bind.annotation.XmlElement;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonProperty;

public class PoolMinimum implements Serializable {

    @JsonProperty("d")
    private Set<PoolParameter> defaultMinimums;

    @JsonProperty("o")
    private Set<PoolParameter> overrideMinimums;

    private static final long serialVersionUID = 1L;

    @JsonCreator
    public PoolMinimum() {
    }

    @XmlElement(name = "default_minimum")
    public Set<PoolParameter> getDefaultMinimums() {
        if (defaultMinimums == null) {
            defaultMinimums = new TreeSet<PoolParameter>();
        }
        return defaultMinimums;
    }

    public void setDefaultMinimums(Set<PoolParameter> defaultMinimums) {
        this.defaultMinimums = defaultMinimums;
    }

    @XmlElement(name = "override_minimum")
    public Set<PoolParameter> getOverrideMinimums() {
        if (overrideMinimums == null) {
            overrideMinimums = new TreeSet<PoolParameter>();
        }
        return overrideMinimums;
    }

    public void setOverrideMinimums(Set<PoolParameter> overrideMinimums) {
        this.overrideMinimums = overrideMinimums;
    }

}
