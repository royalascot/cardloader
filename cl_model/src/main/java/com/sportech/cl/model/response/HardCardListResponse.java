package com.sportech.cl.model.response;

import java.util.Collection;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;

import com.sportech.cl.model.database.HardCardOutline;

public class HardCardListResponse extends ObjectListResponse<HardCardOutline> 
{
	public HardCardListResponse() {}
	public HardCardListResponse(GenericResponse rsp)				{ super(rsp); } 

	@XmlElement(name="card")
	public List<HardCardOutline> getCards() 					{ return getObjects(); }
	public void setCards(Collection<HardCardOutline> cards) 	{ setObjects(cards); }
	
	private static final long serialVersionUID = -1L;
}
