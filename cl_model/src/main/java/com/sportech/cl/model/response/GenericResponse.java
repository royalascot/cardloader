package com.sportech.cl.model.response;

import java.util.ArrayList;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlElement;

import com.sportech.cl.model.common.ErrorCode;

public class GenericResponse implements Serializable 
{
	public GenericResponse() 
	{
	    this.status = ErrorCode.SUCCESS.getId();
	    this.errors = null;
	}

	public GenericResponse(GenericResponse rsp) 
	{
		this.status = rsp.getStatus();
		
		if( rsp.getErrors() == null || rsp.getErrors().isEmpty() )
		    this.errors = null;
		else
		{
		    this.errors = new ArrayList<String>( rsp.getErrors() );
		}
	}
	
	public GenericResponse(int _status, String... _errors) 
	{
	    setResult( _status, _errors );
	}

	public GenericResponse(ErrorCode _status, String... _errors) 
	{
        setResult( _status, _errors );
	}

    public void setResult(ErrorCode _status, String... _errors) 
    { 
        this.setResult(_status.getId(), _errors); 
    }
    
    public void setResult(int _status, String... _errors)
    {
        if( status != _status )
        {
            status = _status;
            setErrorStrings( _status, _errors );
        }
        else
            addErrors( _errors );
    }
    
    public void addErrors( String... _errors )
    {
        if( errors != null )
        {
            for( String error: _errors)
                errors.add(error);
        }
    }
	
	public Boolean	isOK()
	{
		return ErrorCode.isOK(status);
	}
	
	public String toString() {
		String str = "";
		
		str += "status: " + status;
		return str;
	}
	
	protected String innerXmlString() {
	    StringBuilder sb = new StringBuilder();
	    
	    sb.append( "<status>" );
        sb.append( status );
        sb.append( "</status>" );
        
        if( errors != null && !errors.isEmpty() )
        {
            for( String error : errors )
            {
                sb.append( "<error>" );
                sb.append( error );
                sb.append( "</error>" );
            }
        }
        
        
        return sb.toString();
	}
	
	public String toXmlString() {
        StringBuilder sb = new StringBuilder();
        sb.append( "<GenericResponse>" );
        sb.append( innerXmlString() );
        sb.append( "</GenericResponse>" );
        
        return sb.toString();
    }
	
	@XmlElement(name="status")
	public int getStatus()                 { return status; }
	public void setStatus(int _status)     { this.status = _status; }

    @XmlElement(name="error")
	public ArrayList<String> getErrors()               { return errors; }
    public void setErrors(ArrayList<String> errors)    { this.errors = errors; }

	protected void setErrorStrings(int _status, String... _errors)
	{
        errors = null; 
        
        if( !ErrorCode.isOK(status) )
        {
            ErrorCode   code = ErrorCode.getErrorCodeById(_status);
            
            if( code != null )
            {
                errors = new ArrayList<String>();
                errors.add(code.getDescription());
            }

            if( _errors.length > 0 )
            {
                if( errors == null )
                    errors = new ArrayList<String>();
                
                for( String error : _errors )
                    errors.add(error);
            }
        }
	    
	}
    
	/*
	 * Data members
	 */
	protected int                  status;
	protected ArrayList<String>    errors;

	private static final long serialVersionUID = -1L;
}
