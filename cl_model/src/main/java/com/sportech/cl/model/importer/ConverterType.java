package com.sportech.cl.model.importer;

import java.io.Serializable;

import com.sportech.cl.model.common.ExceptionStringWriter;

public enum ConverterType implements Serializable {
	// Converter Types
	 EQUIBASE					( 1, "equibase.pyc",			"Equibase"),
	 EQUIBASE_HARNESS			( 2, "equibase_harness.pyc",  	"Equibase Harness"),
	 UNIVERSAL					( 3, "equibase.pyc",			"Universal Format"),
	 IRELAND					( 4, "ireland.pyc",				"Irish Tote"), 
	 TRACK_INFO_A				( 5, "ireland.pyc",				"Track Info Matinee"),
	 TRACK_INFO_T				( 6, "ireland.pyc",				"Track Info Twilight"),
	 TRACK_INFO_E				( 7, "ireland.pyc",				"Track Info Evening"),
	 EQUIBASE_SIMULCAST			( 8, "equibase.pyc", 			"Equibase Simulcast"),
	 AUSTRALIA_A				( 9, "AUA",		 				"Australia A"),
	 AUSTRALIA_B				( 10, "AUB", 					"Australia B"),
	 AUSTRALIA_C				( 11, "AUC", 					"Australia C"),
	 NEW_ZEALAND				( 12, "NZ", 					"New Zealand"),
	 BETFRED_RMS                ( 13, "BETFRED_RMS",            "Betfred RMS File");
	 
   private final Integer 	id;
   private final String 	filename;
   private final String 	description;

   ConverterType(Integer id, String filename, String description) {
	   this.id = id;
   	   this.filename = filename;
   	   this.description = description;
   }

   public static ConverterType getConverterTypeById(int passedInt) {
       for(ConverterType ct : ConverterType.values()) {
           if(ct.getId().equals(passedInt)) {
               return ct;
           }
       }
       
       return null;
   }

   public static ConverterType getConverterTypeById(Integer passedInt) {
       if(passedInt != null) {
           return getConverterTypeById(passedInt.intValue());
   	    }
   	    else {
   	        return null;
   	    }
   }

   public boolean equals(ConverterType ct) {
	   	return id.equals(ct.getId());
   }
   
   public String toString()  {
       StringBuffer str = new StringBuffer();
   	    try {
			str.append("ConverterType(" + id + ",<" + filename + ">,<" + description + ">)");
		} 
   	    catch (RuntimeException e) {
   	        ExceptionStringWriter.exception2str(e, str);
		}
   	    return str.toString();
   }
   
   public Integer getId() {
       return id;
   }
   
   public String getDescription() {
       return description;
   }

   public String getFilename() {
       return filename;
   }
   
   static final long serialVersionUID = -1L;	

}
