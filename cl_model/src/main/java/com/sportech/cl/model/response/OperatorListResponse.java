package com.sportech.cl.model.response;

import java.util.Collection;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;

import com.sportech.cl.model.database.OperatorOutline;

public class OperatorListResponse extends ObjectListResponse<OperatorOutline> 
{
    public OperatorListResponse() {}
    public OperatorListResponse(GenericResponse rsp)      { super(rsp); } 

    @XmlElement(name="operator")
    public List<OperatorOutline>  getOperators()                    { return getObjects(); }
    public void setOperators(Collection<OperatorOutline> ops)       { setObjects(ops); }
    
    private static final long serialVersionUID = -1L;
}
