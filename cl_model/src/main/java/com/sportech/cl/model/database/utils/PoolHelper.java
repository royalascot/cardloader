package com.sportech.cl.model.database.utils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.TreeSet;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.MutablePair;
import org.apache.log4j.Logger;

import com.sportech.cl.model.comparator.PoolComparator;
import com.sportech.cl.model.database.HardCard;
import com.sportech.cl.model.database.HardPool;
import com.sportech.cl.model.database.HardRace;
import com.sportech.cl.model.database.PoolParameter;
import com.sportech.cl.model.database.PoolTableCell;
import com.sportech.cl.model.database.PoolTableRow;
import com.sportech.cl.model.database.Runner;
import com.sportech.common.model.PoolType;
import com.sportech.common.util.DecimalHelper;
import com.sportech.common.util.ListHelper;

public class PoolHelper {

    static private final Logger log = Logger.getLogger(PoolHelper.class);

    private static final long GSL = 40;

    private String oldPoolNames = null;

    private List<Pool> sortedPools = new ArrayList<Pool>();

    private PoolContainer container = null;

    public PoolHelper(PoolContainer container) {
        this.container = container;
    }

    public String getOldPoolNames() {
        return oldPoolNames;
    }

    public void setOldPoolNames(String oldPoolNames) {
        this.oldPoolNames = oldPoolNames;
    }

    public List<Pool> getSortedPools() {
        return sortedPools;
    }

    public void setSortedPools(List<Pool> sortedPools) {
        this.sortedPools = sortedPools;
    }

    public void disablePoolsforRace(Long raceNumber) {
        if (container == null || container.getPoolCollection() == null || raceNumber == null) {
            return;
        }
        long mask = (1 << (raceNumber - 1));
        List<Pool> toBeRemoved = new ArrayList<Pool>();
        for (Pool pp : container.getPoolCollection()) {
            PoolType poolType = PoolType.fromId(pp.getPoolTypeId());
            if (poolType.isMultiRace()) {
                if ((pp.getRaceBitmap() & mask) > 0L) {
                    log.info("Remove pool " + poolType.getCode() + "for race " + raceNumber);
                    toBeRemoved.add(pp);
                }
            } else {
                if (isRaceInPool(raceNumber, pp.getRaceBitmap())) {
                    long newMask = pp.getRaceBitmap() & (~mask);
                    Pool p = container.addPool(pp.getPoolTypeId(), newMask);
                    p.setPoolCode(pp.getPoolCode());
                    p.setPoolName(pp.getPoolName());
                    p.setGuarantee(pp.getGuarantee());
                    p.setTotePoolType(pp.getTotePoolType());
                    toBeRemoved.add(pp);
                }
            }
        }
        for (Pool pp : toBeRemoved) {
            container.removePool(pp);
        }
    }

    public static void refreshSinglePool(PoolContainer container, Integer poolTypeId, Long raceNumber, Boolean checked) {
        if (container == null || container.getPoolCollection() == null || poolTypeId == null || raceNumber == null
                || checked == null) {
            return;
        }
        boolean found = false;
        long mask = (1 << (raceNumber - 1));
        for (Pool pp : container.getPoolCollection()) {
            if (pp.getPoolTypeId().intValue() == poolTypeId) {
                if (isRaceInPool(raceNumber, pp.getRaceBitmap())) {
                    found = true;
                    long newMask;
                    if (checked) {
                        newMask = pp.getRaceBitmap() | mask;
                    } else {
                        mask = ~mask;
                        newMask = pp.getRaceBitmap() & mask;
                    }
                    container.removePool(pp);
                    if (newMask != 0L) {
                        Pool p = container.addPool(poolTypeId, newMask);
                        p.setPoolCode(pp.getPoolCode());
                        p.setPoolName(pp.getPoolName());
                        p.setGuarantee(pp.getGuarantee());
                        p.setTotePoolType(pp.getTotePoolType());
                    }
                    break;
                }
            }
        }
        if (checked && !found) {
            PoolType poolType = PoolType.fromId(poolTypeId);
            Pool p = container.addPool(poolTypeId, mask);
            p.setTotePoolType(poolType.getToteId());
        }
    }

    public static void refreshMultiPool(PoolContainer container, Integer poolTypeId, Long raceNumber, String raceList) {
        if (container == null || container.getPoolCollection() == null || poolTypeId == null || raceNumber == null
                || raceList == null) {
            return;
        }
        long mask = 1 << (raceNumber - 1);
        long lowerMask = mask - 1;
        boolean found = false;
        for (Pool pp : container.getPoolCollection()) {
            if (pp.getPoolTypeId().intValue() == poolTypeId) {
                long maskResult = pp.getRaceBitmap() & lowerMask;
                if (maskResult > 0L) {
                    log.info("Skip pool " + Long.toBinaryString(maskResult) + ",bitmap:" + Long.toBinaryString(pp.getRaceBitmap())
                            + ",mask" + Long.toBinaryString(lowerMask));
                    continue;
                }
                if ((pp.getRaceBitmap() & mask) > 0L) {
                    found = true;
                    if (StringUtils.isBlank(raceList)) {
                        container.removePool(pp);
                        log.info("Remove pool " + raceNumber);
                        return;
                    } else {
                        log.info("Update pool " + raceNumber + " from " + Long.toBinaryString(pp.getRaceBitmap()));
                        long newMask = getBitmap(mask, raceList);
                        Pool p = container.addPool(poolTypeId, newMask);
                        p.setPoolCode(pp.getPoolCode());
                        p.setPoolName(pp.getPoolName());
                        p.setGuarantee(pp.getGuarantee());
                        p.setTotePoolType(pp.getTotePoolType());
                        container.removePool(pp);
                        log.info("Update pool " + raceNumber + " to " + Long.toBinaryString(pp.getRaceBitmap()));
                        break;
                    }
                }
            }
        }
        if (!found && !StringUtils.isBlank(raceList)) {
            PoolType poolType = PoolType.fromId(poolTypeId);
            long newMask = getBitmap(mask, raceList);
            Pool pp = container.addPool(poolTypeId, newMask);
            pp.setTotePoolType(poolType.getToteId());
            log.info("Add pool " + raceNumber + " to " + Long.toBinaryString(pp.getRaceBitmap()));
        }
    }

    private static long getBitmap(long mask, String raceList) {
        Collection<Long> races = ListHelper.parseRaceList(raceList);
        for (Long l : races) {
            mask |= 1 << (l - 1);
        }
        return mask;
    }

    public static String buildRaceList(Long raceNumber, Set<Long> s) {
        String raceList = " ";
        if (s.size() == 0) {
            raceList = " ";
        } else {
            s.remove(raceNumber);
            if (s.size() == 1) {
                raceList = StringUtils.join(s, ",");
            } else {
                raceList = buildRaceList(s);
            }
        }
        return raceList;
    }

    private static String buildRaceList(Collection<Long> raceCollection) {
        List<MutablePair<Long, Long>> ranges = new ArrayList<MutablePair<Long, Long>>();
        long start = -100L, end = -100L;
        MutablePair<Long, Long> currentRange = null;
        List<Long> races = new ArrayList<Long>();
        races.addAll(raceCollection);
        Collections.sort(races);
        for (Long l : races) {
            if (start + 1 < l) {
                start = l;
                end = l;
                currentRange = new MutablePair<Long, Long>(start, end);
                ranges.add(currentRange);
            } else if (end + 1 == l) {
                end++;
                start++;
                currentRange.setRight(l);
            }
        }
        List<String> rangeTexts = new ArrayList<String>();
        for (MutablePair<Long, Long> p : ranges) {
            if (p.getLeft().longValue() == p.getRight().longValue()) {
                rangeTexts.add("" + p.getLeft());
            } else {
                rangeTexts.add("" + p.getLeft() + "-" + p.getRight());
            }
        }

        return StringUtils.join(rangeTexts, ",");
    }

    public static boolean isRaceInPool(long r, long bitmap) {
        return ((1 << (r - 1)) & bitmap) > 0;
    }

    public static Long getRacesFromBitmap(Long bitmap, Set<Long> races) {
        if (bitmap == null || races == null) {
            return null;
        }
        long firstRace = 0L;
        for (long i = 1; i <= 31; i++) {
            if (isRaceInPool(i, bitmap)) {
                if (firstRace == 0L) {
                    firstRace = i;
                }
                races.add(i);
            }
        }
        return firstRace;
    }

    public static Long getBitmapFromRaces(String races) {
        Long bitmap = 0L;
        if (!StringUtils.isEmpty(races)) {
            String[] raceNumbers = races.split(",");
            for (String r : raceNumbers) {
                bitmap |= 1 << (Long.parseLong(r) - 1);
            }
        }
        return bitmap;
    }

    public static List<Long> getDefaultLegs(PoolType pt, long raceNumber, long raceCount) {
        List<Long> legs = new ArrayList<Long>();
        if (pt.getRaceCount() + raceNumber - 1 <= raceCount) {
            for (int i = 1; i < pt.getRaceCount(); i++) {
                legs.add(raceNumber + i);
            }
        }
        return legs;
    }

    public <T extends Pool> boolean setupSortedPools(Collection<T> pools) {
        List<Pool> poolList = new ArrayList<Pool>();
        for (T t : pools) {
            poolList.add((Pool) t);
        }
        Collections.sort(poolList, new PoolComparator());
        sortedPools = poolList;
        Collection<String> p = getPoolNames();
        String poolNames = StringUtils.join(p, ",");
        if (!StringUtils.equals(poolNames, oldPoolNames)) {
            oldPoolNames = poolNames;
            return true;
        }
        return false;
    }

    public Collection<String> getPoolNames() {
        List<String> poolList = new ArrayList<String>();
        Set<Integer> poolColumns = new HashSet<Integer>();
        for (Pool p : sortedPools) {
            PoolType pt = PoolType.fromId(p.getPoolTypeId());
            if (pt != null) {
                if (!poolColumns.contains(p.getPoolTypeId())) {
                    poolColumns.add(p.getPoolTypeId());
                    poolList.add(pt.getName());
                }
            }
        }
        return poolList;
    }

    public static void updatePoolProperties(PoolContainer container, PoolTableCell cell) {
        if (container == null || container.getPoolCollection() == null || cell == null) {
            return;
        }
        int raceNumber = cell.getRaceNumber().intValue();
        long mask = 1 << (raceNumber - 1);
        long lowerMask = ~(mask - 1);
        if (PoolTableCell.MULTIPLE.equals(cell.getCellType())) {
            for (Pool pp : container.getPoolCollection()) {
                if (pp.getPoolTypeId().intValue() == cell.getPoolTypeId().intValue()) {
                    if (((pp.getRaceBitmap() & lowerMask) & mask) > 0L) {
                        pp.setPoolName(cell.getPoolName());
                        pp.setPoolCode(cell.getPoolCode());
                        pp.setGuarantee(cell.getPoolGuarantee());
                        break;
                    }
                }
            }
        } else {
            for (Pool pp : container.getPoolCollection()) {
                if (pp.getPoolTypeId().intValue() == cell.getPoolTypeId().intValue()) {
                    if ((pp.getRaceBitmap() & mask) > 0L) {
                        if ((pp.getRaceBitmap() & (~mask)) == 0L) {
                            pp.setPoolName(cell.getPoolName());
                            pp.setPoolCode(cell.getPoolCode());
                            pp.setGuarantee(cell.getPoolGuarantee());
                        } else {
                            Pool newPool = container.addPool(pp.getPoolTypeId(), mask);
                            newPool.setPoolName(cell.getPoolName());
                            newPool.setPoolCode(cell.getPoolCode());
                            newPool.setGuarantee(cell.getPoolGuarantee());
                            newPool.setTotePoolType(pp.getTotePoolType());
                            newPool = container.addPool(pp.getPoolTypeId(), pp.getRaceBitmap() & (~mask));
                            newPool.setPoolName(pp.getPoolName());
                            newPool.setPoolCode(pp.getPoolCode());
                            newPool.setGuarantee(pp.getGuarantee());
                            newPool.setTotePoolType(pp.getTotePoolType());
                            container.removePool(pp);
                        }
                        break;
                    }
                }
            }
        }
    }

    public List<Integer> getPoolTypeIds() {
        List<Integer> poolList = new ArrayList<Integer>();
        Set<Integer> poolColumns = new HashSet<Integer>();
        for (Pool p : sortedPools) {
            PoolType pt = PoolType.fromId(p.getPoolTypeId());
            if (pt != null) {
                if (!poolColumns.contains(p.getPoolTypeId())) {
                    poolColumns.add(p.getPoolTypeId());
                    poolList.add(p.getPoolTypeId());
                }
            }
        }
        return poolList;
    }

    public Collection<PoolTableRow> getPoolTableRows(long raceCount) {
        SortedMap<Long, PoolTableRow> rows = new TreeMap<Long, PoolTableRow>();
        List<Integer> columnIds = getPoolTypeIds();
        for (long i = 1; i <= raceCount; i++) {
            PoolTableRow row = new PoolTableRow();
            row.setRaceNumber(i);
            row.setPoolTypeIds(columnIds);
            row.setRaceCount(raceCount);
            rows.put(i, row);
            if (container.hasRunners()) {
                HardRace race = container.getPoolRace(i);
                if (race != null && race.getRunners() != null) {
                    TreeSet<Long> runnerList = new TreeSet<Long>();
                    for (Runner r : race.getRunners()) {
                        if (r.getScratched() != null && r.getScratched()) {
                            continue;
                        }
                        String rn = r.getProgramNumber();
                        if (rn != null) {
                            rn = rn.replaceAll("\\D", "");
                            runnerList.add(Long.parseLong(rn));
                        }
                    }
                    row.setRunners(ListHelper.build(runnerList));
                    row.setPostTime(DateUtils.toPostTimeStr(race.getPostTime()));
                    row.setPlacePositions(race.getNumberPositions());
                }
            }
        }

        for (Pool p : sortedPools) {
            PoolType pt = PoolType.fromId(p.getPoolTypeId());
            if (pt != null) {
                Set<Long> raceNumbers = new HashSet<Long>();
                Long r = PoolHelper.getRacesFromBitmap(p.getRaceBitmap(), raceNumbers);
                if (r != null && r > 0) {
                    if (pt.isMultiRace()) {
                        String raceList = PoolHelper.buildRaceList(r, raceNumbers);
                        PoolTableCell cell = new PoolTableCell(raceCount, r, pt.getId(), raceList, pt.getRaceCount());
                        cell.setPoolCode(p.getPoolCode());
                        cell.setPoolName(p.getPoolName());
                        cell.setPoolGuarantee(p.getGuarantee());
                        cell.setSelectedLegs(new ArrayList<Long>());
                        cell.getSelectedLegs().addAll(raceNumbers);
                        PoolTableRow row = rows.get(r);
                        if (row != null) {
                            row.addCell(pt.getId(), cell);
                        }
                    } else {
                        for (long raceNumber = 1; raceNumber <= rows.size(); raceNumber++) {
                            if (raceNumbers.contains(raceNumber)) {
                                PoolTableCell cell = new PoolTableCell(raceCount, raceNumber, pt.getId(), true);
                                cell.setPoolCode(p.getPoolCode());
                                cell.setPoolName(p.getPoolName());
                                cell.setPoolGuarantee(p.getGuarantee());
                                PoolTableRow row = rows.get(raceNumber);
                                if (row != null) {
                                    row.addCell(pt.getId(), cell);
                                }
                            }
                        }
                    }
                }
            }
        }
        return rows.values();
    }

    public static Set<PoolParameter> clonePoolParameters(Set<PoolParameter> from, boolean cloneAll) {
        TreeSet<PoolParameter> params = new TreeSet<PoolParameter>();
        for (PoolParameter p : from) {
            if (cloneAll || p.getId() < GSL) {
                PoolParameter pm = new PoolParameter();
                pm.setId(p.getId());
                pm.setIncrementAmount(DecimalHelper.toCents(p.getIncrementAmount()));
                pm.setMinAmount(DecimalHelper.toCents(p.getMinAmount()));
                pm.setPoolType(p.getPoolType());
                pm.setRaces(p.getRaces());
                pm.setParameterType(p.getParameterType());
                params.add(pm);
            }
        }
        return params;
    }

    public static void populatePool(HardCard card, HardPool hardPool, Collection<Long> races) {
        try {

            PoolType poolType = PoolType.fromId(hardPool.getPoolTypeId());

            if (poolType == null) {
                log.error("Unknown pool type id:" + hardPool.getPoolTypeId());
                return;
            }

            int legCount = poolType.getRaceCount();
            if (legCount == 1) {
                hardPool.setParent(card);
                hardPool.setPoolTypeId(poolType.getId());
                hardPool.setTotePoolType(poolType.getToteId());
                hardPool.setRaceBitmap(0L);
                long raceMap = 0L;
                for (Long raceNumber : races) {
                    raceMap |= 1L << (raceNumber - 1);
                }
                hardPool.setRaceBitmap(raceMap);
            } else {
                hardPool.setParent(card);
                hardPool.setPoolTypeId(poolType.getId());
                hardPool.setTotePoolType(poolType.getToteId());
                hardPool.setRaceBitmap(0L);
                long legs = 0L;
                for (Long raceNumber : races) {
                    legs |= 1L << (raceNumber - 1);
                }
                hardPool.setRaceBitmap(legs);
            }
        } catch (Exception e) {
            log.error("Error populating pool", e);
        }
    }

}
