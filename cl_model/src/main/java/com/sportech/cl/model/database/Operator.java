package com.sportech.cl.model.database;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlElement;

import com.sportech.cl.model.database.utils.PersistentWithId;

@Entity
@Table(name="operators")
public class Operator  extends OperatorBase implements Serializable, PersistentWithId<Operator>
{

    @OneToMany(cascade=CascadeType.REFRESH, fetch=FetchType.EAGER)
    @JoinColumn(name="actor_id", insertable = false, updatable = false)
    @org.hibernate.annotations.Where(clause="actor_type_fk = 2")
    private Set<UserOutline>   users = new HashSet<UserOutline>();
    
    @XmlElement(name="user")
    public Set<UserOutline> getUsers()                  { return users; }
    public void setUsers(Set<UserOutline> users)        { this.users = users; }

    @Override
    public void copy4persistence(Operator from)
    {
        setFirstName( from.getFirstName() );
        setLastName( from.getLastName() );
        setOfficePhone( from.getOfficePhone() );
        setOfficeLocation( from.getOfficeLocation() );
        
        // 'active' state is to be managed by explicit service, NOT by 'update' 
        // operation - so 'active' is not copied here
    }

    private static final long serialVersionUID = -1L;

}
