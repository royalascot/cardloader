package com.sportech.cl.model.database.utils;

import java.beans.PropertyDescriptor;
import java.util.Date;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import com.sportech.common.model.CardType;
import com.sportech.common.model.PerformanceType;

public class PropertyHelper {

    static private final Logger log = Logger.getLogger(PropertyHelper.class);

    @SuppressWarnings("serial")
    static private final HashSet<Class<?>> supportTypes = new HashSet<Class<?>>() {
        {
            add(String.class);
            add(Integer.class);
            add(Short.class);
            add(Long.class);
            add(Date.class);
            add(Boolean.class);
            add(CardType.class);
            add(PerformanceType.class);
        }
    };

    @SuppressWarnings("rawtypes")
    public static void populateValues(Object dest, Object from, String[] excludes) {
        try {
            Map map = BeanUtils.describe(from);
            Set<String> excluded = new HashSet<String>();
            if (excludes != null) {
                for (String s : excludes) {
                    excluded.add(StringUtils.trim(s));
                }
            }
            for (Object o : map.keySet()) {
                String k = (String) o;
                if (excluded.contains(k)) {
                    continue;
                }
                PropertyDescriptor pd = PropertyUtils.getPropertyDescriptor(from, k);
                if (supportTypes.contains(pd.getPropertyType())) {
                    Object value = PropertyUtils.getProperty(from, k);
                    if (value != null) {
                        PropertyDescriptor tpd = PropertyUtils.getPropertyDescriptor(dest, k);
                        if (tpd != null) {
                            PropertyUtils.setProperty(dest, k, value);
                        }
                    }
                }
            }
        } catch (Exception e) {
            log.error("Error copying values", e);
        }
    }

}
