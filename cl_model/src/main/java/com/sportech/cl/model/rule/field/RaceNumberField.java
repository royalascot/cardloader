package com.sportech.cl.model.rule.field;

import com.sportech.cl.model.database.HardRace;
import com.sportech.cl.model.rule.ContainerType;
import com.sportech.cl.model.rule.RuleContext;
import com.sportech.cl.model.rule.RuleDataType;
import com.sportech.cl.model.rule.RuleField;

public class RaceNumberField extends RuleField {
    
    public RaceNumberField() {
        super("Race Number", "Race Number", RuleDataType.Integer, ContainerType.Race);
    }
    
    @Override
    public Object getValue(RuleContext context) {
        if (context == null) {
            return null;
        }
        HardRace race = context.getRace();
        if (race == null || race.getNumber() == null) {
            return null;
        }
	    return race.getNumber().intValue();
    }
    
}