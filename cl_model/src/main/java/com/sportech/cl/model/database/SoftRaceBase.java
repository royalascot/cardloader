package com.sportech.cl.model.database;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.MappedSuperclass;
import javax.persistence.OneToOne;

@MappedSuperclass
public class SoftRaceBase implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "soft_race_pk")
	private Long id;

	@Column(name = "soft_race_no")
	private Long number;

	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "hard_race_fk")
	private HardRace hardRace;

	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "race_id")
	private SoftRaceBase parentRace;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getNumber() {
		return number;
	}

	public void setNumber(Long number) {
		this.number = number;
	}

	public HardRace getHardRace() {
		return hardRace;
	}

	public void setHardRace(HardRace hardRace) {
		this.hardRace = hardRace;
	}

	public SoftRaceBase getParentRace() {
		return parentRace;
	}

	public void setParentRace(SoftRaceBase parentRace) {
		this.parentRace = parentRace;
	}

	public HardRace getRace() {
		SoftRaceBase b = this;
		do {
			HardRace r = b.getHardRace();
			if (r != null) {
				return r;
			}
			b = b.getParentRace();
		} while (b != null);
		return null;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((number == null) ? 0 : number.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SoftRaceBase other = (SoftRaceBase) obj;

		if (number != null && other.number != null) {
			return number.equals(other.number);
		}

		return false;
	}

	private static final long serialVersionUID = -1L;

}
