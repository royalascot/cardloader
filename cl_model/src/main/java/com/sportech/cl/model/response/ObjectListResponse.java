package com.sportech.cl.model.response;

import java.util.ArrayList;
import java.util.List;
import java.util.Collection;

import javax.xml.bind.annotation.XmlTransient;

import com.sportech.cl.model.common.ErrorCode;

public class ObjectListResponse<ObjType>  extends GenericResponse
{

	public ObjectListResponse() {}
	public ObjectListResponse(GenericResponse rsp)                 { super(rsp); } 
	public ObjectListResponse(ErrorCode	code, String... _errors)   { super( code, _errors ); }
	public ObjectListResponse(int _status, String... _errors)      { super(_status, _errors); }
	
	public ObjectListResponse(ObjType	obj)
	{
		super();
		
		if( obj != null ) {
			objects = new ArrayList<ObjType>();
			
			objects.add(obj);
		}
	}
	
	public ObjectListResponse(Collection<ObjType> objects) 
	{
	    super();
	    
	    if( objects != null ) {
	        this.objects = new ArrayList<ObjType>(objects);
	    }
    }

	@XmlTransient
	public List<ObjType> getObjects() 				    	{ return objects; }
	public void setObjects(Collection<ObjType> objects) 	{ this.objects = new ArrayList<ObjType>(objects); }
	
	public void add( ObjType obj ) 						{ objects.add(obj); }
	
	
	protected ArrayList<ObjType> objects;

	private static final long serialVersionUID = -1L;
}
