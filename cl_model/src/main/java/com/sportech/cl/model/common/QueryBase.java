package com.sportech.cl.model.common;

public abstract class QueryBase {
    protected String _query;
    
    public QueryBase() {}
    public QueryBase(String query)  { _query = query; }
    
    public String getQuery()                { return _query; }
    public void setQuery( String newValue ) { _query = newValue; }
}
