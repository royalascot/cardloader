package com.sportech.cl.model.common;

public class PredicateValue {

    private String _predicate;
    private Integer _value;

    public PredicateValue() {
        _predicate = IntegerPredicate.EQ.getCode();
        _value = 0;
    }
    
    public PredicateValue(String predicate, Integer value) {
        this._predicate = predicate;
        this._value = value;
    }
    
    public String getPredicate()                { return _predicate;}
    public void setPredicate(String newValue)   { _predicate = newValue; }
    
    public Integer getValue()                   { return _value; }
    public void setValue(Integer newValue)      { _value = newValue; }
    
    public IntegerPredicate getIntPredicate()   { return IntegerPredicate.ivalueOf(_predicate); }

}
