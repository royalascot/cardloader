package com.sportech.cl.model.database.utils;

public interface PersistentParent<T> extends PersistentWithId<T>
{
    public void    setChildParents();
}
