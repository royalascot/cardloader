package com.sportech.cl.model.database;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Table;

import com.sportech.cl.model.database.utils.EntityWithId;

@Entity
@Table(name="operators")
public class OperatorOutline  extends OperatorBase implements Serializable, EntityWithId
{
    public OperatorOutline() {}

    private static final long serialVersionUID = -1L;
}
