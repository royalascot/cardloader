package com.sportech.cl.model.rule;

import com.sportech.cl.model.rule.field.PlaceRunnerCountField;
import com.sportech.cl.model.rule.field.RaceCountField;
import com.sportech.cl.model.rule.field.RacePoolNamesField;
import com.sportech.cl.model.rule.field.RunnerCountField;

public class TemplateDefinitions {
    
    public static RuleCondition postTimeRuleCondition = new RuleCondition(RuleType.Race, RuleDefinitions.postTimeField,RelationOperatorType.GREATER, "01:00");
    public static RuleCondition poolNameRuleCondition = new RuleCondition(RuleType.SingleLegPool, new RacePoolNamesField(), RelationOperatorType.CONTAINS, "OMN");
    public static RuleCondition raceNumberRuleCondition = new RuleCondition(RuleType.MultiLegPool, new RaceCountField(), RelationOperatorType.GREATER, "6");
    public static RuleCondition runnerNumberRuleCondition = new RuleCondition(RuleType.SingleLegPool, new RunnerCountField(), RelationOperatorType.GREATER, "5");
    public static RuleCondition omnPoolCondition = new RuleCondition(RuleType.SingleLegPool, new PlaceRunnerCountField(), RelationOperatorType.GREATER, "5");
    public static RuleTemplate postTimeRule = new RuleTemplate("Race Exclution", "Post Time", postTimeRuleCondition, "RaceExclusion", null);
     
}
