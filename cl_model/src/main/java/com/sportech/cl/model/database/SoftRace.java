package com.sportech.cl.model.database;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;

import com.sportech.cl.model.database.utils.PersistentWithParent;

@Entity
@Table(name = "soft_race")
public class SoftRace implements Serializable, PersistentWithParent<SoftRace, SoftCard> {

	@ManyToOne
	@JoinColumn(name = "soft_card_fk")
	@XmlTransient
	private SoftCard parent;

	@XmlTransient
	public SoftCard getParent() {
		return parent;
	}

	public void setParent(SoftCard parent) {
		this.parent = parent;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "soft_race_pk")
	private Long id;

	@Column(name = "soft_race_no")
	private Long number;

	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "hard_race_fk")
	@XmlTransient
	private HardRace hardRace;

	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "race_id")
	@XmlTransient
	private SoftRace parentRace;

	@XmlTransient
	@Transient
	private String source;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@XmlElement(name = "softRaceNo")
	public Long getNumber() {
		return number;
	}

	public void setNumber(Long number) {
		this.number = number;
	}

	@XmlTransient
	public HardRace getHardRace() {
		return hardRace;
	}

	public void setHardRace(HardRace hardRace) {
		this.hardRace = hardRace;
	}

	@XmlTransient
	public SoftRace getParentRace() {
		return parentRace;
	}

	public void setParentRace(SoftRace parentRace) {
		this.parentRace = parentRace;
	}

	@XmlTransient
	@Transient
	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	@XmlTransient
	public HardRace getRace() {
		SoftRace b = this;
		do {
			HardRace r = b.getHardRace();
			if (r != null) {
				return r;
			}
			b = b.getParentRace();
		} while (b != null);
		return null;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((number == null) ? 0 : number.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SoftRace other = (SoftRace) obj;

		if (number != null && other.number != null) {
			return number.equals(other.number);
		}

		return false;
	}

	@Override
	public boolean equals4parent(SoftRace obj) {
		return equals(obj);
	}

	@Override
	public void copy4persistence(SoftRace from) {
		this.setHardRace(from.getHardRace());
		this.setNumber(from.getNumber());
		this.setParentRace(from.getParentRace());
	}

	@Override
	public void setParentCascade(SoftCard pt) {
		setParent(pt);
	}

	private static final long serialVersionUID = -1L;

}
