package com.sportech.cl.model.rule;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import com.sportech.cl.model.database.utils.DateUtils;

public class RuleCondition {

    static private final Logger log = Logger.getLogger(RuleCondition.class);

    private RuleField field;
    private String value;
    private String actionName;
    private RuleType ruleType;
    private LogicalOperatorType logicalOperator;
    private Set<RuleCondition> rules;
    private RelationOperatorType relationOperator;
    private String params;

    public RuleCondition() {        
    }
    
    public RuleCondition(RuleType t, RuleCondition[] r) {
        this.ruleType = t;
        this.logicalOperator = LogicalOperatorType.AND;
        rules = new HashSet<RuleCondition>();
        rules.addAll(Arrays.asList(r));
    }
    
    public RuleCondition(RuleType t, RuleField f, RelationOperatorType op, String v) {
        this.rules = null;
        this.ruleType = t;
        this.field = f;
        this.relationOperator = op;
        this.value = v;
    }
    
    public RuleField getField() {
        return field;
    }

    public void setField(RuleField field) {
        this.field = field;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public RuleType getRuleType() {
        return ruleType;
    }

    public void setRuleType(RuleType ruleType) {
        this.ruleType = ruleType;
    }

    public LogicalOperatorType getLogicalOperator() {
        return logicalOperator;
    }

    public void setLogicalOperator(LogicalOperatorType logicalOperator) {
        this.logicalOperator = logicalOperator;
    }

    public Set<RuleCondition> getRules() {
        return rules;
    }

    public void setRules(Set<RuleCondition> rules) {
        this.rules = rules;
    }

    public RelationOperatorType getRelationOperator() {
        return relationOperator;
    }

    public void setRelationOperator(RelationOperatorType relationOperator) {
        this.relationOperator = relationOperator;
    }

    public String getActionName() {
        return actionName;
    }

    public void setActionName(String actionName) {
        this.actionName = actionName;
    }

	public String getParams() {
		return params;
	}

	public void setParams(String params) {
		this.params = params;
	}

	public boolean eval(RuleContext context) {
        if (getRules() != null) {
            return evaluateContainer(context);
        }
        return evalRelation(context);
    }

    private boolean evaluateContainer(RuleContext context) {
        if (getLogicalOperator() == null) {
            setLogicalOperator(LogicalOperatorType.AND);
        }
        if (getLogicalOperator() == LogicalOperatorType.AND) {
            for (RuleCondition rule : getRules()) {
                boolean v = rule.eval(context);
                if (!v) {
                    return v;
                }
            }
            return true;
        } else {
            for (RuleCondition rule : getRules()) {
                boolean v = rule.eval(context);
                if (v) {
                    return true;
                }
            }
            return false;
        }
    }

    private boolean evalRelation(RuleContext context) {
        
        if (StringUtils.equalsIgnoreCase(getField().getFieldName(), "postTime")) {
            return applyDateTimeRule(context);
        }
        
        String ruleValue = getValue();
        Object value = getField().getValue(context);
        switch (getField().getDataType()) {
        case String:
            String stringValue = (String) value;
            switch (getRelationOperator()) {
            case CONTAINS:
                if (StringUtils.containsIgnoreCase(stringValue, ruleValue)) {
                    return true;
                }
                return false;
            default:
                return false;
            }
        case Integer:
            Integer intValue = (Integer) value;
            if (intValue == null || ruleValue == null) {
                return false;
            }
            Integer intRuleValue = Integer.parseInt(ruleValue);
            switch (getRelationOperator()) {
            case GREATER:
                if (intValue > intRuleValue) {
                    return true;
                }
                return false;
            case LESS:
                if (intValue < intRuleValue) {
                    return true;
                }
                return false;
            default:
                return false;
            }
        default:
            log.error("empty data type:" + getField().getDataType());
            return false;
        }
        
    }

    private boolean applyDateTimeRule(RuleContext context) {
        try {
            String filterValue = getValue();
            if (!StringUtils.isEmpty(filterValue)) {
                int nextDay = 0;
                if (StringUtils.contains(filterValue, "Next") && filterValue.length() >= 5) {
                    filterValue = filterValue.substring(0, 5);
                    nextDay = 1440;
                }
                DateFormat df = new SimpleDateFormat("HH:mm");
                Date result = df.parse(filterValue);
                result = DateUtils.addMinute(result, nextDay);
                Date value = (Date) getField().getValue(context);
                if (value != null) {
                    if (context.getFirstPostTime() != null) {
                        if (value.getTime() < context.getFirstPostTime().getTime()) {
                            value = DateUtils.addMinute(value, 1440);
                        }
                    }
                    if (getRelationOperator() == RelationOperatorType.GREATER) {
                        if (value.getTime() > result.getTime()) {
                            return true;
                        }
                    }
                    if (getRelationOperator() == RelationOperatorType.LESS) {
                        if (value.getTime() < result.getTime()) {
                            return true;
                        }
                    }
                }
            }
        } catch (Exception e) {
            log.error("Error applying race rule", e);
        }
        return false;
    }

}
