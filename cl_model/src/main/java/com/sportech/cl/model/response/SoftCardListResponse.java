package com.sportech.cl.model.response;

import java.util.Collection;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;

import com.sportech.cl.model.database.SoftCardOutline;

public class SoftCardListResponse extends ObjectListResponse<SoftCardOutline> 
{
    public SoftCardListResponse() {}
    public SoftCardListResponse(GenericResponse rsp)                { super(rsp); } 

    @XmlElement(name="card")
    public List<SoftCardOutline> getCards()                     { return getObjects(); }
    public void setCards(Collection<SoftCardOutline> cards)     { setObjects(cards); }
    
    private static final long serialVersionUID = -1L;
}
