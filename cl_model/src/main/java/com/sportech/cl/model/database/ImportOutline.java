package com.sportech.cl.model.database;

import javax.persistence.Entity;
import javax.persistence.Table;

import com.sportech.cl.model.database.utils.PersistentWithId;

@Entity
@Table(name="import")
public class ImportOutline extends ImportBase implements PersistentWithId<ImportOutline>, TrackAccessFilterable
{
    public static final String DATE_PROP_NAME = "timestamp";  

    public ImportOutline() {}

    @Override
    public void copy4persistence(ImportOutline from) 
    {
        super.copy(from);
    }
    
    private static final long serialVersionUID = -1L;
}
