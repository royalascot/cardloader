package com.sportech.cl.model.database;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlTransient;

import com.sportech.cl.model.database.utils.PersistentWithParent;
import com.sportech.cl.model.database.utils.Pool;

@Entity
@Table(name = "pattern_pool")
public class PatternPool extends PoolBase implements Serializable, PersistentWithParent<PatternPool, Pattern>, Pool {

	private Pattern parent;

	@ManyToOne
	@JoinColumn(name = "pattern_fk")
	@XmlTransient
	public Pattern getParent() {
		return parent;
	}

	public void setParent(Pattern parent) {
		this.parent = parent;
	}

	@Override
	public boolean equals4parent(PatternPool obj) {
		return equals(obj);
	}

	@Override
	public void setParentCascade(Pattern pt) {
		setParent(pt);
	}

	@Override
	public void copy4persistence(PatternPool from) {
		copy(from);
	}

	private static final long serialVersionUID = -1L;
}
