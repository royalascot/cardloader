package com.sportech.cl.model.database;

import java.io.Serializable;
import java.util.List;
import java.util.TimeZone;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.sportech.cl.model.database.utils.PersistentWithId;
import com.sportech.common.model.CardType;
import com.sportech.common.model.PerformanceType;

@Entity
@Table(name = "dict_track")
public class Track implements Serializable, PersistentWithId<Track>, TrackAccessFilterable, Comparable<Track> {

	private Long id;

	private String equibaseId;

	private String countryCode;

	private String name;

	private String toteTrackId;

	private Boolean active;

	private String trackCode;

	private PerformanceType perfType;

	private CardType trackType;

	private TimeZone timeZone;

	private PassThroughBag passThroughs;

	private List<Group> groups;

	private List<Tote> totes;

	private CardConfig cardTemplate;

	private Pattern defaultPattern;

	private Boolean autoGenerate;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "dict_track_pk")
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name = "equibase_id")
	public String getEquibaseId() {
		return equibaseId;
	}

	public void setEquibaseId(String equibaseId) {
		this.equibaseId = equibaseId;
	}

	@Column(name = "country_code")
	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	@Column(name = "track_code")
	public String getTrackCode() {
		return trackCode;
	}

	public void setTrackCode(String trackCode) {
		this.trackCode = trackCode;
	}

	@Column(name = "perf_type")
	public PerformanceType getPerfType() {
		return perfType;
	}

	public void setPerfType(PerformanceType perfType) {
		this.perfType = perfType;
	}

	@Column(name = "name")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "tote_track_id")
	public String getToteTrackId() {
		return toteTrackId;
	}

	public void setToteTrackId(String toteTrackId) {
		this.toteTrackId = toteTrackId;
	}

	@Column(name = "active")
	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	@Column(name = "track_type")
	public CardType getTrackType() {
		return trackType;
	}

	public void setTrackType(CardType trackType) {
		this.trackType = trackType;
	}

	@ManyToMany(mappedBy = "tracks")
	public List<Tote> getTotes() {
		return totes;
	}

	public void setTotes(List<Tote> totes) {
		this.totes = totes;
	}

	@ManyToMany(mappedBy = "tracks")
	public List<Group> getGroups() {
		return groups;
	}

	public void setGroups(List<Group> groups) {
		this.groups = groups;
	}

	@Column(name = "dict_track_pk", insertable = false, updatable = false)
	public Long getTrackId() {
		return id;
	}

	public void setTrackId(Long v) {

	}

	@Column(name = "auto_generate")
	public Boolean getAutoGenerate() {
		return autoGenerate;
	}

	public void setAutoGenerate(Boolean autoGenerate) {
		this.autoGenerate = autoGenerate;
	}

	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "pattern_id")
	public Pattern getDefaultPattern() {
		return defaultPattern;
	}

	public void setDefaultPattern(Pattern defaultPattern) {
		this.defaultPattern = defaultPattern;
	}

	@Column(name = "time_zone")
	public TimeZone getTimeZone() {
		return timeZone;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "card_template_id")
	public CardConfig getCardConfig() {
		return cardTemplate;
	}

	public void setCardConfig(CardConfig cardTemplate) {
		this.cardTemplate = cardTemplate;
	}

	public void setTimeZone(TimeZone timeZone) {
		this.timeZone = timeZone;
	}

	@Transient
	public PassThroughBag getPassThroughs() {
		return passThroughs;
	}

	public void setPassThroughs(PassThroughBag passThroughs) {
		this.passThroughs = passThroughs;
	}

	@Override
	public void copy4persistence(Track from) {
		setEquibaseId(from.getEquibaseId());
		setCountryCode(from.getCountryCode());
		setName(from.getName());
		setToteTrackId(from.getToteTrackId());
		setTrackCode(from.getTrackCode());
		setTrackType(from.getTrackType());
		setTimeZone(from.getTimeZone());
		setPerfType(from.getPerfType());
		setActive(from.getActive());
		setPassThroughs(from.getPassThroughs());
		setCardConfig(from.getCardConfig());
		setDefaultPattern(from.getDefaultPattern());
		setAutoGenerate(from.getAutoGenerate());
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((active == null) ? 0 : active.hashCode());
		result = prime * result + ((equibaseId == null) ? 0 : equibaseId.hashCode());
		result = prime * result + ((trackType == null) ? 0 : trackType.hashCode());
		result = prime * result + ((countryCode == null) ? 0 : countryCode.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Track other = (Track) obj;

		if (active == null) {
			if (other.active != null) {
				return false;
			}
		} else if (!active.equals(other.active)) {
			return false;
		}

		if (equibaseId == null) {
			if (other.equibaseId != null) {
				return false;
			}
		} else if (!equibaseId.equals(other.equibaseId)) {
			return false;
		}

		if (this.trackType == null) {
			if (other.trackType != null) {
				return false;
			}
		} else if (!trackType.equals(other.trackType)) {
			return false;
		}

		if (this.countryCode == null) {
			if (other.countryCode != null) {
				return false;
			}
		} else if (!countryCode.equals(other.countryCode)) {
			return false;
		}

		return true;
	}

	@Override
	public int compareTo(Track t) {
		if (t == null) {
			return -1;
		}
		if (getActive() == null || t.getActive() == null) {
			return -1;
		}
		int c = getActive().compareTo(t.getActive());
		if (c != 0) {
			return c;
		}

		if (getEquibaseId() == null || t.getEquibaseId() == null) {
			return -1;
		}
		c = getEquibaseId().compareTo(t.getEquibaseId());
		if (c != 0) {
			return c;
		}

		if (getTrackType() == null || t.getTrackType() == null) {
			return -1;
		}
		c = getTrackType().compareTo(t.getTrackType());
		if (c != 0) {
			return c;
		}

		if (getCountryCode() == null || t.getCountryCode() == null) {
			return -1;
		}
		return getCountryCode().compareTo(t.getCountryCode());
	}

	private static final long serialVersionUID = -1L;

}
