package com.sportech.cl.model.database;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;

@Entity
@Table(name="cards_in_use")
public class SoftCardInUse  implements Serializable
{
    @Id @Column(name="soft_card_fk")    private Long    softCardId;
    @Id @Column(name="actor_fk")        private Long    toteId;
    
    @OneToOne(cascade=CascadeType.REFRESH, fetch=FetchType.EAGER)
    @JoinColumn(name="actor_fk", insertable = false, updatable = false)
    private ToteOutline        tote;
    
    @XmlTransient
    public Long getSoftCardId()                     { return softCardId; }
    public void setSoftCardId(Long softCardId)      { this.softCardId = softCardId; }

    @XmlElement( name = "id" )
    public Long getToteId()                         { return toteId; }
    public void setToteId(Long toteId)              { this.toteId = toteId; }
    
    @XmlElement
    public String   getName()                       { return tote.getDescription(); }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result
                + ((softCardId == null) ? 0 : softCardId.hashCode());
        result = prime * result + ((toteId == null) ? 0 : toteId.hashCode());
        return result;
    }
    
    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        SoftCardInUse other = (SoftCardInUse) obj;
        if (softCardId == null)
        {
            if (other.softCardId != null)
                return false;
        } else if (!softCardId.equals(other.softCardId))
            return false;
        if (toteId == null)
        {
            if (other.toteId != null)
                return false;
        } else if (!toteId.equals(other.toteId))
            return false;
        return true;
    }

    private static final long serialVersionUID = -1L;
}
