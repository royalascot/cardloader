package com.sportech.cl.model.response;

import com.sportech.cl.model.database.Operator;

public class OperatorResponse extends ObjectResponse<Operator>
{
    public OperatorResponse() {}
    public OperatorResponse(GenericResponse rsp)   { super(rsp); }

    public Operator getOperator()                  { return getObject(); }
    public void setOperator(Operator op)           { setObject( op ); }

    private static final long serialVersionUID = -1L;
}
