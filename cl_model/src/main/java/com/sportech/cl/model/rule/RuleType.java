package com.sportech.cl.model.rule;

public enum RuleType {
    
    Race,
    
    SingleLegPool,
    
    MultiLegPool
    
}
