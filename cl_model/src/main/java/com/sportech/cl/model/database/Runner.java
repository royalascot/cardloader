package com.sportech.cl.model.database;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;

import org.apache.commons.lang3.StringUtils;

import com.sportech.cl.model.database.utils.PersistentWithParent;

@Entity
@Table(name = "runner")
public class Runner implements Serializable, PersistentWithParent<Runner, HardRace> {
	private Long id;
	private String name;
	private String shortName;
	private Long position;
	private String programNumber;
	private String jockeyName;
	private String weightCarried;
	private String weightUnits;
	private String coupleIndicator;
	private String mornlineOdds;
	private Boolean scratched;
	private Boolean alsoEligible;

	private HardRace parent;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "runner_pk")
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@ManyToOne
	@JoinColumn(name = "hard_race_fk")
	@XmlTransient
	public HardRace getParent() {
		return parent;
	}

	public void setParent(HardRace parent) {
		this.parent = parent;
	}

	@Column(name = "name")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "position")
	public Long getPosition() {
		return position;
	}

	public void setPosition(Long position) {
		this.position = position;
	}

	@Column(name = "program_number")
	public String getProgramNumber() {
		return programNumber;
	}

	public void setProgramNumber(String programNumber) {
		this.programNumber = programNumber;
	}

	@Column(name = "jockey_name")
	public String getJockeyName() {
		return jockeyName;
	}

	public void setJockeyName(String jockeyName) {
		this.jockeyName = jockeyName;
	}

	@XmlElement(name = "weightCarried")
	@Transient
	public Integer getWeightAsInteger() {
		if (StringUtils.isNotBlank(getWeightCarried())) {
			try {
				float f = Float.parseFloat(getWeightCarried());
				if (f > 0) {
					return (int) f;
				}
			} catch (Exception e) {
			}
		}
		return null;
	}

	@Column(name = "weight_carried")
	@XmlTransient
	public String getWeightCarried() {
		return weightCarried;
	}

	public void setWeightCarried(String weightCarried) {
		this.weightCarried = weightCarried;
	}

	@Column(name = "weight_units")
	public String getWeightUnits() {
		return weightUnits;
	}

	public void setWeightUnits(String weightUnits) {
		this.weightUnits = weightUnits;
	}

	@Column(name = "couple_indicator")
	public String getCoupleIndicator() {
		return coupleIndicator;
	}

	public void setCoupleIndicator(String coupleIndicator) {
		this.coupleIndicator = coupleIndicator;
	}

	@Column(name = "mornline_odds")
	public String getMornlineOdds() {
		return mornlineOdds;
	}

	public void setMornlineOdds(String mornlineOdds) {
		this.mornlineOdds = mornlineOdds;
	}

	@Column(name = "scratched")
	public Boolean getScratched() {
		return scratched;
	}

	public void setScratched(Boolean scratched) {
		this.scratched = scratched;
	}

	@Column(name = "also_eligible")
	public Boolean getAlsoEligible() {
		return alsoEligible;
	}

	public void setAlsoEligible(Boolean alsoEligible) {
		this.alsoEligible = alsoEligible;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((position == null) ? 0 : position.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Runner other = (Runner) obj;

		if (id != null && other.id != null)
			return id.equals(other.id);

		if (position != null && other.position != null)
			return position.equals(other.position);

		return false;
	}

	@Override
	public boolean equals4parent(Runner obj) {
		return equals(obj);
	}

	@Override
	public void copy4persistence(Runner from) {
		this.setName(from.getName());
		this.setShortName(from.getShortName());
		this.setPosition(from.getPosition());
		this.setProgramNumber(from.getProgramNumber());
		this.setJockeyName(from.getJockeyName());
		this.setWeightCarried(from.getWeightCarried());
		this.setWeightUnits(from.getWeightUnits());
		this.setCoupleIndicator(from.getCoupleIndicator());
		this.setMornlineOdds(from.getMornlineOdds());
		this.setScratched(from.getScratched());
		this.setAlsoEligible(from.getAlsoEligible());
	}

	@Override
	public void setParentCascade(HardRace pt) {
		setParent(pt);
	}

	@Column(name = "short_name")
	public String getShortName() {
		return shortName;
	}

	public void setShortName(String shortName) {
		this.shortName = shortName;
	}

	private static final long serialVersionUID = -1L;

}
