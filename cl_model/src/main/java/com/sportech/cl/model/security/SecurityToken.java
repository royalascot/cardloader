package com.sportech.cl.model.security;

import java.io.Serializable;
import java.util.TimeZone;

import com.sportech.cl.model.database.Role;
import com.sportech.cl.model.database.User;

public class SecurityToken implements Serializable {
	private String loginName = null;
	private Integer userId = null;
	private Integer actorTypeId = null;
	private Integer actorId = null;
	private Integer roleId = null;
	private TimeZone timeZone = null;

	private User user;

	public SecurityToken() {
	}

	public SecurityToken(User user) {
		this.user = user;
		this.timeZone = TimeZone.getDefault();
		if (user.getTimeZone() != null) {
			this.timeZone = user.getTimeZone();
		}
		this.loginName = user.getIdentifier();
		this.userId = user.getId().intValue();
		this.actorTypeId = user.getActorTypeId();
		this.actorId = user.getActorId();
		this.roleId = user.getRoleId();
	}

	public SecurityToken(String loginName, Integer userId, ActorType actorType, Integer actorId, Integer roleId) {
		this.loginName = loginName;
		this.userId = userId;
		this.actorTypeId = actorType.getId();
		this.actorId = actorId;
		this.roleId = roleId;
	}

	public SecurityToken(String loginName, Integer userId, Integer actorTypeId, Integer actorId, Integer roleId) {
		this.loginName = loginName;
		this.userId = userId;
		this.actorTypeId = actorTypeId;
		this.actorId = actorId;
		this.roleId = roleId;
	}

	@Override
	public String toString() {
		return "SecurityToken [loginName=" + loginName + ", userId=" + userId + ", actorTypeId=" + actorTypeId + ", actorId="
		        + actorId + ", roleId=" + roleId + "]";
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getLoginName() {
		return loginName;
	}

	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer actorId) {
		this.userId = actorId;
	}

	public Integer getActorTypeId() {
		return actorTypeId;
	}

	public void setActorTypeId(Integer actorTypeId) {
		this.actorTypeId = actorTypeId;
	}

	public Integer getActorId() {
		return actorId;
	}

	public void setActorId(Integer actorId) {
		this.actorId = actorId;
	}

	public Integer getRoleId() {
		return roleId;
	}

	public void setRoleId(Integer roleId) {
		this.roleId = roleId;
	}

	public TimeZone getTimeZone() {
		return timeZone;
	}

	public void setTimeZone(TimeZone timeZone) {
		this.timeZone = timeZone;
	}

	public static boolean isAdmin(SecurityToken token) {
		if (token == null) {
			return false;
		}
		User user = token.getUser();
		if (user == null || user.getRoles() == null) {
			return false;
		}
		for (Role r : user.getRoles()) {
			if ("Administrator".equals(r.getName())) {
				return true;
			}
		}
		return false;
	}

	public static SecurityToken getDummyToken() {
		SecurityToken st = new SecurityToken();
		st.setActorId(1);
		st.setActorTypeId(2);
		st.setLoginName("admin");
		st.setRoleId(1);
		st.setUserId(1);
		return st;
	}
	
	private static final long serialVersionUID = -1L;
}
