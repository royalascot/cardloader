package com.sportech.cl.services.dictionaries;

import org.apache.log4j.Logger;

import com.sportech.cl.global.utils.beans.BeanLocatorHelper;
import com.sportech.cl.global.utils.config.ClConfig;

public class TracksRemoteLocator 
{
    private static final String JNDI_NAME= BeanLocatorHelper.JNDI_NAME_PREFIX + "TracksBean!com.sportech.cl.services.dictionaries.TracksRemote";
    
    private static TracksRemote  iface = null;
    
    public static TracksRemote   getRemoteIface(ClConfig cfg)
    {
        if( iface == null ) {
            try {
                iface = (TracksRemote)BeanLocatorHelper.getInitialContext(cfg).lookup(JNDI_NAME);
            }
            catch( Exception e ) {
                Logger.getLogger(TracksRemoteLocator.class).error("Can't locate remote interface : ", e);
            }
        }
    
        return iface;
    }
}
