package com.sportech.cl.services.common;

import com.sportech.cl.model.response.VersionResponse;

public interface VersionsInterface
{
    public VersionResponse getVersions();
}
