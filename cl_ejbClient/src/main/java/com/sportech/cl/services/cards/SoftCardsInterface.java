package com.sportech.cl.services.cards;

import java.util.Date;

import com.sportech.cl.model.common.PageParams;
import com.sportech.cl.model.database.SoftCard;
import com.sportech.cl.model.database.SoftCardOutline;
import com.sportech.cl.model.response.AddEntityResponse;
import com.sportech.cl.model.response.GenericResponse;
import com.sportech.cl.model.response.SoftCardListResponse;
import com.sportech.cl.model.response.SoftCardResponse;
import com.sportech.cl.model.security.SecurityToken;

public interface SoftCardsInterface
{
    public SoftCardResponse         get(SecurityToken token, Long id);
    public SoftCardListResponse     find(SecurityToken token, SoftCardOutline[] criteria, Date startDate, Date endDate, Boolean useLike, PageParams pages);
    public AddEntityResponse        add(SecurityToken token, SoftCard card);
    public GenericResponse          update(SecurityToken token, Long id, SoftCard new_card);
    public GenericResponse          delete(SecurityToken token, Long id);
    public GenericResponse          approve(SecurityToken token, Long id, Boolean approved);

    public AddEntityResponse        createFromHardCard(SecurityToken token, Long hardCardId, Long patternId, Integer mergeTypeId, Boolean onlyPatternRaces, boolean withPublish);
}
