package com.sportech.cl.services.tote;

import com.sportech.cl.model.response.GenericResponse;
import com.sportech.cl.model.response.ToteCardListResponse;
import com.sportech.cl.model.response.ToteCardResponse;
import com.sportech.cl.model.security.SecurityToken;

public interface ToteInterface
{
    public ToteCardListResponse     findCurrentCards(SecurityToken token);
    public ToteCardResponse         getCard(SecurityToken token, Long id);
    public GenericResponse          setCardInUse(SecurityToken token, Long id, Boolean isInUse );
}
