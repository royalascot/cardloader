package com.sportech.cl.services.security;

import javax.ejb.Remote;

@Remote
public interface AuthenticateRemote extends AuthenticateInterface 
{
	public static final String JNDI_NAME="cl/ejb_cl/AuthenticateBean!com.sportech.cl.services.security.AuthenticateRemote";
}
