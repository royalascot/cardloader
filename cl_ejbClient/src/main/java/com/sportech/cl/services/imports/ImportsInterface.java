package com.sportech.cl.services.imports;

import java.util.Date;

import com.sportech.cl.model.common.PageParams;
import com.sportech.cl.model.database.ImportOutline;
import com.sportech.cl.model.response.ImportListResponse;
import com.sportech.cl.model.response.ImportResponse;
import com.sportech.cl.model.response.StringResponse;
import com.sportech.cl.model.security.SecurityToken;

public interface ImportsInterface {
    
    public StringResponse getImportLog(SecurityToken token, Long id);

    public ImportResponse          get(SecurityToken token, Long id);
    public ImportListResponse      find(SecurityToken token, ImportOutline[] query, Date startDate, Date endDate, Boolean useLike, PageParams pages);

}
