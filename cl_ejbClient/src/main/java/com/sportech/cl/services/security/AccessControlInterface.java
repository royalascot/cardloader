package com.sportech.cl.services.security;

import java.util.List;

import com.sportech.cl.model.database.Ace;
import com.sportech.cl.model.response.AceListResponse;
import com.sportech.cl.model.response.GenericResponse;
import com.sportech.cl.model.security.ObjectType;
import com.sportech.cl.model.security.SecurityToken;

public interface AccessControlInterface {
    
    // Internal 
    public void SetCreator(SecurityToken token, ObjectType objType, Long objId);
    public boolean HasPrivilege(SecurityToken token, ObjectType objType, Long objId, Long accessMask);
    public void CreateDefaultEntries(ObjectType objType, Long objId);

    // Public
    public AceListResponse      getObjectACL(SecurityToken token, Integer objTypeId, Long objId);
    public GenericResponse      setObjectACL(SecurityToken token, Integer objTypeId, Long objId, List<Ace> acl );
    public AceListResponse      getObjectTypeACL(SecurityToken token, Integer objTypeId);
    public GenericResponse      setObjectTypeACL(SecurityToken token, Integer objTypeId, List<Ace> acl );
    public AceListResponse      getDefaultACL(SecurityToken token, Integer objTypeId);
    public GenericResponse      setDefaultACL(SecurityToken token, Integer objTypeId, List<Ace> acl );
}
