package com.sportech.cl.services.cards;

import org.apache.log4j.Logger;

import com.sportech.cl.global.utils.beans.BeanLocatorHelper;
import com.sportech.cl.global.utils.config.ClConfig;

public class SoftCardsRemoteLocator 
{
    private static final String JNDI_NAME= BeanLocatorHelper.JNDI_NAME_PREFIX + "SoftCardsBean!com.sportech.cl.services.cards.SoftCardsRemote";
    
    private static SoftCardsRemote  iface = null;
    
    public static SoftCardsRemote   getRemoteIface(ClConfig cfg)
    {
        if( iface == null ) {
            try {
                iface = (SoftCardsRemote)BeanLocatorHelper.getInitialContext(cfg).lookup(JNDI_NAME);
            }
            catch( Exception e ) {
                Logger.getLogger(SoftCardsRemoteLocator.class).error("Can't locate remote interface : ", e);
            }
        }
    
        return iface;
    }
}
