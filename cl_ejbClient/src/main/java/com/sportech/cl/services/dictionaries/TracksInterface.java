package com.sportech.cl.services.dictionaries;

import java.util.List;

import com.sportech.cl.model.common.PageParams;
import com.sportech.cl.model.database.Pattern;
import com.sportech.cl.model.database.Track;
import com.sportech.cl.model.response.AddEntityResponse;
import com.sportech.cl.model.response.GenericResponse;
import com.sportech.cl.model.response.TrackListResponse;
import com.sportech.cl.model.security.SecurityToken;

public interface TracksInterface
{
    public Track                 get(Long id);
    public TrackListResponse     find(SecurityToken token, Track[] criteria, Boolean useLike, PageParams pages);
    public AddEntityResponse     add(SecurityToken token, Track track);
    public GenericResponse       update(SecurityToken token, Long id, Track new_track);
    public GenericResponse       enable(SecurityToken token, Long id, Boolean enabled);
    public GenericResponse       delete(SecurityToken token, Long id);
    public List<Pattern>         getTrackPatterns(Track track);
}
