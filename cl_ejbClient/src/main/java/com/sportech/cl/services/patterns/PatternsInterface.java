package com.sportech.cl.services.patterns;


import com.sportech.cl.model.security.SecurityToken;
import com.sportech.cl.model.response.PatternResponse;
import com.sportech.cl.model.response.PatternListResponse;
import com.sportech.cl.model.response.PatternUsedListResponse;
import com.sportech.cl.model.response.AddEntityResponse;
import com.sportech.cl.model.response.GenericResponse;

import com.sportech.cl.model.common.PageParams;
import com.sportech.cl.model.database.Pattern;
import com.sportech.cl.model.database.PatternOutline;

public interface PatternsInterface {

	public PatternResponse		getPattern(SecurityToken token, Long id);
	public AddEntityResponse	addPattern(SecurityToken token, Pattern pattern);
	public GenericResponse		updatePattern(SecurityToken token, Long id, Pattern pattern);
	public GenericResponse		deletePattern(SecurityToken token, Long id);
    public PatternListResponse  find(SecurityToken token, PatternOutline[] criteria, Boolean useLike, PageParams pages);
    public PatternUsedListResponse  find4HardCard(SecurityToken token, Long hardCardId);
}
