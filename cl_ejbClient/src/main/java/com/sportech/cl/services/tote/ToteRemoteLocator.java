package com.sportech.cl.services.tote;

import org.apache.log4j.Logger;

import com.sportech.cl.global.utils.beans.BeanLocatorHelper;
import com.sportech.cl.global.utils.config.ClConfig;

public class ToteRemoteLocator 
{
    private static final String JNDI_NAME= BeanLocatorHelper.JNDI_NAME_PREFIX + "ToteBean!com.sportech.cl.services.tote.ToteRemote";
    
    private static ToteRemote  iface = null;
    
    public static ToteRemote   getRemoteIface(ClConfig cfg)
    {
        if( iface == null ) {
            try {
                iface = (ToteRemote)BeanLocatorHelper.getInitialContext(cfg).lookup(JNDI_NAME);
            }
            catch( Exception e ) {
                Logger.getLogger(ToteRemoteLocator.class).error("Can't locate remote interface : ", e);
            }
        }
    
        return iface;
    }
}
