package com.sportech.cl.services.cards;

import org.apache.log4j.Logger;

import com.sportech.cl.global.utils.beans.BeanLocatorHelper;
import com.sportech.cl.global.utils.config.ClConfig;

public class HardCardsRemoteLocator 
{
	private static final String JNDI_NAME= BeanLocatorHelper.JNDI_NAME_PREFIX + "HardCardsBean!com.sportech.cl.services.cards.HardCardsRemote";
	
	private static HardCardsRemote	iface = null;
	
	public static HardCardsRemote	getRemoteIface(ClConfig cfg)
	{
		if( iface == null ) {
			try {
			    iface = (HardCardsRemote)BeanLocatorHelper.getInitialContext(cfg).lookup(JNDI_NAME);
			}
			catch( Exception e ) {
				Logger.getLogger(HardCardsRemoteLocator.class).error("Can't locate remote interface : ", e);
			}
		}
	
		return iface;
	}
}
