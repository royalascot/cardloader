package com.sportech.cl.services.dictionaries;

import com.sportech.cl.model.common.PageParams;
import com.sportech.cl.model.database.Tote;
import com.sportech.cl.model.database.ToteOutline;
import com.sportech.cl.model.response.AddEntityResponse;
import com.sportech.cl.model.response.GenericResponse;
import com.sportech.cl.model.response.ToteListResponse;
import com.sportech.cl.model.response.ToteResponse;
import com.sportech.cl.model.security.SecurityToken;

public interface TotesInterface
{
    public ToteResponse          get(SecurityToken token, Long id);
    public ToteListResponse      find(SecurityToken token, ToteOutline[] criteria, Boolean useLike, PageParams pages);
    public AddEntityResponse     add(SecurityToken token, Tote tote, String username, String password);
    public GenericResponse       update(SecurityToken token, Long id, Tote new_tote);
    public GenericResponse       enable(SecurityToken token, Long id, Boolean enabled);
    public GenericResponse       delete(SecurityToken token, Long id);
    public GenericResponse       saveTote(Tote tote);
}
