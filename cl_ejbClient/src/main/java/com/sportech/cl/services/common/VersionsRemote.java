package com.sportech.cl.services.common;

import javax.ejb.Remote;

@Remote
public interface VersionsRemote extends VersionsInterface
{
}
