package com.sportech.cl.services.dictionaries;

import java.util.List;

import com.sportech.cl.model.database.CardConfig;

public interface PoolTypesInterface
{
    
    public List<CardConfig>  find(CardConfig filter);
    public CardConfig		get(Long id);
    public CardConfig		save(CardConfig item);
	public boolean			delete(Long id);
    
}

