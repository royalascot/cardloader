package com.sportech.cl.services.security;

public interface AccessControlRemote extends AccessControlInterface {
    public static final String JNDI_NAME="cl/ejb_cl/AccessControlBean!com.sportech.cl.services.security.AccessControlRemote";
}
