package com.sportech.cl.services.security;

import org.apache.log4j.Logger;

import com.sportech.cl.global.utils.beans.BeanLocatorHelper;
import com.sportech.cl.global.utils.config.ClConfig;

public class AuthenticateRemoteLocator {
	private static final String JNDI_NAME= BeanLocatorHelper.JNDI_NAME_PREFIX + "AuthenticateBean!com.sportech.cl.services.security.AuthenticateRemote";
	
	private static AuthenticateRemote	iface = null;
	
	public static AuthenticateRemote	getRemoteIface(ClConfig cfg)
	{
		if( iface == null ) {
			try {
			    iface = (AuthenticateRemote)BeanLocatorHelper.getInitialContext(cfg).lookup(JNDI_NAME);
			}
			catch( Exception e ) {
				Logger.getLogger(AuthenticateRemoteLocator.class).error("Can't locate remote interface : ", e);
			}
		}
	
		return iface;
	}
}
