package com.sportech.cl.services.cards;

import java.util.Date;

import com.sportech.cl.model.common.PageParams;
import com.sportech.cl.model.database.HardCard;
import com.sportech.cl.model.database.HardCardOutline;
import com.sportech.cl.model.response.AddEntityResponse;
import com.sportech.cl.model.response.GenericResponse;
import com.sportech.cl.model.response.HardCardListResponse;
import com.sportech.cl.model.response.HardCardResponse;
import com.sportech.cl.model.security.SecurityToken;

public interface HardCardsInterface {
	public HardCardResponse			get(SecurityToken token, Long id);
	public HardCardListResponse		find(SecurityToken token, HardCardOutline[] criteria, Date startDate, Date endDate, Boolean useLike, PageParams pages);
	public AddEntityResponse		add(SecurityToken token, HardCard card);
	public GenericResponse			update(SecurityToken token, Long id, HardCard new_card);
	public GenericResponse			delete(SecurityToken token, Long id);
    public GenericResponse       	approve(SecurityToken token, Long id, Boolean approved);
    public int	 					generateSoftCards(Long id);
    public String					generateRif(Date date);

}
