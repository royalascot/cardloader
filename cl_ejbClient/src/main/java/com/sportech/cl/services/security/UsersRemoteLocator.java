package com.sportech.cl.services.security;

import org.apache.log4j.Logger;

import com.sportech.cl.global.utils.beans.BeanLocatorHelper;
import com.sportech.cl.global.utils.config.ClConfig;

public class UsersRemoteLocator 
{
    private static final String JNDI_NAME= BeanLocatorHelper.JNDI_NAME_PREFIX + "UsersBean!com.sportech.cl.services.security.UsersRemote";
    
    private static UsersRemote  iface = null;
    
    public static UsersRemote   getRemoteIface(ClConfig cfg)
    {
        if( iface == null ) {
            try {
                iface = (UsersRemote)BeanLocatorHelper.getInitialContext(cfg).lookup(JNDI_NAME);
            }
            catch( Exception e ) {
                Logger.getLogger(UsersRemoteLocator.class).error("Can't locate remote interface : ", e);
            }
        }
    
        return iface;
    }
}
