package com.sportech.cl.services.imports;

import com.sportech.cl.model.database.Import;
import com.sportech.cl.model.response.AddEntityResponse;
import com.sportech.cl.model.response.StringResponse;
import com.sportech.cl.model.security.SecurityToken;

public interface ImportsHelperInterface {
    public AddEntityResponse addImportSource(SecurityToken token, Import theImport);
    public StringResponse getImportStatus(SecurityToken token, Long id);
    
}
