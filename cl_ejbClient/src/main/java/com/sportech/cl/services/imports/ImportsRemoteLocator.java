package com.sportech.cl.services.imports;

import org.apache.log4j.Logger;

import com.sportech.cl.global.utils.beans.BeanLocatorHelper;
import com.sportech.cl.global.utils.config.ClConfig;

public class ImportsRemoteLocator {
	private static final String JNDI_NAME= BeanLocatorHelper.JNDI_NAME_PREFIX + "ImportsBean!com.sportech.cl.services.imports.ImportsRemote";
	
	private static ImportsRemote	iface = null;
	
	public static ImportsRemote	getRemoteIface(ClConfig cfg)
	{
		if( iface == null ) {
			try {
			    iface = (ImportsRemote)BeanLocatorHelper.getInitialContext(cfg).lookup(JNDI_NAME);
			}
			catch( Exception e ) {
				Logger.getLogger(ImportsRemoteLocator.class).error("Can't locate remote interface : ", e);
			}
		}
		
		return iface;
	}
}
