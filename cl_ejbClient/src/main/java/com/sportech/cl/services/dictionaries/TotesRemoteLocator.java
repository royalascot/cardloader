package com.sportech.cl.services.dictionaries;

import org.apache.log4j.Logger;

import com.sportech.cl.global.utils.beans.BeanLocatorHelper;
import com.sportech.cl.global.utils.config.ClConfig;

public class TotesRemoteLocator 
{
    private static final String JNDI_NAME= BeanLocatorHelper.JNDI_NAME_PREFIX + "TotesBean!com.sportech.cl.services.dictionaries.TotesRemote";
    
    private static TotesRemote  iface = null;
    
    public static TotesRemote   getRemoteIface(ClConfig cfg)
    {
        if( iface == null ) {
            try {
                iface = (TotesRemote)BeanLocatorHelper.getInitialContext(cfg).lookup(JNDI_NAME);
            }
            catch( Exception e ) {
                Logger.getLogger(TotesRemoteLocator.class).error("Can't locate remote interface : ", e);
            }
        }
    
        return iface;
    }
}
