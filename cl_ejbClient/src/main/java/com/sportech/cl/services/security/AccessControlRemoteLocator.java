package com.sportech.cl.services.security;

import com.sportech.cl.global.utils.beans.BeanLocatorHelper;
import com.sportech.cl.global.utils.config.ClConfig;
import org.apache.log4j.Logger;

public class AccessControlRemoteLocator {
    private static final String JNDI_NAME= BeanLocatorHelper.JNDI_NAME_PREFIX + "AccessControlBean!com.sportech.cl.services.security.AccessControlRemote";
    
    private static AccessControlRemote  iface = null;
    
    public static AccessControlRemote   getRemoteIface(ClConfig cfg)
    {
        if( iface == null ) {
            try {
                iface = (AccessControlRemote)BeanLocatorHelper.getInitialContext(cfg).lookup(JNDI_NAME);
            }
            catch( Exception e ) {
                Logger.getLogger(AccessControlRemoteLocator.class).error("Can't locate remote interface : ", e);
            }
        }
    
        return iface;
    }
}
