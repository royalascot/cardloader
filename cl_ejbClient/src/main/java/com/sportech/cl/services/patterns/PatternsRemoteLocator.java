package com.sportech.cl.services.patterns;

import org.apache.log4j.Logger;

import com.sportech.cl.global.utils.beans.BeanLocatorHelper;
import com.sportech.cl.global.utils.config.ClConfig;

public class PatternsRemoteLocator {
	private static final String JNDI_NAME= BeanLocatorHelper.JNDI_NAME_PREFIX + "PatternsBean!com.sportech.cl.services.patterns.PatternsRemote";
	
	private static PatternsRemote	iface = null;
	
	public static PatternsRemote	getRemoteIface(ClConfig cfg)
	{
		if( iface == null ) {
			try {
			    iface = (PatternsRemote)BeanLocatorHelper.getInitialContext(cfg).lookup(JNDI_NAME);
			}
			catch( Exception e ) {
				Logger.getLogger(PatternsRemoteLocator.class).error("Can't locate remote interface : ", e);
			}
		}
	
		return iface;
	}
	
}
