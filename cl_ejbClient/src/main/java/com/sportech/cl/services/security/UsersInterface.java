package com.sportech.cl.services.security;

import java.util.List;

import com.sportech.cl.model.common.PageParams;
import com.sportech.cl.model.database.Group;
import com.sportech.cl.model.database.Role;
import com.sportech.cl.model.database.Tote;
import com.sportech.cl.model.database.User;
import com.sportech.cl.model.response.AddEntityResponse;
import com.sportech.cl.model.response.GenericResponse;
import com.sportech.cl.model.response.ObjectResponse;
import com.sportech.cl.model.security.SecurityToken;

public interface UsersInterface {

	public GenericResponse setUserPassword(SecurityToken token, String username, String password);

	public List<User> find(SecurityToken token, User[] query, Boolean useLike, PageParams pages);
	
	public List<Group> find(SecurityToken token, Group[] query, Boolean useLike, PageParams pages);
	
	public List<Role> find(SecurityToken token, Role[] query, Boolean useLike, PageParams pages);
	
	public AddEntityResponse add(SecurityToken token, User user);

	public ObjectResponse<User> get(SecurityToken token, Long id);

	public GenericResponse update(SecurityToken token, Long id, User new_user);
	
	public GenericResponse delete(SecurityToken token, Long id);
	
	public Group getGroup(Long id);
	
	public User getUser(Long id);
	
	public GenericResponse saveGroup(Group group);
	
	public GenericResponse deleteGroup(Long id);
	
	public List<Tote> getAllTotes();

}
