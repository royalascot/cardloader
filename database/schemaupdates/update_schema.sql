ALTER TABLE `card_loader`.`hard_race` 
ADD COLUMN `distance_text` VARCHAR(50) NULL AFTER `perf_type`,
ADD COLUMN `race_code` VARCHAR(50) NULL AFTER `distance_text`,
ADD COLUMN `handicap` INT NULL AFTER `race_code`;

ALTER TABLE `card_loader`.`hard_card` 
ADD COLUMN `feature_race` INT NULL AFTER `settings`;

ALTER TABLE `card_loader`.`soft_card` 
ADD COLUMN `feature_race` INT NULL AFTER `first_race_time`;

ALTER TABLE `card_loader`.`runner` 
ADD COLUMN `short_name` VARCHAR(45) NULL AFTER `also_eligible`;

-- 12/11/14 Adding guarantee fields.

ALTER TABLE `card_loader`.`hard_pool` 
ADD COLUMN `guarantee` DECIMAL(10,2) NULL AFTER `tote_pool_type`;

ALTER TABLE `card_loader`.`soft_pool` 
ADD COLUMN `guarantee` DECIMAL(10,2) NULL AFTER `tote_pool_type`;

ALTER TABLE `card_loader`.`pattern_pool` 
ADD COLUMN `guarantee` DECIMAL(10,2) NULL AFTER `tote_pool_type`;

ALTER TABLE `card_loader`.`soft_race` 
ADD COLUMN `race_id` INT NULL AFTER `soft_race_no`;

ALTER TABLE `card_loader`.`soft_race` 
DROP FOREIGN KEY `fk_soft_race_1`;
ALTER TABLE `card_loader`.`soft_race` 
CHANGE COLUMN `hard_race_fk` `hard_race_fk` INT(11) NULL ;
ALTER TABLE `card_loader`.`soft_race` 
ADD CONSTRAINT `fk_soft_race_1`
  FOREIGN KEY (`hard_race_fk`)
  REFERENCES `card_loader`.`hard_race` (`hard_race_pk`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

ALTER TABLE `card_loader`.`soft_card` 
ADD COLUMN `source` VARCHAR(50) NULL AFTER `feature_race`;

CREATE or REPLACE VIEW v_soft_card_outline AS 
SELECT
    sc.soft_card_pk 				as soft_card_pk
   ,sc.description				as description
   ,dt.dict_track_pk			as dict_track_fk
   ,dt.name 					as track_name
   ,dt.equibase_id				as track_equibase_id
   ,dt.country_code				as track_country_code
   ,sc.sparse_races 			as sparse_races
   ,sc.sparse_pools				as sparse_pools
   ,sc.load_code				as load_code
   ,sc.pattern_fk				as pattern_fk
   ,sc.card_type				as card_type
   ,sc.perf_type				as perf_type
   ,sc.first_race_time          as first_race_time
   ,pat.description				as pattern_description
   ,sc.approved					as approved
   ,sc.hard_card_fk				as hard_card_fk
   ,sc.card_date				as card_date
   ,sc.download_list			as download_list
   ,sc.access_list		        as access_list
   ,sc.tote_approval_list		as tote_approval_list
   ,sc.feature_race			    as feature_race
   ,sc.source					as source
   ,IFNULL(rc.race_count,0)			as race_count
   ,IFNULL(inuse.in_use_count,0)		as in_use_count
FROM 
    soft_card sc left outer join hard_card hc on sc.hard_card_fk = hc.hard_card_pk
       left outer join dict_track dt on hc.dict_track_fk = dt.dict_track_pk
          left outer join v_utl_soft_card_race_count rc on sc.soft_card_pk = rc.soft_card_fk
             left outer join v_utl_soft_card_in_use_count inuse on sc.soft_card_pk = inuse.soft_card_fk
                left outer join pattern pat on sc.pattern_fk = pat.pattern_pk
;


ALTER TABLE `card_loader`.`hard_pool` 
ADD COLUMN `bonus_guarantee` DECIMAL(10,2) NULL AFTER `guarantee`,
ADD COLUMN `brought_forward` DECIMAL(10,2) NULL AFTER `bonus_guarantee`,
ADD COLUMN `bonus_brought_forward` DECIMAL(10,2) NULL AFTER `brought_forward`,
ADD COLUMN `status` VARCHAR(50) NULL AFTER `bonus_brought_forward`,
ADD COLUMN `force_full_collation` TINYINT NULL AFTER `status`,
ADD COLUMN `fractional_betting` TINYINT NULL AFTER `force_full_collation`,
ADD COLUMN `force_willpay` TINYINT NULL AFTER `fractional_betting`,
ADD COLUMN `itsp_allowed` TINYINT NULL AFTER `force_willpay`,
ADD COLUMN `norminated` TINYINT NULL AFTER `itsp_allowed`;

ALTER TABLE `card_loader`.`soft_pool` 
ADD COLUMN `bonus_guarantee` DECIMAL(10,2) NULL AFTER `guarantee`,
ADD COLUMN `brought_forward` DECIMAL(10,2) NULL AFTER `bonus_guarantee`,
ADD COLUMN `bonus_brought_forward` DECIMAL(10,2) NULL AFTER `brought_forward`,
ADD COLUMN `status` VARCHAR(50) NULL AFTER `bonus_brought_forward`,
ADD COLUMN `force_full_collation` TINYINT NULL AFTER `status`,
ADD COLUMN `fractional_betting` TINYINT NULL AFTER `force_full_collation`,
ADD COLUMN `force_willpay` TINYINT NULL AFTER `fractional_betting`,
ADD COLUMN `itsp_allowed` TINYINT NULL AFTER `force_willpay`,
ADD COLUMN `norminated` TINYINT NULL AFTER `itsp_allowed`;

ALTER TABLE `card_loader`.`pattern_pool` 
ADD COLUMN `bonus_guarantee` DECIMAL(10,2) NULL AFTER `guarantee`,
ADD COLUMN `brought_forward` DECIMAL(10,2) NULL AFTER `bonus_guarantee`,
ADD COLUMN `bonus_brought_forward` DECIMAL(10,2) NULL AFTER `brought_forward`,
ADD COLUMN `status` VARCHAR(50) NULL AFTER `bonus_brought_forward`,
ADD COLUMN `force_full_collation` TINYINT NULL AFTER `status`,
ADD COLUMN `fractional_betting` TINYINT NULL AFTER `force_full_collation`,
ADD COLUMN `force_willpay` TINYINT NULL AFTER `fractional_betting`,
ADD COLUMN `itsp_allowed` TINYINT NULL AFTER `force_willpay`,
ADD COLUMN `norminated` TINYINT NULL AFTER `itsp_allowed`;


ALTER TABLE `card_loader`.`dict_track` 
ADD COLUMN `pattern_id` INT NULL AFTER `card_template_id`;
ALTER TABLE `card_loader`.`dict_track` 
ADD COLUMN `auto_generate` TINYINT NULL AFTER `pattern_id`;

ALTER TABLE `card_loader`.`hard_race` 
ADD COLUMN `clone_source` VARCHAR(45) NULL AFTER `handicap`;


ALTER TABLE `card_loader`.`hard_race` 
ADD COLUMN `number_positions` INT NULL AFTER `clone_source`;

ALTER TABLE `card_loader`.`hard_pool` 
DROP FOREIGN KEY `fk_hard_pool_2`;
ALTER TABLE `card_loader`.`soft_pool` 
DROP FOREIGN KEY `fk_soft_pool_map_2`;
ALTER TABLE `card_loader`.`pattern_pool` 
DROP FOREIGN KEY `fk_pattern_pool_2`;

-- Do not drop it. Old branches need this table.
-- DROP TABLE `card_loader`.`dict_pool`;


ALTER TABLE `card_loader`.`hard_card` 
ADD COLUMN `config_id` INT NULL AFTER `feature_race`;

ALTER TABLE `card_loader`.`soft_card` 
ADD COLUMN `config_id` INT NULL AFTER `source`;

CREATE 
    ALGORITHM = UNDEFINED 
    DEFINER = `root`@`localhost` 
    SQL SECURITY DEFINER
VIEW `v_soft_card_outline` AS
    select 
        `sc`.`soft_card_pk` AS `soft_card_pk`,
        `sc`.`description` AS `description`,
        `dt`.`dict_track_pk` AS `dict_track_fk`,
        `dt`.`name` AS `track_name`,
        `dt`.`equibase_id` AS `track_equibase_id`,
        `dt`.`country_code` AS `track_country_code`,
        `sc`.`sparse_races` AS `sparse_races`,
        `sc`.`sparse_pools` AS `sparse_pools`,
        `sc`.`load_code` AS `load_code`,
        `sc`.`pattern_fk` AS `pattern_fk`,
        `sc`.`card_type` AS `card_type`,
        `sc`.`perf_type` AS `perf_type`,
        `sc`.`first_race_time` AS `first_race_time`,
        `pat`.`description` AS `pattern_description`,
        `sc`.`approved` AS `approved`,
        `sc`.`hard_card_fk` AS `hard_card_fk`,
        `sc`.`card_date` AS `card_date`,
        `sc`.`download_list` AS `download_list`,
        `sc`.`access_list` AS `access_list`,
        `sc`.`tote_approval_list` AS `tote_approval_list`,
        `sc`.`feature_race` AS `feature_race`,
        `sc`.`source` AS `source`,
	    `sc`.`config_id` AS `config_id`,
        ifnull(`rc`.`race_count`, 0) AS `race_count`,
        ifnull(`inuse`.`in_use_count`, 0) AS `in_use_count`
    from
        (((((`soft_card` `sc`
        left join `hard_card` `hc` ON ((`sc`.`hard_card_fk` = `hc`.`hard_card_pk`)))
        left join `dict_track` `dt` ON ((`hc`.`dict_track_fk` = `dt`.`dict_track_pk`)))
        left join `v_utl_soft_card_race_count` `rc` ON ((`sc`.`soft_card_pk` = `rc`.`soft_card_fk`)))
        left join `v_utl_soft_card_in_use_count` `inuse` ON ((`sc`.`soft_card_pk` = `inuse`.`soft_card_fk`)))
        left join `pattern` `pat` ON ((`sc`.`pattern_fk` = `pat`.`pattern_pk`)))

		
ALTER TABLE `card_loader`.`soft_card` 
ADD INDEX `fk_config_idx` (`config_id` ASC);
ALTER TABLE `card_loader`.`soft_card` 
ADD CONSTRAINT `fk_config`
  FOREIGN KEY (`config_id`)
  REFERENCES `card_loader`.`card_template` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;
  
ALTER TABLE `card_loader`.`hard_card` 
ADD INDEX `fk_host_card_config_idx` (`config_id` ASC);
ALTER TABLE `card_loader`.`hard_card` 
ADD CONSTRAINT `fk_host_card_config`
  FOREIGN KEY (`config_id`)
  REFERENCES `card_loader`.`card_template` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

ALTER TABLE `card_loader`.`card_template` 
ADD UNIQUE INDEX `idx_card_template_unique_name` (`name` ASC);
  

		