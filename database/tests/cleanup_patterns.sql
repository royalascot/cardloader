SET @var = 
'RUNP,
ARANMNQ,
AAUP,
BAUP,
CBYPNQ,
CRCNO,
DELNA,
FILAPT,
FMTNMNQ,
FMTNQ,
FPKOU,
GLFNS,
GLFNSR,
INYNQ,
MW8,
PIMNO,
PHAUPT,
SACNMU,
STBNSC,
CFRNMU';

delete from soft_race where soft_card_fk in (select soft_card.soft_card_pk from soft_card where FIND_IN_SET(load_code, @var) > 0);
delete from soft_pool where soft_card_fk in (select soft_card.soft_card_pk from soft_card where FIND_IN_SET(load_code, @var) > 0);
delete from cards_in_use where soft_card_fk in (select soft_card.soft_card_pk from soft_card where FIND_IN_SET(load_code, @var) > 0);
delete from soft_card where FIND_IN_SET(load_code, @var) > 0;
delete from pattern_pool where pattern_fk in (select pattern.pattern_pk from pattern where FIND_IN_SET(load_code, @var) > 0);
delete from pattern where FIND_IN_SET(load_code, @var) > 0;
