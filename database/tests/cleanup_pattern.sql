SET @var = 'AUA';
delete from soft_race where soft_card_fk in (select soft_card.soft_card_pk from soft_card where load_code = @var);
delete from soft_pool where soft_card_fk in (select soft_card.soft_card_pk from soft_card where load_code = @var);
delete from cards_in_use where soft_card_fk in (select soft_card.soft_card_pk from soft_card where load_code = @var);
delete from soft_card where load_code = @var;
delete from pattern_pool where pattern_fk in (select pattern.pattern_pk from pattern where load_code = @var);
delete from pattern where load_code = @var;


