delete from runner; 
delete from soft_race;
delete from hard_race;
delete from hard_pool;
delete from soft_pool;
delete from cards_in_use;
delete from soft_card;
delete from hard_card;
delete from import;
delete from ftp_import;

delete from pattern_pool;
delete from pattern;
delete from dict_track;