/*
-- Query: SELECT * FROM card_loader.virtual_track
LIMIT 0, 1000

-- Date: 2015-05-05 10:56
*/
INSERT INTO `virtual_track` (`id`,`name`,`time_zone`,`track_code`,`category`,`last_used_time`,`kironTrackId`,`kironEventType`) VALUES (1,'Aries',NULL,'ARIES','Horse1','2014-12-08 12:07:26',1,'H');
INSERT INTO `virtual_track` (`id`,`name`,`time_zone`,`track_code`,`category`,`last_used_time`,`kironTrackId`,`kironEventType`) VALUES (2,'Krypton',NULL,'KRYPT','Horse2','2014-12-08 12:07:26',2,'H');
INSERT INTO `virtual_track` (`id`,`name`,`time_zone`,`track_code`,`category`,`last_used_time`,`kironTrackId`,`kironEventType`) VALUES (3,'Libra',NULL,'LIBRA','Horse2','2014-12-08 12:07:26',2,'H');
INSERT INTO `virtual_track` (`id`,`name`,`time_zone`,`track_code`,`category`,`last_used_time`,`kironTrackId`,`kironEventType`) VALUES (4,'Valhalla',NULL,'VALHA','Horse2','2014-12-08 12:07:26',2,'H');
INSERT INTO `virtual_track` (`id`,`name`,`time_zone`,`track_code`,`category`,`last_used_time`,`kironTrackId`,`kironEventType`) VALUES (7,'Gemini',NULL,'GEMIN','Horse1','2014-12-11 09:52:00',1,'H');
INSERT INTO `virtual_track` (`id`,`name`,`time_zone`,`track_code`,`category`,`last_used_time`,`kironTrackId`,`kironEventType`) VALUES (8,'Hercules',NULL,'HERCU','Harness','2014-12-08 12:07:26',1,'G');
INSERT INTO `virtual_track` (`id`,`name`,`time_zone`,`track_code`,`category`,`last_used_time`,`kironTrackId`,`kironEventType`) VALUES (10,'Jupiter',NULL,'JUPIT','Horse1','2014-12-08 12:07:26',1,'H');
INSERT INTO `virtual_track` (`id`,`name`,`time_zone`,`track_code`,`category`,`last_used_time`,`kironTrackId`,`kironEventType`) VALUES (12,'Scorpio',NULL,'SCORP','Harness','2014-12-08 12:07:26',1,'G');
INSERT INTO `virtual_track` (`id`,`name`,`time_zone`,`track_code`,`category`,`last_used_time`,`kironTrackId`,`kironEventType`) VALUES (13,'Mercury',NULL,'MERCU','Horse1','2014-12-08 12:07:26',1,'H');
INSERT INTO `virtual_track` (`id`,`name`,`time_zone`,`track_code`,`category`,`last_used_time`,`kironTrackId`,`kironEventType`) VALUES (14,'Neptune',NULL,'NEPTU','Horse2','2014-12-08 12:07:26',2,'H');
INSERT INTO `virtual_track` (`id`,`name`,`time_zone`,`track_code`,`category`,`last_used_time`,`kironTrackId`,`kironEventType`) VALUES (15,'Orion',NULL,'ORION','Harness','2014-12-08 12:07:26',1,'G');
INSERT INTO `virtual_track` (`id`,`name`,`time_zone`,`track_code`,`category`,`last_used_time`,`kironTrackId`,`kironEventType`) VALUES (16,'Pegasus',NULL,'PEGAS','Harness','2014-12-08 12:07:26',1,'G');
INSERT INTO `virtual_track` (`id`,`name`,`time_zone`,`track_code`,`category`,`last_used_time`,`kironTrackId`,`kironEventType`) VALUES (20,'Taurus',NULL,'TAURU','Horse2','2014-12-08 12:07:26',2,'H');
INSERT INTO `virtual_track` (`id`,`name`,`time_zone`,`track_code`,`category`,`last_used_time`,`kironTrackId`,`kironEventType`) VALUES (22,'Virgo',NULL,'VIRGO','Dog','2014-12-08 12:07:26',1,'D');
INSERT INTO `virtual_track` (`id`,`name`,`time_zone`,`track_code`,`category`,`last_used_time`,`kironTrackId`,`kironEventType`) VALUES (27,'Aquarius',NULL,'AQUAR','Dog','2014-12-08 12:07:26',1,'D');
INSERT INTO `virtual_track` (`id`,`name`,`time_zone`,`track_code`,`category`,`last_used_time`,`kironTrackId`,`kironEventType`) VALUES (33,'Galileo',NULL,'GALIL','Dog','2014-12-08 12:07:26',1,'D');
INSERT INTO `virtual_track` (`id`,`name`,`time_zone`,`track_code`,`category`,`last_used_time`,`kironTrackId`,`kironEventType`) VALUES (42,'Pisces',NULL,'PISCE','Dog','2014-12-08 12:07:26',1,'D');
INSERT INTO `virtual_track` (`id`,`name`,`time_zone`,`track_code`,`category`,`last_used_time`,`kironTrackId`,`kironEventType`) VALUES (45,'Saturn',NULL,'SATUR','Dog','2014-12-08 12:07:26',1,'D');
INSERT INTO `virtual_track` (`id`,`name`,`time_zone`,`track_code`,`category`,`last_used_time`,`kironTrackId`,`kironEventType`) VALUES (48,'Venus',NULL,'VENUS','Horse1','2014-12-11 09:51:00',1,'H');
INSERT INTO `virtual_track` (`id`,`name`,`time_zone`,`track_code`,`category`,`last_used_time`,`kironTrackId`,`kironEventType`) VALUES (53,'Draconis',NULL,'DRACO','Harness','2014-12-08 12:07:26',1,'G');
