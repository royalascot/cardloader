ALTER TABLE `cl`.`dict_track` 
ADD UNIQUE INDEX `unique_totecode` (`country_code` ASC, `tote_track_id` ASC);
ALTER TABLE `cl_35`.`dict_track` 
ADD UNIQUE INDEX `unique_track_totecode` (`tote_track_id` ASC, `country_code` ASC);

update dict_track set perf_type = 0 where perf_type is null;
 update dict_track set track_type = 0 where track_type is null;
 update dict_track set country_code = 'UK' where country_code is null;