drop schema card_loader;

CREATE SCHEMA IF NOT EXISTS `card_loader` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;

use `card_loader`;

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;

-- -----------------------------------------------------
-- function schema_version
-- -----------------------------------------------------
DROP function IF EXISTS `schema_version`;

DELIMITER $$
CREATE FUNCTION `schema_version`() RETURNS varchar(16) CHARSET utf8
    NO SQL
    DETERMINISTIC
return "1.1.0.0.2"$$

DELIMITER ;

DROP TABLE IF EXISTS `card_template`;
CREATE TABLE IF NOT EXISTS `card_template` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `name` VARCHAR(100) NOT NULL,
  `description` VARCHAR(100),
  `settings`  VARCHAR(20000),
  PRIMARY KEY (`id`) )
ENGINE = InnoDB;

DROP TABLE IF EXISTS `auditlog`;
CREATE TABLE IF NOT EXISTS `auditlog` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `username` VARCHAR(50) NULL,
  `category` VARCHAR(50) NULL,
  `area` VARCHAR(50) NULL,
  `action` VARCHAR(50) NULL,
  `status` VARCHAR(50) NULL,
  `content` VARCHAR(1000) NULL,
  `source_data` VARCHAR(1000) NULL,
  `timestamp` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `groups` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `name` VARCHAR(100) NOT NULL,
  `category` VARCHAR(45),
  PRIMARY KEY (`id`) )
ENGINE = InnoDB;

CREATE  TABLE IF NOT EXISTS `group_user` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `group_id` INT NOT NULL,
  `user_id` INT NOT NULL,
  PRIMARY KEY (`id`) ,
  INDEX `fk_group_user_group_id_idx` (`group_id` ASC) ,
  INDEX `fk_group_user_user_id_idx` (`user_id` ASC) 
);
 
DROP table IF EXISTS `dict_role`;
CREATE  TABLE `dict_role` 
(
  `id` 		INT NOT NULL AUTO_INCREMENT,
  `name` 	VARCHAR(100) NOT NULL,
  PRIMARY KEY (`id`) 
);
 
DROP TABLE IF EXISTS `group_role`;
CREATE TABLE IF NOT EXISTS `group_role` (
  `id` int NOT NULL AUTO_INCREMENT,
  `group_id` int NOT NULL,
  `role_id` int NOT NULL,
  PRIMARY KEY (`id`)
);

DROP TABLE IF EXISTS `group_track`;
CREATE TABLE IF NOT EXISTS `group_track` (
  `id` int NOT NULL AUTO_INCREMENT,
  `group_id` int NOT NULL,
  `track_id` int NOT NULL,
  PRIMARY KEY (`id`)
);

DROP TABLE IF EXISTS `group_tote`;
CREATE TABLE IF NOT EXISTS `group_tote` (
  `id` int NOT NULL AUTO_INCREMENT,
  `group_id` int NOT NULL,
  `tote_id` int NOT NULL,
  PRIMARY KEY (`id`)
);

DROP TABLE IF EXISTS `track_tote`;
CREATE TABLE IF NOT EXISTS `track_tote` (
  `id` int NOT NULL AUTO_INCREMENT,
  `track_id` int NOT NULL,
  `tote_id` int NOT NULL,
  PRIMARY KEY (`id`)
);

-- -----------------------------------------------------
-- ac_entry
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ac_entry`;
CREATE TABLE IF NOT EXISTS `ac_entry` (
  `ac_pk` int(11) NOT NULL AUTO_INCREMENT,
  `permissions_bitmap` int(11) DEFAULT '0' COMMENT 'Permissions Flags\n\n# definition\n0x01 = SELECT\n0x02 = UPDATE\n0x04 = INSERT\n0x08 = DELETE\n0x10 = CREATOR\n0x100 = DEFAULT' ,
  `actor_type_fk` int(11) DEFAULT NULL,
  `actor_id` int(11) DEFAULT NULL,
  `object_type_fk` int(11) DEFAULT NULL,
  `object_id` int(11) DEFAULT NULL COMMENT 'psuedo foreign key - used in conjunction with object_type_fk to concretely identify object ',
  PRIMARY KEY (`ac_pk`)
) ENGINE=InnoDB;

-- -----------------------------------------------------
-- totes
-- -----------------------------------------------------

DROP TABLE IF EXISTS `totes`;
CREATE TABLE IF NOT EXISTS `totes` (
  `tote_pk` INT NOT NULL AUTO_INCREMENT ,
  `description` VARCHAR(145) NOT NULL,
  `tote_code` VARCHAR(20), 
  `region` VARCHAR(45) NULL DEFAULT NULL ,
  `active` TINYINT(1) NOT NULL DEFAULT 1 ,
  `login_name` VARCHAR(100),
  `password` VARCHAR(200),
  `time_zone` VARCHAR(50),
  `autoenabled` INT,
  PRIMARY KEY (`tote_pk`),
  UNIQUE INDEX `tote_code_UNIQUE` (`tote_code` ASC)
)
ENGINE = InnoDB;

-- -----------------------------------------------------
-- users
-- -----------------------------------------------------

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` 
(
  `user_pk` INT NOT NULL AUTO_INCREMENT ,
  `identifier` VARCHAR(45) NOT NULL ,
  `password` VARCHAR(64) NOT NULL ,
  `salt` VARCHAR(45) NOT NULL ,
  `actor_type_fk` INT NOT NULL ,
  `actor_id` INT NOT NULL ,
  `role_fk` INT NOT NULL ,
  `active` TINYINT(1) NOT NULL DEFAULT 1 ,
  `first_name` VARCHAR(45) NULL,
  `last_name` VARCHAR(45) NULL,
  `office_phone` VARCHAR(45) NULL,
  `office_location` VARCHAR(45) NULL,
  `tra_code` VARCHAR(45) NULL ,
  `time_zone` VARCHAR(50),

  PRIMARY KEY (`user_pk`) ,
  UNIQUE INDEX `identifier_UNIQUE` (`identifier` ASC) ,
  INDEX `fk_users_1_idx` (`actor_type_fk` ASC) ,
  INDEX `fk_roles_idx` (`role_fk` ASC)
)
ENGINE = InnoDB;

-- -----------------------------------------------------
-- cards_in_use
-- -----------------------------------------------------

DROP TABLE IF EXISTS `cards_in_use`;
CREATE TABLE IF NOT EXISTS `cards_in_use` (
  `soft_card_fk` INT NULL COMMENT 'The card in use' ,
  `actor_fk` INT NULL COMMENT 'Who is using the card' ,
  INDEX `fk_cards_in_use_1_idx` (`soft_card_fk` ASC) ,
  CONSTRAINT `fk_cards_in_use_1`
    FOREIGN KEY (`soft_card_fk` )
    REFERENCES `soft_card` (`soft_card_pk` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

-- -----------------------------------------------------
-- dict_pool
-- -----------------------------------------------------

DROP TABLE IF EXISTS `dict_pool`;
CREATE TABLE IF NOT EXISTS `dict_pool` (
  `dict_pool_pk` INT NOT NULL ,
  `description` VARCHAR(45) NOT NULL ,
  `abbreviation` varchar(5) NULL,
  `tote_pool_id` INT NOT NULL ,
  `multi_leg` TINYINT(1) NULL ,
  `vari_leg` TINYINT(1) NULL ,
  `exchange` TINYINT(1) NULL,
  `leg_count` int NULL,
  `race_count` int NULL,
  `max_race_count` int NULL,
  PRIMARY KEY (`dict_pool_pk`) )
ENGINE = InnoDB;

-- -----------------------------------------------------
-- dict_track
-- -----------------------------------------------------

DROP TABLE IF EXISTS `dict_track`;
CREATE TABLE IF NOT EXISTS `dict_track` (
  `dict_track_pk` INT NOT NULL AUTO_INCREMENT ,
  `equibase_id` VARCHAR(55) NULL,
  `country_code` VARCHAR(55) NULL,
  `name` VARCHAR(145) NOT NULL,
  `tote_track_id` VARCHAR(45) NULL,
  `time_zone` VARCHAR(50),
  `track_code` VARCHAR(50),
  `perf_type` INT,
  `track_type` INT,
  `active` TINYINT(1) NOT NULL DEFAULT 1,
  `card_template_id` INT,
  PRIMARY KEY (`dict_track_pk`),
  CONSTRAINT `fk_card_template_1`
    FOREIGN KEY (`card_template_id` )
    REFERENCES `card_template` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

-- -----------------------------------------------------
-- hard_card
-- -----------------------------------------------------

DROP TABLE IF EXISTS `hard_card`;
CREATE TABLE IF NOT EXISTS `hard_card` (
  `hard_card_pk` INT NOT NULL AUTO_INCREMENT,
  `dict_track_fk` INT NULL ,
  `import_fk` INT NULL ,
  `event_code` VARCHAR(45) NULL ,
  `provider` VARCHAR(45) NULL ,
  `card_date` DATE NULL ,
  `enabled` TINYINT(1) NULL DEFAULT false ,
  `approved` TINYINT(1) NULL DEFAULT false ,
  `description` VARCHAR(100),
  `card_type` INT,
  `perf_type` INT,
  `first_race_time` TIME NULL,
  `meeting_code` VARCHAR(40),
  `pass_throughs` VARCHAR(8000),
  `settings` VARCHAR(10000),
  `feature_race` INT,
  PRIMARY KEY (`hard_card_pk`) ,
  INDEX `fk_hard_card_1_idx` (`dict_track_fk` ASC) ,
  INDEX `fk_hard_card_2_idx` (`import_fk` ASC) ,
  CONSTRAINT `fk_hard_card_1`
    FOREIGN KEY (`dict_track_fk` )
    REFERENCES `dict_track` (`dict_track_pk` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_hard_card_2`
    FOREIGN KEY (`import_fk` )
    REFERENCES `import` (`import_pk` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB AUTO_INCREMENT=1000;

-- -----------------------------------------------------
-- hard_pool
-- -----------------------------------------------------

DROP TABLE IF EXISTS `hard_pool`;
CREATE TABLE IF NOT EXISTS `hard_pool` (
  `pool_pk` INT NOT NULL AUTO_INCREMENT ,
  `hard_card_fk` INT NOT NULL ,
  `dict_pool_fk` INT NOT NULL ,
  `race_bitmap` bigint(20) DEFAULT NULL ,
  `track_code` varchar(45) NULL,
  `track_name` varchar(100) NULL,
  `pool_code` varchar(40) NULL,
  `pool_name` varchar(40) NULL,
  `tote_pool_type` INT NULL,
  `guarantee` decimal(10,4),
  `bonus_guarantee` decimal(10,4),
  `brought_forward` decimal(10,4),
  `bonus_brought_forward` decimal(10,4),
  `status` varchar(50),
  `force_full_collation` TINYINT,
  `fractional_betting` TINYINT,
  `force_willpay` TINYINT,
  `itsp_allowed` TINYINT,
  `norminated` TINYINT,
  PRIMARY KEY (`pool_pk`) ,
  INDEX `fk_hard_pool_1_idx` (`hard_card_fk` ASC) ,
  INDEX `fk_hard_pool_2_idx` (`dict_pool_fk` ASC) ,
  CONSTRAINT `fk_hard_pool_1`
    FOREIGN KEY (`hard_card_fk` )
    REFERENCES `hard_card` (`hard_card_pk` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_hard_pool_2`
    FOREIGN KEY (`dict_pool_fk` )
    REFERENCES `dict_pool` (`dict_pool_pk` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

-- -----------------------------------------------------
-- hard_race
-- -----------------------------------------------------

DROP TABLE IF EXISTS `hard_race`;
CREATE TABLE IF NOT EXISTS `hard_race` (
  `hard_race_pk` INT NOT NULL AUTO_INCREMENT ,
  `hard_card_fk` INT NULL ,
  `number` INT NULL ,
  `post_time` TIME NULL ,
  `distance` VARCHAR(100) NULL ,
  `distance_unit` VARCHAR(45) NULL ,
  `evening` tinyint(1) NULL,
  `breed` varchar(50) NULL,
  `course` varchar(45) NULL,
  `race_type` varchar(45) NULL COMMENT 'i.e. MAIDEN CLAIMING, STARTER ALLOWANCE',
  `race_name_short` varchar(50) NULL,
  `race_name_long` varchar(1300) NULL,
  `purse` decimal(10,2) NULL,
  `wager_text` varchar(1000), 
  `pool_id_list` varchar(1000), 
  `enabled` TINYINT(1) NULL DEFAULT false,
  `event_id` INT NULL,
  `perf_type` INT NULL,
  `race_code` varchar(50) NULL,
  `handicap` INT NULL,
  `distance_text` varchar(50) null,
  PRIMARY KEY (`hard_race_pk`) ,
  INDEX `fk_hard_race_1_idx` (`hard_card_fk` ASC) ,
  CONSTRAINT `fk_hard_race_1`
    FOREIGN KEY (`hard_card_fk` )
    REFERENCES `hard_card` (`hard_card_pk` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

-- -----------------------------------------------------
-- import
-- -----------------------------------------------------

DROP TABLE IF EXISTS `import`;
CREATE TABLE IF NOT EXISTS `import` (
  `import_pk` INT NOT NULL AUTO_INCREMENT ,
  `dict_track_fk` INT NULL ,
  `original_filename` VARCHAR(255) NULL ,
  `timestamp` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ,
  `checksum` INT UNSIGNED NOT NULL DEFAULT 0 ,
  `status` VARCHAR(45) NOT NULL ,
  `log` LONGBLOB NOT NULL ,
  `file_date` TIMESTAMP,
  PRIMARY KEY (`import_pk`) )
ENGINE = InnoDB;

DROP TABLE IF EXISTS `ftp_import`;
CREATE TABLE IF NOT EXISTS `ftp_import` (
  `import_pk` INT NOT NULL AUTO_INCREMENT ,
  `dict_track_fk` INT NULL ,
  `original_filename` VARCHAR(255) NULL ,
  `timestamp` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ,
  `checksum` INT UNSIGNED NOT NULL DEFAULT 0 ,
  `file_date` TIMESTAMP,
  `status` VARCHAR(45),
  PRIMARY KEY (`import_pk`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- pattern
-- -----------------------------------------------------

DROP TABLE IF EXISTS `pattern`;
CREATE TABLE IF NOT EXISTS `pattern` 
(
  `pattern_pk` INT NOT NULL AUTO_INCREMENT ,
  `description` VARCHAR(145) NULL ,
  `race_count` TINYINT NULL COMMENT 'This is the count of races in the pattern' ,
  `load_code` VARCHAR(20) NULL DEFAULT NULL ,
  `merge_type` INT NULL , 
  `force_race_count` TINYINT(1) NULL ,
  `dict_track_fk` INT NULL DEFAULT NULL ,
  `tote_list` VARCHAR(3000) NULL,
  `race_rules` VARCHAR(10000) NULL,
  PRIMARY KEY (`pattern_pk`)
 ,CONSTRAINT `fk_pattern_1`
  FOREIGN KEY (`dict_track_fk` )
  REFERENCES `card_loader`.`dict_track` (`dict_track_pk` )
  ON DELETE NO ACTION
  ON UPDATE NO ACTION
 ,INDEX `fk_pattern_1_idx` (`dict_track_fk` ASC) 
)
ENGINE = InnoDB;

-- -----------------------------------------------------
-- pattern_pool
-- -----------------------------------------------------

DROP TABLE IF EXISTS `pattern_pool`;
CREATE TABLE IF NOT EXISTS `pattern_pool` (
  `pool_pk` INT NOT NULL AUTO_INCREMENT, 
  `pattern_fk` INT NULL ,
  `dict_pool_fk` INT NULL ,
  `race_bitmap` BIGINT NULL COMMENT 'This is a bitmap indicating the races that the pool applies to.' ,
  `pool_code` varchar(40) NULL,
  `pool_name` varchar(40) NULL,
  `tote_pool_type` INT NULL,
  `guarantee` decimal(10,4),
  `bonus_guarantee` decimal(10,4),
  `brought_forward` decimal(10,4),
  `bonus_brought_forward` decimal(10,4),
  `status` varchar(50),
  `force_full_collation` TINYINT,
  `fractional_betting` TINYINT,
  `force_willpay` TINYINT,
  `itsp_allowed` TINYINT,
  `norminated` TINYINT,
  PRIMARY KEY (`pool_pk`) ,
  INDEX `fk_patternpool_map_1_idx` (`pattern_fk` ASC) ,
  INDEX `fk_patternpool_map_2_idx` (`dict_pool_fk` ASC) ,  
  CONSTRAINT `fk_pattern_pool_1`
    FOREIGN KEY (`pattern_fk` )
    REFERENCES `pattern` (`pattern_pk` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_pattern_pool_2`
    FOREIGN KEY (`dict_pool_fk` )
    REFERENCES `dict_pool` (`dict_pool_pk` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

-- -----------------------------------------------------
-- runner
-- -----------------------------------------------------

DROP TABLE IF EXISTS `runner`;
CREATE TABLE IF NOT EXISTS `runner` (
  `runner_pk` INT NOT NULL AUTO_INCREMENT ,
  `hard_race_fk` INT NOT NULL ,
  `name` VARCHAR(145) NULL ,
  `short_name` VARCHAR(50) NULL ,
  `position` SMALLINT NULL ,
  `program_number` VARCHAR(50) NULL ,
  `jockey_name` VARCHAR(45) NULL ,
  `weight_carried` varchar(50) NULL,
  `weight_units` varchar(50) NULL,
  `couple_indicator` VARCHAR(50) NULL ,
  `mornline_odds` VARCHAR(130) NULL ,
  `scratched` TINYINT(1) NULL DEFAULT false ,
  `also_eligible` TINYINT(1) NULL ,
  PRIMARY KEY (`runner_pk`) ,
  INDEX `fk_runners1_idx` (`hard_race_fk` ASC) ,
  CONSTRAINT `fk_runners1`
    FOREIGN KEY (`hard_race_fk` )
    REFERENCES `hard_race` (`hard_race_pk` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

-- -----------------------------------------------------
-- soft_card
-- -----------------------------------------------------

DROP TABLE IF EXISTS `soft_card`;
CREATE TABLE IF NOT EXISTS `soft_card` (
  `soft_card_pk` INT NOT NULL AUTO_INCREMENT,
  `dict_track_fk` INT NULL ,
  `description` VARCHAR(145) NOT NULL ,
  `sparse_races` TINYINT(1) NULL DEFAULT false ,
  `sparse_pools` TINYINT(1) NULL ,
  `load_code` VARCHAR(50) ,
  `pattern_fk` INT NULL ,
  `approved` TINYINT(1) NULL DEFAULT false ,
  `hard_card_fk` INT NULL ,
  `card_date` DATE NULL ,
  `download_list` VARCHAR(1000),
  `access_list` VARCHAR(1000),
  `tote_approval_list` VARCHAR(500),
  `card_type` INT,
  `perf_type` INT,
  `first_race_time` TIME,
  `feature_race` INT,
  `source` VARCHAR(50),
  PRIMARY KEY (`soft_card_pk`) ,
  INDEX `fk_soft_card_1_idx` (`pattern_fk` ASC) ,
  INDEX `fk_soft_hard_card_idx` (`hard_card_fk` ASC) ,
  CONSTRAINT `fk_soft_card_1`
    FOREIGN KEY (`pattern_fk` )
    REFERENCES `pattern` (`pattern_pk` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_soft_hard_card`
    FOREIGN KEY (`hard_card_fk` )
    REFERENCES `hard_card` (`hard_card_pk` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
)
ENGINE = InnoDB AUTO_INCREMENT=100000;

-- -----------------------------------------------------
-- soft_pool
-- -----------------------------------------------------

DROP TABLE IF EXISTS `soft_pool`;
CREATE TABLE IF NOT EXISTS `soft_pool` (
  `pool_pk` INT NOT NULL AUTO_INCREMENT ,
  `soft_card_fk` INT NOT NULL ,
  `dict_pool_fk` INT NOT NULL ,
  `race_bitmap` bigint(20) DEFAULT NULL ,
  `pool_code` varchar(40) NULL,
  `pool_name` varchar(40) NULL,
  `tote_pool_type` INT NULL,
  `guarantee` decimal(10,4),
  `bonus_guarantee` decimal(10,4),
  `brought_forward` decimal(10,4),
  `bonus_brought_forward` decimal(10,4),
  `status` varchar(50),
  `force_full_collation` TINYINT,
  `fractional_betting` TINYINT,
  `force_willpay` TINYINT,
  `itsp_allowed` TINYINT,
  `norminated` TINYINT,
  INDEX `fk_soft_pool_map_2_idx` (`dict_pool_fk` ASC) ,
  INDEX `fk_soft_pools_1_idx` (`soft_card_fk` ASC) ,
  PRIMARY KEY (`pool_pk`) ,
  CONSTRAINT `fk_soft_pool_map_2`
    FOREIGN KEY (`dict_pool_fk` )
    REFERENCES `dict_pool` (`dict_pool_pk` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_soft_pools_1`
    FOREIGN KEY (`soft_card_fk` )
    REFERENCES `soft_card` (`soft_card_pk` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

-- -----------------------------------------------------
-- soft_race
-- -----------------------------------------------------

DROP TABLE IF EXISTS `soft_race`;
CREATE TABLE IF NOT EXISTS `soft_race` (
  `soft_race_pk` INT NOT NULL AUTO_INCREMENT ,
  `soft_card_fk` INT NOT NULL ,
  `hard_race_fk` INT NULL ,
  `soft_race_no` TINYINT NULL ,
  `race_id` INT NULL,
  INDEX `fk_soft_race_map_1_idx` (`hard_race_fk` ASC) ,
  INDEX `fk_soft_race_map_2_idx` (`soft_card_fk` ASC) ,
  PRIMARY KEY (`soft_race_pk`) ,
  CONSTRAINT `fk_soft_race_1`
    FOREIGN KEY (`hard_race_fk` )
    REFERENCES `hard_race` (`hard_race_pk` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_soft_race_2`
    FOREIGN KEY (`soft_card_fk` )
    REFERENCES `soft_card` (`soft_card_pk` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

-- -----------------------------------------------------
-- v_utl_hard_card_race_count
-- -----------------------------------------------------
CREATE or REPLACE VIEW v_utl_hard_card_race_count AS
SELECT
    hr.hard_card_fk		as hard_card_fk
   ,count(hr.hard_race_pk)	as race_count
FROM
    hard_race hr
GROUP BY
    hr.hard_card_fk
;

CREATE or REPLACE VIEW v_pattern_count AS
SELECT
    dict_track_fk	as dict_track_fk
   ,count(pattern_pk)	as pattern_count
FROM
    pattern
GROUP BY
    dict_track_fk
;

-- -----------------------------------------------------
-- v_hard_card_outline
-- -----------------------------------------------------
CREATE or REPLACE VIEW v_hard_card_outline AS 
SELECT
    hc.hard_card_pk 				as hard_card_pk
   ,hc.dict_track_fk				as dict_track_fk
   ,p.pattern_count				    as pattern_count
   ,dt.name					as track_name
   ,dt.equibase_id				as track_equibase_id
   ,dt.country_code				as track_country_code
   ,hc.import_fk 				as import_fk
   ,hc.event_code				as event_code
   ,hc.card_type				as card_type
   ,hc.perf_type			    as perf_type
   ,hc.provider					as provider
   ,hc.card_date				as card_date
   ,hc.enabled					as enabled
   ,hc.approved					as approved
   ,hc.description			    as description
   ,hc.first_race_time	        as first_race_time
   ,hc.feature_race			    as feature_race
   ,IFNULL(rc.race_count,0)			as race_count
FROM 
    hard_card hc left outer join dict_track dt on hc.dict_track_fk = dt.dict_track_pk
	left outer join v_pattern_count p on (hc.dict_track_fk = p.dict_track_fk)
       left outer join v_utl_hard_card_race_count rc on hc.hard_card_pk = rc.hard_card_fk
;

-- -----------------------------------------------------
-- v_soft_card_outline
-- -----------------------------------------------------


CREATE or REPLACE VIEW v_utl_soft_card_race_count AS
SELECT
    sr.soft_card_fk		as soft_card_fk
   ,count(sr.soft_race_pk)	as race_count
FROM
    soft_race sr
GROUP BY
    sr.soft_card_fk
;

CREATE or REPLACE VIEW v_utl_soft_card_in_use_count AS
SELECT
    su.soft_card_fk		as soft_card_fk
   ,count(su.actor_fk)		as in_use_count
FROM
    cards_in_use su
GROUP BY
    su.soft_card_fk
;

CREATE or REPLACE VIEW v_soft_card_outline AS 
SELECT
    sc.soft_card_pk 				as soft_card_pk
   ,sc.description				as description
   ,dt.dict_track_pk			as dict_track_fk
   ,dt.name 					as track_name
   ,dt.equibase_id				as track_equibase_id
   ,dt.country_code				as track_country_code
   ,sc.sparse_races 			as sparse_races
   ,sc.sparse_pools				as sparse_pools
   ,sc.load_code				as load_code
   ,sc.pattern_fk				as pattern_fk
   ,sc.card_type				as card_type
   ,sc.perf_type				as perf_type
   ,sc.first_race_time          as first_race_time
   ,pat.description				as pattern_description
   ,sc.approved					as approved
   ,sc.hard_card_fk				as hard_card_fk
   ,sc.card_date				as card_date
   ,sc.download_list			as download_list
   ,sc.access_list		        as access_list
   ,sc.tote_approval_list		as tote_approval_list
   ,sc.feature_race			    as feature_race
   ,sc.source					as source
   ,IFNULL(rc.race_count,0)			as race_count
   ,IFNULL(inuse.in_use_count,0)		as in_use_count
FROM 
    soft_card sc left outer join hard_card hc on sc.hard_card_fk = hc.hard_card_pk
       left outer join dict_track dt on hc.dict_track_fk = dt.dict_track_pk
          left outer join v_utl_soft_card_race_count rc on sc.soft_card_pk = rc.soft_card_fk
             left outer join v_utl_soft_card_in_use_count inuse on sc.soft_card_pk = inuse.soft_card_fk
                left outer join pattern pat on sc.pattern_fk = pat.pattern_pk
;

-- -----------------------------------------------------
-- v_pattern_outline
-- -----------------------------------------------------

CREATE or REPLACE VIEW v_pattern_outline AS 
SELECT
    pt.pattern_pk 				as pattern_pk
   ,pt.description				as description
   ,pt.race_count				as race_count
   ,pt.load_code				as load_code
   ,pt.merge_type				as merge_type
   ,pt.tote_list				as tote_list
   ,pt.force_race_count				as force_race_count
   ,pt.dict_track_fk				as dict_track_fk
   ,dt.name					as track_name
   ,dt.equibase_id				as track_equibase_id
   ,dt.country_code				as track_country_code
FROM 
    pattern pt left outer join dict_track dt on pt.dict_track_fk = dt.dict_track_pk
;

CREATE or REPLACE VIEW v_tote_outline AS 
SELECT
  t.*, tt.track_id
   FROM 
    totes t left outer join track_tote tt on t.tote_pk = tt.tote_id
;

SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

