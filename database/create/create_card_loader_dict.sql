use card_loader;

truncate table `dict_role`;

INSERT INTO `dict_role` 
VALUES 
(1, 'Administrator'),
(2, 'Import Viewer'),
-- (3, 'Import Editor'),
-- (4, 'Host Card Viewer'),
(5, 'Host Card Editor'),
-- (6, 'Load Card Viewer'),
(7, 'Load Card Editor'),
-- (8, 'Pattern Viewer'),
(9, 'Pattern Editor'),
(10, 'Group Admin'),
(11, 'User Admin'),
(12, 'Track Admin'),
(13, 'Tote Admin'),
(14, 'Tote Card Approver');

commit;

truncate table groups;
truncate table group_role;
delete from dict_pool;


INSERT INTO groups (name, category) values ('Administrator', 'Admin');
INSERT INTO group_role (group_id, role_id)
values
(1, 1);

-- -----------------------------------------------------
-- dict_pool
-- -----------------------------------------------------
LOCK TABLES `dict_pool` WRITE;

INSERT INTO
  `dict_pool`
VALUES
    (1,'Win','WIN',1,0,0,0,1,1,1),
    (2,'Place','PLC',2,0,0,0,1,1,1),
    (3,'Show','SHW',3,0,0,0,1,1,1),
    (4,'Exacta','EXA',4,0,0,0,2,1,1),
    (5,'Quinella','QNL',5,0,0,0,2,1,1),
    (6,'Trifecta','TRI',6,0,0,0,3,1,1),
    (7,'Double','DBL',7,1,0,0,2,2,2),
    (8,'Pick 3','PK3',8,1,0,0,3,3,3),
    (9,'Pick 4','PK4',9,1,0,0,4,4,4),
    (10,'Pick 5','PK5',10,1,0,0,5,5,5),
    (11,'Pick 6','PK6',11,1,0,0,6,6,6),
    (12,'Pick 7','PK7',12,1,0,0,7,7,7),
    (13,'Pick 8','PK8',13,1,0,0,8,8,8),
    (14,'Pick 9','PK9',14,1,0,0,9,9,9),
    (15,'Pick 10','P10',15,1,0,0,10,10,10),
    (16,'Twin Quinella','TQN',16,1,0,1,2,2,2),
    (17,'Double Quinella','DQL',17,1,0,0,4,2,2),
    (18,'Double Exacta','DEX',18,1,0,0,4,2,2),
    (19,'Twin Trifecta','TTR',19,1,0,1,3,2,2),
    (20,'Exact Tri','TET',20,1,0,1,3,2,2),
    (21,'Superfecta','SFC',21,0,0,0,4,1,1),
    (22,'Tri Super','BTS',22,1,0,1,3,2,2),
    (23,'PPT PER','PPT',23,1,0,0,7,3,3),
    (24,'Super Super','TSS',24,1,0,1,4,2,2),
    (25,'Omni','OMN',25,0,0,0,2,1,1),
    (26,'Racingo','RNG',26,1,0,0,9,3,3),
    (27,'Choose 6','CH6',27,1,0,0,6,6,6),
    (28,'PentaFecta','E5N',28,0,0,0,5,1,1),
    (29,'1-2-3','OTT',29,1,1,0,4,4,10),
    (30,'Multiple','WXN',30,1,1,1,2,2,15),
    (31,'Variable Pick','VPK',31,1,1,0,2,2,15),
    (40,'Grand Slam','GSL',9,1,0,0,4,4,4);

  
UNLOCK TABLES;


COMMIT;

