use card_loader;

INSERT INTO groups (name) values ('admin');
INSERT INTO users (user_pk, identifier, password, salt, actor_type_fk, actor_id, role_fk )
             VALUES (1, 'admin', '8c6976e5b5410415bde908bd4dee15dfb167a9c873fc4bb8a81f6f2ab448a918', 'admin', 2, 1, 1 );
INSERT INTO group_user (group_id, user_id) values (1,1);
COMMIT;