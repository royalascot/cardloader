CREATE USER 'cl.jboss'@'localhost' IDENTIFIED BY 'jUs&AgeS39';
CREATE USER 'cl.jboss'@'127.0.0.1' IDENTIFIED BY 'jUs&AgeS39';
CREATE USER 'cl.jboss'@'%' IDENTIFIED BY 'jUs&AgeS39';
CREATE USER 'jboss'@'%' IDENTIFIED BY 'admin';

GRANT ALL PRIVILEGES ON `card_loader`.* TO 'cl.jboss'@'localhost';
GRANT ALL PRIVILEGES ON `card_loader`.* TO 'cl.jboss'@'127.0.0.1';
GRANT ALL PRIVILEGES ON `card_loader`.* TO 'cl.jboss'@'%';
GRANT ALL PRIVILEGES ON `card_loader`.* TO 'jboss'@'%';

CREATE USER 'cl.python'@'localhost' IDENTIFIED BY 'tsK89lard$';
CREATE USER 'cl.python'@'127.0.0.1' IDENTIFIED BY 'tsK89lard$';
 
GRANT INSERT, UPDATE, SELECT ON `card_loader`.`import` TO 'cl.python'@'localhost';
GRANT INSERT, UPDATE, SELECT ON `card_loader`.`import_files` TO 'cl.python'@'localhost';
GRANT INSERT, SELECT ON `card_loader`.`hard_card` TO 'cl.python'@'localhost';
GRANT INSERT, SELECT ON `card_loader`.`hard_race` TO 'cl.python'@'localhost';
GRANT INSERT, UPDATE, SELECT ON `card_loader`.`hard_pool` TO 'cl.python'@'localhost';
GRANT INSERT, SELECT ON `card_loader`.`runner` TO 'cl.python'@'localhost';
GRANT INSERT, SELECT ON `card_loader`.`dict_track` TO 'cl.python'@'localhost';
GRANT SELECT ON `card_loader`.`dict_pool` TO 'cl.python'@'localhost';
 
GRANT INSERT, UPDATE, SELECT ON `card_loader`.`import` TO 'cl.python'@'127.0.0.1';
GRANT INSERT, UPDATE, SELECT ON `card_loader`.`hard_card` TO 'cl.python'@'127.0.0.1';
GRANT INSERT, SELECT ON `card_loader`.`hard_card` TO 'cl.python'@'127.0.0.1';
GRANT INSERT, SELECT ON `card_loader`.`hard_race` TO 'cl.python'@'127.0.0.1';
GRANT INSERT, UPDATE, SELECT ON `card_loader`.`hard_pool` TO 'cl.python'@'127.0.0.1';
GRANT INSERT, SELECT ON `card_loader`.`runner` TO 'cl.python'@'127.0.0.1';
GRANT INSERT, SELECT ON `card_loader`.`dict_track` TO 'cl.python'@'127.0.0.1';
GRANT SELECT ON `card_loader`.`dict_pool` TO 'cl.python'@'127.0.0.1';
 
CREATE USER 'dev'@'%' IDENTIFIED BY '[theusual]';
 
GRANT ALL PRIVILEGES ON `card_loader`.* TO 'dev'@'%';
 
FLUSH privileges;
 
exit