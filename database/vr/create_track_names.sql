
insert into virtual_track (name, category) values(
'Amdromeda','Horse'),(
'Bolide','Horse'),(
'Capricornus','Horse'),(
'Delphinus','Horse'),(
'Equulei','Horse'),(
'Fornacis','Horse'),(
'Gemini','Horse'),(
'Hercules','Horse'),(
'Incubus','Horse'),(
'Jupiter','Horse'),(
'Kepler','Horse'),(
'Libra','Horse'),(
'Mercurio','Horse'),(
'Neptune','Horse'),(
'Orion','Horse'),(
'Pegasus','Horse'),(
'Quasar','Horse'),(
'Reticuli','Horse'),(
'Sagittarius','Horse'),(
'Taurus','Horse'),(
'Uranus','Horse'),(
'Virgo','Horse'),(
'Wotan','Horse'),(
'Xanthus','Horse'),(
'Yemanja','Horse'),(
'Zodiac','Horse'
);

insert into virtual_track (name, category) values(
'Aquarius','Dog'),(
'Bootes','Dog'),(
'Cassiopea','Dog'),(
'Draconis','Dog'),(
'Ephemeris','Dog'),(
'Faculae','Dog'),(
'Galileo','Dog'),(
'Hydra','Dog'),(
'Interstellar','Dog'),(
'Juno','Dog'),(
'Krypton','Dog'),(
'Leonis','Dog'),(
'Martes','Dog'),(
'Neutrino','Dog'),(
'Omega','Dog'),(
'Pisces','Dog'),(
'Quincunx','Dog'),(
'Regolith','Dog'),(
'Saturno','Dog'),(
'Terrestre','Dog'),(
'Ursa Major','Dog'),(
'Venus','Dog'),(
'Walkyries','Dog'),(
'Xochipili','Dog'),(
'Yahweh','Dog'),(
'Zenith','Dog');

update virtual_track set last_used_time = current_time();
