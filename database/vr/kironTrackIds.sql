/*
 * Schema v. 1.0.0.2
 * Added KironTrackId, and kironEventType for differentiation of 
 * new horse track. Setting default of 1 for all 
 * other tracks with only one track given.
 * 
 */
ALTER TABLE `card_loader`.`virtual_track`
ADD COLUMN `kironTrackId` INT(5) NULL AFTER `track_code`,
ADD COLUMN `kironEventType` VARCHAR(5) NULL AFTER `kironTrackId`;

UPDATE  `card_loader`.`virtual_track` SET `kironTrackId` = 1, `kironEventType` = 'H' WHERE `category`="Horse";
UPDATE  `card_loader`.`virtual_track` SET `kironTrackId` = 2, `kironEventType` = 'H' WHERE `category`="Horse2";
UPDATE  `card_loader`.`virtual_track` SET `kironTrackId` = 1, `kironEventType` = 'D' WHERE `category`="Dog";
UPDATE  `card_loader`.`virtual_track` SET `kironTrackId` = 1, `kironEventType` = 'G' WHERE `category`="Harness";