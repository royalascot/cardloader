use `card_loader`;

DROP TABLE IF EXISTS `virtual_track`;
CREATE TABLE IF NOT EXISTS `virtual_track` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(50) NULL,
  `time_zone` VARCHAR(50),
  `track_code` VARCHAR(50),
  `category` VARCHAR(50) NULL,
  `last_used_time` TIMESTAMP,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB;

DROP TABLE IF EXISTS `request_log`;
CREATE TABLE IF NOT EXISTS `request_log` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `username` VARCHAR(50) NULL,
  `category` VARCHAR(50) NULL,
  `area` VARCHAR(50) NULL,
  `action` VARCHAR(50) NULL,
  `status` VARCHAR(50) NULL,
  `request` VARCHAR(2000) NULL,
  `response` text,
  `timestamp` TIMESTAMP,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB;

DROP TABLE IF EXISTS `card_schedule`;
CREATE TABLE IF NOT EXISTS `card_schedule` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `card_date` DATE NULL,
  `first_post_time` TIME NULL,
  `schedule_time` DATETIME NULL,
  `name` VARCHAR(250) NULL,
  `status` INT NULL,
  `distance` VARCHAR(50),
  `runner_count` int,
  `race_count` int,
  `race_type` VARCHAR(50),
  `track_id` int,
  `race_interval` int,
  `display_color` VARCHAR(50),
  PRIMARY KEY (`id`) )
ENGINE = InnoDB;

DROP TABLE IF EXISTS `schedule_sequence`;
CREATE TABLE IF NOT EXISTS `schedule_sequence` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `card_date` DATE NULL,
  `first_post_time` TIME NULL,
  `schedule_time` DATETIME NULL,
  `name` VARCHAR(250) NULL,
  `status` INT NULL,
  `distance` VARCHAR(50),
  `runner_count` int,
  `race_count` int,
  `race_type` VARCHAR(50),
  `track_id` int,
  `race_interval` int,
  `start_date` DATE,
  `end_date` DATE,
  `sequence_name` VARCHAR(100),
  `display_color` VARCHAR(50),
  PRIMARY KEY (`id`) )
ENGINE = InnoDB;
