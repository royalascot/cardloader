package com.sportech.cl.utils;

import java.io.InputStream;

import org.apache.commons.io.IOUtils;
import org.junit.Assert;
import org.junit.Test;

import com.sportech.cl.model.database.HardCard;
import com.sportech.cl.model.database.HardRace;
import com.sportech.cl.model.importer.ConverterType;
import com.sportech.cl.utils.importer.CardImporter;
import com.sportech.cl.utils.importer.CardImporterFactory;

public class ScratchedRunnerTest {

	@Test
	public void testScratchedRunner() {
		InputStream stream = ClassLoader.getSystemResourceAsStream("FINALD20140305MNR_USA.xml");
		CardImporter importer = CardImporterFactory.create(ConverterType.EQUIBASE, null);
		try {
			byte[] data = IOUtils.toByteArray(stream);
			HardCard card = importer.parse(data).get(0);
			Assert.assertTrue(card.getRaces().size() == 9);
			for (HardRace r : card.getRaces()) {
				if (r.getNumber() == 1L) {
					Assert.assertTrue(r.getRunners().size() == 9);
					Assert.assertTrue(r.getRunners().get(0).getScratched() == false);
					Assert.assertTrue(r.getRunners().get(8).getScratched() == true);
					break;
				}
			}
		} catch (Exception e) {
			Assert.fail();
		}
	}

}
