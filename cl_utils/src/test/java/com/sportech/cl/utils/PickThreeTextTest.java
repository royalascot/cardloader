package com.sportech.cl.utils;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import com.sportech.cl.model.importer.PoolList;
import com.sportech.cl.utils.importer.PoolTextParser;

public class PickThreeTextTest {

	private PoolTextParser parser = new PoolTextParser("/pooltypeparserules.json");
	
	@Test
	public void testFirstLegTriSuper() {
		String text = "Win-place-show-quiniela-trifecta  Superfecta-pick Three Starts-tri Super 2nd Half" ;
		List<Integer> secondLegs = new ArrayList<Integer>();
		List<Integer> pools = parser.parse(text, secondLegs);
		Assert.assertTrue(pools.size() == 7);
		Assert.assertTrue(pools.contains(8));
		Assert.assertTrue(pools.contains(21));
		Assert.assertTrue(!pools.contains(22));
	}
	
	@Test
	public void testThirdLegTwinTri() {
		String text = "Win-place-show-quiniela  Superfecta-pick Three 3rd Leg-twin Tri 1st Half" ;
		List<Integer> secondLegs = new ArrayList<Integer>();
		List<Integer> pools = parser.parse(text, secondLegs);
		Assert.assertTrue(pools.size() == 6);
		Assert.assertTrue(!pools.contains(8));
		Assert.assertTrue(pools.contains(19));
		Assert.assertTrue(!pools.contains(22));
	}
	
	
	@Test
	public void testFirstLeg() {
		String text = "Win-place-show-quiniela-trifecta  Superfecta-pick Three Starts Here";
		List<Integer> pools = parser.parse(text, null);
		Assert.assertTrue(pools.size() == 7);
		Assert.assertTrue(pools.contains(8));
		Assert.assertTrue(pools.contains(21));
		Assert.assertTrue(!pools.contains(22));
	}
	
	@Test
	public void testSecondLegTwinTri() {
		String text = "Win-place-show-quiniela-trifecta  Superfecta-pick Three 2nd Leg-twin Tri 2nd Half";
		List<Integer> secondLegs = new ArrayList<Integer>();
		List<Integer> pools = parser.parse(text, secondLegs);
		Assert.assertTrue(pools.size() == 6);
		Assert.assertTrue(!pools.contains(8));
		Assert.assertTrue(pools.contains(21));
		Assert.assertTrue(!pools.contains(19));
		Assert.assertTrue(secondLegs.contains(8));
		Assert.assertTrue(secondLegs.contains(19));
	}
	
	@Test
	public void testSecondLeg() {
		String text = "Win-place-show-quiniela  Superfecta-pick Three 2nd Leg-tri Super 1st Half";
		List<Integer> secondLegs = new ArrayList<Integer>();
		List<Integer> pools = parser.parse(text, secondLegs);
		Assert.assertTrue(pools.size() == 6);
		Assert.assertTrue(!pools.contains(8));
		Assert.assertTrue(pools.contains(21));
		Assert.assertTrue(pools.contains(22));
		Assert.assertTrue(secondLegs.contains(8));
	}

	@Test
	public void testThirdLeg() {
		String text = "Win-place-show-quiniela-trifecta  Superfecta-pick Three 3rd Leg";
		PoolList pList = parser.parse(text);
		Assert.assertTrue(pList != null);
		Assert.assertTrue(pList.getLegs().size() == 2);
		Assert.assertTrue(pList.getLegs().get(1).getLegNumber() == 3);
		Assert.assertTrue(pList.getLegs().get(1).getPoolIds()[0] == 8);
	}

	@Test
	public void testPickThree1() {
		String text = "Second Half Daily Double / Exactor /.20 Triactor / .20 Pick 3 (Races 2-3-4)" ;
		List<Integer> secondLegs = new ArrayList<Integer>();
		List<Integer> pools = parser.parse(text, secondLegs);
		Assert.assertTrue(pools.size() == 6);
		Assert.assertTrue(pools.contains(8));
		Assert.assertTrue(pools.contains(4));
		Assert.assertTrue(pools.contains(6));
		Assert.assertTrue(!pools.contains(7));
		Assert.assertTrue(secondLegs.contains(7));
	}

	@Test
	public void testPickThree2() {
		String text = "$1 Exacta / $1 Trifecta Superfecta (.10 cent minimum wager) $1 Pick Three" ;
		List<Integer> secondLegs = new ArrayList<Integer>();
		List<Integer> pools = parser.parse(text, secondLegs);
		Assert.assertTrue(pools.size() == 7);
		Assert.assertTrue(pools.contains(8));
		Assert.assertTrue(pools.contains(4));
		Assert.assertTrue(pools.contains(6));
		Assert.assertTrue(pools.contains(21));
		Assert.assertTrue(!pools.contains(7));
	}

}
