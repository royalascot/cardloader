package com.sportech.cl.utils;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.junit.Assert;
import org.junit.Test;

import com.sportech.cl.utils.importer.PoolTextParser;

public class PickPoolTextTest {
	
	private PoolTextParser parser = new PoolTextParser("/pooltypeparserules.json");

	@Test
	public void testPick6() {
		String text = "WIN, PLACE, EXACTA, TRIFECTA, PICK 6, PLACE 6";
		List<Integer> pools = parser.parse(text, null);
		Assert.assertTrue(pools.contains(11));
	}

	public void testPK4SecondLegPattern() {
		String text = "Exacta, Quinella, Trifecta &amp; 10 Cent Superfecta, 2nd Leg 50 Cent Pick 4, 1st Leg $1 Pick 3, 2nd Half Daily Double";
		Pattern includeMatchPattern1 = Pattern.compile("", Pattern.CASE_INSENSITIVE);
		String matched = match(includeMatchPattern1, text);
		Assert.assertTrue(matched != null);
		Pattern includeMatchPattern2 = Pattern.compile("(2nd|second|2da)+\\W*(leg|race)\\W*(of)?\\W*\\d+\\W*(cent)?\\W*((pick|pk|pic|ick|win|bet)+\\W*(three|3)+)", Pattern.CASE_INSENSITIVE);
		String matched2 = match(includeMatchPattern2, text);
		Assert.assertTrue(matched2 != null);

	}
	
	private static String match(Pattern p, String text) {
		String matched = null;
		Matcher m = p.matcher(text);
		if (m.find()) {
			matched = m.group();
		}
		return matched;
	}

	@Test
	public void testPK3SecondLeg() {
		String text = "Exacta, Quinella, Trifecta &amp; 10 Cent Superfecta, 3rd Leg 50 Cent Pick 4, 2nd Leg $1 Pick 3, 1st Leg 10 Cent 'Flamingo Six' Pick 6"; 
		List<Integer> secondLegs = new ArrayList<Integer>();
		List<Integer> pools = parser.parse(text, secondLegs);
		Assert.assertTrue(pools.size() == 8);
		Assert.assertTrue(pools.contains(1));
		Assert.assertTrue(pools.contains(2));
		Assert.assertTrue(pools.contains(3));
		Assert.assertTrue(pools.contains(4));
		Assert.assertTrue(pools.contains(5));
		Assert.assertTrue(pools.contains(6));	
		Assert.assertTrue(pools.contains(11));
		Assert.assertTrue(!pools.contains(9));	
		Assert.assertTrue(!pools.contains(8));
		Assert.assertTrue(secondLegs.contains(8));
		Assert.assertTrue(secondLegs.contains(9));
		Assert.assertTrue(pools.contains(21));
	}


	@Test
	public void testPK4SecondLeg() {
		String text = "Exacta, Quinella, Trifecta &amp; 10 Cent Superfecta, 2nd Leg 50 Cent Pick 4, 1st Leg $1 Pick 3, 2nd Half Daily Double"; 
		List<Integer> secondLegs = new ArrayList<Integer>();
		List<Integer> pools = parser.parse(text, secondLegs);
		Assert.assertTrue(pools.size() == 8);
		Assert.assertTrue(pools.contains(1));
		Assert.assertTrue(pools.contains(2));
		Assert.assertTrue(pools.contains(3));
		Assert.assertTrue(pools.contains(4));
		Assert.assertTrue(pools.contains(5));
		Assert.assertTrue(pools.contains(6));	
		Assert.assertTrue(pools.contains(8));
		Assert.assertTrue(!pools.contains(9));	
		Assert.assertTrue(!pools.contains(7));
		Assert.assertTrue(secondLegs.contains(7));
		Assert.assertTrue(secondLegs.contains(9));
		Assert.assertTrue(pools.contains(21));
	}

	@Test
	public void testCalJackpot6() {
		String text = "EXACTA - 20 CENT TRIFECTA - 10 CENT CAL EXPO JACKPOT 6 (16% TAKEOUT) (3-4-5-6-7-8)"; 
		List<Integer> secondLegs = new ArrayList<Integer>();
		List<Integer> pools = parser.parse(text, secondLegs);
		Assert.assertTrue(pools.size() == 6);
		Assert.assertTrue(pools.contains(1));
		Assert.assertTrue(pools.contains(2));
		Assert.assertTrue(pools.contains(3));
		Assert.assertTrue(pools.contains(4));
		Assert.assertTrue(pools.contains(6));		
		Assert.assertTrue(pools.contains(11));		
	}
	 
	@Test
	public void testGolden6() {
		String text = "$1 Exacta / $1 Trifecta / $1 Superfecta (.10 min) $0.20 Golden Pick 6 (Races 5-10)$1 Pick 3 (Races 5-7) $2 Rolling Double"; 
		List<Integer> secondLegs = new ArrayList<Integer>();
		List<Integer> pools = parser.parse(text, secondLegs);
		Assert.assertTrue(pools.size() == 9);
		Assert.assertTrue(pools.contains(1));
		Assert.assertTrue(pools.contains(2));
		Assert.assertTrue(pools.contains(3));
		Assert.assertTrue(pools.contains(4));
		Assert.assertTrue(pools.contains(6));
		Assert.assertTrue(pools.contains(7));
		Assert.assertTrue(pools.contains(8));
		Assert.assertTrue(pools.contains(11));
		Assert.assertTrue(pools.contains(21));
	}	
	
	@Test
	public void testJersey6() {
		String text = "Win, Place and Show Exacta, 50-Cent Trifecta and 10-Cent Superfecta Daily Double (Races 1-2)/50-Cent Pick 3 (Races 1-2-3) 10-Cent Jersey 6 (Races 1-2-3-4-5-6)"; 
		List<Integer> secondLegs = new ArrayList<Integer>();
		List<Integer> pools = parser.parse(text, secondLegs);
		Assert.assertTrue(pools.size() == 9);
		Assert.assertTrue(pools.contains(1));
		Assert.assertTrue(pools.contains(2));
		Assert.assertTrue(pools.contains(3));
		Assert.assertTrue(pools.contains(4));
		Assert.assertTrue(pools.contains(6));
		Assert.assertTrue(pools.contains(7));
		Assert.assertTrue(pools.contains(8));
		Assert.assertTrue(pools.contains(11));
		Assert.assertTrue(pools.contains(21));
	}
	
	@Test
	public void testChurchillLucky7() {
		String text = "Daily Double / Exacta / Trifecta / Superfecta / Pick 3 (Races 5-6-7) Lucky 7 (Races 5-11)"; 
		List<Integer> secondLegs = new ArrayList<Integer>();
		List<Integer> pools = parser.parse(text, secondLegs);
		Assert.assertTrue(pools.size() == 9);
		Assert.assertTrue(pools.contains(1));
		Assert.assertTrue(pools.contains(2));
		Assert.assertTrue(pools.contains(3));
		Assert.assertTrue(pools.contains(4));
		Assert.assertTrue(pools.contains(6));
		Assert.assertTrue(pools.contains(7));
		Assert.assertTrue(pools.contains(8));
		Assert.assertTrue(pools.contains(12));
		Assert.assertTrue(pools.contains(21));
	}
	
	@Test
	public void testFonnerPk5() {
		String text = "DINSDALE AUTOMOTIVE $5,000 P5 JACKPOT .50 MIN - $4,000 GUARANTEED MIN. PAYOUT PICK 3 WAGERING (RACES 5,6,7) EXACTA & TRIFECTA WAGERING";
		List<Integer> secondLegs = new ArrayList<Integer>();
		List<Integer> pools = parser.parse(text, secondLegs);
		Assert.assertTrue(pools.size() == 7);
		Assert.assertTrue(pools.contains(1));
		Assert.assertTrue(pools.contains(2));
		Assert.assertTrue(pools.contains(3));
		Assert.assertTrue(pools.contains(4));
		Assert.assertTrue(pools.contains(6));	
		Assert.assertTrue(pools.contains(8));	
		Assert.assertTrue(pools.contains(10));
	}
	
	@Test
	public void testZIAPK4() {
		String text = "$1 Exacta / .50 Cent Trifecta / Ten Cent Superfecta/ 3rd Leg Pick Three 1st Leg .50 Cent Pick Four"; 
		List<Integer> secondLegs = new ArrayList<Integer>();
		List<Integer> pools = parser.parse(text, secondLegs);
		Assert.assertTrue(pools.size() == 7);
		Assert.assertTrue(pools.contains(1));
		Assert.assertTrue(pools.contains(2));
		Assert.assertTrue(pools.contains(3));
		Assert.assertTrue(pools.contains(4));
		Assert.assertTrue(pools.contains(6));
		Assert.assertTrue(!pools.contains(11));
		Assert.assertTrue(pools.contains(21));
		Assert.assertTrue(pools.contains(9));
		Assert.assertTrue(!pools.contains(8));
		Assert.assertTrue(secondLegs.contains(8));
	}

	@Test
	public void testRace1() {
		String text = "Exacta, Quiniela, Trifecta, .10 Cent Super, 1st Half Of Daily Double Be My Bubba - 08/ 5/2000 - 29.33"; 
		List<Integer> pools = parser.parse(text, null);
		Assert.assertTrue(pools.size() == 8);
		Assert.assertTrue(pools.contains(1));
		Assert.assertTrue(pools.contains(2));
		Assert.assertTrue(pools.contains(3));
		Assert.assertTrue(pools.contains(4));
		Assert.assertTrue(pools.contains(5));
		Assert.assertTrue(pools.contains(6));
		Assert.assertTrue(pools.contains(7));
		Assert.assertTrue(pools.contains(21));
	}

	@Test
	public void testRace2() {
		String text = "Quiniela, Trifecta, .10 Cent Super, Exacta, 2nd Half Of Daily Double Rk Inn Thegroove - 05/29/2002 - 37.96";
		List<Integer> secondLegs = new ArrayList<Integer>();
		List<Integer> pools = parser.parse(text, secondLegs);
		Assert.assertTrue(pools.size() == 7);
		Assert.assertTrue(pools.contains(1));
		Assert.assertTrue(pools.contains(2));
		Assert.assertTrue(pools.contains(3));
		Assert.assertTrue(pools.contains(4));
		Assert.assertTrue(pools.contains(5));
		Assert.assertTrue(pools.contains(6));
		Assert.assertTrue(!pools.contains(11));
		Assert.assertTrue(pools.contains(21));
		Assert.assertTrue(!pools.contains(7));
		Assert.assertTrue(secondLegs.contains(7));
	}

	@Test
	public void testRace8() {
		String text = "Quiniela, Trifecta, .10 Cent Super, Exacta, 2nd Half Of Tri Super Be My Bubba - 08/ 5/2000 - 29.33"; 
		List<Integer> secondLegs = new ArrayList<Integer>();
		List<Integer> pools = parser.parse(text, secondLegs);
		Assert.assertTrue(pools.size() == 7);
		Assert.assertTrue(pools.contains(1));
		Assert.assertTrue(pools.contains(2));
		Assert.assertTrue(pools.contains(3));
		Assert.assertTrue(pools.contains(4));
		Assert.assertTrue(pools.contains(5));
		Assert.assertTrue(pools.contains(6));
		Assert.assertTrue(!pools.contains(11));
		Assert.assertTrue(pools.contains(21));
		Assert.assertTrue(!pools.contains(7));
		Assert.assertTrue(secondLegs.contains(22));
	}

	@Test
	public void testRace12() {
		String text = "Quiniela, Exacta, .10 Cent Super, Twin Tri, Trifecta Be My Bubba - 08/ 5/2000 - 29.33"; 
		List<Integer> secondLegs = new ArrayList<Integer>();
		List<Integer> pools = parser.parse(text, secondLegs);
		Assert.assertTrue(pools.size() == 8);
		Assert.assertTrue(pools.contains(1));
		Assert.assertTrue(pools.contains(2));
		Assert.assertTrue(pools.contains(3));
		Assert.assertTrue(pools.contains(4));
		Assert.assertTrue(pools.contains(5));
		Assert.assertTrue(pools.contains(6));
		Assert.assertTrue(!pools.contains(11));
		Assert.assertTrue(pools.contains(21));
		Assert.assertTrue(pools.contains(19));
		Assert.assertTrue(!pools.contains(7));
		Assert.assertTrue(!secondLegs.contains(22));
	}

	@Test
	public void testRace14() {
		String text = "Quiniela, Trifecta, .10 Cent Super, Exacta, 2nd Half Of Twin Tri Be My Bubba - 08/ 5/2000 - 29.33"; 
		List<Integer> secondLegs = new ArrayList<Integer>();
		List<Integer> pools = parser.parse(text, secondLegs);
		Assert.assertTrue(pools.size() == 7);
		Assert.assertTrue(pools.contains(1));
		Assert.assertTrue(pools.contains(2));
		Assert.assertTrue(pools.contains(3));
		Assert.assertTrue(pools.contains(4));
		Assert.assertTrue(pools.contains(5));
		Assert.assertTrue(pools.contains(6));
		Assert.assertTrue(!pools.contains(11));
		Assert.assertTrue(pools.contains(21));
		Assert.assertTrue(!pools.contains(19));
		Assert.assertTrue(!pools.contains(7));
		Assert.assertTrue(secondLegs.contains(19));
	}

	@Test
	public void testRace15() {
		String text = "Quiniela, Exacta, Trifecta, .10 Cent Super, 2nd Half Of Late Daily Double Be My Bubba - 08/ 5/2000 - 29.33"; 
		List<Integer> secondLegs = new ArrayList<Integer>();
		List<Integer> pools = parser.parse(text, secondLegs);
		Assert.assertTrue(pools.size() == 7);
		Assert.assertTrue(pools.contains(1));
		Assert.assertTrue(pools.contains(2));
		Assert.assertTrue(pools.contains(3));
		Assert.assertTrue(pools.contains(4));
		Assert.assertTrue(pools.contains(5));
		Assert.assertTrue(pools.contains(6));
		Assert.assertTrue(!pools.contains(11));
		Assert.assertTrue(pools.contains(21));
		Assert.assertTrue(!pools.contains(19));
		Assert.assertTrue(!pools.contains(7));
		Assert.assertTrue(secondLegs.contains(7));
	}

	@Test
	public void testRiver2() {
		String text = "Wps-quiniela-perfecta-trifecta-superfecta($.10)-pic 3 First Race Of Pic 3 This Race Second Half Daily Double"; 
		List<Integer> secondLegs = new ArrayList<Integer>();
		List<Integer> pools = parser.parse(text, secondLegs);
		Assert.assertTrue(pools.size() == 8);
		Assert.assertTrue(pools.contains(1));
		Assert.assertTrue(pools.contains(2));
		Assert.assertTrue(pools.contains(3));
		Assert.assertTrue(pools.contains(4));
		Assert.assertTrue(pools.contains(5));
		Assert.assertTrue(pools.contains(6));
		Assert.assertTrue(pools.contains(8));
		Assert.assertTrue(!pools.contains(11));
		Assert.assertTrue(pools.contains(21));
		Assert.assertTrue(!pools.contains(19));
		Assert.assertTrue(!pools.contains(7));
		Assert.assertTrue(secondLegs.contains(7));
	}

	@Test
	public void testRiver3() {
		String text = "Wps-quiniela-perfecta-trifecta-superfecta($.10) Second Race Of Pic 3 This Race"; 
		List<Integer> secondLegs = new ArrayList<Integer>();
		List<Integer> pools = parser.parse(text, secondLegs);
		Assert.assertTrue(pools.size() == 7);
		Assert.assertTrue(pools.contains(1));
		Assert.assertTrue(pools.contains(2));
		Assert.assertTrue(pools.contains(3));
		Assert.assertTrue(pools.contains(4));
		Assert.assertTrue(pools.contains(5));
		Assert.assertTrue(pools.contains(6));
		Assert.assertTrue(!pools.contains(11));
		Assert.assertTrue(pools.contains(21));
		Assert.assertTrue(!pools.contains(19));
		Assert.assertTrue(!pools.contains(8));
		Assert.assertTrue(secondLegs.contains(8));
	}

	@Test
	public void testRiver4() {
		String text = "Wps-quiniela-perf-trifecta-super($.10) Third Race Of Pic 3 This Race"; 
		List<Integer> secondLegs = new ArrayList<Integer>();
		List<Integer> pools = parser.parse(text, secondLegs);
		Assert.assertTrue(pools.size() == 6);
		Assert.assertTrue(pools.contains(1));
		Assert.assertTrue(pools.contains(2));
		Assert.assertTrue(pools.contains(3));
		Assert.assertTrue(!pools.contains(4));
		Assert.assertTrue(pools.contains(5));
		Assert.assertTrue(pools.contains(6));
		Assert.assertTrue(!pools.contains(11));
		Assert.assertTrue(pools.contains(21));
		Assert.assertTrue(!pools.contains(19));
		Assert.assertTrue(!pools.contains(8));
		Assert.assertTrue(secondLegs.contains(8));
	}

	@Test
	public void testRiver5() {
		String text = "Wps-quiniela-perfecta-superfecta($.10)-tri-super  First Half Tri-super This Race"; 
		List<Integer> secondLegs = new ArrayList<Integer>();
		List<Integer> pools = parser.parse(text, secondLegs);
		Assert.assertTrue(pools.size() == 7);
		Assert.assertTrue(pools.contains(1));
		Assert.assertTrue(pools.contains(2));
		Assert.assertTrue(pools.contains(3));
		Assert.assertTrue(pools.contains(22));
		Assert.assertTrue(pools.contains(5));
		Assert.assertTrue(!pools.contains(6));
		Assert.assertTrue(!pools.contains(11));
		Assert.assertTrue(pools.contains(21));
		Assert.assertTrue(!pools.contains(19));
		Assert.assertTrue(!pools.contains(8));
		Assert.assertTrue(!secondLegs.contains(8));
	}

	@Test
	public void testRiver6() {
		String text = "Wps-quiniela-perfecta-trifecta-superfecta($.10)  Second Half Tri-super This Race" ; 
		List<Integer> secondLegs = new ArrayList<Integer>();
		List<Integer> pools = parser.parse(text, secondLegs);
		Assert.assertTrue(pools.size() == 7);
		Assert.assertTrue(pools.contains(1));
		Assert.assertTrue(pools.contains(2));
		Assert.assertTrue(pools.contains(3));
		Assert.assertTrue(pools.contains(4));
		Assert.assertTrue(pools.contains(5));
		Assert.assertTrue(pools.contains(6));
		Assert.assertTrue(!pools.contains(11));
		Assert.assertTrue(pools.contains(21));
		Assert.assertTrue(!pools.contains(19));
		Assert.assertTrue(!pools.contains(22));
		Assert.assertTrue(secondLegs.contains(22));
	}

	@Test
	public void testRiver10() {
		String text = "Wps-quiniela-perfecta-superfecta($.10)-twin-tri Second Race Of Pic 3 This Race First Half Twin-tri This Race"  ; 
		List<Integer> secondLegs = new ArrayList<Integer>();
		List<Integer> pools = parser.parse(text, secondLegs);
		Assert.assertTrue(pools.size() == 7);
		Assert.assertTrue(pools.contains(1));
		Assert.assertTrue(pools.contains(2));
		Assert.assertTrue(pools.contains(3));
		Assert.assertTrue(pools.contains(4));
		Assert.assertTrue(pools.contains(5));
		Assert.assertTrue(!pools.contains(6));
		Assert.assertTrue(!pools.contains(11));
		Assert.assertTrue(pools.contains(21));
		Assert.assertTrue(pools.contains(19));
		Assert.assertTrue(!pools.contains(8));
		Assert.assertTrue(secondLegs.contains(8));
	}

	@Test
	public void testRiver13() {
		String text = "Wps-quiniela-perfecta-trifecta-superfecta($.10)-double-quin Second Half Twin-tri This Race First Half Double -quin This Race"; 
		List<Integer> secondLegs = new ArrayList<Integer>();
		List<Integer> pools = parser.parse(text, secondLegs);
		Assert.assertTrue(pools.size() == 8);
		Assert.assertTrue(pools.contains(1));
		Assert.assertTrue(pools.contains(2));
		Assert.assertTrue(pools.contains(3));
		Assert.assertTrue(pools.contains(4));
		Assert.assertTrue(pools.contains(5));
		Assert.assertTrue(pools.contains(6));
		Assert.assertTrue(!pools.contains(11));
		Assert.assertTrue(pools.contains(21));
		Assert.assertTrue(!pools.contains(19));
		Assert.assertTrue(!pools.contains(7));
		Assert.assertTrue(pools.contains(17));
		Assert.assertTrue(secondLegs.contains(19));
	}

	@Test
	public void testRiver14() {
		String text = "Wps-quiniela-perfecta-trifecta-superfecta($.10)  Second Half Of Double-quin This Race"; 
		List<Integer> secondLegs = new ArrayList<Integer>();
		List<Integer> pools = parser.parse(text, secondLegs);
		Assert.assertTrue(pools.size() == 7);
		Assert.assertTrue(pools.contains(1));
		Assert.assertTrue(pools.contains(2));
		Assert.assertTrue(pools.contains(3));
		Assert.assertTrue(pools.contains(4));
		Assert.assertTrue(pools.contains(5));
		Assert.assertTrue(pools.contains(6));
		Assert.assertTrue(!pools.contains(11));
		Assert.assertTrue(pools.contains(21));
		Assert.assertTrue(!pools.contains(19));
		Assert.assertTrue(!pools.contains(17));
		Assert.assertTrue(secondLegs.contains(17));
	}


	@Test
	public void testNoShow2() {
		String text = "WIN, PLACE, EXACTA, TRIFECTA, SWINGER";
		List<Integer> pools = parser.parse(text, null);
		Assert.assertTrue(pools.size() == 5);
		Assert.assertTrue(!pools.contains(3));
	}

	@Test
	public void testNoShow3() {
		String text = "EXACTA TRIFECTA NO SHOW WAGERING";
		List<Integer> pools = parser.parse(text, null);
		Assert.assertTrue(pools.size() == 4);
		Assert.assertTrue(!pools.contains(3));
	}

	@Test
	public void testShow1() {
		String text = "Win, Place, Show, Exacta, Quinella, Trifecta, Superfecta";
		List<Integer> pools = parser.parse(text, null);
		Assert.assertTrue(pools.size() == 7);
		Assert.assertTrue(pools.contains(3));
	}
	


}
