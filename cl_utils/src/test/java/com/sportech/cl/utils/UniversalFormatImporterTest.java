package com.sportech.cl.utils;

import java.io.InputStream;

import org.apache.commons.io.IOUtils;
import org.junit.Assert;
import org.junit.Test;

import com.sportech.cl.model.database.HardCard;
import com.sportech.cl.model.database.HardPool;
import com.sportech.cl.model.importer.ConverterType;
import com.sportech.cl.utils.importer.CardImporter;
import com.sportech.cl.utils.importer.CardImporterFactory;

public class UniversalFormatImporterTest {

	@Test
	public void testBasic() {
		InputStream stream = ClassLoader.getSystemResourceAsStream("S_20131218_20131217_173627.xml");
		CardImporter importer = CardImporterFactory.create(ConverterType.UNIVERSAL, null);
		try {
			byte[] data = IOUtils.toByteArray(stream);
			HardCard card = importer.parse(data).get(0);
			Assert.assertTrue(card.getRaces().size() == 12);
			Assert.assertTrue(card.getTrack().getEquibaseId().equals("S"));
			Assert.assertTrue(card.getPools().size() == 9);
		} catch (Exception e) {
			Assert.fail();
		}
	}

	@Test
	public void testExaAndExaAlias() {
		InputStream stream = ClassLoader.getSystemResourceAsStream("Tä_20131218_20131217_173635.xml");
		CardImporter importer = CardImporterFactory.create(ConverterType.UNIVERSAL, null);
		try {
			byte[] data = IOUtils.toByteArray(stream);
			HardCard card = importer.parse(data).get(0);
			Assert.assertTrue(card.getRaces().size() == 5);
			Assert.assertTrue(card.getTrack().getEquibaseId().equals("Tä"));
			Assert.assertTrue(card.getPools().size() == 7);

			for (HardPool p : card.getPools()) {
			    if (p.getPoolTypeId() == 3 && p.getRaceBitmap() != 6L) {
	                 Assert.assertTrue(p.getPoolCode().equals("K"));
                    Assert.assertTrue(p.getPoolName().equals("Komb"));			        
			    }
			    else if (p.getPoolTypeId() == 3 && p.getRaceBitmap() == 6L) {
	                Assert.assertTrue(p.getPoolCode().equals("SK"));
	                Assert.assertTrue(p.getPoolName().equals("Specialkomb"));
			    }			    
			}
			
		} catch (Exception e) {
			Assert.fail();
		}
	}

}
