package com.sportech.cl.utils;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import com.sportech.cl.utils.importer.PoolTextParser;

public class PoolParserTest {

    private PoolTextParser parser = new PoolTextParser("/pooltypeparserules.json");

    private String sampleWagerText = "FIRST HALF $2 EARLY DAILY DOUBLE (RACES 1 &amp; 2) ($.50) PICK 5 (RACES 1-2-3-4-5) $2 TRIFECTA $2 PERFECTA $1 TRIFECTA BOX $1 PERFECTA BOX ($.50) SUPERFECTA";
    

    @Test
    public void testSwinger() {
        String text = "WIN, PLACE, EXACTA, TRIFECTA, SWINGER";
        List<Integer> pools = parser.parse(text, null);
        Assert.assertTrue(pools.contains(25));
    }

    @Test
    public void testTrackInfo() {
        String trackInfoText = "1ra.mitad Doble Seleccion/quiniela/exacta/trifecta/apuesta Derecha ";
        List<Integer> pools = parser.parse(trackInfoText, null);
        Assert.assertTrue(pools.size() == 8);
        Assert.assertTrue(pools.contains(21));
        Assert.assertTrue(pools.contains(7));
    }

    @Test
    public void testTwinTri1() {
        String trackInfoText = "wps-quiniela-perfecta-superfecta($.10)-twin-tri";
        List<Integer> pools = parser.parse(trackInfoText, null);
        Assert.assertTrue(pools.size() == 7);
        Assert.assertTrue(pools.contains(19));
    }

    @Test
    public void testTwinTri2() {
        String trackInfoText = "Exacta, Quinella, 10 Cent Super, Twin Tri";
        List<Integer> pools = parser.parse(trackInfoText, null);
        Assert.assertTrue(pools.size() == 7);
        Assert.assertTrue(pools.contains(19));
    }

    @Test
    public void testNoTwinTri() {
        String trackInfoText = "Exacta, Quinella, 10 Cent Super, Trifecta, 2nd Half Twin Tri";
        List<Integer> pools = parser.parse(trackInfoText, null);
        Assert.assertTrue(pools.size() == 7);
        Assert.assertTrue(!pools.contains(19));
    }

    @Test
    public void testSuper() {
        String trackInfoText = "Exacta, Quinella, Superfecta, Super Hi-five";
        List<Integer> pools = parser.parse(trackInfoText, null);
        Assert.assertTrue(pools.size() == 7);
        Assert.assertTrue(pools.contains(21));
        Assert.assertTrue(pools.contains(28));
    }

    @Test
    public void testSuper1() {
        String trackInfoText = "EXACTA, TRIFECTA &amp; SUPERECTA";
        List<Integer> pools = parser.parse(trackInfoText, null);
        Assert.assertTrue(pools.size() == 6);
        Assert.assertTrue(pools.contains(21));
        Assert.assertTrue(pools.contains(4));
    }

    @Test
    public void testTriSuper() {
        String trackInfoText = "Exacta, Quinella, 10 Cent Super, Tri-super";
        List<Integer> pools = parser.parse(trackInfoText, null);
        Assert.assertTrue(pools.size() == 7);
        Assert.assertTrue(pools.contains(21));
        Assert.assertTrue(pools.contains(22));
    }

    @Test
    public void testNoTriSuper1() {
        String trackInfoText = "Exacta, Quinella, 10 Cent Super, Trifecta, 2nd Half Tri-super";
        List<Integer> secondLegs = new ArrayList<Integer>();
        List<Integer> pools = parser.parse(trackInfoText, secondLegs);
        Assert.assertTrue(pools.size() == 7);
        Assert.assertTrue(pools.contains(21));
        Assert.assertTrue(!pools.contains(22));
        Assert.assertTrue(secondLegs.contains(22));
    }

    @Test
    public void testNoTriSuper2() {
        String text = "Exacta, Quinella, Super, 2nd Half Tri-super, Daily Double Races 13,14";
        List<Integer> secondLegs = new ArrayList<Integer>();
        List<Integer> pools = parser.parse(text, secondLegs);
        Assert.assertTrue(pools.size() == 7);
        Assert.assertTrue(pools.contains(7));
        Assert.assertTrue(pools.contains(21));
        Assert.assertTrue(!pools.contains(22));
        Assert.assertTrue(secondLegs.contains(22));
    }

    @Test
    public void testNoSuper() {
        String trackInfoText = "EXACTA - 50 CENT TRIFECTA PICK 3 (3-4-5) NO SUPERFECTA WAGERING";
        List<Integer> pools = parser.parse(trackInfoText, null);
        Assert.assertTrue(pools.size() == 6);
        Assert.assertTrue(pools.contains(4));
        Assert.assertTrue(pools.contains(6));
        Assert.assertTrue(pools.contains(8));
        Assert.assertTrue(!pools.contains(21));
        Assert.assertTrue(!pools.contains(28));
    }

    @Test
    public void testBasic() {
        String text = "any text";
        List<Integer> pools = parser.parse(text, null);
        Assert.assertTrue(pools.size() == 3);
    }

    @Test
    public void testExacta() {
        String text = "Exacta";
        List<Integer> pools = parser.parse(text, null);
        Assert.assertTrue(pools.size() == 4);
    }

    @Test
    public void testExacta50() {
        String text = "1ST HALF DOUBLE (1-2)-EX-50 CENT TRI-10 CENT SUPER &amp; 50 CENT PK 5 (1-2-3-4-5)";
        List<Integer> pools = parser.parse(text, null);
        Assert.assertTrue(pools.contains(4));
        Assert.assertTrue(pools.contains(6));
        Assert.assertTrue(pools.contains(7));
        Assert.assertTrue(pools.contains(10));
        Assert.assertTrue(pools.contains(21));
    }

    @Test
    public void testExacta1() {
        String text = "Ex. / Tri. (.10 Cent Min.) / Super. (.10 Cent Min.) Double ( 6-7)(12% Takeout) / Pick 3 (6-7-8 )(12% Takeout / .50 Cent Min.) Pick 4 (6-7-8-9 / .50 Cent Min.)";
        List<Integer> pools = parser.parse(text, null);
        Assert.assertTrue(pools.size() == 9);
        Assert.assertTrue(pools.contains(4));
        Assert.assertTrue(pools.contains(6));
        Assert.assertTrue(pools.contains(7));
        Assert.assertTrue(pools.contains(8));
        Assert.assertTrue(pools.contains(9));
        Assert.assertTrue(pools.contains(21));
    }
    
    @Test
    public void testExacta2() {
        String text = "PERFECTA & 50 CENT TRIFECTA";
        List<Integer> pools = parser.parse(text, null);
        Assert.assertTrue(pools.size() == 5);
        Assert.assertTrue(pools.contains(4));
        Assert.assertTrue(pools.contains(6));
       
    }
    
    @Test
    public void testExacta3() {
        String text = "PERFECTA, 50 CENT TRIFECTA & 50 CENT SUPERFECTA";
        List<Integer> pools = parser.parse(text, null);
        Assert.assertTrue(pools.size() == 6);
        Assert.assertTrue(pools.contains(4));
        Assert.assertTrue(pools.contains(6));
        Assert.assertTrue(pools.contains(21));
    }
    
    @Test
    public void testExacta4() {
        String text = "DAILY DOUBLE (1-2) PERFECTA & 50 CENT TRIFECTA";
        List<Integer> pools = parser.parse(text, null);
        Assert.assertTrue(pools.size() == 6);
        Assert.assertTrue(pools.contains(4));
        Assert.assertTrue(pools.contains(6));
        Assert.assertTrue(pools.contains(7));
    }

    @Test
    public void testSuper50() {
        String text = "1ST HALF DOUBLE (1-2)-EX-50 CENT TRI-10 CENT SUPER50 CENT PK 5 (1-2-3-4-5)";
        List<Integer> pools = parser.parse(text, null);
        Assert.assertTrue(pools.contains(4));
        Assert.assertTrue(pools.contains(6));
        Assert.assertTrue(pools.contains(7));
        Assert.assertTrue(pools.contains(10));
        Assert.assertTrue(pools.contains(21));
        Assert.assertTrue(!pools.contains(28));
    }

    @Test
    public void testSampleWagerText() {
        String text = sampleWagerText;
        List<Integer> pools = parser.parse(text, null);
        Assert.assertTrue(pools.size() == 8);
    }

    @Test
    public void testDouble1() {
        String text = "Exacta, Trifecta (.50), Super (.10), Double Wagers";
        List<Integer> pools = parser.parse(text, null);
        Assert.assertTrue(pools.size() == 7);
        Assert.assertTrue(pools.contains(7));
        Assert.assertTrue(pools.contains(21));
        Assert.assertTrue(!pools.contains(22));
    }

    @Test
    public void testDouble2() {
        String text = "EARLY DOUBLE (1-2)($1) EXACTA($1)-TRIFECTA(50 CENT)";
        List<Integer> pools = parser.parse(text, null);
        Assert.assertTrue(pools.size() == 6);
        Assert.assertTrue(pools.contains(7));
        Assert.assertTrue(pools.contains(4));
        Assert.assertTrue(pools.contains(6));
    }

    @Test
    public void testDouble3() {
        String text = "MID DOUBLE (4-5) ($1) EXACTA ($1)- TRIFECTA (50 CENT) SUPERFECTA (10 CENT)";
        List<Integer> pools = parser.parse(text, null);
        Assert.assertTrue(pools.size() == 7);
        Assert.assertTrue(pools.contains(7));
        Assert.assertTrue(pools.contains(4));
        Assert.assertTrue(pools.contains(6));
        Assert.assertTrue(pools.contains(21));
    }

    @Test
    public void testDouble4() {
        String text = "LATE DOUBLE (8-9)($1) EXACTA($1)-TRIFECTA(50 CENT) SUPERFECTA(10 CENT)";
        List<Integer> pools = parser.parse(text, null);
        Assert.assertTrue(pools.size() == 7);
        Assert.assertTrue(pools.contains(7));
        Assert.assertTrue(pools.contains(4));
        Assert.assertTrue(pools.contains(6));
        Assert.assertTrue(pools.contains(21));
    }

    @Test
    public void testDouble5() {
        String text = "LATE DOUBLE (RACES 10 and 11), EXACTOR, $0.20 SUPERFECTA, $0.20 TRIACTOR";
        List<Integer> pools = parser.parse(text, null);
        Assert.assertTrue(pools.size() == 7);
        Assert.assertTrue(pools.contains(7));
        Assert.assertTrue(pools.contains(4));
        Assert.assertTrue(pools.contains(6));
        Assert.assertTrue(pools.contains(21));
    }

    @Test
    public void testDouble6() {
        String text = "Exacta / Trifecta (.10 Cent Minimum) / Superfecta (.10 Cent Minimum) Double (Races 1-2)(12% Takeout) Pick 3 (Races 1-2-3)(12% Takeout)(.50 Cent Minimum)";
        List<Integer> secondLegs = new ArrayList<Integer>();
        List<Integer> pools = parser.parse(text, secondLegs);
        Assert.assertTrue(pools.size() == 8);
        Assert.assertTrue(pools.contains(4));
        Assert.assertTrue(pools.contains(6));
        Assert.assertTrue(pools.contains(7));
        Assert.assertTrue(pools.contains(8));
        Assert.assertTrue(pools.contains(21));
        Assert.assertTrue(!pools.contains(22));
    }

    @Test
    public void testDouble7() {
        String text = "$1 Exacta / $1 Trifecta $2 Rolling Double/ $1 Rolling Pick Three (Races 1-2-3) $0.50 Pick 5 (Races 1-2-3-4-5) $1 Superfecta (.10 Min.)";
        List<Integer> secondLegs = new ArrayList<Integer>();
        List<Integer> pools = parser.parse(text, secondLegs);
        Assert.assertTrue(pools.size() == 9);
        Assert.assertTrue(pools.contains(4));
        Assert.assertTrue(pools.contains(6));
        Assert.assertTrue(pools.contains(7));
        Assert.assertTrue(pools.contains(8));
        Assert.assertTrue(pools.contains(10));
        Assert.assertTrue(pools.contains(21));
        Assert.assertTrue(!pools.contains(22));
    }
    @Test
    public void testDouble8() {
        String text = "$1 Exacta / $1 Trifecta / $1 Superfecta (.10 min) $2 Rolling Double / $1 Pick 3 (Races 1-2-3) $0.50 Pick 5 (Races 1-2-3-4-5)";
        List<Integer> secondLegs = new ArrayList<Integer>();
        List<Integer> pools = parser.parse(text, secondLegs);
        Assert.assertTrue(pools.size() == 9);
        Assert.assertTrue(pools.contains(4));
        Assert.assertTrue(pools.contains(6));
        Assert.assertTrue(pools.contains(7));
        Assert.assertTrue(pools.contains(8));
        Assert.assertTrue(pools.contains(10));
        Assert.assertTrue(pools.contains(21));
        Assert.assertTrue(!pools.contains(22));
    }
        
    @Test
    public void testDouble9() {
        String text = "$1 Exacta / $1 Trifecta / $1 Superfecta (.10 min) $2 Rolling Double/ $1 Pick 3 (Races 2-3-4) $0.50 Pick 4 (Races 2-3-4-5)";
        List<Integer> secondLegs = new ArrayList<Integer>();
        List<Integer> pools = parser.parse(text, secondLegs);
        Assert.assertTrue(pools.size() == 9);
        Assert.assertTrue(pools.contains(4));
        Assert.assertTrue(pools.contains(6));
        Assert.assertTrue(pools.contains(7));
        Assert.assertTrue(pools.contains(8));
        Assert.assertTrue(pools.contains(9));
        Assert.assertTrue(pools.contains(21));
        Assert.assertTrue(!pools.contains(22));
    }
    
    @Test
    public void testDouble10() {
        String text = "$1 Exacta / $1 Trifecta / $1 Superfecta (.10 min) $1 Pick 3 (Races 3-4-5)/$2 Rolling Double";
        List<Integer> secondLegs = new ArrayList<Integer>();
        List<Integer> pools = parser.parse(text, secondLegs);
        Assert.assertTrue(pools.size() == 8);
        Assert.assertTrue(pools.contains(4));
        Assert.assertTrue(pools.contains(6));
        Assert.assertTrue(pools.contains(7));
        Assert.assertTrue(pools.contains(8));
        Assert.assertTrue(pools.contains(21));
        Assert.assertTrue(!pools.contains(22));
    }
    
    @Test
    public void testDouble11() {
        String text = "$1 Exacta / $1 Trifecta / $1 Superfecta (.10 min) $2 Rolling Double / $1 Pick 3 (Races 4-5-6)";
        List<Integer> secondLegs = new ArrayList<Integer>();
        List<Integer> pools = parser.parse(text, secondLegs);
        Assert.assertTrue(pools.size() == 8);
        Assert.assertTrue(pools.contains(4));
        Assert.assertTrue(pools.contains(6));
        Assert.assertTrue(pools.contains(7));
        Assert.assertTrue(pools.contains(8));
        Assert.assertTrue(pools.contains(21));
        Assert.assertTrue(!pools.contains(22));
    }
    
    @Test
    public void testDouble12() {
        String text = "$1 Exacta / $1 Trifecta / $1 Superfecta (.10 min) $0.20 Golden Pick 6 (Races 5-10)$1 Pick 3 (Races 5-7) $2 Rolling Double";
        List<Integer> secondLegs = new ArrayList<Integer>();
        List<Integer> pools = parser.parse(text, secondLegs);
        Assert.assertTrue(pools.size() == 9);
        Assert.assertTrue(pools.contains(4));
        Assert.assertTrue(pools.contains(6));
        Assert.assertTrue(pools.contains(7));
        Assert.assertTrue(pools.contains(8));
        Assert.assertTrue(pools.contains(11));
        Assert.assertTrue(pools.contains(21));
        Assert.assertTrue(!pools.contains(22));
    }
    
    @Test
    public void testDouble13() {
        String text = "$1 Exacta / $1 Trifecta / $1 Superfecta (.10 Min.) / $2 Double";
        List<Integer> secondLegs = new ArrayList<Integer>();
        List<Integer> pools = parser.parse(text, secondLegs);
        Assert.assertTrue(pools.size() == 7);
        Assert.assertTrue(pools.contains(4));
        Assert.assertTrue(pools.contains(6));
        Assert.assertTrue(pools.contains(7));
        Assert.assertTrue(pools.contains(21));
        Assert.assertTrue(!pools.contains(22));
    }
    
    @Test
    public void testWin4() {
        String text = "EXACTOR, $0.20 SUPERFECTA, $0.20 TRIACTOR, WIN FOUR";
        List<Integer> pools = parser.parse(text, null);
        Assert.assertTrue(pools.size() == 7);
        Assert.assertTrue(pools.contains(4));
        Assert.assertTrue(pools.contains(6));
        Assert.assertTrue(pools.contains(21));
        Assert.assertTrue(pools.contains(9));
    }

    @Test
    public void testPick4() {
        String text = "LAST LEG PICK 3 $1 PICK 4 (5-6-7-8) $1 SUPERFECTA $1 EXACTA-$1 TRIFECTA";
        List<Integer> secondLegs = new ArrayList<Integer>();
        List<Integer> pools = parser.parse(text, secondLegs);
        Assert.assertTrue(pools.size() == 7);
        Assert.assertTrue(pools.contains(4));
        Assert.assertTrue(pools.contains(6));
        Assert.assertTrue(pools.contains(21));
        Assert.assertTrue(pools.contains(9));
        Assert.assertTrue(!pools.contains(8));
        Assert.assertTrue(secondLegs.contains(8));
    }

    @Test
    public void testPick41() {
        String text = "PICK 4 (5-6-7-8) $1 SUPERFECTA $1 EXACTA-$1 TRIFECTA";
        List<Integer> secondLegs = new ArrayList<Integer>();
        List<Integer> pools = parser.parse(text, secondLegs);
        Assert.assertTrue(pools.size() == 7);
        Assert.assertTrue(pools.contains(4));
        Assert.assertTrue(pools.contains(6));
        Assert.assertTrue(pools.contains(21));
        Assert.assertTrue(pools.contains(9));
        Assert.assertTrue(!pools.contains(8));
    }

    @Test
    public void testPickAll() {
        String text = "$1 Exacta / $1 Trifecta $1 Place Pick All Starts Here/Superfecta (.10 cent minimum) $1 Early Double / $1 Pick Four";
        List<Integer> secondLegs = new ArrayList<Integer>();
        List<Integer> pools = parser.parse(text, secondLegs);
        Assert.assertTrue(pools.contains(-1));
        Assert.assertTrue(pools.contains(4));
        Assert.assertTrue(pools.contains(6));
        Assert.assertTrue(pools.contains(7));
        Assert.assertTrue(pools.contains(9));
        Assert.assertTrue(pools.contains(21));
    }

}
