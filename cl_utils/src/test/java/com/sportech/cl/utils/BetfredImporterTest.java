package com.sportech.cl.utils;

import java.io.InputStream;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.junit.Assert;
import org.junit.Test;

import com.sportech.cl.model.database.HardCard;
import com.sportech.cl.utils.importer.BetfredImporter;

public class BetfredImporterTest {
    
    @Test
    public void testRmsMirrorCard() {
        InputStream stream = ClassLoader.getSystemResourceAsStream("rms_mirror_card.xml");
        BetfredImporter importer = new BetfredImporter(null);
        try {
            byte[] data = IOUtils.toByteArray(stream);
            HardCard card = importer.parse(data).get(0);
            Assert.assertTrue(card.getRaces().size() == 6);
        } catch (Exception e) {
            Assert.fail();
        }
    }

    @Test
    public void testRmsWithMirrorCard() {
        InputStream stream = ClassLoader.getSystemResourceAsStream("rms_card_20150110.xml");
        BetfredImporter importer = new BetfredImporter(null);
        try {
            byte[] data = IOUtils.toByteArray(stream);
            List<HardCard> cards = importer.parse(data);
            HardCard card = cards.get(14);
            Assert.assertTrue(card.getRaces().size() == 6);
            Assert.assertTrue(cards.size() == 17);
        } catch (Exception e) {
            Assert.fail();
        }
    }

    @Test
    public void testRmsWithoutMirrorCard() {
        InputStream stream = ClassLoader.getSystemResourceAsStream("rms_card_20150108.xml");
        BetfredImporter importer = new BetfredImporter(null);
        try {
            byte[] data = IOUtils.toByteArray(stream);
            List<HardCard> cards = importer.parse(data);
            Assert.assertTrue(cards.size() == 17);
        } catch (Exception e) {
            Assert.fail();
        }
    }

}
