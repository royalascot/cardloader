package com.sportech.cl.utils;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.junit.Assert;
import org.junit.Test;

import com.sportech.cl.model.database.HardCard;
import com.sportech.cl.model.database.HardRace;
import com.sportech.cl.model.importer.ConverterType;
import com.sportech.cl.utils.exporter.RifExporter;
import com.sportech.cl.utils.importer.CardImporter;
import com.sportech.cl.utils.importer.CardImporterFactory;

public class RifExporterTest {
	
	@Test
	public void testRifExporter() {
		InputStream stream = ClassLoader.getSystemResourceAsStream("FINALD20140305MNR_USA.xml");
		CardImporter importer = CardImporterFactory.create(ConverterType.EQUIBASE, null);
		try {
			byte[] data = IOUtils.toByteArray(stream);
			HardCard card = importer.parse(data).get(0);
			Assert.assertTrue(card.getRaces().size() == 9);
			for (HardRace r : card.getRaces()) {
				if (r.getNumber() == 1L) {
					Assert.assertTrue(r.getRunners().size() == 9);
					break;
				}
			}
			RifExporter exporter = new RifExporter();
			List<HardCard> cards = new ArrayList<HardCard>();
			cards.add(card);
			String result = exporter.build(card.getCardDate(), cards); 
			Assert.assertTrue(result != null);
			Assert.assertTrue(StringUtils.contains(result, "\"RDH\",\"03/05/2014\",1,,66,,,"));
			Assert.assertTrue(StringUtils.contains(result, "\"RD1\",\"MNR5-MAR-14M\",9,\"6\",6,\"Bluff Road\",\"Eddie Martin, Jr.\",\"6\""));
		} catch (Exception e) {
			Assert.fail();
		}
	}

}
