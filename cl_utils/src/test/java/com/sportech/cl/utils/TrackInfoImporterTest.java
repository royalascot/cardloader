package com.sportech.cl.utils;

import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.junit.Assert;
import org.junit.Test;

import com.sportech.cl.model.database.HardCard;
import com.sportech.cl.model.database.HardRace;
import com.sportech.cl.model.importer.ConverterType;
import com.sportech.cl.utils.importer.CardImporter;
import com.sportech.cl.utils.importer.CardImporterFactory;

public class TrackInfoImporterTest {
	
	@Test
	public void testTrackInfoParser() {
		InputStream stream = ClassLoader.getSystemResourceAsStream("GTS$20140502E-$$$$-TD08.xml");
		CardImporter importer = CardImporterFactory.create(ConverterType.TRACK_INFO_E, null);
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
			Date d = sdf.parse("20140502");
			byte[] data = IOUtils.toByteArray(stream);
			HardCard card = importer.parse(data).get(0);
			Assert.assertTrue(DateUtils.isSameDay(card.getCardDate(), d));
			Assert.assertTrue(card.getRaces().size() == 16);
			for (HardRace r : card.getRaces()) {
				if (r.getNumber() == 1L) {
					Assert.assertTrue(r.getRunners().size() == 8);
					break;
				}
			}
		} catch (Exception e) {
			Assert.fail();
		}
	}
	
}