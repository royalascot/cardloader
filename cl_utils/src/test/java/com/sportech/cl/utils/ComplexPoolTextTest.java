package com.sportech.cl.utils;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import com.sportech.cl.utils.importer.PoolTextParser;

public class ComplexPoolTextTest {
	
	private PoolTextParser parser = new PoolTextParser("/pooltypeparserules.json");
	
	@Test
	public void testNoShow1() {
		String text = "$2 WP / $2 Exacta / Trifecta (min .50 cent) Pick 3 (Races 2-3-4 min .50 cent) / Pick 6 (Races 2-7 min. 50 cent)";
		List<Integer> pools = parser.parse(text, null);
		Assert.assertTrue(pools.size() == 6);
		Assert.assertTrue(!pools.contains(3));
	}

	@Test
	public void testNoShow2() {
		String text = "WIN, PLACE, EXACTA, TRIFECTA, SWINGER";
		List<Integer> pools = parser.parse(text, null);
		Assert.assertTrue(pools.size() == 5);
		Assert.assertTrue(!pools.contains(3));
	}

	@Test
	public void testNoShow3() {
		String text = "EXACTA TRIFECTA NO SHOW WAGERING";
		List<Integer> pools = parser.parse(text, null);
		Assert.assertTrue(pools.size() == 4);
		Assert.assertTrue(!pools.contains(3));
	}

	@Test
	public void testWinOnly1() {
		String text = "Win(only), Exacta, Trifecta";
		List<Integer> pools = parser.parse(text, null);
		Assert.assertTrue(pools.size() == 3);
		Assert.assertTrue(!pools.contains(3));
		Assert.assertTrue(!pools.contains(2));
	}

	@Test
	public void testWinOnly2() {
		String text = "EXACTA-TRIFECTA NO PLACE WAGERING NO SHOW WAGERING";
		List<Integer> pools = parser.parse(text, null);
		Assert.assertTrue(pools.size() == 3);
		Assert.assertTrue(!pools.contains(3));
		Assert.assertTrue(!pools.contains(2));
	}

	@Test
	public void testShow1() {
		String text = "Win, Place, Show, Exacta, Quinella, Trifecta, Superfecta";
		List<Integer> pools = parser.parse(text, null);
		Assert.assertTrue(pools.size() == 7);
		Assert.assertTrue(pools.contains(3));
	}
	
	@Test
	public void testNoPlaceNoShow() {
		String text = "EXACTA &amp; TRIFECTA NO PLACE &amp; SHOW WAGERING";
		List<Integer> pools = parser.parse(text, null);
		Assert.assertTrue(pools.size() == 3);
		Assert.assertTrue(!pools.contains(3));
		Assert.assertTrue(!pools.contains(2));
	}
	
	@Test
	public void testPenta1() {
		String text = "EXACTA, TRIFECTA,  &amp;  20 CENT SUPER - HIGH FIVE";
		List<Integer> pools = parser.parse(text, null);
		Assert.assertTrue(pools.size() == 6);
		Assert.assertTrue(pools.contains(28));
		Assert.assertTrue(!pools.contains(21));
	}

	@Test
	public void testPenta2() {
		String text = "Second Half Late Double / Exactor /.20 Triactor /.20 Superfecta /.20 Jackpot Hi5";
		List<Integer> secondLegs = new ArrayList<Integer>();
		List<Integer> pools = parser.parse(text, secondLegs);
		Assert.assertTrue(pools.size() == 7);
		Assert.assertTrue(pools.contains(4));
		Assert.assertTrue(pools.contains(6));
		Assert.assertTrue(pools.contains(21));
		Assert.assertTrue(pools.contains(28));
		Assert.assertTrue(!pools.contains(7));
		Assert.assertTrue(secondLegs.contains(7));
	}

	@Test
	public void testDQL() {
		String text = "Wps-quiniela-perfecta-trifecta-superfecta($.10)-double-quin Second Half Twin-tri This Race First Half Double -quin This Race";
		List<Integer> secondLegs = new ArrayList<Integer>();
		List<Integer> pools = parser.parse(text, secondLegs);
		Assert.assertTrue(pools.size() == 8);
		Assert.assertTrue(pools.contains(4));
		Assert.assertTrue(pools.contains(5));
		Assert.assertTrue(pools.contains(6));
		Assert.assertTrue(pools.contains(21));
		Assert.assertTrue(pools.contains(17));
		
		Assert.assertTrue(!pools.contains(7));
		Assert.assertTrue(!pools.contains(19));		
		Assert.assertTrue(secondLegs.contains(19));
		Assert.assertTrue(!secondLegs.contains(7));
		
	}
	
	@Test
	public void testNoDQL() {
		String text = "2ND HALF LATE DAILY DOUBLE QUINELLA-EXACTA &amp; TRIFECTA WAGERING";
		List<Integer> secondLegs = new ArrayList<Integer>();
		List<Integer> pools = parser.parse(text, secondLegs);
		Assert.assertTrue(pools.size() == 6);
		Assert.assertTrue(pools.contains(5));
		Assert.assertTrue(pools.contains(6));
		Assert.assertTrue(!pools.contains(7));
		Assert.assertTrue(!pools.contains(17));
		Assert.assertTrue(secondLegs.contains(7));
	}
	
	@Test
	public void testPick9() {
		String text = "$2 Win/Place/Show / $2 Daily Double (1-2) / $2 Exacta / $0.50 Trifecta $0.10 Superfecta / $1 Pick 3 (1-3) / $0.10 Jackpot Pick 9 (1-9) (15% takeout)";
		List<Integer> pools = parser.parse(text, null);
		Assert.assertTrue(pools.size() == 9);
		Assert.assertTrue(pools.contains(1));
		Assert.assertTrue(pools.contains(2));
		Assert.assertTrue(pools.contains(3));
		Assert.assertTrue(pools.contains(4));
		Assert.assertTrue(pools.contains(6));
		Assert.assertTrue(pools.contains(7));
		Assert.assertTrue(pools.contains(8));
		Assert.assertTrue(pools.contains(14));
		Assert.assertTrue(pools.contains(21));
	}

}
