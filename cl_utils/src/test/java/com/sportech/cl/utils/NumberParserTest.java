package com.sportech.cl.utils;

import java.util.ArrayList;

import org.junit.Assert;
import org.junit.Test;

import com.sportech.cl.utils.importer.NumberTextParser;

public class NumberParserTest {
	
	@Test
	public void testZero() {
		String text = "Zero";
		Long result = NumberTextParser.parseNumberText(text);
		Assert.assertTrue(result == 0);
		result = NumberTextParser.parseNumberText("");
		Assert.assertTrue(result == 0);
	}

	@Test
	public void test121() {
		String text = "One hundred and twenty-one";
		Long result = NumberTextParser.parseNumberText(text);
		Assert.assertTrue(result == 121);
	}

	@Test
	public void test121000300() {
		String text = "One hundred and twenty-one million and three hundred";
		Long result = NumberTextParser.parseNumberText(text);
		Assert.assertTrue(result == 121000300);
	}

	@Test
	public void test121000319() {
		String text = "One hundred and twenty-one million and three hundred nineteen";
		Long result = NumberTextParser.parseNumberText(text);
		Assert.assertTrue(result == 121000319);
	}

	@Test
	public void testOneMile() {
		String text = "One Mile";
		ArrayList<String> words = new ArrayList<String>();
		Long result = NumberTextParser.parseNumberText(text, words);
		Assert.assertTrue(result == 1);
		Assert.assertTrue(words.get(0).equals("mile"));
	}

}
