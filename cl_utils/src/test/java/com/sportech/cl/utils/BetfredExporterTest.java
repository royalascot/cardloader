package com.sportech.cl.utils;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.junit.Assert;
import org.junit.Test;

import com.sportech.cl.model.database.HardCard;
import com.sportech.cl.utils.exporter.BetfredExporter;
import com.sportech.cl.utils.importer.BetfredImporter;
import com.sportech.cl.utils.sis.SisParserHelper;

public class BetfredExporterTest {
	
    @Test
    public void testRmsExporter() {
        InputStream stream = ClassLoader.getSystemResourceAsStream("rms_card_20150110.xml");
        BetfredImporter importer = new BetfredImporter(null);
        try {
            byte[] data = IOUtils.toByteArray(stream);
            List<HardCard> cards = importer.parse(data);
            HardCard card = cards.get(14);
            Assert.assertTrue(card.getRaces().size() == 6);
            Assert.assertTrue(cards.size() == 17);
            BetfredExporter exporter = new BetfredExporter();
            ByteArrayOutputStream os = new ByteArrayOutputStream();
            exporter.export(os, cards);
            cards = importer.parse(os.toByteArray());
        } catch (Exception e) {
            Assert.fail();
        }
    }

    @Test
    public void testDistance1() {
    	String distance = "2m 6f";
		int meter = SisParserHelper.parseDistance(distance, "mtrs");
		int f = SisParserHelper.parseDistance(distance, "f");
		int m = SisParserHelper.parseDistance(distance, "m");
		int y = SisParserHelper.parseDistance(distance, "yds");
		Assert.assertTrue(meter == 0);
		Assert.assertTrue(m == 2);
		Assert.assertTrue(f == 6);
		Assert.assertTrue(y == 0);
    }

    @Test
    public void testDistance2() {
    	String distance = "435mtrs";
		int meter = SisParserHelper.parseDistance(distance, "mtrs");
		int f = SisParserHelper.parseDistance(distance, "f");
		int m = SisParserHelper.parseDistance(distance, "m");
		int y = SisParserHelper.parseDistance(distance, "yds");
		Assert.assertTrue(meter == 435);
		Assert.assertTrue(m == 0);
		Assert.assertTrue(f == 0);
		Assert.assertTrue(y == 0);
    }

    @Test
    public void testDistance3() {
    	String distance = "5f 110yds";
		int meter = SisParserHelper.parseDistance(distance, "mtrs");
		int f = SisParserHelper.parseDistance(distance, "f");
		int m = SisParserHelper.parseDistance(distance, "m");
		int y = SisParserHelper.parseDistance(distance, "yds");
		Assert.assertTrue(meter == 0);
		Assert.assertTrue(m == 0);
		Assert.assertTrue(f == 5);
		Assert.assertTrue(y == 110);
    }

}
