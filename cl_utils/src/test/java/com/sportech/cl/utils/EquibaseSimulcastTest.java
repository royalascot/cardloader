package com.sportech.cl.utils;

import java.io.InputStream;

import org.apache.commons.io.IOUtils;
import org.junit.Assert;
import org.junit.Test;

import com.sportech.cl.model.database.HardCard;
import com.sportech.cl.model.database.HardRace;
import com.sportech.cl.model.importer.ConverterType;
import com.sportech.cl.utils.importer.CardImporter;
import com.sportech.cl.utils.importer.CardImporterFactory;

public class EquibaseSimulcastTest {
	
	@Test
	public void testEquibaseSimulcast() {
		InputStream stream = ClassLoader.getSystemResourceAsStream("SIMD20140626TIP_IRE.xml");
		CardImporter importer = CardImporterFactory.create(ConverterType.EQUIBASE_SIMULCAST, null);
		try {
			byte[] data = IOUtils.toByteArray(stream);
			HardCard card = importer.parse(data).get(0);
			Assert.assertTrue(card.getRaces().size() == 7);
			for (HardRace r : card.getRaces()) {
				if (r.getNumber() == 5L) {
					Assert.assertTrue(r.getRunners().size() == 13);
					break;
				}
			}
		} catch (Exception e) {
			Assert.fail();
		}
	}
}
