package com.sportech.cl.global.utils.config;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlElement;

public class ParserConfig implements Serializable
{
	public ParserConfig() {}
	
	public ParserConfig(String importScriptsDir, Integer importTimeout) 
	{
		super();
		setImportScriptsDir(importScriptsDir);
		setImportTimeout(importTimeout);
	}

	@XmlElement(name="importScriptsDir")
	public String getImportScriptsDir() 						{ return importScriptsDir; }
	public void setImportScriptsDir(String importScriptsDir) 	{ this.importScriptsDir = importScriptsDir; }
	
	@XmlElement(name="importTimeout")
	public Integer getImportTimeout()               { return importTimeout; }
    public void setImportTimeout(Integer newValue)  { this.importTimeout = newValue; }
    
    @XmlElement(name="importPythonPath")
    public String getImportPythonPath()                         { return importPythonPath; }
    public void setImportPythonPath(String importPythonPath)    { this.importPythonPath = importPythonPath; }

	private String	importScriptsDir = "/opt/cl_importer";
	private Integer importTimeout = 60*1000;
	private String  importPythonPath = "/usr/local/bin/python";
	
	private static final long serialVersionUID = -1L;
}
