package com.sportech.cl.global.utils.beans;

import java.util.Properties;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import com.sportech.cl.global.utils.config.ClConfig;

public class BeanLocatorHelper {
	public static final String JNDI_NAME_PREFIX = "ejb:cl/ejb_cl/";
	
	public static Context	getInitialContext(ClConfig cfg) throws NamingException
	{
		if( ctx == null )
		{
		    Properties properties = new Properties();
		    
		    properties.put(Context.URL_PKG_PREFIXES, "org.jboss.ejb.client.naming");

		    ctx = new InitialContext(properties);
		}
		
		return ctx;
	}
	
	private static Context ctx = null;
}
