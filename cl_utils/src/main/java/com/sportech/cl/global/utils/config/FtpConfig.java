package com.sportech.cl.global.utils.config;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlElement;

public class FtpConfig implements Serializable {

	private String ftpServer = "199.115.17.4";
	private String ftpUsername = "ebet2";
	private String ftpPassword = "horse123";
	private String ftpDirectoryName = "/scast";
	private String ftpFileMasks = "";
	private String ftpCheckTime = "";
	private String downloadPath;

	private static final long serialVersionUID = -1L;

	public FtpConfig() {
	}

	@XmlElement(name = "download-path")
	public String getDownloadPath() {
		return downloadPath;
	}

	public void setDownloadPath(String outputPath) {
		this.downloadPath = outputPath;
	}

	@XmlElement(name = "server-name")
	public String getFtpServer() {
		return ftpServer;
	}

	public void setFtpServer(String ftpServer) {
		this.ftpServer = ftpServer;
	}

	@XmlElement(name = "username")
	public String getFtpUsername() {
		return ftpUsername;
	}

	public void setFtpUsername(String ftpUsername) {
		this.ftpUsername = ftpUsername;
	}

	@XmlElement(name = "password")
	public String getFtpPassword() {
		return ftpPassword;
	}

	public void setFtpPassword(String ftpPassword) {
		this.ftpPassword = ftpPassword;
	}

	@XmlElement(name = "directory-name")
	public String getFtpDirectoryName() {
		return ftpDirectoryName;
	}

	public void setFtpDirectoryName(String ftpDirectoryName) {
		this.ftpDirectoryName = ftpDirectoryName;
	}

	@XmlElement(name = "file-masks")
	public String getFtpFileMasks() {
		return ftpFileMasks;
	}

	public void setFtpFileMasks(String ftpFileMasks) {
		this.ftpFileMasks = ftpFileMasks;
	}

	@XmlElement(name = "check-site-time")
	public String getFtpCheckTime() {
		return ftpCheckTime;
	}

	public void setFtpCheckTime(String ftpCheckTime) {
		this.ftpCheckTime = ftpCheckTime;
	}

}
