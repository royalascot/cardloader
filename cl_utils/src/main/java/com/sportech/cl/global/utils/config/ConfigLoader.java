package com.sportech.cl.global.utils.config;

import java.io.FileInputStream;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;

import org.apache.log4j.Logger;

import com.sportech.cl.model.common.PoolMergeType;

public class ConfigLoader 
{
	private static final String CFG_FILE_NAME = "card_loader_config.xml";
	
	private static Logger log = Logger.getLogger(ConfigLoader.class);
	
	protected static ClConfig  theConfig = null;
	
	synchronized public static ClConfig	getCfg() 
	{
		if(theConfig == null) {
			try {
				//
				// Configuration file has predefined name like below:
				// <JBOSS_HOME>/standalone/configuration/card_loader_config.xml
				//
				String configFileName = "";
				String separator = getPathSeparator();
				configFileName = System.getProperty("jboss.server.config.dir") + separator + CFG_FILE_NAME;

				
				JAXBContext jc = JAXBContext.newInstance(com.sportech.cl.global.utils.config.ClConfig.class);
				Unmarshaller u = jc.createUnmarshaller();
				theConfig = (ClConfig)u.unmarshal(new FileInputStream(configFileName));
			}
			catch (Exception e ) {
				log.error("Can't load or process Card Loader configuration file !" , e );
				log.error("Initializind default Card Loader configuration !");
				
				theConfig = new ClConfig();
			}
			
			// Dump configuration here on first load
			StringBuffer buf = new StringBuffer();
			buf.append("Card Loader configuration loaded: \n");
			buf.append("\tCommon Configuration:\n");
			buf.append("\t\tpoolsMergeDef: '" + theConfig.getCommonConfig().getPoolsMergeDef() + "'.\n");
			buf.append("\tParser Configuration:\n");
			buf.append("\t\timportScriptsDir: '" + theConfig.getParserConfig().getImportScriptsDir() + "'.\n");
			buf.append("\t\timportTimeout: '" + theConfig.getParserConfig().getImportTimeout().toString() + "'.\n");
			
			log.info(buf.toString());
		}
		
		return theConfig;
	}

	private static String getPathSeparator() {
		String separator;
		
		String osName = System.getProperty("os.name");
	   	if(osName != null && osName.toLowerCase().contains("windows")) {
	   		separator = "\\";
	   	}
	   	else {
	   		separator = "/";
	   	}
	   	
	   	return separator;
	}

	public static PoolMergeType getDefaultMergeType(Integer mergeTypeId) {
		PoolMergeType mergeType = null;
	
		if (mergeTypeId != null) {
			mergeType = PoolMergeType.getById(mergeTypeId);
		}
		
		if (mergeType == null) {
			mergeType = PoolMergeType.valueOf(PoolMergeType.class, ConfigLoader.getCfg().getCommonConfig().getPoolsMergeDef());
			if (mergeType == null)
				mergeType = PoolMergeType.PATTERN_ONLY;
		}
		return mergeType;
	}
}
