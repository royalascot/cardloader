package com.sportech.cl.global.utils.config;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlElement;

public class CommonConfig implements Serializable {

	public CommonConfig() {
	}

	public CommonConfig(String poolsMergeDef, Boolean logSOAP) {
		this.poolsMergeDef = poolsMergeDef;
		this.logSOAP = logSOAP;
	}

	@XmlElement(name = "poolsMergeDef")
	public String getPoolsMergeDef() {
		return poolsMergeDef;
	}

	public void setPoolsMergeDef(String poolsMergeDef) {
		this.poolsMergeDef = poolsMergeDef;
	}

	@XmlElement(name = "create-new-track")
	public Boolean getCreateNewTrack() {
		return createNewTrack;
	}

	public void setCreateNewTrack(Boolean createNewTrack) {
		this.createNewTrack = createNewTrack;
	}

	@XmlElement(name = "log-soap-messages")
	public Boolean getLogSOAP() {
		return logSOAP;
	}

	public void setLogSOAP(Boolean logSOAP) {
		this.logSOAP = logSOAP;
	}

	@XmlElement(name = "incoming-file-path")
	public String getIncomingFilePath() {
		return incomingFilePath;
	}

	public void setIncomingFilePath(String incomingFilePath) {
		this.incomingFilePath = incomingFilePath;
	}

	@XmlElement(name = "processed-file-path")
	public String getProcessedFilePath() {
		return processedFilePath;
	}

	public void setProcessedFilePath(String processedFilePath) {
		this.processedFilePath = processedFilePath;
	}

	@XmlElement(name = "error-file-path")
	public String getErrorFilePath() {
		return errorFilePath;
	}

	public void setErrorFilePath(String errorFilePath) {
		this.errorFilePath = errorFilePath;
	}

	@XmlElement(name = "ftp-server-name")
	public String getFtpServer() {
		return ftpServer;
	}

	public void setFtpServer(String ftpServer) {
		this.ftpServer = ftpServer;
	}

	@XmlElement(name = "ftp-username")
	public String getFtpUsername() {
		return ftpUsername;
	}

	public void setFtpUsername(String ftpUsername) {
		this.ftpUsername = ftpUsername;
	}

	@XmlElement(name = "ftp-password")
	public String getFtpPassword() {
		return ftpPassword;
	}

	public void setFtpPassword(String ftpPassword) {
		this.ftpPassword = ftpPassword;
	}

	@XmlElement(name = "ftp-directory-name")
	public String getFtpDirectoryName() {
		return ftpDirectoryName;
	}

	public void setFtpDirectoryName(String ftpDirectoryName) {
		this.ftpDirectoryName = ftpDirectoryName;
	}

	@XmlElement(name = "ftp-file-masks")
	public String getFtpFileMasks() {
		return ftpFileMasks;
	}

	public void setFtpFileMasks(String ftpFileMasks) {
		this.ftpFileMasks = ftpFileMasks;
	}

	@XmlElement(name = "ftp-check-site-time")
	public String getFtpCheckTime() {
		return ftpCheckTime;
	}

	public void setFtpCheckTime(String ftpCheckTime) {
		this.ftpCheckTime = ftpCheckTime;
	}

	@XmlElement(name = "track-info-check-time")
	public String getTrackInfoCheckTime() {
		return trackInfoCheckTime;
	}

	public void setTrackInfoCheckTime(String trackInfoCheckTime) {
		this.trackInfoCheckTime = trackInfoCheckTime;
	}

	@XmlElement(name = "sis-messages-path")
	public String getSisFilePath() {
		return sisFilePath;
	}

	public void setSisFilePath(String sisFilePath) {
		this.sisFilePath = sisFilePath;
	}

	@XmlElement(name = "betfred-file-path")
	public String getBetfredFilePath() {
        return betfredFilePath;
    }

    public void setBetfredFilePath(String betfredFilePath) {
        this.betfredFilePath = betfredFilePath;
    }

    @XmlElement(name = "data-retention-days")
	public String getDataRetentionDays() {
		return dataRetentionDays;
	}

	public void setDataRetentionDays(String dataRetentionDays) {
		this.dataRetentionDays = dataRetentionDays;
	}

	/**
	 * Data members
	 */

	private String incomingFilePath;
	private String processedFilePath;
	private String errorFilePath;
	private String sisFilePath;
	private String poolsMergeDef = "HARD_CARD_ONLY";
	private Boolean logSOAP = false;
	private Boolean createNewTrack = false;
	private String dataRetentionDays = "7";
	private String betfredFilePath;

	/**
	 * Equibase configurations
	 * 
	 */
	private String ftpServer = "199.115.17.4";
	private String ftpUsername = "ebet2";
	private String ftpPassword = "horse123";
	private String ftpDirectoryName = "/scast";
	private String ftpFileMasks = "";
	private String ftpCheckTime = "";

	/**
	 * Track Info configurations
	 */
	private String trackInfoCheckTime = "";

	private static final long serialVersionUID = -1L;
}
