package com.sportech.cl.global.utils.config;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="cardLoaderConfig")
public class ClConfig implements Serializable 
{
	public ClConfig() {}

	@XmlElement(name="commonConfig")
	public CommonConfig getCommonConfig() 					{ return commonConfig; }
	public void setCommonConfig(CommonConfig commonConfig) 	{ this.commonConfig = commonConfig;	}

	@XmlElement(name="parserConfig")
	public ParserConfig getParserConfig() 					{ return parserConfig; }
	public void setParserConfig(ParserConfig parserConfig) 	{ this.parserConfig = parserConfig; }

	private CommonConfig	commonConfig = new CommonConfig();
	private ParserConfig	parserConfig = new ParserConfig();
	
	private FtpConfig equibaseConfig = new FtpConfig();
	private FtpConfig equibaseSimulcastConfig = new FtpConfig();
	private FtpConfig trackmasterConfig = new FtpConfig();
	private FtpConfig trackinfoConfig = new FtpConfig();
	private FtpConfig australiaConfig = new FtpConfig();
	
	@XmlElement(name="equibase_simulcast")
	public FtpConfig getEquibaseSimulcastConfig() {
		return equibaseSimulcastConfig;
	}

	public void setEquibaseSimulcastConfig(FtpConfig equibaseSimulcastConfig) {
		this.equibaseSimulcastConfig = equibaseSimulcastConfig;
	}

	@XmlElement(name="equibase")
	public FtpConfig getEquibaseConfig() {
		return equibaseConfig;
	}

	public void setEquibaseConfig(FtpConfig equibaseConfig) {
		this.equibaseConfig = equibaseConfig;
	}

	@XmlElement(name="trackmaster")
	public FtpConfig getTrackmasterConfig() {
		return trackmasterConfig;
	}

	public void setTrackmasterConfig(FtpConfig trackmasterConfig) {
		this.trackmasterConfig = trackmasterConfig;
	}

	@XmlElement(name="trackinfo")
	public FtpConfig getTrackinfoConfig() {
		return trackinfoConfig;
	}

	public void setTrackinfoConfig(FtpConfig trackinfoConfig) {
		this.trackinfoConfig = trackinfoConfig;
	}

	@XmlElement(name="Australia")
	public FtpConfig getAustraliaConfig() {
		return australiaConfig;
	}

	public void setAustraliaConfig(FtpConfig australiaConfig) {
		this.australiaConfig = australiaConfig;
	}

	private static final long serialVersionUID = -1L;
}
