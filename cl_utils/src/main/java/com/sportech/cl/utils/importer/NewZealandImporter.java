package com.sportech.cl.utils.importer;

import org.apache.commons.lang3.StringUtils;

import com.sportech.cl.model.database.utils.JSonHelper;
import com.sportech.cl.model.importer.PoolLeg;
import com.sportech.cl.model.importer.PoolList;

public class NewZealandImporter extends EquibaseSimulcastImporter {
	
	private static final int NZ_PLACE = 2;
	private static final int CL_SHOW = 3;
	
	@Override
	protected String convertPools(String pools) {
		if (StringUtils.isBlank(pools)) {
			return pools;
		}
		PoolList poolList = JSonHelper.fromJSon(PoolList.class, pools);
		if (poolList != null) {
			for (PoolLeg pl : poolList.getLegs()) {
				Integer[] pids = pl.getPoolIds();
				if  (pids != null) {
					for (int i = 0; i < pids.length; i++) {
						if (pids[i] == NZ_PLACE) {
							pids[i] = CL_SHOW;
						}
					}
				}
			}
			return JSonHelper.toJSon(poolList);
		}
		return pools;
	}

}
