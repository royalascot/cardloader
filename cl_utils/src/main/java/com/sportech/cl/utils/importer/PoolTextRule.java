package com.sportech.cl.utils.importer;

import java.io.Serializable;
import java.util.regex.Pattern;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonProperty;

public class PoolTextRule implements Serializable, Comparable<PoolTextRule> {

	private static final long serialVersionUID = 1L;

	@JsonCreator
	public PoolTextRule() {
	}

	@JsonProperty("poolName")
	private String poolName;

	@JsonProperty("poolTypeId")
	private int poolTypeId;

	@JsonProperty("priority")
	private int priority;

	@JsonProperty("includeMatches")
	private String[] includeMatches;

	@JsonProperty("excludeMatches")
	private String[] exclcudeMatches;

	@JsonProperty("positiveMatches")
	private String[] positiveMatches;

	@JsonProperty("negativeMatches")
	private String[] negativeMatches;
	
	@JsonProperty("secondLegMatches")
	private String[] secondLegMatches;
	
	private Pattern[] includeMatchesPatterns;
	
	private Pattern[] excludeMatchesPatterns;
	
	private Pattern[] positivePatterns;

	private Pattern[] negativePatterns;
	
	private Pattern[] secondLegPatterns;
	
	@JsonProperty("races")
	private PoolRuleRace[] races;
	
	public String[] getSecondLegMatches() {
		return secondLegMatches;
	}

	public void setSecondLegMatches(String[] secondLegMatches) {
		this.secondLegMatches = secondLegMatches;
		this.secondLegMatches = secondLegMatches;
		if (secondLegMatches != null) {
			secondLegPatterns = new Pattern[secondLegMatches.length];
			int i = 0;
			for (String regex : secondLegMatches) {
				secondLegPatterns[i++] = Pattern.compile(regex, Pattern.CASE_INSENSITIVE);
			}
		}
	}

	public Pattern[] getSecondLegPatterns() {
		return secondLegPatterns;
	}

	public void setSecondLegPatterns(Pattern[] secondLegPatterns) {
		this.secondLegPatterns = secondLegPatterns;
	}

	public Pattern[] getIncludeMatchesPatterns() {
		return includeMatchesPatterns;
	}

	public void setIncludeMatchesPatterns(Pattern[] includeMatchesPatterns) {
		this.includeMatchesPatterns = includeMatchesPatterns;
	}

	public Pattern[] getExcludeMatchesPatterns() {
		return excludeMatchesPatterns;
	}

	public void setExcludeMatchesPatterns(Pattern[] excludeMatchesPatterns) {
		this.excludeMatchesPatterns = excludeMatchesPatterns;
	}
	
	public String getPoolName() {
		return poolName;
	}

	public void setPoolName(String poolName) {
		this.poolName = poolName;
	}

	public int getPoolTypeId() {
		return poolTypeId;
	}

	public void setPoolTypeId(int poolTypeId) {
		this.poolTypeId = poolTypeId;
	}

	public String[] getIncludeMatches() {
		return includeMatches;
	}

	public int getPriority() {
		return priority;
	}

	public void setPriority(int priority) {
		this.priority = priority;
	}

	public String[] getPositiveMatches() {
		return positiveMatches;
	}

	public void setPositiveMatches(String[] positiveMatches) {
		this.positiveMatches = positiveMatches;
		if (positiveMatches != null) {
			positivePatterns = new Pattern[positiveMatches.length];
			int i = 0;
			for (String regex : positiveMatches) {
				positivePatterns[i++] = Pattern.compile(regex, Pattern.CASE_INSENSITIVE);
			}
		}
	}

	public String[] getNegativeMatches() {
		return negativeMatches;
	}

	public void setNegativeMatches(String[] negativeMatches) {
		this.negativeMatches = negativeMatches;
		if (negativeMatches != null) {
			negativePatterns = new Pattern[negativeMatches.length];
			int i = 0;
			for (String regex : negativeMatches) {
				negativePatterns[i++] = Pattern.compile(regex, Pattern.CASE_INSENSITIVE);
			}
		}
	}

	public void setIncludeMatches(String[] inlcudeMatches) {
		this.includeMatches = inlcudeMatches;
		if (includeMatches != null) {
			includeMatchesPatterns = new Pattern[includeMatches.length];
			int i = 0;
			for (String regex : includeMatches) {
				includeMatchesPatterns[i++] = Pattern.compile(regex, Pattern.UNICODE_CASE | Pattern.CASE_INSENSITIVE);
			}
		}
	}

	public String[] getExclcudeMatches() {
		return exclcudeMatches;
	}

	public void setExclcudeMatches(String[] exclcudeMatches) {
		this.exclcudeMatches = exclcudeMatches;
		if (exclcudeMatches != null) {
			excludeMatchesPatterns = new Pattern[exclcudeMatches.length];
			int i = 0;
			for (String regex : exclcudeMatches) {
				excludeMatchesPatterns[i++] = Pattern.compile(regex, Pattern.CASE_INSENSITIVE);
			}
		}
	}

	public Pattern[] getPositivePatterns() {
		return positivePatterns;
	}

	public void setPositivePatterns(Pattern[] positivePatterns) {
		this.positivePatterns = positivePatterns;
	}

	public Pattern[] getNegativePatterns() {
		return negativePatterns;
	}

	public void setNegativePatterns(Pattern[] negativePatterns) {
		this.negativePatterns = negativePatterns;
	}

	public PoolRuleRace[] getRaces() {
		return races;
	}

	public void setRaces(PoolRuleRace[] races) {
		this.races = races;
	}

	@Override
	public int compareTo(PoolTextRule r) {
		if (r != null) {
			return r.getPriority() - this.getPriority();
		}
		return -1;
	}
	
}