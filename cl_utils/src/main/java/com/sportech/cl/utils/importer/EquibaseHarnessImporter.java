package com.sportech.cl.utils.importer;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.stream.StreamSource;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import com.sportech.cl.model.database.HardCard;
import com.sportech.cl.model.database.HardRace;
import com.sportech.cl.model.database.Runner;
import com.sportech.cl.model.database.Track;
import com.sportech.cl.model.schema.equibaseharness.EntryRaceType;
import com.sportech.cl.model.schema.equibaseharness.RaceCardType;
import com.sportech.cl.model.schema.equibaseharness.StarterType;
import com.sportech.cl.model.schema.equibaseharness.TrackType;
import com.sportech.cl.utils.helpers.ParserHelper;
import com.sportech.common.model.CardType;

public class EquibaseHarnessImporter implements CardImporter {

	static private final Logger log = Logger.getLogger(EquibaseHarnessImporter.class);

	private static final String TOTE_PREFIX = "harness_";
	private static final String WEIGHT_UNIT = "lb";
	private static final String PROVIDER_NAME = "Equibase Company LLC";
	private PoolTextParser poolParser = new PoolTextParser("/pooltypeparserules.json");

	@Override
	public List<HardCard> parse(byte[] data) throws ImportException {
	    List<HardCard> cards = new ArrayList<HardCard>();
		try {
			JAXBContext jc = JAXBContext.newInstance(RaceCardType.class);
			Unmarshaller unmarshaller = jc.createUnmarshaller();
			InputStream is = new ByteArrayInputStream(data);
			JAXBElement<RaceCardType> o = unmarshaller.unmarshal(new StreamSource(is), RaceCardType.class);
			cards.add(importCard(o.getValue()));
		} catch (ImportException ie) {
			throw ie;
		} catch (Exception e) {
			log.error(e);
			throw new ImportException("Invalid input Equibase file.");
		}
		return cards;
	}

	private Track importTrack(RaceCardType raceCard) {
		Track track = new Track();
		TrackType equibaseTrack = raceCard.getTrack();
		if (equibaseTrack != null) {
			track.setEquibaseId(equibaseTrack.getTrackId());
			track.setToteTrackId(TOTE_PREFIX + equibaseTrack.getTrackId());
			track.setName(equibaseTrack.getTrackName());
			track.setCountryCode("USA");
			track.setTrackType(CardType.Harness);
			track.setActive(true);
			return track;
		}
		return null;
	}

	private static Date parseDate(String dateString) throws ParseException {
		if (StringUtils.isNotEmpty(dateString)) {
			SimpleDateFormat format = new SimpleDateFormat("hh:mma");
			Date date = format.parse(dateString);
			return date;
		}
		return null;
	}

	private Runner importRunner(StarterType starter) throws ImportException {
		try {
			Runner runner = new Runner();
			String space = " ";
			if (!StringUtils.isEmpty(starter.getDriver().getMiddleName())) {
				space = " " + starter.getDriver().getMiddleName() + " ";
			}
			runner.setJockeyName(starter.getDriver().getFirstName() + space + starter.getDriver().getLastName());
			runner.setMornlineOdds(ParserHelper.formatOdds(starter.getOdds()));
			runner.setName(starter.getHorseName());
			runner.setShortName(ParserHelper.getRunnerShortName(starter.getHorseName()));
			runner.setWeightCarried("" + starter.getWeight());
			runner.setWeightUnits(WEIGHT_UNIT);
			runner.setScratched(false);
			runner.setProgramNumber(starter.getProgramNumber());
			runner.setPosition((long) starter.getPostPosition());
			return runner;
		} catch (Exception e) {
			log.error("Error importing runners", e);
			throw new ImportException("Error importing runners.");
		}
	}

	private Set<HardRace> importRaces(RaceCardType raceCard, HardCard HardCard) throws ImportException {

		HashSet<HardRace> races = new HashSet<HardRace>();

		try {
			for (EntryRaceType race : raceCard.getRaces().getEntryRace()) {
				HardRace hr = new HardRace();
				hr.setParent(HardCard);
				hr.setNumber((long) race.getRaceNumber());
				hr.setBreed(race.getBreedOfRace());
				hr.setRaceType(race.getGait());
				hr.setCourse(race.getSurface());
				hr.setEvening(false);
				ArrayList<String> words = new ArrayList<String>();
				Long distance = NumberTextParser.parseNumberText(race.getDistance(), words);
				if (distance != null && distance > 0L) {
					hr.setDistance("" + distance);
					if (words.size() > 0) {
						hr.setDistanceUnit(StringUtils.capitalize(words.get(0)));
					}
				}
				hr.setDistanceText(hr.getDistance() + hr.getDistanceUnit());
				hr.setPostTime(parseDate(race.getPostTime()));
				hr.setPurse(race.getPurse());
				hr.setRaceNameShort(StringUtils.abbreviate(race.getRaceText(), 20));
				hr.setRaceNameLong(race.getRaceText());
				String wagerText = race.getWagerText();
				hr.setWagerText(wagerText);

				hr.setPoolIdList(poolParser.parseToString(wagerText));

				for (StarterType s : race.getStarters().getStarter()) {
					Runner runner = importRunner(s);
					runner.setParent(hr);
					hr.getRunners().add(runner);
				}
				races.add(hr);
			}
		} catch (ImportException ie) {
			throw ie;
		} catch (Exception e) {
			log.error("Error importing races", e);
			throw new ImportException("Failed to import races.");
		}
		return races;
	}

	private HardCard importCard(RaceCardType raceCard) throws ImportException {
		try {
		    HardCard card = new HardCard();
			card.setCardType(com.sportech.common.model.CardType.Harness);			
			try {
				card.setCardDate(new SimpleDateFormat("yyyyMMdd").parse("" + raceCard.getRaceDate()));
			} catch (Exception e) {
				log.error("Error parsing card date", e);
				throw new ImportException("Invalid card date format.");
			}
			card.setProvider(PROVIDER_NAME);
			card.setRaces(importRaces(raceCard, card));
			card.setTrack(importTrack(raceCard));

			return card;
			
		} catch (ImportException ie) {
			throw ie;
		} catch (Exception e) {
			log.error("Error importing card", e);
			throw new ImportException("Error imporing card.");
		}
	}
}
