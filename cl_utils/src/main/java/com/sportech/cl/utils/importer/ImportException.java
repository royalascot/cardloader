/**
 * 
 */
package com.sportech.cl.utils.importer;

/**
 * @author jyu
 *
 */
@SuppressWarnings("serial")
public class ImportException extends Exception {
	
	public ImportException(String message) {
		super(message);
	}
	
}

