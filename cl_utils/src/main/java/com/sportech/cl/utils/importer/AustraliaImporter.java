package com.sportech.cl.utils.importer;

import com.sportech.common.model.CardType;
import com.sportech.cl.model.database.Track;
import com.sportech.cl.model.importer.ConverterType;
import com.sportech.cl.model.schema.equibase.simulcast.RaceCard;

public class AustraliaImporter extends EquibaseSimulcastImporter {
	
	private static final String TOTE_PREFIX = "aus_";
	private static final String COUNTRY_CODE = "AUS";
	
	private ConverterType converterType;
	
	public AustraliaImporter(ConverterType converterType) {
		this.converterType = converterType;
	}
	
	protected Track importTrack(RaceCard raceCard) {
		Track track = new Track();
		track.setEquibaseId(getEquibaseId());
		track.setCountryCode(COUNTRY_CODE);
		track.setToteTrackId(TOTE_PREFIX + getEquibaseId());
		track.setName(getTrackName());
		track.setTrackType(CardType.Thoroughbred);
		track.setActive(true);
		return track;
	}

	private String getEquibaseId() {
		return converterType.getFilename();
	}
	
	private String getTrackName() {
		return converterType.getDescription();
	}
	
}
