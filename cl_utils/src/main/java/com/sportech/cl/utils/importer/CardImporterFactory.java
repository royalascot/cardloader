package com.sportech.cl.utils.importer;

import java.util.Collection;

import com.sportech.cl.model.database.Track;
import com.sportech.cl.model.importer.ConverterType;
import com.sportech.common.model.PerformanceType;

public class CardImporterFactory {
	
	public static CardImporter create(ConverterType conv, Collection<Track> tracks) {
		
		switch (conv) {
		case EQUIBASE:
			return new EquibaseImporter();
		case EQUIBASE_HARNESS:
			return new EquibaseHarnessImporter();
		case EQUIBASE_SIMULCAST:
			return new EquibaseSimulcastImporter();
		case UNIVERSAL:
			return new UniversalFormatImporter();
		case IRELAND:
			return null;		
		case TRACK_INFO_E:
			return new TrackInfoImporter(PerformanceType.Evening);
		case TRACK_INFO_A:
			return new TrackInfoImporter(PerformanceType.Matinee);
		case TRACK_INFO_T:
			return new TrackInfoImporter(PerformanceType.Twilight);
		case AUSTRALIA_A:
			return new AustraliaImporter(conv);
		case AUSTRALIA_B:
			return new AustraliaImporter(conv);
		case AUSTRALIA_C:
			return new AustraliaImporter(conv);
		case NEW_ZEALAND:
			return new NewZealandImporter();
		case BETFRED_RMS:
		    return new BetfredImporter(tracks);
		}
		
		return null;
	}
	
}
