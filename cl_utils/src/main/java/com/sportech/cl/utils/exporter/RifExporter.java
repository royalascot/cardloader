package com.sportech.cl.utils.exporter;

import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;

import org.apache.commons.lang3.StringUtils;

import com.sportech.cl.model.database.HardCard;
import com.sportech.cl.model.database.HardRace;
import com.sportech.cl.model.database.Runner;
import com.sportech.cl.utils.helpers.ParserHelper;
import com.sportech.common.model.PerformanceType;

public class RifExporter {
	
	private static final SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
	private static final SimpleDateFormat keyDateFormat = new SimpleDateFormat("d-MMM-yy");
	
	private static final String lineFormat = "\"RD1\",\"%s\",%d,\"%s\",%s,\"%s\",\"%s\",\"%s\"";
	private static final String headerFormat = "\"RDH\",\"%s\",%d,,%d,,,";
	
	public String build(Date date, Collection<HardCard> cards) {
		StringBuilder sb = new StringBuilder();
		int count = 0;	
		int cardCount = 0;
		for (HardCard card : cards) {
			count += exportCard(sb, card);
			cardCount++;
		}		
		return String.format(headerFormat, dateFormat.format(date), cardCount, count) + "\r\n" + sb.toString();
	}
	
	private int exportCard(StringBuilder sb, HardCard card) {
		int count = 0;
		
		String meetingCode = card.getMeetingCode();
		if (StringUtils.isEmpty(meetingCode)) {
			meetingCode = card.getTrack().getTrackCode(); 
		}	
		if (StringUtils.isEmpty(meetingCode)) {
			meetingCode = card.getTrack().getEquibaseId();
		}
		
		Date d = card.getCardDate();
		String perfType = "M";
		if (card.getPerfType() == PerformanceType.Evening) {
			perfType = "E";
		}
		else if (card.getPerfType() == PerformanceType.Twilight) {
			perfType = "T";
		}
		for (HardRace r : card.getRaces()) {
			Long number = r.getNumber();
			String key = meetingCode + keyDateFormat.format(d) + perfType;
			key = StringUtils.upperCase(key);
			for (Runner runner : r.getRunners()) {
				String ownerNumber = runner.getProgramNumber();
				if (!StringUtils.isEmpty(ownerNumber)) {
					String owner = ownerNumber.replaceAll("\\D+", "");
					String line = String.format(lineFormat, key, number, 
							StringUtils.defaultString(runner.getProgramNumber()), 
							owner, 
							StringUtils.defaultString(runner.getName()),
							StringUtils.defaultString(runner.getJockeyName()),
							ParserHelper.formatOdds(runner.getMornlineOdds()));
					sb.append(line);
					sb.append("\r\n");
					count++;
				}				
			}
		}
		return count;
	}
	

}
