package com.sportech.cl.utils.importer;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.stream.StreamSource;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import com.sportech.cl.model.database.HardCard;
import com.sportech.cl.model.database.HardPool;
import com.sportech.cl.model.database.HardRace;
import com.sportech.cl.model.database.PassThroughBag;
import com.sportech.cl.model.database.Runner;
import com.sportech.cl.model.database.Track;
import com.sportech.cl.model.database.utils.PoolHelper;
import com.sportech.cl.model.schema.betfred.rms.CloneDetailsType;
import com.sportech.cl.model.schema.betfred.rms.LengthType;
import com.sportech.cl.model.schema.betfred.rms.MeetingDetailsType;
import com.sportech.cl.model.schema.betfred.rms.MeetingType;
import com.sportech.cl.model.schema.betfred.rms.PoolDetailsType;
import com.sportech.cl.model.schema.betfred.rms.PoolType;
import com.sportech.cl.model.schema.betfred.rms.RaceDetailsType;
import com.sportech.cl.model.schema.betfred.rms.RaceType;
import com.sportech.cl.model.schema.betfred.rms.RacecardType;
import com.sportech.cl.model.schema.betfred.rms.RunnerType;
import com.sportech.cl.utils.sis.CourseEntry;
import com.sportech.cl.utils.sis.SisParserHelper;
import com.sportech.common.model.CardType;
import com.sportech.common.model.PerformanceType;
import com.sportech.common.util.DateHelper;
import com.sportech.common.util.JSonHelper;
import com.sportech.common.util.ListHelper;

public class BetfredImporter implements CardImporter {

    static private final Logger log = Logger.getLogger(BetfredImporter.class);

    static private final SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
    static private final SimpleDateFormat postTimeFormat = new SimpleDateFormat("HH:mm");
    static private final DecimalFormat amountFormat = new DecimalFormat("###,###,###.##");

    private Collection<Track> tracks = null;

    public BetfredImporter(Collection<Track> tracks) {
        this.tracks = tracks;
    }

    public List<HardCard> parse(byte[] data) throws ImportException {

        try {

            JAXBContext jc = JAXBContext.newInstance(RacecardType.class);
            Unmarshaller unmarshaller = jc.createUnmarshaller();
            InputStream is = new ByteArrayInputStream(data);
            JAXBElement<RacecardType> o = unmarshaller.unmarshal(new StreamSource(is), RacecardType.class);
            return importCard(o.getValue());

        } catch (ImportException ie) {
            throw ie;
        } catch (Exception e) {
            log.error(e);
            throw new ImportException("Invalid input Betfred file.");
        }
    }

    private HardRace importRace(HardCard card, RaceType race) throws ImportException {

        HardRace hr = new HardRace();
        hr.setNumberPositions(2);
        try {

            hr.setNumber(race.getNumber().longValue());
            CloneDetailsType clone = race.getCloneDetails();
            if (clone != null) {
                card.setProvider("Clone");
                String source = clone.getCourse() + ":" + clone.getRace();
                hr.setCloneSource(source);
                log.info("Found clone race " + race.getNumber() + " from source " + source);
            }
            RaceDetailsType detail = race.getRaceDetails();
            if (detail != null) {
                hr.setRaceCode(detail.getRacing());
                hr.setCourse(detail.getTrack());
                hr.setRaceType(detail.getGrade());
                if (detail.getTime() != null) {
                    hr.setPostTime(DateHelper.stringToDate(postTimeFormat, detail.getTime()));
                }

                if (detail.isHandicap() != null) {
                    hr.setHandicap(detail.isHandicap());
                }

                hr.setRaceNameLong(detail.getName());
                hr.setRaceNameShort(StringUtils.abbreviate(detail.getName(), 20));
            }

            if (race.getLength() != null) {
                String distance = "";
                LengthType l = race.getLength();
                if (l.getMiles() != null && l.getMiles() > 0) {
                    distance += " " + l.getMiles() + "m";
                }
                if (l.getFurlongs() != null && l.getFurlongs() > 0) {
                    distance += " " + l.getFurlongs() + "f";
                }
                if (l.getYards() != null && l.getYards() > 0) {
                    distance += " " + l.getYards() + "yd";
                }
                if (l.getMetres() != null && l.getMetres() > 0) {
                    distance += " " + l.getMetres() + "m";
                }
                hr.setDistanceText(distance);
            }

            if (race.getRunners() != null) {
                Byte count = race.getRunners().getCount();
                if (race.getRunners().getRunner() != null) {
                    for (RunnerType r : race.getRunners().getRunner()) {
                        Runner runner = new Runner();
                        runner.setName(r.getName());
                        runner.setJockeyName(r.getRider());
                        runner.setShortName(r.getAbbreviatedName());
                        runner.setCoupleIndicator(r.getCoupleGroup());
                        runner.setPosition(r.getNumber().longValue());
                        runner.setProgramNumber("" + r.getNumber());
                        runner.setMornlineOdds(r.getPrice());
                        if (StringUtils.equalsIgnoreCase("NonRunner",r.getStatus())) {
                        	runner.setScratched(true);
                        }
                        runner.setParent(hr);
                        hr.getRunners().add(runner);
                    }
                } else {
                    if (count != null) {
                        for (int i = 1; i <= count; i++) {
                            Runner runner = new Runner();
                            runner.setName("Runner " + i);
                            runner.setPosition((long) i);
                            runner.setProgramNumber("" + i);
                            runner.setParent(hr);
                            hr.getRunners().add(runner);
                        }
                    }
                }
                // Set place positions
                // Place: Runner must finish within the first two places (in a
                // 5–7 runner race),
                // three places (8–15 runners and non-handicaps with 16+
                // runners) or four places (handicaps with 16+ runners).
                int size = hr.getRunners().size();
                if (size >= 8 && size < 16) {
                    hr.setNumberPositions(3);
                } else if (size >= 16) {
                    if (hr.getHandicap() != null && hr.getHandicap()) {
                        hr.setNumberPositions(4);
                    } else {
                        hr.setNumberPositions(3);
                    }
                }

            }
            if (race.getPools() != null) {
                importRacePools(card, hr, race);
            }

        } catch (Exception e) {
            log.error("Error importing race", e);
            throw new ImportException("Error importing race.");
        }
        return hr;
    }

    private void importRacePools(HardCard card, HardRace parent, RaceType race) throws ImportException {
        try {
            for (PoolType p : race.getPools().getPool()) {
                com.sportech.common.model.PoolType pt = SisParserHelper.getBetfredPool(p.getType());
                if (pt != null) {
                    HardPool pool = new HardPool();
                    pool.setPoolTypeId(pt.getId());
                    List<Long> races = new ArrayList<Long>();
                    races.add(race.getNumber().longValue());
                    populatePool(p.getPoolDetails(), pool);
                    PoolHelper.populatePool(card, pool, races);
                    card.getPools().add(pool);
                }
            }
        } catch (Exception e) {
            log.error("Error importing pools", e);
            throw new ImportException("Error importing pools.");
        }
    }

    private BigDecimal fromString(String s) {
        if (StringUtils.isBlank(s)) {
            return null;
        }
        amountFormat.setParseBigDecimal(true);
        BigDecimal amount = null;
        try {
            amount = (BigDecimal) amountFormat.parse(s);
        } catch (Exception e) {
            log.error("Invalid amount text:" + s);
        }
        return amount;
    }

    private void populatePool(PoolDetailsType pt, HardPool pool) {
        pool.setGuarantee(fromString(pt.getGuarantee()));
        pool.setBroughtForward(fromString(pt.getBroughtForward()));
        pool.setBonusGuarantee(fromString(pt.getBonusGuarantee()));
        pool.setBonusBroughtForward(fromString(pt.getBonusBroughtForward()));
        pool.setStatus(pt.getStatus());
        pool.setIsNorminated(pt.isIsNominated());
        pool.setItspAllowed(pt.isITSPAllowed());
    }

    private void importMeetingPools(HardCard card, MeetingType meeting) throws ImportException {
        try {

            for (PoolType p : meeting.getPools().getPool()) {
                com.sportech.common.model.PoolType pt = SisParserHelper.getBetfredPool(p.getType());
                if (pt != null) {
                    HardPool pool = new HardPool();
                    pool.setPoolTypeId(pt.getId());
                    List<Long> races = new ArrayList<Long>();
                    if (p.getLegs() != null) {
                        String l = p.getLegs().getRaces();
                        races.addAll(ListHelper.parseRaceList(l));
                    }
                    populatePool(p.getPoolDetails(), pool);
                    PoolHelper.populatePool(card, pool, races);
                    card.getPools().add(pool);
                }
            }
        } catch (Exception e) {
            log.error("Error importing pools", e);
            throw new ImportException("Error importing pools.");
        }
    }

    private void importMeeting(MeetingType meeting, HardCard card) throws ImportException {

        try {

            MeetingDetailsType details = meeting.getMeetingDetails();
            if (details != null) {
                card.setDescription(details.getName());
                PassThroughBag bag = new PassThroughBag();
                bag.setMeetingName(details.getName());
                bag.setMeetingCodeUK(meeting.getToteCourse());
                card.setPassThroughs(JSonHelper.toJSon(bag));
            }

            if (StringUtils.isBlank(card.getDescription())) {
                if (meeting.getSIS() != null) {
                    card.setDescription(meeting.getSIS().getName());
                }
            }

            if (meeting.getRaces() != null) {
                if (card.getRaces() == null) {
                    Set<HardRace> races = new HashSet<HardRace>();
                    card.setRaces(races);
                }
                for (RaceType rt : meeting.getRaces().getRace()) {
                    HardRace hr = importRace(card, rt);
                    hr.setParent(card);
                    card.getRaces().add(hr);
                }
            }

            if (meeting.getPools() != null) {
                importMeetingPools(card, meeting);
            }

        } catch (Exception e) {
            log.error("Error importing meeting", e);
            throw new ImportException("Error importing races.");
        }
    }

    private List<HardCard> importCard(RacecardType raceCard) throws ImportException {

        List<HardCard> cards = new ArrayList<HardCard>();
        try {

            if (raceCard.getMeeting() != null) {
                for (MeetingType m : raceCard.getMeeting()) {
                    HardCard card = new HardCard();
                    card.setCardDate(DateHelper.stringToDate(sdf, raceCard.getDate()));
                    card.setProvider("Betfred");
                    card.setPerfType(PerformanceType.Matinee);
                    card.setCardType(null);
                    card.setTrack(importTrack(m));
                    card.setPools(new HashSet<HardPool>());
                    importMeeting(m, card);
                    cards.add(card);
                }
            }

            return cards;

        } catch (Exception e) {
            log.error("Error importing race card", e);
            throw new ImportException("Error importing race card.");
        }
    }

    private Track importTrack(MeetingType data) {

        String toteCourse = data.getToteCourse();
        String name = null;
        if (data.getMeetingDetails() != null) {
            name = data.getMeetingDetails().getName();
        }
        String countryCode = "UK";
        if (data.getSIS() != null) {
            if (StringUtils.isBlank(name)) {
                name = StringUtils.trim(data.getSIS().getName());
            }
            countryCode = data.getSIS().getCountry();
        }
        countryCode = SisParserHelper.normalizeCountryCode(countryCode);
        Track track = null;
        if (data != null && data.getToteCourse() != null) {
            if (tracks != null) {
                for (Track t : tracks) {
                    if (!StringUtils.equalsIgnoreCase(t.getCountryCode(), countryCode)) {
                        continue;
                    }
                    if (StringUtils.equalsIgnoreCase(t.getToteTrackId(), toteCourse)) {
                        track = t;
                        log.info("Found matching track '" + track.getName() + "' with id " + track.getId());
                        break;
                    }
                }
                if (track == null) {
                    track = createDefaultTrack(data, toteCourse);
                }
            } else {
                CourseEntry course = SisParserHelper.getTrackfromToteCode(countryCode, toteCourse, name);
                if (course != null) {
                    track = new Track();
                    track.setTrackType(null);
                    track.setEquibaseId(course.trackCode);
                    track.setCountryCode(course.countryCode);
                    track.setToteTrackId(course.toteCourse);
                    track.setName(course.name);
                    track.setActive(true);
                } else {                    
                    track = createDefaultTrack(data, toteCourse);
                }
            }
        }
        return track;
    }

    private Track createDefaultTrack(MeetingType data, String toteCourse) {
        log.error("Can not find track by tote course:" + toteCourse);
        Track track = new Track();
        track.setTrackType(CardType.Thoroughbred);
        track.setPerfType(PerformanceType.Matinee);
        track.setEquibaseId(toteCourse);
        track.setCountryCode("UK");
        track.setToteTrackId(toteCourse);
        if (data.getSIS() != null && StringUtils.isNotEmpty(data.getSIS().getName())) {
            track.setName(data.getSIS().getName());
        } else {
            track.setName(toteCourse);
        }
        track.setActive(true);
        return track;
    }

}
