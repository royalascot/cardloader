package com.sportech.cl.utils.sis;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import com.sportech.common.model.PoolType;

@SuppressWarnings("serial")
public class SisParserHelper {

	static private final Logger log = Logger.getLogger(SisParserHelper.class);

	private static Map<String, String> multiLegPoolMap = new HashMap<String, String>() {
		{
			// put("W", "PK4");
			put("J", "PK6");
			put("L", "VPK");
			put("Q", "PK4");
		}
	};

	private static Map<String, PoolType> betfredPoolMap = new HashMap<String, PoolType>() {
		{
			put("Win", PoolType.WIN);
			put("Place", PoolType.PLC);
			put("Exacta", PoolType.EXA);
			put("Trifecta", PoolType.TRI);
			put("Jackpot", PoolType.VPK);
			put("Double", PoolType.DBL);
			put("Tote Double", PoolType.DBL);
			put("Treble", PoolType.PK3);
			put("Tote Treble", PoolType.PK3);
			put("Quadpot", PoolType.PK4);
			put("Placepot", PoolType.PK6);
			put("Swinger", PoolType.OMN);
			put("Scoop6", PoolType.PKX);
			put("Soccer6", PoolType.PKX);
		}
	};

	private static volatile Map<String, List<CourseEntry>> toteCodeMap = null;

	private static Map<String, String> subcodeMap = new HashMap<String, String>() {
		{
			// Horse
			put("NH", "National Hunt Meeting");
			put("FL", "Flat Meeting");
			put("CO", "Combined Meeting");
			put("TR", "Trotting");

			// Dog
			put("NB", "Non-BAGS (evening) meeting");
			put("BA", "BAGS meeting");
			put("VR", "Virtual meeting");
			put("NA", "None of the above");
		}
	};

	public static String getRaceCode(String subCode) {
		String raceCode = "";
		if (StringUtils.isNotBlank(subCode)) {
			String rc = subcodeMap.get(subCode);
			if (rc != null) {
				raceCode = rc;
			} else {
				raceCode = subCode;
			}
		}
		return raceCode;
	}

	public static PoolType getBetfredPool(String name) {
		PoolType i = betfredPoolMap.get(name);
		if (i == null) {
			log.error("Unknown betfred pool name:" + name);
		}
		return i;
	}

	public static String getBetfredPoolName(PoolType pt) {
		for (String n : betfredPoolMap.keySet()) {
			if (betfredPoolMap.get(n) == pt) {
				return n;
			}
		}
		return "";
	}

	public static String getCode(String text) {
		if (text != null) {
			String c = multiLegPoolMap.get(text);
			return c;
		}
		return null;
	}

	public static String getToteCode(String countryCode, String sisCode, String trackName) {
		if (toteCodeMap == null) {
			initToteCodes();
		}
		String name = StringUtils.trim(trackName);
		String text = countryCode + "," + sisCode;
		List<CourseEntry> entries = toteCodeMap.get(text);
		if (entries == null) {
			return "";
		}
		for (CourseEntry c : entries) {
			if (StringUtils.startsWithIgnoreCase(c.name, name)) {
				return c.toteCourse;
			}
		}
		if (entries.size() > 0) {
			return entries.get(0).toteCourse;
		}
		return null;
	}

	public static CourseEntry getTrackfromToteCode(String cCode, String tCode, String trackName) {
		if (toteCodeMap == null) {
			initToteCodes();
		}
		String name = StringUtils.trim(trackName);
		String countryCode = normalizeCountryCode(cCode);
		String toteCode = StringUtils.trim(tCode);
		for (String s : toteCodeMap.keySet()) {
			List<CourseEntry> entries = toteCodeMap.get(s);
			if (entries == null || entries.size() == 0) {
				continue;
			}
			for (CourseEntry entry : entries) {
				if (StringUtils.equalsIgnoreCase(toteCode, entry.toteCourse) && StringUtils.equalsIgnoreCase(countryCode, entry.countryCode)) {
					if (entries.size() == 1) {
						log.info("Found entry " + entries.get(0).name + " with track code " + entries.get(0).trackCode);
						return entries.get(0);
					}
					if (StringUtils.isNotEmpty(name)) {
						if (StringUtils.startsWithIgnoreCase(entry.name, name)) {
							log.info("Found entry " + entry.name + " with track code " + entry.trackCode);
							return entry;
						}
					} else {
						log.info("Found entry " + entry.name + " with track code " + entry.trackCode);
						return entry;
					}
				}
			}
		}
		return null;
	}

	private static synchronized void initToteCodes() {
		if (toteCodeMap != null) {
			return;
		}
		toteCodeMap = new HashMap<String, List<CourseEntry>>();
		try {
			BufferedReader is = new BufferedReader(new InputStreamReader(SisParserHelper.class.getResourceAsStream("/coursecodes.csv")));
			String line;
			while ((line = is.readLine()) != null) {
				if (StringUtils.isNotEmpty(line)) {
					String[] parts = line.split(",");
					if (parts != null && parts.length > 5) {
						parts[5] = normalizeCountryCode(parts[5]);
						CourseEntry entry = new CourseEntry();
						entry.name = parts[1];
						entry.countryCode = parts[5];
						entry.trackCode = parts[4];
						entry.toteCourse = parts[3];
						String key = parts[5] + "," + parts[4];
						List<CourseEntry> entries = toteCodeMap.get(key);
						if (entries == null) {
							entries = new ArrayList<CourseEntry>();
							toteCodeMap.put(key, entries);
						}
						entries.add(entry);
					}
				}
			}
			is.close();
		} catch (Exception e) {
			log.error("Error create pool text parser", e);
		}
	}

	public static String normalizeCountryCode(String country) {
		String countryCode = "UK";
		if (StringUtils.isNotBlank(country)) {
			countryCode = StringUtils.trim(country);
			if (StringUtils.equalsIgnoreCase(countryCode, "IR")) {
				countryCode = "IE";
			}
		}
		return countryCode;
	}

	public static Short parseDistance(String distance, String unit) {
		String matched = null;
		Pattern p = Pattern.compile("\\b\\d+" + unit + "\\b", Pattern.CASE_INSENSITIVE);
		Pattern pn = Pattern.compile("\\d+", Pattern.CASE_INSENSITIVE);
		Matcher m = p.matcher(distance);
		if (m.find()) {
			matched = m.group();
			Matcher mn = pn.matcher(matched);
			if (mn.find()) {
				matched = mn.group();
				if (StringUtils.isNumeric(matched)) {
					return Short.parseShort(matched);
				}
			}
		}
		return 0;
	}

}
