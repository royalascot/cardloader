package com.sportech.cl.utils.importer;

import java.io.Serializable;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonProperty;

@SuppressWarnings("serial")
public class PoolRuleRace implements Serializable, Comparable<PoolRuleRace> {

	@JsonCreator
	public PoolRuleRace() {
	}
	
	@JsonProperty("race")
	private Integer legNumber;
	
	@JsonProperty("includes")
	private String[] includes;
	
	@JsonProperty("excludes")
	private String[] excludes;

	public Integer getLegNumber() {
		return legNumber;
	}

	public void setLegNumber(Integer legNumber) {
		this.legNumber = legNumber;
	}

	public String[] getIncludes() {
		return includes;
	}

	public void setIncludes(String[] includes) {
		this.includes = includes;
	}

	public String[] getExcludes() {
		return excludes;
	}

	public void setExcludes(String[] excludes) {
		this.excludes = excludes;
	}

	@Override
	public int compareTo(PoolRuleRace r) {
		if (r != null) {
			return r.getLegNumber() - this.getLegNumber();
		}
		return -1;
	}

}