/**
 * 
 */
package com.sportech.cl.utils.importer;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;
import org.codehaus.jackson.map.ObjectMapper;

import com.sportech.cl.model.database.utils.JSonHelper;
import com.sportech.cl.model.importer.PoolLeg;
import com.sportech.cl.model.importer.PoolList;

/**
 * @author jyu
 * 
 */
public class PoolTextParser {

	static private final Logger log = Logger.getLogger(PoolTextParser.class);

	private List<PoolTextRule> rules = new ArrayList<PoolTextRule>();

	private final static int PLACE_POOL = 2;
	private final static int SHOW_POOL = 3;
	
	public PoolTextParser(String ruleFile) {
		init(ruleFile);
	}

	private void init(String ruleFile) {
		try {
			InputStream is = PoolTextParser.class.getResourceAsStream(ruleFile);
			ObjectMapper jsonMapper = new ObjectMapper();
			PoolTextRule[] r = jsonMapper.readValue(is, PoolTextRule[].class);
			rules.addAll(Arrays.asList(r));
			Collections.sort(rules);
		} catch (Exception e) {
			log.error("Error create pool text parser", e);
		}
	}

	public String parseToString(String wagerText) {
		PoolList pools = parse(wagerText);
		String json = JSonHelper.toJSon(pools);
		return json;
	}

	public List<Integer> parse(String wagerText, List<Integer> secondLegs) {
		PoolList pools = parse(wagerText);
		if (secondLegs != null && pools.getLegs().size() > 1) {
			for (int i = 1; i < pools.getLegs().size(); i++) {
				secondLegs.addAll(Arrays.asList(pools.getLegs().get(i).getPoolIds()));
			}
		}
		return Arrays.asList(pools.getLegs().get(0).getPoolIds());
	}

	public PoolList parse(String wagerText) {
		PoolList plist = new PoolList();
		Set<Integer> pools = new HashSet<Integer>();
		List<PoolLeg> legs = new ArrayList<PoolLeg>();
		PoolLeg firstLeg = new PoolLeg();
		firstLeg.setLegNumber(1);
		legs.add(firstLeg);
		String textLine = wagerText;
		for (PoolTextRule rule : rules) {
			boolean excluded = false;
			if (rule.getIncludeMatchesPatterns() != null) {
				for (Pattern includeMatchPattern : rule.getIncludeMatchesPatterns()) {
					String matched = match(includeMatchPattern, textLine);
					if (matched != null) {
						boolean foundMatch = false;
						for (Pattern excludeMatchPattern : rule.getExcludeMatchesPatterns()) {
							String excludeMatch = match(excludeMatchPattern, textLine);
							if (excludeMatch != null) {
								textLine = textLine.replace(excludeMatch, "");
								excluded = true;
								foundMatch = true;
								break;
							}
						}
						if (!foundMatch) {
							textLine = textLine.replace(matched, "");
							if (!excluded) {
								pools.add(rule.getPoolTypeId());
							}
						}
						break;
					}
				}
				
				for (Pattern excludeMatchPattern : rule.getExcludeMatchesPatterns()) {
					String excludeMatch = match(excludeMatchPattern, textLine);
					if (excludeMatch != null) {
						excluded = true;
						break;
					}
				}
				
				if (!excluded) {
					// Complex rules
					if (rule.getPositivePatterns() != null && rule.getNegativePatterns() != null) {
						int size = rule.getPositivePatterns().length;
						for (int i = 0; i < size; i++) {
							if (i < rule.getNegativePatterns().length) {
								Pattern positive = rule.getPositivePatterns()[i];
								Pattern negative = rule.getNegativePatterns()[i];
								String matched = match(positive, textLine);
								if (matched != null) {
									String excludeMatch = match(negative, textLine);
									if (excludeMatch == null) {
										pools.add(rule.getPoolTypeId());
										break;
									}
								}
							}
						}
					}
				}
			}

			// Multi-leg rules
			if (rule.getRaces() != null) {
				Arrays.sort(rule.getRaces());
				for (PoolRuleRace prr : rule.getRaces()) {
					for (String include : prr.getIncludes()) {
						Pattern includeMatchPattern = Pattern.compile(include, Pattern.CASE_INSENSITIVE);
						String matched = match(includeMatchPattern, textLine);
						while (matched != null) {
							if (prr.getLegNumber() == 1) {
								pools.add(rule.getPoolTypeId());
							} else {
								PoolLeg pl = new PoolLeg();
								pl.setLegNumber(prr.getLegNumber());
								pl.setPoolIds(new Integer[] { (rule.getPoolTypeId()) });
								legs.add(pl);
							}
							textLine = textLine.replace(matched, "");
							matched = match(includeMatchPattern, textLine);
						}
					}
				}
			}
		}
		
		// If no place, then no show either.
		if (!pools.contains(PLACE_POOL)) {
			pools.remove(SHOW_POOL);			
		}
		
		firstLeg.setPoolIds(pools.toArray(new Integer[0]));
		plist.setLegs(legs);
		return plist;
	}

	private static String match(Pattern p, String text) {
		String matched = null;
		Matcher m = p.matcher(text);
		if (m.find()) {
			matched = m.group();
		}
		return matched;
	}
}