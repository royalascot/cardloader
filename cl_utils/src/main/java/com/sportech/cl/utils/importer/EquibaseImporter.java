package com.sportech.cl.utils.importer;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.stream.StreamSource;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.text.WordUtils;
import org.apache.log4j.Logger;

import com.sportech.cl.model.database.HardCard;
import com.sportech.cl.model.database.HardRace;
import com.sportech.cl.model.database.Runner;
import com.sportech.cl.model.database.Track;
import com.sportech.cl.model.database.utils.DateUtils;
import com.sportech.cl.model.schema.equibase.EntryRace;
import com.sportech.cl.model.schema.equibase.EntryStart;
import com.sportech.cl.model.schema.equibase.RaceCard;
import com.sportech.cl.utils.helpers.ParserHelper;
import com.sportech.common.model.CardType;
import com.sportech.common.model.PerformanceType;

public class EquibaseImporter implements CardImporter {

	static private final Logger log = Logger.getLogger(EquibaseImporter.class);

	private static final String TOTE_PREFIX = "th_def_";
	private static final String WEIGHT_UNIT = "lb";
	private static final String PROVIDER_NAME = "Equibase Company LLC";
	private PoolTextParser poolParser = new PoolTextParser("/pooltypeparserules.json");

	@SuppressWarnings("serial")
	private HashMap<String, String> distUnits = new HashMap<String, String>() {
		{
			put("F", "Furlong");
			put("M", "Mile");
			put("Y", "Yard");
		}
	};

	@Override
	public List<HardCard> parse(byte[] data) throws ImportException {
	    List<HardCard> cards = new ArrayList<HardCard>();
		try {
			JAXBContext jc = JAXBContext.newInstance(RaceCard.class);
			Unmarshaller unmarshaller = jc.createUnmarshaller();
			InputStream is = new ByteArrayInputStream(data);
			JAXBElement<RaceCard> o = unmarshaller.unmarshal(new StreamSource(is), RaceCard.class);
			cards.add(importCard(o.getValue()));
		} catch (ImportException ie) {
			throw ie;
		} catch (Exception e) {
			log.error(e);
			throw new ImportException("Invalid input Equibase file.");
		}
		return cards;
	}

	private Track importTrack(RaceCard raceCard) {
		Track track = new Track();
		com.sportech.cl.model.schema.equibase.Track equibaseTrack = raceCard.getTrack();
		if (equibaseTrack != null) {
			track.setEquibaseId(equibaseTrack.getTrackID());
			track.setCountryCode(equibaseTrack.getCountry());
			track.setToteTrackId(TOTE_PREFIX + equibaseTrack.getTrackID());
			track.setName(equibaseTrack.getTrackName());
			track.setTrackType(CardType.Thoroughbred);
			track.setActive(true);
			return track;
		}
		return null;
	}

	private static boolean isDay(String dayEvening) {
		return StringUtils.equalsIgnoreCase(dayEvening, "d");
	}

	private static Date parseDate(String dateString) throws ParseException {
		if (StringUtils.isNotEmpty(dateString)) {
			SimpleDateFormat format = new SimpleDateFormat("hh:mma");
			Date date = format.parse(dateString);
			return date;
		}
		return null;
	}

	private Runner importRunner(EntryStart starter) throws ImportException {

		try {

			Runner runner = new Runner();
			String space = " ";
			String mid = WordUtils.initials(starter.getJockey().getMiddleName());
			if (!StringUtils.isEmpty(mid)) {
				space = " " + mid + "." + " ";
			}
			runner.setJockeyName(starter.getJockey().getFirstName() + space + starter.getJockey().getLastName());
			runner.setMornlineOdds(ParserHelper.formatOdds(starter.getOdds()));
			runner.setName(starter.getHorse().getHorseName());
			runner.setShortName(ParserHelper.getRunnerShortName(runner.getName()));
			runner.setWeightCarried("" + starter.getWeightCarried());
			runner.setWeightUnits(WEIGHT_UNIT);
			runner.setScratched(false);
			runner.setProgramNumber(starter.getProgramNumber());
			runner.setPosition((long) starter.getPostPosition());
			runner.setCoupleIndicator(starter.getCoupledIndicator());
			if (starter.getPostPosition() == 99) {
				log.info("Runner is scratched:" + runner.getName());
				runner.setScratched(true);
			}
			else if (starter.getScratchIndicator() != null && StringUtils.equalsIgnoreCase("S", starter.getScratchIndicator().getValue())) {
				log.info("Runner is scratched:" + runner.getName());
				runner.setScratched(true);			
			}
			return runner;
		} catch (Exception e) {
			log.error("Error importing runners", e);
			throw new ImportException("Error importing runners.");
		}
	}

	private Set<HardRace> importRaces(RaceCard raceCard, HardCard HardCard) throws ImportException {
		HashSet<HardRace> races = new HashSet<HardRace>();
		try {
			for (EntryRace race : raceCard.getRace()) {
				HardRace hr = new HardRace();
				hr.setParent(HardCard);
				hr.setNumber((long) race.getRaceNumber());
				hr.setBreed(race.getBreedType().getValue());
				hr.setRaceType(race.getRaceType().getDescription());
				hr.setCourse(race.getCourse().getCourseType().getDescription());
				String d = race.getDistance().getPublishedValue();
				if (!StringUtils.isEmpty(d)) {
					hr.setDistance(StringUtils.substring(d, 0, d.length() - 1));
					String du = StringUtils.substring(d,  d.length() - 1, d.length());
					hr.setDistanceUnit(distUnits.get(du));
				} else {
					hr.setDistance("" + race.getDistance().getDistanceId());
					hr.setDistanceUnit(distUnits.get(race.getDistance().getDistanceUnit().getValue()));
				}		
				hr.setDistanceText(hr.getDistance() + hr.getDistanceUnit());
				hr.setEvening(!isDay(race.getDayEvening()));
				if (hr.getEvening()) {
					HardCard.setPerfType(PerformanceType.Evening);
				}
				hr.setPostTime(parseDate(race.getPostTime()));
				hr.setPurse(race.getPurseUSA());
				hr.setRaceNameLong(race.getConditionText());				
				if (!StringUtils.isEmpty(race.getRaceName())) {
					hr.setRaceNameShort(StringUtils.abbreviate(race.getRaceName(), 20));
				}
				else {
				    hr.setRaceNameShort("");
				}
				String wagerText = race.getWagerText();
				hr.setWagerText(wagerText);
				hr.setPoolIdList(poolParser.parseToString(wagerText));
				
				for (EntryStart s : race.getStarters()) {
					Runner runner = importRunner(s);
					if (runner != null) {
						runner.setParent(hr);
						hr.getRunners().add(runner);
					}
				}
				races.add(hr);
			}
		} catch (ImportException ie) {
			throw ie;
		} catch (Exception e) {
			log.error("Error importing races", e);
			throw new ImportException("Error importing races.");
		}
		return races;
	}

	private HardCard importCard(RaceCard raceCard) throws ImportException {
		
		try {
			
		    HardCard card = new HardCard();
			card.setCardType(com.sportech.common.model.CardType.Thoroughbred);			
			card.setPerfType(PerformanceType.Matinee);
			card.setCardDate(DateUtils.getCardDate(raceCard.getRaceDate()));
			card.setProvider(PROVIDER_NAME);
			card.setRaces(importRaces(raceCard, card));
			card.setTrack(importTrack(raceCard));
			
			return card;

		} catch (ImportException ie) {
			throw ie;
		} catch (Exception e) {
			log.error("Error importing card", e);
			throw new ImportException("Error importing card.");
		}

	}
}