package com.sportech.cl.utils.exporter;

import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.Marshaller;
import javax.xml.namespace.QName;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import com.sportech.cl.model.database.HardCard;
import com.sportech.cl.model.database.HardRace;
import com.sportech.cl.model.database.Runner;
import com.sportech.cl.model.database.SoftCard;
import com.sportech.cl.model.database.SoftRace;
import com.sportech.cl.model.database.utils.Pool;
import com.sportech.cl.model.database.utils.PoolContainer;
import com.sportech.cl.model.database.utils.PoolHelper;
import com.sportech.cl.model.schema.betfred.rms.LegsType;
import com.sportech.cl.model.schema.betfred.rms.LengthType;
import com.sportech.cl.model.schema.betfred.rms.MeetingDetailsType;
import com.sportech.cl.model.schema.betfred.rms.MeetingPoolsType;
import com.sportech.cl.model.schema.betfred.rms.MeetingType;
import com.sportech.cl.model.schema.betfred.rms.PoolDetailsType;
import com.sportech.cl.model.schema.betfred.rms.PoolType;
import com.sportech.cl.model.schema.betfred.rms.PoolsType;
import com.sportech.cl.model.schema.betfred.rms.RaceDetailsType;
import com.sportech.cl.model.schema.betfred.rms.RaceType;
import com.sportech.cl.model.schema.betfred.rms.RacecardDetailsType;
import com.sportech.cl.model.schema.betfred.rms.RacecardType;
import com.sportech.cl.model.schema.betfred.rms.RacesType;
import com.sportech.cl.model.schema.betfred.rms.RunnerType;
import com.sportech.cl.model.schema.betfred.rms.RunnersType;
import com.sportech.cl.model.schema.betfred.rms.SISType;
import com.sportech.cl.utils.sis.SisParserHelper;
import com.sportech.common.util.DateHelper;

public class BetfredExporter {

	static private final Logger log = Logger.getLogger(BetfredExporter.class);

	public void export(OutputStream os, List<HardCard> cards) {
		try {
			RacecardType raceCard = exportCard(cards);
			JAXBContext jc = JAXBContext.newInstance(RacecardType.class);
			Marshaller marshaller = jc.createMarshaller();
			marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			marshaller.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");

			QName qName = new QName("", "RaceCardType", "ll");
			JAXBElement<RacecardType> root = new JAXBElement<RacecardType>(qName, RacecardType.class, raceCard);
			marshaller.marshal(root, os);
			os.flush();
			os.close();
		} catch (Exception e) {
			log.error("Error exporting cards", e);
		}
	}

	public void export(List<SoftCard> cards, OutputStream os) {
		try {
			RacecardType raceCard = exportSoftCard(cards);
			JAXBContext jc = JAXBContext.newInstance(RacecardType.class);
			Marshaller marshaller = jc.createMarshaller();
			marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			marshaller.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");

			QName qName = new QName("", "RaceCardType", "ll");
			JAXBElement<RacecardType> root = new JAXBElement<RacecardType>(qName, RacecardType.class, raceCard);
			marshaller.marshal(root, os);
			os.flush();
			os.close();
		} catch (Exception e) {
			log.error("Error exporting cards", e);
		}
	}

	private void exportRace(HardRace hr, RaceType race) {

		try {
			race.setNumber(hr.getNumber().shortValue());
			RaceDetailsType detail = new RaceDetailsType();
			race.setRaceDetails(detail);
			detail.setRacing(hr.getRaceCode());
			detail.setTrack(hr.getCourse());
			detail.setGrade(hr.getCourse());
			detail.setTime(DateHelper.dateToStr("HH:mm", hr.getPostTime()));
			detail.setHandicap(hr.getHandicap());
			detail.setName(hr.getRaceNameLong());

			String distance = hr.getDistanceText();
			LengthType lt = new LengthType();
			lt.setMetres(SisParserHelper.parseDistance(distance, "mtrs"));
			lt.setFurlongs(SisParserHelper.parseDistance(distance, "f"));
			lt.setMiles(SisParserHelper.parseDistance(distance, "m"));
			lt.setYards(SisParserHelper.parseDistance(distance, "yds"));
			race.setLength(lt);
			RunnersType runners = new RunnersType();
			for (Runner runner : hr.getRunners()) {
				RunnerType r = new RunnerType();
				r.setName(runner.getName());
				r.setRider(runner.getJockeyName());
				r.setAbbreviatedName(runner.getShortName());
				r.setNumber(runner.getPosition().byteValue());
				r.setPrice(runner.getMornlineOdds());
				r.setCoupleGroup(runner.getCoupleIndicator());
				r.setFavourite("false");
				if (runner.getScratched() != null && runner.getScratched()) {
					r.setStatus("NonRunner");
				} else {
					r.setStatus("Running");
				}
				runners.getRunner().add(r);
			}

			runners.setCount((byte) hr.getRunners().size());
			race.setRunners(runners);

		} catch (Exception e) {
			log.error("Error exporting race", e);
		}
	}

	private void exportPools(PoolContainer card, MeetingType meeting) {
		try {
			Collection<Pool> pools = card.getPoolCollection();
			for (Pool p : pools) {
				com.sportech.common.model.PoolType pt = com.sportech.common.model.PoolType.fromId(p.getPoolTypeId());
				PoolType ps = new PoolType();
				ps.setType(SisParserHelper.getBetfredPoolName(pt));
				PoolDetailsType detail = new PoolDetailsType();
				detail.setStatus("Normal");
				ps.setPoolDetails(detail);
				if (pt.isMultiRace()) {
					if (meeting.getPools() == null) {
						meeting.setPools(new MeetingPoolsType());
					}
					Set<Long> races = new HashSet<Long>();
					PoolHelper.getRacesFromBitmap(p.getRaceBitmap(), races);
					LegsType legs = new LegsType();
					legs.setRaces(StringUtils.join(races, ","));
					ps.setLegs(legs);
					meeting.getPools().getPool().add(ps);
				} else {
					Set<Long> races = new HashSet<Long>();
					PoolHelper.getRacesFromBitmap(p.getRaceBitmap(), races);
					for (Long r : races) {
						for (RaceType rt : meeting.getRaces().getRace()) {
							if (rt.getNumber() == r.shortValue()) {
								if (rt.getPools() == null) {
									rt.setPools(new PoolsType());
								}
								ps.setLegs(null);
								rt.getPools().getPool().add(ps);
								break;
							}
						}
					}
				}
			}
		} catch (Exception e) {
			log.error("Error exporting pools", e);
		}
	}

	private void exportMeeting(RacecardType rc, HardCard card) {
		try {
			MeetingType mt = new MeetingType();
			mt.setToteCourse(card.getTrack().getToteTrackId());
			rc.getMeeting().add(mt);
			MeetingDetailsType details = new MeetingDetailsType();
			details.setName(card.getDescription());
			mt.setMeetingDetails(details);

			SISType sis = new SISType();
			sis.setDailyCode("Manual");
			sis.setCountry(card.getTrack().getCountryCode());
			sis.setName(card.getDescription());
			sis.setSisCourse(card.getTrack().getEquibaseId());
			mt.setSIS(sis);

			RacesType races = new RacesType();
			mt.setRaces(races);
			List<HardRace> sorted = getSortedRaces(card.getRaces());
			for (HardRace hr : sorted) {
				RaceType r = new RaceType();
				exportRace(hr, r);
				races.getRace().add(r);
			}
			exportPools(card, mt);
		} catch (Exception e) {
			log.error("Error importing meeting", e);
		}
	}

	private void exportMeeting(RacecardType rc, SoftCard card) {
		try {
			MeetingType mt = new MeetingType();
			mt.setToteCourse(card.getTrack().getToteTrackId());
			rc.getMeeting().add(mt);
			MeetingDetailsType details = new MeetingDetailsType();
			details.setName(card.getDescription());
			mt.setMeetingDetails(details);

			SISType sis = new SISType();
			sis.setDailyCode("Manual");
			sis.setCountry(card.getTrack().getCountryCode());
			sis.setName(card.getDescription());
			sis.setSisCourse(card.getTrack().getEquibaseId());
			mt.setSIS(sis);

			RacesType races = new RacesType();
			mt.setRaces(races);
			List<HardRace> hardRaces = new ArrayList<HardRace>();
			for (SoftRace sr : card.getRaces()) {
				hardRaces.add(sr.getRace());
			}
			List<HardRace> sorted = getSortedRaces(hardRaces);
			for (HardRace hr : sorted) {
				RaceType r = new RaceType();
				exportRace(hr, r);
				races.getRace().add(r);
			}
			exportPools(card, mt);
		} catch (Exception e) {
			log.error("Error importing meeting", e);
		}
	}

	private RacecardType exportCard(List<HardCard> cards) {
		RacecardType raceCard = new RacecardType();
		RacecardDetailsType detail = new RacecardDetailsType();
		detail.setDataFeed("SIS");
		detail.setType("Raceday");
		detail.setDescription("Overnight Control Racecard");
		detail.setStatus("Committed");
		raceCard.setRacecardDetails(detail);
		try {
			for (HardCard card : cards) {
				raceCard.setDate(DateHelper.dateToStr("yyyy/MM/dd", card.getCardDate()));
				exportMeeting(raceCard, card);
			}
		} catch (Exception e) {
			log.error("Error exporting card", e);
		}
		return raceCard;
	}

	private RacecardType exportSoftCard(List<SoftCard> cards) {
		RacecardType raceCard = new RacecardType();
		RacecardDetailsType detail = new RacecardDetailsType();
		detail.setDataFeed("SIS");
		detail.setType("Raceday");
		detail.setDescription("Overnight Control Racecard");
		detail.setStatus("Committed");
		raceCard.setRacecardDetails(detail);
		try {
			for (SoftCard card : cards) {
				raceCard.setDate(DateHelper.dateToStr("yyyy/MM/dd", card.getCardDate()));
				exportMeeting(raceCard, card);
			}
		} catch (Exception e) {
			log.error("Error exporting card", e);
		}
		return raceCard;
	}

	private static List<HardRace> getSortedRaces(Collection<HardRace> races) {
		List<HardRace> sorted = new ArrayList<HardRace>();
		sorted.addAll(races);
		Collections.sort(sorted, new Comparator<HardRace>() {
			@Override
			public int compare(HardRace hr1, HardRace hr2) {
				return hr1.getNumber().intValue() - hr2.getNumber().intValue();
			}
		});
		return sorted;
	}
	
}
