package com.sportech.cl.utils.helpers;

import org.apache.commons.lang3.StringUtils;

public class ParserHelper {
	
	public static String formatOdds(String odds) {
		if (StringUtils.isEmpty(odds)) {
			return "";
		}
		String result = StringUtils.replace(odds, "-", "/");
		result = result.replaceAll("/1",  "");
		return result;
	}

	public static String formatCountryCode(String cc) {
		if (StringUtils.isEmpty(cc)) {
			return "";
		}
		if (StringUtils.equalsIgnoreCase(cc,  "DEN")) {
			return "DK";
		}
		return cc;	
	}
	
	public static String getRunnerShortName(String name) {
	    return StringUtils.abbreviate(name, 18);
	}
	
}
