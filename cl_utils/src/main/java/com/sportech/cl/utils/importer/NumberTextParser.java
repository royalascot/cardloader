package com.sportech.cl.utils.importer;

import java.util.HashMap;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

@SuppressWarnings("serial")
public class NumberTextParser {
	
	private static final HashMap<String, Integer> units = new HashMap<String, Integer>() {
		{
			put("zero", 0);
			put("one", 1);
			put("two", 2);
			put("three", 3);
			put("four", 4);
			put("five", 5);
			put("six", 6);
			put("seven", 7);
			put("eight", 8);
			put("nine", 9);
			put("ten", 10);
			put("eleven", 11);
			put("twelve", 12);
			put("thirteen", 13);
			put("fourteen", 14);
			put("fifteen", 15);
			put("sixteen", 16);
			put("seventeen", 17);
			put("eighteen", 18);
			put("nineteen", 19);
			put("twenty", 20);
			put("thirty", 30);
			put("forty", 40);
			put("fifty", 50);
			put("sixty", 60);
			put("seventy", 70);
			put("eighty", 80);
			put("ninety", 90);
		}
	};

	private static final HashMap<String, Long> scales = new HashMap<String, Long>() {
		{
			put("and", 1L);
			put("hundred", 100L);
			put("thousand", 1000L);
			put("million", 1000000L);
			put("billion", 1000000000L);
			put("trillion", 1000000000000L);
		}
	};

	public static Long parseNumberText(String textnum) {
		return parseNumberText(textnum, null);
	}
	
	public static Long parseNumberText(String textnum, List<String> illegalWords) {
		if (StringUtils.isBlank(textnum)) {
			return 0L;
		}
		textnum = textnum.replace('-', ' ');
		textnum = textnum.toLowerCase();
		
		long current = 0L, result = 0L;

		String[] words = textnum.split(" ");

		for (String word : words) {
			if (!units.containsKey(word) && !scales.containsKey(word)) {
				if (illegalWords != null) {
					illegalWords.add(word);
				}
				continue;
			}
			long scale = 1L, increment = 0L;
			if (scales.containsKey(word)) {
				increment = 0L;
				scale = scales.get(word);
			} else {
				scale = 1L;
				increment = units.get(word);
			}
			current = current * scale + increment;
			if (scale > 100) {
				result += current;
				current = 0L;
			}
		}
		return result + current;
	}

}
