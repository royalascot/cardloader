package com.sportech.cl.utils.importer;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.stream.StreamSource;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import com.sportech.cl.model.database.HardCard;
import com.sportech.cl.model.database.HardPool;
import com.sportech.cl.model.database.HardRace;
import com.sportech.cl.model.database.PassThroughBag;
import com.sportech.cl.model.database.Runner;
import com.sportech.cl.model.database.Track;
import com.sportech.cl.model.database.utils.JSonHelper;
import com.sportech.cl.model.database.utils.PoolHelper;
import com.sportech.cl.model.schema.universal.Card;
import com.sportech.cl.utils.helpers.ParserHelper;
import com.sportech.common.model.PerformanceType;
import com.sportech.common.model.PoolType;

public class UniversalFormatImporter implements CardImporter {

    static private final Logger log = Logger.getLogger(UniversalFormatImporter.class);

    private static final String TOTE_PREFIX = "generic_";

    @Override
    public List<HardCard> parse(byte[] data) throws ImportException {

        List<HardCard> cards = new ArrayList<HardCard>();

        try {

            JAXBContext jc = JAXBContext.newInstance(Card.class);
            Unmarshaller unmarshaller = jc.createUnmarshaller();
            InputStream is = new ByteArrayInputStream(data);
            JAXBElement<Card> o = unmarshaller.unmarshal(new StreamSource(is), Card.class);
            cards.add(importCard(o.getValue()));

        } catch (ImportException ie) {
            throw ie;
        } catch (Exception e) {
            log.error(e);
            throw new ImportException("Invalid input XML file.");
        }

        return cards;

    }

    private Track importTrack(Card raceCard) {
        Track track = new Track();
        com.sportech.cl.model.schema.universal.Track xmlTrack = raceCard.getMeeting().getTrack();
        if (xmlTrack != null) {
            track.setEquibaseId(xmlTrack.getCode());
            track.setCountryCode(ParserHelper.formatCountryCode(xmlTrack.getCountryCode()));
            track.setToteTrackId(TOTE_PREFIX + xmlTrack.getCode());
            track.setName(xmlTrack.getName());
            track.setTrackType(com.sportech.common.model.CardType.Thoroughbred);
            PassThroughBag p = new PassThroughBag();
            p.setTrackId(xmlTrack.getTrackId());
            track.setPassThroughs(p);
            track.setActive(true);
            return track;
        }
        return null;
    }

    private Runner importRunner(com.sportech.cl.model.schema.universal.Runner starter) throws ImportException {
        try {
            Runner runner = new Runner();
            runner.setJockeyName(starter.getJockeyName());
            runner.setMornlineOdds(ParserHelper.formatOdds(starter.getOdds()));
            runner.setName(starter.getHorseName());
            runner.setShortName(ParserHelper.getRunnerShortName(runner.getName()));
            if (StringUtils.isNotBlank(starter.getWeightCarried())) {
                runner.setWeightCarried(starter.getWeightCarried());
            }
            runner.setWeightUnits(StringUtils.abbreviate(starter.getWeightUnits(), 45));
            runner.setScratched(starter.isScratched());
            if (runner.getScratched() == null) {
                runner.setScratched(false);
            }
            runner.setProgramNumber("" + starter.getProgramPos());
            runner.setPosition((long) starter.getStartPos());
            runner.setCoupleIndicator(starter.getCoupleInd());
            return runner;
        } catch (Exception e) {
            log.error("Error importing runners", e);
            throw new ImportException("Error importing runners.");
        }
    }

    private Set<HardRace> importRaces(Card raceCard, HardCard HardCard) throws ImportException {
        HashSet<HardRace> races = new HashSet<HardRace>();
        try {
            for (com.sportech.cl.model.schema.universal.Race race : raceCard.getMeeting().getRace()) {
                HardRace hr = new HardRace();
                hr.setParent(HardCard);
                hr.setNumber((long) race.getNumber());
                hr.setBreed(race.getBreed());
                hr.setRaceType(race.getType());
                hr.setCourse(race.getCourse());
                hr.setDistance(race.getDistance());
                hr.setDistanceUnit(race.getDistUnits());
                hr.setDistanceText(hr.getDistance() + hr.getDistanceUnit());
                hr.setEvening(false);
                if (race.isEvening() != null) {
                    hr.setEvening(race.isEvening());
                }
                if (hr.getEvening()) {
                    HardCard.setPerfType(PerformanceType.Evening);
                }
                Calendar postTime = race.getPostTime();
                if (postTime != null) {
                    GregorianCalendar g = new GregorianCalendar();
                    g.set(0, 0, 0, postTime.get(Calendar.HOUR_OF_DAY), postTime.get(Calendar.MINUTE), 0);
                    hr.setPostTime(g.getTime());
                }
                if (race.getPurse() != null && race.getPurse().size() > 0) {
                    hr.setPurse(new BigDecimal(race.getPurse().get(0).getAmount()));
                }
                hr.setRaceNameLong(race.getNameLong());
                hr.setRaceNameShort(StringUtils.abbreviate(race.getNameLong(), 20));
                if (!StringUtils.isEmpty(race.getNameShort())) {
                    hr.setRaceNameShort(StringUtils.abbreviate(race.getNameShort(), 20));
                }

                for (com.sportech.cl.model.schema.universal.Runner s : race.getRunner()) {
                    Runner runner = importRunner(s);
                    runner.setParent(hr);
                    hr.getRunners().add(runner);
                }
                races.add(hr);
            }

        } catch (ImportException ie) {
            throw ie;
        } catch (Exception e) {
            log.error("Error importing races", e);
            throw new ImportException("Error importing races.");
        }
        return races;
    }

    private HardCard importCard(Card raceCard) throws ImportException {

        try {
            HardCard card = new HardCard();
            card.setCardType(com.sportech.common.model.CardType.Thoroughbred);
            Calendar c = raceCard.getMeeting().getDate();
            GregorianCalendar g = new GregorianCalendar();
            g.set(c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH), 0, 0);
            card.setCardDate(g.getTime());
            card.setPerfType(PerformanceType.Matinee);
            card.setEventCode(raceCard.getMeeting().getItspEventCode());
            card.setProvider(raceCard.getProvider());
            card.setRaces(importRaces(raceCard, card));
            Track track = importTrack(raceCard);
            card.setTrack(track);
            PassThroughBag p = track.getPassThroughs();
            if (p == null) {
                p = new PassThroughBag();
            }
            p.setHostMeetNumber(raceCard.getMeeting().getHostMeetNum());
            card.setPassThroughs(JSonHelper.toJSon(p));
            importPools(card, raceCard.getMeeting());
            return card;

        } catch (ImportException ie) {
            throw ie;
        } catch (Exception e) {
            log.error("Error importing card", e);
            throw new ImportException("Error importing card.");
        }
    }

    private void importPools(HardCard card, com.sportech.cl.model.schema.universal.Meeting meeting) throws ImportException {
        try {
            List<HardPool> poolList = new ArrayList<HardPool>();
            for (com.sportech.cl.model.schema.universal.Pool p : meeting.getPool()) {
                HardPool pool = new HardPool();
                PoolType pt = PoolType.fromCode(p.getCode());
                if (pt != null) {
                    pool.setPoolTypeId(pt.getId());
                    pool.setPoolCode(p.getPoolAbbr());
                    pool.setPoolName(p.getPoolName());
                }
                List<Long> races = new ArrayList<Long>();
                for (Integer raceNumber : p.getRace()) {
                    races.add(raceNumber.longValue());
                }
                PoolHelper.populatePool(card, pool, races);
                poolList.add(pool);
            }
            card.setPools(new HashSet<HardPool>());
            card.getPools().addAll(poolList);
        } catch (Exception e) {
            log.error("Error importing pools", e);
            throw new ImportException("Error importing pools.");
        }
    }

}
