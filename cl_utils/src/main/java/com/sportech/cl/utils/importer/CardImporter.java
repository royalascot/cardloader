package com.sportech.cl.utils.importer;

import java.util.List;

import com.sportech.cl.model.database.HardCard;

public interface CardImporter {
	
	public List<HardCard> parse(byte[] data) throws ImportException;
	
}
