package com.sportech.cl.utils.importer;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.stream.StreamSource;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import com.sportech.cl.model.database.HardCard;
import com.sportech.cl.model.database.HardRace;
import com.sportech.cl.model.database.Runner;
import com.sportech.cl.model.database.Track;
import com.sportech.cl.model.database.utils.DateUtils;
import com.sportech.cl.model.schema.trackinfo.greyhound.CardType;
import com.sportech.cl.model.schema.trackinfo.greyhound.EntryType;
import com.sportech.cl.model.schema.trackinfo.greyhound.EventType;
import com.sportech.cl.model.schema.trackinfo.greyhound.XmlType;
import com.sportech.cl.utils.helpers.ParserHelper;
import com.sportech.common.model.PerformanceType;

public class TrackInfoImporter implements CardImporter {

	static private final Logger log = Logger.getLogger(EquibaseImporter.class);

	private static final String TOTE_PREFIX = "dog_";
	private static final String WEIGHT_UNIT = "lb";
	private static final String PROVIDER_NAME = "Track Info";

	private PerformanceType perfType;

	private PoolTextParser poolParser = new PoolTextParser("/pooltypeparserules.json");

	public TrackInfoImporter(PerformanceType perfType) {
		this.perfType = perfType;
	}

	@Override
	public List<HardCard> parse(byte[] data) throws ImportException {

	    List<HardCard> cards = new ArrayList<HardCard>();
	    
		try {
			JAXBContext jc = JAXBContext.newInstance(XmlType.class);
			Unmarshaller unmarshaller = jc.createUnmarshaller();
			InputStream is = new ByteArrayInputStream(data);
			JAXBElement<XmlType> o = unmarshaller.unmarshal(new StreamSource(is), XmlType.class);
			cards.add(importCard(o.getValue().getCard()));
		} catch (ImportException ie) {
			throw ie;
		} catch (Exception e) {
			log.error(e);
			throw new ImportException("Invalid input track info file.");
		}
		
		return cards;
		
	}

	private Track importTrack(CardType raceCard) {
		Track track = new Track();
		track.setPerfType(perfType);
		track.setEquibaseId(raceCard.getTrackCode());
		track.setCountryCode("USA");
		track.setToteTrackId(TOTE_PREFIX + track.getEquibaseId());
		track.setName(raceCard.getTrackName());
		track.setTrackType(com.sportech.common.model.CardType.GreyHound);
		track.setActive(true);
		return track;
	}

	private static Date parseDate(String dateString) throws ParseException {
		if (StringUtils.isNotEmpty(dateString)) {
			SimpleDateFormat format = new SimpleDateFormat("hh:mma");
			Date date = format.parse(dateString);
			return date;
		}
		return null;
	}

	private Runner importRunner(EntryType starter) throws ImportException {

		try {
			Runner runner = new Runner();
			runner.setJockeyName("");
			runner.setMornlineOdds(ParserHelper.formatOdds(starter.getMorningOdds()));
			runner.setName(starter.getGreyhoundName());
			runner.setShortName(ParserHelper.getRunnerShortName(runner.getName()));
			runner.setWeightCarried(starter.getGreyhoundWeight());
			runner.setWeightUnits(WEIGHT_UNIT);
			runner.setScratched(false);
			runner.setProgramNumber("" + starter.getBetNumber());
			runner.setPosition((long) starter.getBetNumber());
			return runner;
		} catch (Exception e) {
			log.error("Error importing runners", e);
			throw new ImportException("Error importing runners.");
		}
	}

	private Set<HardRace> importRaces(CardType raceCard, HardCard HardCard) throws ImportException {
		HashSet<HardRace> races = new HashSet<HardRace>();
		try {
			for (EventType race : raceCard.getEvent()) {
				HardRace hr = new HardRace();
				hr.setParent(HardCard);
				hr.setNumber((long) race.getRaceNumber());
				hr.setBreed("G");
				hr.setRaceType(race.getRaceGrade());
				hr.setCourse("");
				hr.setDistance(race.getRaceLength());
				if (StringUtils.contains(race.getRaceLength(), "-")) {
					hr.setDistanceUnit("Mile");
				} else {
					hr.setDistanceUnit("Yards");
				}
				hr.setDistanceText(hr.getDistance() + hr.getDistanceUnit());
				hr.setEvening(false);
				hr.setPerfType(this.perfType);
				hr.setPostTime(parseDate(race.getPostTime()));
				hr.setRaceNameLong(race.getHeaderText());
				hr.setRaceNameShort(StringUtils.abbreviate(race.getHeaderText(), 20));
		
				String wagerText = race.getHeaderText();
				hr.setWagerText(wagerText);
				hr.setPoolIdList(poolParser.parseToString(wagerText));

				for (EntryType s : race.getEntry()) {
					Runner runner = importRunner(s);
					if (runner != null) {
						runner.setParent(hr);
						hr.getRunners().add(runner);
					}
				}
				races.add(hr);
			}
		} catch (ImportException ie) {
			throw ie;
		} catch (Exception e) {
			log.error("Error importing races", e);
			throw new ImportException("Error importing races.");
		}
		return races;
	}

	private HardCard importCard(CardType raceCard) throws ImportException {
		try {
		    HardCard card = new HardCard();
			String c = raceCard.getCardDate();
			if (c != null) {
				SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
				Date d = sdf.parse(c);
				card.setCardDate(DateUtils.fixYear(d));
			}
			card.setFirstRaceTime(parseDate(raceCard.getFirstRacePosttime()));
			card.setProvider(PROVIDER_NAME);
			card.setRaces(importRaces(raceCard, card));
			card.setTrack(importTrack(raceCard));
			card.setPerfType(perfType);
			card.setCardType(com.sportech.common.model.CardType.GreyHound);
			return card;
		} catch (ImportException ie) {
			throw ie;
		} catch (Exception e) {
			log.error("Error importing card", e);
			throw new ImportException("Error importing card.");
		}

	}
}
